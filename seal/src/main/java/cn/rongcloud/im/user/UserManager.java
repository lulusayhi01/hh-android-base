package cn.rongcloud.im.user;

import android.content.Context;
import android.content.SharedPreferences;

import cn.rongcloud.im.SealConst;
import cn.rongcloud.im.server.response.user.UserInfo;

/**
 * Created by Administrator on 2018/8/10 0010.
 */

public class UserManager {


    private static volatile UserManager instance = null;

    private UserManager() {
    }

    public static UserManager getInstance() {
        if (instance == null) {
            synchronized (UserManager.class) {
                if (instance == null) {
                    instance = new UserManager();
                }
            }
        }
        return instance;
    }

    public UserInfo getUserInfo(Context context){
        if (context == null) return null;
        UserInfo userInfo=new UserInfo();
        SharedPreferences config = context.getSharedPreferences("config", Context.MODE_PRIVATE);
        String userId = config.getString(SealConst.SEALTALK_LOGIN_ID, "");
        String username = config.getString(SealConst.SEALTALK_LOGIN_NAME, "");
        String userPortrait = config.getString(SealConst.SEALTALK_LOGING_PORTRAIT, "");
        userInfo.setUserid(userId);
        userInfo.setHeadimage(userPortrait);
        userInfo.setUsername(username);
        return userInfo;
    }


}
