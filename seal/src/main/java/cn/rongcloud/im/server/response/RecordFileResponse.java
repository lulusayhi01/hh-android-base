package cn.rongcloud.im.server.response;

import java.io.Serializable;
import java.util.List;

/**
 * @Author: huluhong
 * @Time: 2018/11/11 0010
 * @Description：This is PeopleNearbyResponse
 */

public class RecordFileResponse extends BaseResponse{


    private List<DataBean> data;

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean implements Serializable {
        /**
         * createtime : 2018-11-27 15:46:57
         * dt : 1
         * ft : 3
         * id : 32
         * imgpath : http://oss-aliyun-liululu.oss-cn-shanghai.aliyuncs.com/upload/154330481722210671.jpg
         * oid : 30
         * remark : ddhsbsjshssjj但是就是是
         * status : 1
         * type : 10001
         */

        private String createtime;
        private int dt;
        private int ft;
        private int id;
        private String imgpath;
        private int oid;
        private String remark;
        private int status;
        private int type;

        public String getCreatetime() {
            return createtime;
        }

        public void setCreatetime(String createtime) {
            this.createtime = createtime;
        }

        public int getDt() {
            return dt;
        }

        public void setDt(int dt) {
            this.dt = dt;
        }

        public int getFt() {
            return ft;
        }

        public void setFt(int ft) {
            this.ft = ft;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getImgpath() {
            return imgpath;
        }

        public void setImgpath(String imgpath) {
            this.imgpath = imgpath;
        }

        public int getOid() {
            return oid;
        }

        public void setOid(int oid) {
            this.oid = oid;
        }

        public String getRemark() {
            return remark;
        }

        public void setRemark(String remark) {
            this.remark = remark;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public int getType() {
            return type;
        }

        public void setType(int type) {
            this.type = type;
        }
    }
}
