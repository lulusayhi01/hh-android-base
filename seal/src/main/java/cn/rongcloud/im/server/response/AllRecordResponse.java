package cn.rongcloud.im.server.response;

import java.util.List;

/**
 * @Author: huluhong
 * @Time: 2018/11/11 0010
 * @Description：This is PeopleNearbyResponse
 */

public class AllRecordResponse extends BaseResponse{


    private List<DataBean> data;

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * distance : 34
         * geohash : wtm7znhre4ef
         * headimage : http://p6nlllxof.bkt.clouddn.com/upload/20180705/3f668359c2354f2298f978c6266b560d.jpg
         * id : 40
         * machineCount : 0
         * name : 😂 😃 😄80048004😂 😃 😄
         * phone : 13023600627
         * signature : 67688
         * isf : 72
         * friendRemark : 哈哈哈
         */

        private String distance;
        private String geohash;
        private String headimage;
        private int id;
        private int machineCount;
        private String name;
        private String phone;
        private String signature;
        private int isf;
        private String friendRemark;

        public String getDistance() {
            return distance;
        }

        public void setDistance(String distance) {
            this.distance = distance;
        }

        public String getGeohash() {
            return geohash;
        }

        public void setGeohash(String geohash) {
            this.geohash = geohash;
        }

        public String getHeadimage() {
            return headimage;
        }

        public void setHeadimage(String headimage) {
            this.headimage = headimage;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getMachineCount() {
            return machineCount;
        }

        public void setMachineCount(int machineCount) {
            this.machineCount = machineCount;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getSignature() {
            return signature;
        }

        public void setSignature(String signature) {
            this.signature = signature;
        }

        public int getIsf() {
            return isf;
        }

        public void setIsf(int isf) {
            this.isf = isf;
        }

        public String getFriendRemark() {
            return friendRemark;
        }

        public void setFriendRemark(String friendRemark) {
            this.friendRemark = friendRemark;
        }
    }
}
