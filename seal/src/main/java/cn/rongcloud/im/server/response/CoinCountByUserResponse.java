package cn.rongcloud.im.server.response;


/**
 * Created by huluhong
 */
public class CoinCountByUserResponse extends BaseResponse{


    /**
     * data : 1000
     */

    private String data;//金币

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
