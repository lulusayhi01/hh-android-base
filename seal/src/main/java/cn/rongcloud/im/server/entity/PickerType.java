package cn.rongcloud.im.server.entity;

/**
 * @Author: huluhong
 * @Time: 2018/11/27 0027
 * @Description：This is PickerType
 */

public class PickerType {

    private String id;
    private String value;

    public PickerType() {
    }

    public PickerType(String id, String value) {
        this.id = id;
        this.value = value;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return getValue();
    }
}
