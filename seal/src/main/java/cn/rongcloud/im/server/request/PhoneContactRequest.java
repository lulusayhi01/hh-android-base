package cn.rongcloud.im.server.request;


import cn.rongcloud.im.server.request.base.BaseRequest;

/**
 *
 */
public class PhoneContactRequest extends BaseRequest{

    private String phone;

    public PhoneContactRequest(String phone) {
        this.phone = phone;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
