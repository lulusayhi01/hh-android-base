package cn.rongcloud.im.server.response.friendcircle;


import cn.rongcloud.im.server.response.user.UserInfo;

/**
 *
 * Created by huluhong on 2018/7/7.
 */

public class LikesInfo {

    private UserInfo userInfo;
    private MomentsInfo.DataBean momentsid;

    public interface LikesField {
        String USERID = "userid";
        String MOMENTID = "momentsid";
    }

    public LikesInfo() {
    }

    public LikesInfo(UserInfo userInfo, MomentsInfo.DataBean momentsid) {
        this.userInfo = userInfo;
        this.momentsid = momentsid;
    }

    public UserInfo getUserInfo() {
        return userInfo;
    }

    public void setUserInfo(UserInfo userInfo) {
        this.userInfo = userInfo;
    }

    public MomentsInfo.DataBean getMomentsid() {
        return momentsid;
    }

    public void setMomentsid(MomentsInfo.DataBean momentsid) {
        this.momentsid = momentsid;
    }
}
