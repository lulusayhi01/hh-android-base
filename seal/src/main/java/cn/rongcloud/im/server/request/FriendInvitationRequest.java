package cn.rongcloud.im.server.request;


import cn.rongcloud.im.server.request.base.BaseRequest;

/**
 * Created by AMing on 16/1/7.
 * Company RongCloud
 */
public class FriendInvitationRequest extends BaseRequest{
    /**
     * tuserid=添加的用户id&remark=请求备注
     */
    private String tuserid;
    private String remark;

    public FriendInvitationRequest(String tuserid, String remark) {
        this.tuserid = tuserid;
        this.remark = remark;
    }

    public String getTuserid() {
        return tuserid;
    }

    public void setTuserid(String tuserid) {
        this.tuserid = tuserid;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}
