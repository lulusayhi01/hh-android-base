package cn.rongcloud.im.server.response;

/**
 * Created by Administrator on 2018/9/13 0013.
 */

public class AppVersionResponse extends BaseResponse{


    /**
     * data : {"appSize":"100MB","appType":"android","appUrl":"http://h.kantingyong.com:18081/mbt001.apk","appVersion":"1.0.0","constraint":0,"creattime":"2018-09-12","id":1,"remarker":"新版本上线","status":1}
     */

    private DataBean data;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * appSize : 100MB
         * appType : android
         * appUrl : http://h.kantingyong.com:18081/mbt001.apk
         * appVersion : 1.0.0
         * constraint : 0
         * creattime : 2018-09-12
         * id : 1
         * remarker : 新版本上线
         * status : 1
         */

        private String appSize;
        private String appType;
        private String appUrl;
        private String appVersion;
        private int constraint;//constraint（是否强制更新 0：否 1是）
        private String creattime;
        private int id;
        private String remarker;
        private int status;

        public String getAppSize() {
            return appSize;
        }

        public void setAppSize(String appSize) {
            this.appSize = appSize;
        }

        public String getAppType() {
            return appType;
        }

        public void setAppType(String appType) {
            this.appType = appType;
        }

        public String getAppUrl() {
            return appUrl;
        }

        public void setAppUrl(String appUrl) {
            this.appUrl = appUrl;
        }

        public String getAppVersion() {
            return appVersion;
        }

        public void setAppVersion(String appVersion) {
            this.appVersion = appVersion;
        }

        public int getConstraint() {
            return constraint;
        }

        public void setConstraint(int constraint) {
            this.constraint = constraint;
        }

        public String getCreattime() {
            return creattime;
        }

        public void setCreattime(String creattime) {
            this.creattime = creattime;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getRemarker() {
            return remarker;
        }

        public void setRemarker(String remarker) {
            this.remarker = remarker;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }
    }
}
