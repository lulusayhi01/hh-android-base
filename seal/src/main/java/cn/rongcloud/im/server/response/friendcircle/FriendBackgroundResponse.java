package cn.rongcloud.im.server.response.friendcircle;

import cn.rongcloud.im.server.response.BaseResponse;

/**
 * Created by Administrator on 2018/9/13 0013.
 */

public class FriendBackgroundResponse extends BaseResponse{


    /**
     * data : {"createtime":"2018-09-13 09:03:58","id":28,"imgpath":"http://oss-aliyun-liululu.oss-cn-shanghai.aliyuncs.com/upload/153680063832810348.jpg","status":1,"userid":30}
     */

    private DataBean data;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * createtime : 2018-09-13 09:03:58
         * id : 28
         * imgpath : http://oss-aliyun-liululu.oss-cn-shanghai.aliyuncs.com/upload/153680063832810348.jpg
         * status : 1
         * userid : 30
         */

        private String createtime;
        private int id;
        private String imgpath;
        private int status;
        private int userid;

        public String getCreatetime() {
            return createtime;
        }

        public void setCreatetime(String createtime) {
            this.createtime = createtime;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getImgpath() {
            return imgpath;
        }

        public void setImgpath(String imgpath) {
            this.imgpath = imgpath;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public int getUserid() {
            return userid;
        }

        public void setUserid(int userid) {
            this.userid = userid;
        }
    }
}
