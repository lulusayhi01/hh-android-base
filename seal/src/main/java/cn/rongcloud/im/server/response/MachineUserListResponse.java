package cn.rongcloud.im.server.response;


import java.util.List;

/**
 * Created by huluhong
 */
public class MachineUserListResponse extends BaseResponse{


    private List<DataBean> data;

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * buytype : 5102
         * coinName : 脉宝币
         * coinid : 1
         * id : 3
         * level : 5203
         * machineistatus : 3202
         * manhour : 180
         * muid : 35
         * name :  中级矿机
         * price : 30
         * status : 1
         * coinMinute : 6
         * coinNumber : 0.6
         * coinnum : 100
         * cointype : 1
         */

        private int buytype;
        private String coinName;
        private int coinid;
        private int id;
        private int level;
        private int machineistatus;
        private String manhour;
        private int muid;
        private String name;
        private String price;
        private int status;
        private int coinMinute;
        private String coinNumber;
        private String coinnum;
        private int cointype;

        public int getBuytype() {
            return buytype;
        }

        public void setBuytype(int buytype) {
            this.buytype = buytype;
        }

        public String getCoinName() {
            return coinName;
        }

        public void setCoinName(String coinName) {
            this.coinName = coinName;
        }

        public int getCoinid() {
            return coinid;
        }

        public void setCoinid(int coinid) {
            this.coinid = coinid;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getLevel() {
            return level;
        }

        public void setLevel(int level) {
            this.level = level;
        }

        public int getMachineistatus() {
            return machineistatus;
        }

        public void setMachineistatus(int machineistatus) {
            this.machineistatus = machineistatus;
        }

        public String getManhour() {
            return manhour;
        }

        public void setManhour(String manhour) {
            this.manhour = manhour;
        }

        public int getMuid() {
            return muid;
        }

        public void setMuid(int muid) {
            this.muid = muid;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public int getCoinMinute() {
            return coinMinute;
        }

        public void setCoinMinute(int coinMinute) {
            this.coinMinute = coinMinute;
        }

        public String getCoinNumber() {
            return coinNumber;
        }

        public void setCoinNumber(String coinNumber) {
            this.coinNumber = coinNumber;
        }

        public String getCoinnum() {
            return coinnum;
        }

        public void setCoinnum(String coinnum) {
            this.coinnum = coinnum;
        }

        public int getCointype() {
            return cointype;
        }

        public void setCointype(int cointype) {
            this.cointype = cointype;
        }
    }
}
