package cn.rongcloud.im.server.response;

import java.util.List;

/**
 * @Author: huluhong
 * @Time: 2018/10/9 0009
 * @Description：This is PhoneContactResponse
 */
public class PhoneContactResponse extends BaseResponse{


    private List<DataBean> data;

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * friendSource : 8001
         * friendStatus : 6001
         * headimage : http://oss-aliyun-liululu.oss-cn-shanghai.aliyuncs.com/upload/153787673121710074.jpg
         * id : 30
         * isf : 50
         * machineCount : 0
         * name : 璐璐
         * phone : 15013554615
         * trendsImg : [{"id":208,"imgpath":"http://oss-aliyun-liululu.oss-cn-shanghai.aliyuncs.com/upload/153675244754110849.jpg"},{"id":207,"imgpath":"http://oss-aliyun-liululu.oss-cn-shanghai.aliyuncs.com/upload/153675244741010067.jpg"},{"id":204,"imgpath":"http://oss-aliyun-liululu.oss-cn-shanghai.aliyuncs.com/upload/153656644048910766.jpg"}]
         */

        private String friendSource;
        private int friendStatus;
        private String headimage;
        private int id;
        private int isf;
        private int machineCount;
        private String name;
        private String phone;
        private List<TrendsImgBean> trendsImg;

        public String getFriendSource() {
            return friendSource;
        }

        public void setFriendSource(String friendSource) {
            this.friendSource = friendSource;
        }

        public int getFriendStatus() {
            return friendStatus;
        }

        public void setFriendStatus(int friendStatus) {
            this.friendStatus = friendStatus;
        }

        public String getHeadimage() {
            return headimage;
        }

        public void setHeadimage(String headimage) {
            this.headimage = headimage;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getIsf() {
            return isf;
        }

        public void setIsf(int isf) {
            this.isf = isf;
        }

        public int getMachineCount() {
            return machineCount;
        }

        public void setMachineCount(int machineCount) {
            this.machineCount = machineCount;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public List<TrendsImgBean> getTrendsImg() {
            return trendsImg;
        }

        public void setTrendsImg(List<TrendsImgBean> trendsImg) {
            this.trendsImg = trendsImg;
        }

        public static class TrendsImgBean {
            /**
             * id : 208
             * imgpath : http://oss-aliyun-liululu.oss-cn-shanghai.aliyuncs.com/upload/153675244754110849.jpg
             */

            private int id;
            private String imgpath;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getImgpath() {
                return imgpath;
            }

            public void setImgpath(String imgpath) {
                this.imgpath = imgpath;
            }
        }
    }
}
