package cn.rongcloud.im.server.request;


import cn.rongcloud.im.server.request.base.BaseRequest;

/**
 * Created by AMing on 15/12/24.
 * Company RongCloud
 */
public class LoginRequest extends BaseRequest{


    private String region;
    private String phone;
    private String password;
    private String returnUrl;

    public LoginRequest(String region, String phone, String password, String returnUrl) {
        this.region = region;
        this.phone = phone;
        this.password = password;
        this.returnUrl = returnUrl;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getReturnUrl() {
        return returnUrl;
    }

    public void setReturnUrl(String returnUrl) {
        this.returnUrl = returnUrl;
    }
}
