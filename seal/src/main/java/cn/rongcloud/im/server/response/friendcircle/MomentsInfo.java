package cn.rongcloud.im.server.response.friendcircle;

import com.sj.friendcircle.common.MomentsType;
import com.sj.friendcircle.ui.base.adapter.MultiType;
import com.socks.library.KLog;

import java.util.ArrayList;
import java.util.List;

import cn.rongcloud.im.server.response.BaseResponse;

/**
 * 朋友圈动态
 */

public class MomentsInfo extends BaseResponse {

    private List<DataBean> data;

    public MomentsInfo() {
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean implements MultiType {
        /**
         * trendsComment : [{"content":"The only way ","createtime":"2018-07-24 14:15:25","id":23,"userHeadImage":"https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1531477347932&di=369929b60c0283f7e5f7ef4c72c5b6df&imgtype=0&src=http%3A%2F%2Ftx.haiqq.com%2Fuploads%2Fallimg%2F170508%2F0954333D7-2.jpg","userName":"13501031194","userPhone":"13501031194","userRemark":"测试昵称","userid":25}]
         * trendsCommentCount : 1
         * trendsCommentLike : [{"userName":"13501031194","userPhone":"13501031194","userRemark":"测试昵称","userid":25}]
         * trendsContent : 你说啥就是啥
         * trendsCreateTime : 2018-07-23 15:09:44
         * trendsId : 17
         * trendsImg : [{"id":55,"imgpath":"http://oss-aliyun-liululu.oss-cn-shanghai.aliyuncs.com/upload/153232978453210277.jpg"},{"id":56,"imgpath":"http://oss-aliyun-liululu.oss-cn-shanghai.aliyuncs.com/upload/153232978462710287.jpg"},{"id":57,"imgpath":"http://oss-aliyun-liululu.oss-cn-shanghai.aliyuncs.com/upload/153232978472510025.jpg"}]
         * trendsImgCount : 3
         * userHeadImage : https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1531477347932&di=369929b60c0283f7e5f7ef4c72c5b6df&imgtype=0&src=http%3A%2F%2Ftx.haiqq.com%2Fuploads%2Fallimg%2F170508%2F0954333D7-2.jpg
         * userId : 25
         * userName : 13501031194
         * userPhone : 13501031194
         * userRemark : 测试昵称
         */

        private int trendsCommentCount;
        private String trendsContent;
        private String trendsCreateTime;
        private int trendsId;
        private int trendsImgCount;
        private String userHeadImage;
        private int userId;
        private String userName;
        private String userPhone;
        private String userRemark;
        private List<TrendsCommentBean> trendsComment;
        private List<TrendsCommentLikeBean> trendsCommentLike;
        private List<TrendsImgBean> trendsImg;

        //*********************************************************
        @Override
        public int getItemType() {
            return getMomentType();
        }

        public int getMomentType() {
            if (content == null) {
                KLog.e("朋友圈内容居然是空的？？？？？MDZZ！！！！");
                return MomentsType.EMPTY_CONTENT;
            }
            return content.getMomentType();
        }

        private List<LikesInfo> likesList;
        private List<CommentInfo> commentList;
        private MomentContent content;

        public List<LikesInfo> getLikesList() {
            return likesList;
        }

        public void setLikesList(List<LikesInfo> likesList) {
            this.likesList = likesList;
        }

        public List<CommentInfo> getCommentList() {
            return commentList;
        }

        public void setCommentList(List<CommentInfo> commentList) {
            this.commentList = commentList;
        }

        public MomentContent getContent() {
            return content;
        }

        public void setContent(MomentContent content) {
            this.content = content;
        }

        public void addComment(CommentInfo commentInfo) {
            if (commentInfo == null) return;
            if (this.commentList == null) {
                this.commentList = new ArrayList<>();
            }
            this.commentList.add(commentInfo);
        }

        public void addLikes(LikesInfo likesInfo) {
            if (likesInfo == null) return;
            if (this.likesList == null) {
                this.likesList = new ArrayList<>();
            }
            this.likesList.add(likesInfo);
        }

        public String getLikesObjectid() {
//        String momentid = getMomentid();
//        String userid = LocalHostManager.INSTANCE.getUserid();
//        if (!ToolUtil.isListEmpty(likesList)) {
//            for (LikesInfo likesInfo : likesList) {
//                if (TextUtils.equals(momentid, likesInfo.getMomentsid()) && TextUtils.equals(userid, likesInfo.getUserid())) {
//                    return likesInfo.getObjectId();
//                }
//            }
//        }
            return null;
        }
        //*********************************************************


        public int getTrendsCommentCount() {
            return trendsCommentCount;
        }

        public void setTrendsCommentCount(int trendsCommentCount) {
            this.trendsCommentCount = trendsCommentCount;
        }

        public String getTrendsContent() {
            return trendsContent;
        }

        public void setTrendsContent(String trendsContent) {
            this.trendsContent = trendsContent;
        }

        public String getTrendsCreateTime() {
            return trendsCreateTime;
        }

        public void setTrendsCreateTime(String trendsCreateTime) {
            this.trendsCreateTime = trendsCreateTime;
        }

        public int getTrendsId() {
            return trendsId;
        }

        public void setTrendsId(int trendsId) {
            this.trendsId = trendsId;
        }

        public int getTrendsImgCount() {
            return trendsImgCount;
        }

        public void setTrendsImgCount(int trendsImgCount) {
            this.trendsImgCount = trendsImgCount;
        }

        public String getUserHeadImage() {
            return userHeadImage;
        }

        public void setUserHeadImage(String userHeadImage) {
            this.userHeadImage = userHeadImage;
        }

        public int getUserId() {
            return userId;
        }

        public void setUserId(int userId) {
            this.userId = userId;
        }

        public String getUserName() {
            return userName;
        }

        public void setUserName(String userName) {
            this.userName = userName;
        }

        public String getUserPhone() {
            return userPhone;
        }

        public void setUserPhone(String userPhone) {
            this.userPhone = userPhone;
        }

        public String getUserRemark() {
            return userRemark;
        }

        public void setUserRemark(String userRemark) {
            this.userRemark = userRemark;
        }

        public List<TrendsCommentBean> getTrendsComment() {
            return trendsComment;
        }

        public void setTrendsComment(List<TrendsCommentBean> trendsComment) {
            this.trendsComment = trendsComment;
        }

        public List<TrendsCommentLikeBean> getTrendsCommentLike() {
            return trendsCommentLike;
        }

        public void setTrendsCommentLike(List<TrendsCommentLikeBean> trendsCommentLike) {
            this.trendsCommentLike = trendsCommentLike;
        }

        public List<TrendsImgBean> getTrendsImg() {
            return trendsImg;
        }

        public void setTrendsImg(List<TrendsImgBean> trendsImg) {
            this.trendsImg = trendsImg;
        }

        public static class TrendsCommentBean {
            /**
             * content : The only way
             * createtime : 2018-07-24 14:15:25
             * id : 23
             * userHeadImage : https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1531477347932&di=369929b60c0283f7e5f7ef4c72c5b6df&imgtype=0&src=http%3A%2F%2Ftx.haiqq.com%2Fuploads%2Fallimg%2F170508%2F0954333D7-2.jpg
             * userName : 13501031194
             * userPhone : 13501031194
             * userRemark : 测试昵称
             * userid : 25
             */

            private String content;
            private String createtime;
            private int id;
            private String userHeadImage;
            private String userName;
            private String userPhone;
            private String userRemark;
            private int userid;

            public String getContent() {
                return content;
            }

            public void setContent(String content) {
                this.content = content;
            }

            public String getCreatetime() {
                return createtime;
            }

            public void setCreatetime(String createtime) {
                this.createtime = createtime;
            }

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getUserHeadImage() {
                return userHeadImage;
            }

            public void setUserHeadImage(String userHeadImage) {
                this.userHeadImage = userHeadImage;
            }

            public String getUserName() {
                return userName;
            }

            public void setUserName(String userName) {
                this.userName = userName;
            }

            public String getUserPhone() {
                return userPhone;
            }

            public void setUserPhone(String userPhone) {
                this.userPhone = userPhone;
            }

            public String getUserRemark() {
                return userRemark;
            }

            public void setUserRemark(String userRemark) {
                this.userRemark = userRemark;
            }

            public int getUserid() {
                return userid;
            }

            public void setUserid(int userid) {
                this.userid = userid;
            }
        }

        public static class TrendsCommentLikeBean {
            /**
             * userName : 13501031194
             * userPhone : 13501031194
             * userRemark : 测试昵称
             * userid : 25
             */

            private String userName;
            private String userPhone;
            private String userRemark;
            private int userid;

            public String getUserName() {
                return userName;
            }

            public void setUserName(String userName) {
                this.userName = userName;
            }

            public String getUserPhone() {
                return userPhone;
            }

            public void setUserPhone(String userPhone) {
                this.userPhone = userPhone;
            }

            public String getUserRemark() {
                return userRemark;
            }

            public void setUserRemark(String userRemark) {
                this.userRemark = userRemark;
            }

            public int getUserid() {
                return userid;
            }

            public void setUserid(int userid) {
                this.userid = userid;
            }
        }

        public static class TrendsImgBean {
            /**
             * id : 55
             * imgpath : http://oss-aliyun-liululu.oss-cn-shanghai.aliyuncs.com/upload/153232978453210277.jpg
             */

            private int id;
            private String imgpath;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getImgpath() {
                return imgpath;
            }

            public void setImgpath(String imgpath) {
                this.imgpath = imgpath;
            }
        }
    }


//    private List<DataBean> data;
//
//    public List<DataBean> getData() {
//        return data;
//    }
//
//    public void setData(List<DataBean> data) {
//        this.data = data;
//    }
//
//    public static class DataBean implements MultiType {
//        /**
//         * trendsComment : []
//         * trendsCommentCount : 0
//         * trendsCommentLike : []
//         * trendsContent : 哈哈哈
//         * trendsCreateTime : 2018-07-30 09:54:10
//         * trendsId : 23
//         * trendsImg : [{"id":62,"imgpath":"http://oss-aliyun-liululu.oss-cn-shanghai.aliyuncs.com/upload/153291565035310508.jpg"}]
//         * trendsImgCount : 1
//         * userHeadImage : https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1531477347932&di=369929b60c0283f7e5f7ef4c72c5b6df&imgtype=0&src=http%3A%2F%2Ftx.haiqq.com%2Fuploads%2Fallimg%2F170508%2F0954333D7-2.jpg
//         * userId : 25
//         * userName : 13501031194
//         * userPhone : 13501031194
//         * userRemark : 测试昵称
//         */
//
//        private int trendsCommentCount;
//        private String trendsContent;
//        private String trendsCreateTime;
//        private int trendsId;
//        private int trendsImgCount;
//        private String userHeadImage;
//        private int userId;
//        private String userName;
//        private String userPhone;
//        private String userRemark;
//        private List<?> trendsComment;
//        private List<?> trendsCommentLike;
//        private List<TrendsImgBean> trendsImg;
//
//        public int getTrendsCommentCount() {
//            return trendsCommentCount;
//        }
//
//        public void setTrendsCommentCount(int trendsCommentCount) {
//            this.trendsCommentCount = trendsCommentCount;
//        }
//
//        public String getTrendsContent() {
//            return trendsContent;
//        }
//
//        public void setTrendsContent(String trendsContent) {
//            this.trendsContent = trendsContent;
//        }
//
//        public String getTrendsCreateTime() {
//            return trendsCreateTime;
//        }
//
//        public void setTrendsCreateTime(String trendsCreateTime) {
//            this.trendsCreateTime = trendsCreateTime;
//        }
//
//        public int getTrendsId() {
//            return trendsId;
//        }
//
//        public void setTrendsId(int trendsId) {
//            this.trendsId = trendsId;
//        }
//
//        public int getTrendsImgCount() {
//            return trendsImgCount;
//        }
//
//        public void setTrendsImgCount(int trendsImgCount) {
//            this.trendsImgCount = trendsImgCount;
//        }
//
//        public String getUserHeadImage() {
//            return userHeadImage;
//        }
//
//        public void setUserHeadImage(String userHeadImage) {
//            this.userHeadImage = userHeadImage;
//        }
//
//        public int getUserId() {
//            return userId;
//        }
//
//        public void setUserId(int userId) {
//            this.userId = userId;
//        }
//
//        public String getUserName() {
//            return userName;
//        }
//
//        public void setUserName(String userName) {
//            this.userName = userName;
//        }
//
//        public String getUserPhone() {
//            return userPhone;
//        }
//
//        public void setUserPhone(String userPhone) {
//            this.userPhone = userPhone;
//        }
//
//        public String getUserRemark() {
//            return userRemark;
//        }
//
//        public void setUserRemark(String userRemark) {
//            this.userRemark = userRemark;
//        }
//
//        public List<?> getTrendsComment() {
//            return trendsComment;
//        }
//
//        public void setTrendsComment(List<?> trendsComment) {
//            this.trendsComment = trendsComment;
//        }
//
//        public List<?> getTrendsCommentLike() {
//            return trendsCommentLike;
//        }
//
//        public void setTrendsCommentLike(List<?> trendsCommentLike) {
//            this.trendsCommentLike = trendsCommentLike;
//        }
//
//        public List<TrendsImgBean> getTrendsImg() {
//            return trendsImg;
//        }
//
//        public void setTrendsImg(List<TrendsImgBean> trendsImg) {
//            this.trendsImg = trendsImg;
//        }
//
//

//
//        public static class TrendsImgBean {
//            /**
//             * id : 62
//             * imgpath : http://oss-aliyun-liululu.oss-cn-shanghai.aliyuncs.com/upload/153291565035310508.jpg
//             */
//
//            private int id;
//            private String imgpath;
//
//            public int getId() {
//                return id;
//            }
//
//            public void setId(int id) {
//                this.id = id;
//            }
//
//            public String getImgpath() {
//                return imgpath;
//            }
//
//            public void setImgpath(String imgpath) {
//                this.imgpath = imgpath;
//            }
//        }
//    }
}
