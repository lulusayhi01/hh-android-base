package cn.rongcloud.im.server.response;

import java.io.Serializable;
import java.util.List;

/**
 * @Author: huluhong
 * @Time: 2018/11/11 0010
 * @Description：This is PeopleNearbyResponse
 */

public class RecordBehaviorResponse extends BaseResponse{


    private List<DataBean> data;

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean implements Serializable{
        /**
         * content : 刚从高凑一凑呀
         * createtime : 2018-11-27 19:50:50
         * id : 9
         * remark : 地头都要凑一凑哟一次
         * status : 1
         * timeend : 2018-11-30
         * timestart : 2018-11-27
         * type : 30110
         * userid : 30
         */

        private String content;
        private String createtime;
        private int id;
        private String remark;
        private int status;
        private String timeend;
        private String timestart;
        private int type;
        private int userid;

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public String getCreatetime() {
            return createtime;
        }

        public void setCreatetime(String createtime) {
            this.createtime = createtime;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getRemark() {
            return remark;
        }

        public void setRemark(String remark) {
            this.remark = remark;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public String getTimeend() {
            return timeend;
        }

        public void setTimeend(String timeend) {
            this.timeend = timeend;
        }

        public String getTimestart() {
            return timestart;
        }

        public void setTimestart(String timestart) {
            this.timestart = timestart;
        }

        public int getType() {
            return type;
        }

        public void setType(int type) {
            this.type = type;
        }

        public int getUserid() {
            return userid;
        }

        public void setUserid(int userid) {
            this.userid = userid;
        }
    }
}
