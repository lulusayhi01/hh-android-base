package cn.rongcloud.im.server.request;


import cn.rongcloud.im.server.request.base.BaseRequest;

/**
 * Created by AMing on 15/12/23.
 * Company RongCloud
 */
public class SendCodeRequest extends BaseRequest {

    private String region;

    private String mobile;

    private int action;//action 1=注册 2=找回密码  3=添加银行卡短信验证码 4=提现短信验证码

    private boolean isChannel = true;

    public SendCodeRequest(String region, String mobile, int action, boolean isChannel) {
        this.region = region;
        this.mobile = mobile;
        this.action = action;
        this.isChannel = isChannel;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public int getAction() {
        return action;
    }

    public void setAction(int action) {
        this.action = action;
    }

    public boolean isChannel() {
        return isChannel;
    }

    public void setChannel(boolean channel) {
        isChannel = channel;
    }
}
