package cn.rongcloud.im.server.response;

import java.util.List;

/**
 * Created by Administrator on 2018/9/14 0014.
 */

public class FriendRemarkResponse extends BaseResponse{


    /**
     * data : {"friendRemark":"哈哈哈","headimage":"http://oss-aliyun-liululu.oss-cn-shanghai.aliyuncs.com/upload/153475028922810268.jpg","id":32,"machineCount":0,"name":"啊啊啊","phone":"18767099945","trendsImg":[]}
     */

    private DataBean data;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * friendRemark : 哈哈哈
         * headimage : http://oss-aliyun-liululu.oss-cn-shanghai.aliyuncs.com/upload/153475028922810268.jpg
         * id : 32
         * machineCount : 0
         * name : 啊啊啊
         * phone : 18767099945
         * trendsImg : []
         */

        private String friendRemark;
        private String headimage;
        private int id;
        private int machineCount;
        private String name;
        private String phone;
        private List<?> trendsImg;

        public String getFriendRemark() {
            return friendRemark;
        }

        public void setFriendRemark(String friendRemark) {
            this.friendRemark = friendRemark;
        }

        public String getHeadimage() {
            return headimage;
        }

        public void setHeadimage(String headimage) {
            this.headimage = headimage;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getMachineCount() {
            return machineCount;
        }

        public void setMachineCount(int machineCount) {
            this.machineCount = machineCount;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public List<?> getTrendsImg() {
            return trendsImg;
        }

        public void setTrendsImg(List<?> trendsImg) {
            this.trendsImg = trendsImg;
        }
    }
}
