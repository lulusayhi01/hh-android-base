package cn.rongcloud.im.server.response;

import java.io.Serializable;
import java.util.List;

/**
 * @Author: huluhong
 * @Time: 2018/11/11 0010
 * @Description：This is PeopleNearbyResponse
 */

public class RecordFinancialResponse extends BaseResponse{


    private List<DataBean> data;

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean implements Serializable{
        /**
         * content : 以后还会近近景近景聚
         * createtime : 2018-11-28 09:57:11
         * id : 8
         * num : 336.6
         * status : 1
         * type : 30003
         * userid : 30
         */

        private String content;
        private String createtime;
        private int id;
        private double num;
        private int status;
        private int type;
        private int userid;

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public String getCreatetime() {
            return createtime;
        }

        public void setCreatetime(String createtime) {
            this.createtime = createtime;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public double getNum() {
            return num;
        }

        public void setNum(double num) {
            this.num = num;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public int getType() {
            return type;
        }

        public void setType(int type) {
            this.type = type;
        }

        public int getUserid() {
            return userid;
        }

        public void setUserid(int userid) {
            this.userid = userid;
        }
    }
}
