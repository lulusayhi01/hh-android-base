package cn.rongcloud.im.server.response;

import java.util.List;

/**
 * Created by AMing on 16/1/4.
 * Company RongCloud
 */
public class GetUserInfoByPhoneResponse extends BaseResponse{
    /**
     * data : {"friendSource":"8001","friendStatus":6001,"headimage":"http://oss-aliyun-liululu.oss-cn-shanghai.aliyuncs.com/upload/153787673121710074.jpg","id":30,"isf":50,"machineCount":0,"name":"璐璐","phone":"15013554615","trendsImg":[{"id":208,"imgpath":"http://oss-aliyun-liululu.oss-cn-shanghai.aliyuncs.com/upload/153675244754110849.jpg"},{"id":207,"imgpath":"http://oss-aliyun-liululu.oss-cn-shanghai.aliyuncs.com/upload/153675244741010067.jpg"},{"id":204,"imgpath":"http://oss-aliyun-liululu.oss-cn-shanghai.aliyuncs.com/upload/153656644048910766.jpg"}]}
     */

    private DataBean data;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * friendSource : 8001
         * friendStatus : 6001
         * headimage : http://oss-aliyun-liululu.oss-cn-shanghai.aliyuncs.com/upload/153787673121710074.jpg
         * id : 30
         * isf : 50
         * machineCount : 0
         * name : 璐璐
         * phone : 15013554615
         * trendsImg : [{"id":208,"imgpath":"http://oss-aliyun-liululu.oss-cn-shanghai.aliyuncs.com/upload/153675244754110849.jpg"},{"id":207,"imgpath":"http://oss-aliyun-liululu.oss-cn-shanghai.aliyuncs.com/upload/153675244741010067.jpg"},{"id":204,"imgpath":"http://oss-aliyun-liululu.oss-cn-shanghai.aliyuncs.com/upload/153656644048910766.jpg"}]
         */

        private String friendSource;
        private int friendStatus;
        private String headimage;
        private int id;
        private int isf;
        private int machineCount;
        private String name;
        private String phone;
        private List<TrendsImgBean> trendsImg;

        public String getFriendSource() {
            return friendSource;
        }

        public void setFriendSource(String friendSource) {
            this.friendSource = friendSource;
        }

        public int getFriendStatus() {
            return friendStatus;
        }

        public void setFriendStatus(int friendStatus) {
            this.friendStatus = friendStatus;
        }

        public String getHeadimage() {
            return headimage;
        }

        public void setHeadimage(String headimage) {
            this.headimage = headimage;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getIsf() {
            return isf;
        }

        public void setIsf(int isf) {
            this.isf = isf;
        }

        public int getMachineCount() {
            return machineCount;
        }

        public void setMachineCount(int machineCount) {
            this.machineCount = machineCount;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public List<TrendsImgBean> getTrendsImg() {
            return trendsImg;
        }

        public void setTrendsImg(List<TrendsImgBean> trendsImg) {
            this.trendsImg = trendsImg;
        }

        public static class TrendsImgBean {
            /**
             * id : 208
             * imgpath : http://oss-aliyun-liululu.oss-cn-shanghai.aliyuncs.com/upload/153675244754110849.jpg
             */

            private int id;
            private String imgpath;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getImgpath() {
                return imgpath;
            }

            public void setImgpath(String imgpath) {
                this.imgpath = imgpath;
            }
        }
    }


//    /**
//     * data : {"headimage":"http://p6nlllxof.bkt.clouddn.com/upload/20180705/3f668359c2354f2298f978c6266b560d.jpg","id":30,"name":"15013554615","trendsImg":[]}
//     */
//
//    private DataBean data;
//
//    public DataBean getData() {
//        return data;
//    }
//
//    public void setData(DataBean data) {
//        this.data = data;
//    }
//
//    public static class DataBean {
//        /**
//         * headimage : http://p6nlllxof.bkt.clouddn.com/upload/20180705/3f668359c2354f2298f978c6266b560d.jpg
//         * id : 30
//         * name : 15013554615
//         * trendsImg : []
//         */
//
//        private String headimage;
//        private int id;
//        private String name;
//        private String portraitUri;
//        private List<?> trendsImg;
//
//        public String getHeadimage() {
//            return headimage;
//        }
//
//        public void setHeadimage(String headimage) {
//            this.headimage = headimage;
//        }
//
//        public int getId() {
//            return id;
//        }
//
//        public void setId(int id) {
//            this.id = id;
//        }
//
//        public String getName() {
//            return name;
//        }
//
//        public void setName(String name) {
//            this.name = name;
//        }
//
//        public List<?> getTrendsImg() {
//            return trendsImg;
//        }
//
//        public void setTrendsImg(List<?> trendsImg) {
//            this.trendsImg = trendsImg;
//        }
//
//        public String getPortraitUri() {
//            return portraitUri;
//        }
//
//        public void setPortraitUri(String portraitUri) {
//            this.portraitUri = portraitUri;
//        }
//    }
//
//    /**
//     * {"data":{"headimage":"http://p6nlllxof.bkt.clouddn.com/upload/20180705/3f668359c2354f2298f978c6266b560d.jpg","id":30,"name":"15013554615","trendsImg":[]},"msg":"请求成功","resultCode":0}
//     */
//******


//    /**
//     * code : 200
//     * result : {"id":"10YVscJI3","nickname":"阿明","portraitUri":""}
//     */
//
//    private int code;
//    /**
//     * id : 10YVscJI3
//     * nickname : 阿明
//     * portraitUri :
//     */
//
//    private ResultEntity result;
//
//    public void setCode(int code) {
//        this.code = code;
//    }
//
//    public void setResult(ResultEntity result) {
//        this.result = result;
//    }
//
//    public int getCode() {
//        return code;
//    }
//
//    public ResultEntity getResult() {
//        return result;
//    }
//
//    public static class ResultEntity {
//        private String id;
//        private String nickname;
//        private String portraitUri;
//
//        public void setId(String id) {
//            this.id = id;
//        }
//
//        public void setNickname(String nickname) {
//            this.nickname = nickname;
//        }
//
//        public void setPortraitUri(String portraitUri) {
//            this.portraitUri = portraitUri;
//        }
//
//        public String getId() {
//            return id;
//        }
//
//        public String getNickname() {
//            return nickname;
//        }
//
//        public String getPortraitUri() {
//            return portraitUri;
//        }
//    }
}
