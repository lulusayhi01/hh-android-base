package cn.rongcloud.im.server.response;


import java.util.List;

/**
 * 出售的矿机列表
 *
 * Created by huluhong
 */
public class SellMachineListResponse extends BaseResponse{

    /**
     * {"count":4,"data":[{"buytype":5102,"coinName":"脉宝币","coinid":1,"id":1,"level":5201,"manhour":"180","name":"迷你矿机","price":"5","status":1},{"buytype":5101,"coinName":"脉宝币","coinTypeName":"脉宝币","coinid":1,"coinnum":"100","cointype":1,"id":2,"level":5202,"manhour":"180","name":"初级矿机","price":"","status":1},{"buytype":5102,"coinName":"脉宝币","coinid":1,"id":3,"level":5203,"manhour":"180","name":" 中级矿机","price":"30","status":1},{"buytype":5102,"coinName":"脉宝币","coinid":1,"id":4,"level":5204,"manhour":"180","name":"高级矿机","price":"100","status":1}],"msg":"请求成功","resultCode":0}
     */


    private List<DataBean> data;

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * buytype : 5102
         * coinName : 脉宝币
         * coinid : 1
         * id : 1
         * level : 5201
         * manhour : 180
         * name : 迷你矿机
         * price : 5
         * status : 1
         * coinTypeName : 脉宝币
         * coinnum : 100
         * cointype : 1
         */

        private int buytype;
        private String coinName;
        private int coinid;
        private int id;
        private int level;
        private String manhour;
        private String name;
        private String price;
        private int status;
        private String coinTypeName;
        private String coinnum;
        private int cointype;
        private boolean isSelect;

        public int getBuytype() {
            return buytype;
        }

        public void setBuytype(int buytype) {
            this.buytype = buytype;
        }

        public String getCoinName() {
            return coinName;
        }

        public void setCoinName(String coinName) {
            this.coinName = coinName;
        }

        public int getCoinid() {
            return coinid;
        }

        public void setCoinid(int coinid) {
            this.coinid = coinid;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getLevel() {
            return level;
        }

        public void setLevel(int level) {
            this.level = level;
        }

        public String getManhour() {
            return manhour;
        }

        public void setManhour(String manhour) {
            this.manhour = manhour;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public String getCoinTypeName() {
            return coinTypeName;
        }

        public void setCoinTypeName(String coinTypeName) {
            this.coinTypeName = coinTypeName;
        }

        public String getCoinnum() {
            return coinnum;
        }

        public void setCoinnum(String coinnum) {
            this.coinnum = coinnum;
        }

        public int getCointype() {
            return cointype;
        }

        public void setCointype(int cointype) {
            this.cointype = cointype;
        }

        public boolean isSelect() {
            return isSelect;
        }

        public void setSelect(boolean select) {
            isSelect = select;
        }
    }

}
