package cn.rongcloud.im.server.response;

import java.io.Serializable;

/**
 *
 * Created by Administrator on 2018/7/16 0016.
 */

public class BaseResponse{

    /**
     * {"msg":"短信发送成功","resultCode":0}
     */

    private int resultCode;
    private String msg;
    private int count;//数据条目数

    public int getResultCode() {
        return resultCode;
    }

    public void setResultCode(int resultCode) {
        this.resultCode = resultCode;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
