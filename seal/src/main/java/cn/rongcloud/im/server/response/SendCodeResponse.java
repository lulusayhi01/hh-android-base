package cn.rongcloud.im.server.response;


/**
 * Created by AMing on 15/12/23.
 * Company RongCloud
 */
public class SendCodeResponse {

    /**
     * {"msg":"短信发送成功","resultCode":0}
     */

    private int resultCode;
    private String msg;

    public int getResultCode() {
        return resultCode;
    }

    public void setResultCode(int resultCode) {
        this.resultCode = resultCode;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
