package cn.rongcloud.im.server.response;

import java.io.Serializable;
import java.util.List;

/**
 * @Author: huluhong
 * @Time: 2018/11/11 0010
 * @Description：This is PeopleNearbyResponse
 */

public class RecordCommunicationResponse extends BaseResponse{


    private List<DataBean> data;

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean implements Serializable{
        /**
         * content : http://视频地址
         * createtime : 2018-11-20 10:44:09
         * fuserid : 30
         * id : 5
         * status : 1
         * tuserid : 25
         * type : 30204
         */

        private String content;
        private String createtime;
        private int fuserid;
        private int id;
        private int status;
        private int tuserid;
        private int type;

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public String getCreatetime() {
            return createtime;
        }

        public void setCreatetime(String createtime) {
            this.createtime = createtime;
        }

        public int getFuserid() {
            return fuserid;
        }

        public void setFuserid(int fuserid) {
            this.fuserid = fuserid;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public int getTuserid() {
            return tuserid;
        }

        public void setTuserid(int tuserid) {
            this.tuserid = tuserid;
        }

        public int getType() {
            return type;
        }

        public void setType(int type) {
            this.type = type;
        }
    }
}
