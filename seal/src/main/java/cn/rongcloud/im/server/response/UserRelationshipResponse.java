package cn.rongcloud.im.server.response;

import java.util.List;

/**
 * Created by AMing on 16/1/7.
 * Company RongCloud
 */
public class UserRelationshipResponse extends BaseResponse{

    /**
     * {"count":1,"data":[{"headimage":"http://p6nlllxof.bkt.clouddn.com/upload/20180705/3f668359c2354f2298f978c6266b560d.jpg","id":31,"name":"15107177306","requestId":15,"requestRemker":"我就好哈哈哈哈","requestStatus":5002,"status":1}],"msg":"请求成功","resultCode":0}
     */



    /**
     * count : 1
     * data : [{"headimage":"http://p6nlllxof.bkt.clouddn.com/upload/20180705/3f668359c2354f2298f978c6266b560d.jpg","id":31,"name":"15107177306","requestId":15,"requestRemker":"我就好哈哈哈哈","requestStatus":5002,"status":1}]
     */

    private int count;
    private List<DataBean> data;

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * headimage : http://p6nlllxof.bkt.clouddn.com/upload/20180705/3f668359c2354f2298f978c6266b560d.jpg
         * id : 31
         * name : 15107177306
         * requestId : 15
         * requestRemker : 我就好哈哈哈哈
         * requestStatus : 5002
         * status : 1
         */

        private String headimage;
        private int id;
        private String name;
        private int requestId;
        private String requestRemker;
        private int requestStatus;
        private int status;
        private String updatedAt;//*
        private String displayName;//*

        public String getHeadimage() {
            return headimage;
        }

        public void setHeadimage(String headimage) {
            this.headimage = headimage;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getRequestId() {
            return requestId;
        }

        public void setRequestId(int requestId) {
            this.requestId = requestId;
        }

        public String getRequestRemker() {
            return requestRemker;
        }

        public void setRequestRemker(String requestRemker) {
            this.requestRemker = requestRemker;
        }

        public int getRequestStatus() {
            return requestStatus;
        }

        public void setRequestStatus(int requestStatus) {
            this.requestStatus = requestStatus;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        public String getDisplayName() {
            return displayName;
        }

        public void setDisplayName(String displayName) {
            this.displayName = displayName;
        }
    }



//    /**
//     * code : 200
//     * result : [{"displayName":"","message":"手机号:18622222222昵称:的用户请求添加你为好友","status":11,"updatedAt":"2016-01-07T06:22:55.000Z","user":{"id":"i3gRfA1ml","nickname":"nihaoa","portraitUri":""}}]
//     */
//
//    private int code;
//    /**
//     * displayName :
//     * message : 手机号:18622222222昵称:的用户请求添加你为好友
//     * status : 11
//     * updatedAt : 2016-01-07T06:22:55.000Z
//     * user : {"id":"i3gRfA1ml","nickname":"nihaoa","portraitUri":""}
//     */
//
//    private List<ResultEntity> result;
//
//    public void setCode(int code) {
//        this.code = code;
//    }
//
//    public void setResult(List<ResultEntity> result) {
//        this.result = result;
//    }
//
//    public int getCode() {
//        return code;
//    }
//
//    public List<ResultEntity> getResult() {
//        return result;
//    }
//
//    public static class ResultEntity implements Comparable {
//
//        public ResultEntity(String displayName, String message, int status, String updatedAt, UserEntity user) {
//            this.displayName = displayName;
//            this.message = message;
//            this.status = status;
//            this.updatedAt = updatedAt;
//            this.user = user;
//        }
//
//        public ResultEntity() {
//
//        }
//
//        private String displayName;
//        private String message;
//        private int status;
//        private String updatedAt;
//        /**
//         * id : i3gRfA1ml
//         * nickname : nihaoa
//         * portraitUri :
//         */
//
//        private UserEntity user;
//
//        public void setDisplayName(String displayName) {
//            this.displayName = displayName;
//        }
//
//        public void setMessage(String message) {
//            this.message = message;
//        }
//
//        public void setStatus(int status) {
//            this.status = status;
//        }
//
//        public void setUpdatedAt(String updatedAt) {
//            this.updatedAt = updatedAt;
//        }
//
//        public void setUser(UserEntity user) {
//            this.user = user;
//        }
//
//        public String getDisplayName() {
//            return displayName;
//        }
//
//        public String getMessage() {
//            return message;
//        }
//
//        public int getStatus() {
//            return status;
//        }
//
//        public String getUpdatedAt() {
//            return updatedAt;
//        }
//
//        public UserEntity getUser() {
//            return user;
//        }
//
//        @Override
//        public int compareTo(Object another) {
//            return 0;
//        }
//
//        public static class UserEntity {
//            private String id;
//            private String nickname;
//            private String portraitUri;
//
//            public void setId(String id) {
//                this.id = id;
//            }
//
//            public void setNickname(String nickname) {
//                this.nickname = nickname;
//            }
//
//            public void setPortraitUri(String portraitUri) {
//                this.portraitUri = portraitUri;
//            }
//
//            public String getId() {
//                return id;
//            }
//
//            public String getNickname() {
//                return nickname;
//            }
//
//            public String getPortraitUri() {
//                return portraitUri;
//            }
//        }
//    }
}
