package cn.rongcloud.im.server.response.friendcircle.other;


import cn.rongcloud.im.server.response.BaseResponse;

/**
 *
 */
public class ServiceInfo extends BaseResponse {

    public interface ServiceInfoFields {
        String TITLE = "title";
        String CONTENT = "content";
        String TIPS="tips";
    }

    private String title;
    private String content;
    private String tips;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getTips() {
        return tips;
    }

    public void setTips(String tips) {
        this.tips = tips;
    }
}
