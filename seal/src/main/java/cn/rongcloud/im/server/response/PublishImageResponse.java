package cn.rongcloud.im.server.response;

/**
 * Created by Administrator on 2018/8/9 0009.
 */

public class PublishImageResponse extends BaseResponse{

    private String data;//上传图片返回的id

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
