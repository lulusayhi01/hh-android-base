package cn.rongcloud.im.server.response;


/**
 * Created by AMing on 15/12/24.
 * Company RongCloud
 */
public class LoginResponse extends BaseResponse {


    /**
     * data : {"rapnum":"0","summoney":"10","passwd":"","phone":"15013554615","coldmoney":"0","status":1,"remark":"无","registermoney":"10","loginnum":0,"channelid":0,"headimage":"http://p6nlllxof.bkt.clouddn.com/upload/20180705/3f668359c2354f2298f978c6266b560d.jpg","id":30,"investmoney":"0","level":0,"token":"DCRd05FU/mhfGGsCdllSg3XBKsKU1/gRTOL7CDCNxZimIrYg/qfTOPdAKpHeXEfAbGVvcoTJxGnU1X6bDu+ZvA==","logintime":"1531707204","returnmoney":"0","inviterid":0,"name":"15013554615","usablemoney":"0","creattime":"2018-07-16 10:13:24","fetchmoney":"0","mbtToken":"06fe4245b2c754c8afa37cba63b5a4fc","waitactivemoney":"0"}
     * returnUrl : /user/index
     */

    private DataBean data;
    private String returnUrl;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public String getReturnUrl() {
        return returnUrl;
    }

    public void setReturnUrl(String returnUrl) {
        this.returnUrl = returnUrl;
    }

    public static class DataBean {
        /**
         * rapnum : 0
         * summoney : 10
         * passwd :
         * phone : 15013554615
         * coldmoney : 0
         * status : 1
         * remark : 无
         * registermoney : 10
         * loginnum : 0
         * channelid : 0
         * headimage : http://p6nlllxof.bkt.clouddn.com/upload/20180705/3f668359c2354f2298f978c6266b560d.jpg
         * id : 30
         * investmoney : 0
         * level : 0
         * token : DCRd05FU/mhfGGsCdllSg3XBKsKU1/gRTOL7CDCNxZimIrYg/qfTOPdAKpHeXEfAbGVvcoTJxGnU1X6bDu+ZvA==
         * logintime : 1531707204
         * returnmoney : 0
         * inviterid : 0
         * name : 15013554615
         * usablemoney : 0
         * creattime : 2018-07-16 10:13:24
         * fetchmoney : 0
         * mbtToken : 06fe4245b2c754c8afa37cba63b5a4fc
         * waitactivemoney : 0
         */

        private String rapnum;
        private String summoney;
        private String passwd;
        private String phone;
        private String coldmoney;
        private int status;
        private String remark;
        private String registermoney;
        private int loginnum;
        private int channelid;
        private String headimage;//用户头像
        private int id;
        private String investmoney;
        private int level;
        private String token;
        private String logintime;
        private String returnmoney;
        private int inviterid;
        private String name;
        private String usablemoney;
        private String creattime;
        private String fetchmoney;
        private String mbtToken;
        private String waitactivemoney;
        private String type;//用户类型（0：普通用户  1：医生  2：管理者）
        private int objId;//用户类型的扩展对象表ID
        private int doctorId;

        public int getDoctorId() {
            return doctorId;
        }

        public void setDoctorId(int doctorId) {
            this.doctorId = doctorId;
        }

        public int getObjId() {
            return objId;
        }

        public void setObjId(int objId) {
            this.objId = objId;
        }

        public String getRapnum() {
            return rapnum;
        }

        public void setRapnum(String rapnum) {
            this.rapnum = rapnum;
        }

        public String getSummoney() {
            return summoney;
        }

        public void setSummoney(String summoney) {
            this.summoney = summoney;
        }

        public String getPasswd() {
            return passwd;
        }

        public void setPasswd(String passwd) {
            this.passwd = passwd;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getColdmoney() {
            return coldmoney;
        }

        public void setColdmoney(String coldmoney) {
            this.coldmoney = coldmoney;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public String getRemark() {
            return remark;
        }

        public void setRemark(String remark) {
            this.remark = remark;
        }

        public String getRegistermoney() {
            return registermoney;
        }

        public void setRegistermoney(String registermoney) {
            this.registermoney = registermoney;
        }

        public int getLoginnum() {
            return loginnum;
        }

        public void setLoginnum(int loginnum) {
            this.loginnum = loginnum;
        }

        public int getChannelid() {
            return channelid;
        }

        public void setChannelid(int channelid) {
            this.channelid = channelid;
        }

        public String getHeadimage() {
            return headimage;
        }

        public void setHeadimage(String headimage) {
            this.headimage = headimage;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getInvestmoney() {
            return investmoney;
        }

        public void setInvestmoney(String investmoney) {
            this.investmoney = investmoney;
        }

        public int getLevel() {
            return level;
        }

        public void setLevel(int level) {
            this.level = level;
        }

        public String getToken() {
            return token;
        }

        public void setToken(String token) {
            this.token = token;
        }

        public String getLogintime() {
            return logintime;
        }

        public void setLogintime(String logintime) {
            this.logintime = logintime;
        }

        public String getReturnmoney() {
            return returnmoney;
        }

        public void setReturnmoney(String returnmoney) {
            this.returnmoney = returnmoney;
        }

        public int getInviterid() {
            return inviterid;
        }

        public void setInviterid(int inviterid) {
            this.inviterid = inviterid;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getUsablemoney() {
            return usablemoney;
        }

        public void setUsablemoney(String usablemoney) {
            this.usablemoney = usablemoney;
        }

        public String getCreattime() {
            return creattime;
        }

        public void setCreattime(String creattime) {
            this.creattime = creattime;
        }

        public String getFetchmoney() {
            return fetchmoney;
        }

        public void setFetchmoney(String fetchmoney) {
            this.fetchmoney = fetchmoney;
        }

        public String getMbtToken() {
            return mbtToken;
        }

        public void setMbtToken(String mbtToken) {
            this.mbtToken = mbtToken;
        }

        public String getWaitactivemoney() {
            return waitactivemoney;
        }

        public void setWaitactivemoney(String waitactivemoney) {
            this.waitactivemoney = waitactivemoney;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }
    }
}
