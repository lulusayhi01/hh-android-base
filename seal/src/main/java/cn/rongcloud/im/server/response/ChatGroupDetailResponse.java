package cn.rongcloud.im.server.response;

import java.util.List;

/**
 * 群详情
 */
public class ChatGroupDetailResponse extends BaseResponse{


    /**
     * data : {"code":"1bca6bf3-785d-4962-a2a2-267cd4ad50c1","createtime":"2018-08-28 20:25:57","groupOwnerId":30,"id":46,"name":"公积金,涓涓,璐璐","user":[{"headimage":"http://oss-aliyun-liululu.oss-cn-shanghai.aliyuncs.com/upload/153389023664010270.jpg","id":25,"name":"涓涓","phone":"13501031194"},{"headimage":"http://p6nlllxof.bkt.clouddn.com/upload/20180705/3f668359c2354f2298f978c6266b560d.jpg","id":30,"name":"璐璐","phone":"15013554615"},{"headimage":"http://oss-aliyun-liululu.oss-cn-shanghai.aliyuncs.com/upload/153375949641810172.jpg","id":33,"name":"公积金","phone":"17631330812"}]}
     */

    private DataBean data;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * code : 1bca6bf3-785d-4962-a2a2-267cd4ad50c1
         * createtime : 2018-08-28 20:25:57
         * groupOwnerId : 30
         * id : 46
         * name : 公积金,涓涓,璐璐
         * user : [{"headimage":"http://oss-aliyun-liululu.oss-cn-shanghai.aliyuncs.com/upload/153389023664010270.jpg","id":25,"name":"涓涓","phone":"13501031194"},{"headimage":"http://p6nlllxof.bkt.clouddn.com/upload/20180705/3f668359c2354f2298f978c6266b560d.jpg","id":30,"name":"璐璐","phone":"15013554615"},{"headimage":"http://oss-aliyun-liululu.oss-cn-shanghai.aliyuncs.com/upload/153375949641810172.jpg","id":33,"name":"公积金","phone":"17631330812"}]
         */

        private String code;
        private String createtime;
        private int groupOwnerId;
        private int id;
        private String name;
        private List<UserBean> user;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getCreatetime() {
            return createtime;
        }

        public void setCreatetime(String createtime) {
            this.createtime = createtime;
        }

        public int getGroupOwnerId() {
            return groupOwnerId;
        }

        public void setGroupOwnerId(int groupOwnerId) {
            this.groupOwnerId = groupOwnerId;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public List<UserBean> getUser() {
            return user;
        }

        public void setUser(List<UserBean> user) {
            this.user = user;
        }

        public static class UserBean {
            /**
             * headimage : http://oss-aliyun-liululu.oss-cn-shanghai.aliyuncs.com/upload/153389023664010270.jpg
             * id : 25
             * name : 涓涓
             * phone : 13501031194
             */

            private String headimage;
            private int id;
            private String name;
            private String phone;

            public String getHeadimage() {
                return headimage;
            }

            public void setHeadimage(String headimage) {
                this.headimage = headimage;
            }

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getPhone() {
                return phone;
            }

            public void setPhone(String phone) {
                this.phone = phone;
            }
        }
    }
}
