package cn.rongcloud.im.server.request;


import cn.rongcloud.im.server.request.base.BaseRequest;

/**
 * Created by AMing on 15/12/23.
 * Company RongCloud
 */
public class RegisterRequest extends BaseRequest{

    private String phoneNum;
    private String password;
    private String passsure;//确认密码
    private String vcode;//手机验证码
    private String type;

    public RegisterRequest(String phoneNum, String password, String passsure, String vcode, String type) {
        this.phoneNum = phoneNum;
        this.password = password;
        this.passsure = passsure;
        this.vcode = vcode;
        this.type = type;
    }

    public String getPhoneNum() {
        return phoneNum;
    }

    public void setPhoneNum(String phoneNum) {
        this.phoneNum = phoneNum;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasssure() {
        return passsure;
    }

    public void setPasssure(String passsure) {
        this.passsure = passsure;
    }

    public String getVcode() {
        return vcode;
    }

    public void setVcode(String vcode) {
        this.vcode = vcode;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
