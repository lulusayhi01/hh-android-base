package cn.rongcloud.im.server.response;

import java.util.List;

/**
 * @Author: huluhong
 * @Time: 2018/10/11 0011
 * @Description：This is GroupListResponse
 */

public class GroupListResponse extends BaseResponse {


    private List<DataBean> data;

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * code : e9d08e3e-4453-4342-ac90-5013ac96642c
         * groupOwnerId : 33
         * id : 56
         * name : zfc、过客、璐璐
         * user : [{"headimage":"http://oss-aliyun-liululu.oss-cn-shanghai.aliyuncs.com/upload/153787673121710074.jpg","id":30,"machineCount":0,"name":"璐璐","phone":"15013554615"},{"friendRemark":"哈哈哈","headimage":"http://oss-aliyun-liululu.oss-cn-shanghai.aliyuncs.com/upload/153475028922810268.jpg","id":32,"machineCount":0,"name":"啊啊啊","phone":"18767099945"},{"headimage":"http://oss-aliyun-liululu.oss-cn-shanghai.aliyuncs.com/upload/153375949641810172.jpg","id":33,"machineCount":0,"name":"zfc","phone":"17631330812"},{"headimage":"http://p6nlllxof.bkt.clouddn.com/upload/20180705/3f668359c2354f2298f978c6266b560d.jpg","id":37,"machineCount":0,"name":"过客","phone":"13383305056"}]
         */

        private String code;
        private int groupOwnerId;
        private int id;
        private String name;
        private List<UserBean> user;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public int getGroupOwnerId() {
            return groupOwnerId;
        }

        public void setGroupOwnerId(int groupOwnerId) {
            this.groupOwnerId = groupOwnerId;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public List<UserBean> getUser() {
            return user;
        }

        public void setUser(List<UserBean> user) {
            this.user = user;
        }

        public static class UserBean {
            /**
             * headimage : http://oss-aliyun-liululu.oss-cn-shanghai.aliyuncs.com/upload/153787673121710074.jpg
             * id : 30
             * machineCount : 0
             * name : 璐璐
             * phone : 15013554615
             * friendRemark : 哈哈哈
             */

            private String headimage;
            private int id;
            private int machineCount;
            private String name;
            private String phone;
            private String friendRemark;

            public String getHeadimage() {
                return headimage;
            }

            public void setHeadimage(String headimage) {
                this.headimage = headimage;
            }

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public int getMachineCount() {
                return machineCount;
            }

            public void setMachineCount(int machineCount) {
                this.machineCount = machineCount;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getPhone() {
                return phone;
            }

            public void setPhone(String phone) {
                this.phone = phone;
            }

            public String getFriendRemark() {
                return friendRemark;
            }

            public void setFriendRemark(String friendRemark) {
                this.friendRemark = friendRemark;
            }
        }
    }
}
