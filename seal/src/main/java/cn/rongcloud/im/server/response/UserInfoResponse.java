package cn.rongcloud.im.server.response;


import java.util.List;

/**
 * 用户详情数据
 * Created by huluhong
 */
public class UserInfoResponse extends BaseResponse{


    /**
     * data : {"belongcode":"uM4ClHbL","channelid":0,"coldmoney":"0","creattime":"2018-07-16 10:13:24","fetchmoney":"0","friendStatus":6001,"headimage":"http://p6nlllxof.bkt.clouddn.com/upload/20180705/3f668359c2354f2298f978c6266b560d.jpg","id":30,"investmoney":"0","inviterid":0,"isf":50,"level":0,"loginnum":0,"logintime":"1531707204","name":"15013554615","passwd":"079362f9fa1b7c78a6e408916c90bbd6","phone":"15013554615","rapnum":"0","registermoney":"10","remark":"无","returnmoney":"0","status":1,"summoney":"10","token":"DCRd05FU/mhfGGsCdllSg3XBKsKU1/gRTOL7CDCNxZimIrYg/qfTOPdAKpHeXEfAbGVvcoTJxGnU1X6bDu+ZvA==","trendsImg":[{"id":146,"imgpath":"http://oss-aliyun-liululu.oss-cn-shanghai.aliyuncs.com/upload/153379936857710272.jpg"},{"id":147,"imgpath":"http://oss-aliyun-liululu.oss-cn-shanghai.aliyuncs.com/upload/153379941170510870.jpg"},{"id":148,"imgpath":"http://oss-aliyun-liululu.oss-cn-shanghai.aliyuncs.com/upload/153379957718410031.jpg"},{"id":151,"imgpath":"http://oss-aliyun-liululu.oss-cn-shanghai.aliyuncs.com/upload/153380304613410415.jpg"},{"id":152,"imgpath":"http://oss-aliyun-liululu.oss-cn-shanghai.aliyuncs.com/upload/153380753977610110.jpg"},{"id":153,"imgpath":"http://oss-aliyun-liululu.oss-cn-shanghai.aliyuncs.com/upload/153380753990610656.jpg"},{"id":154,"imgpath":"http://oss-aliyun-liululu.oss-cn-shanghai.aliyuncs.com/upload/153386419099010812.jpg"},{"id":160,"imgpath":"http://oss-aliyun-liululu.oss-cn-shanghai.aliyuncs.com/upload/153388235840710637.jpg"},{"id":168,"imgpath":"http://oss-aliyun-liululu.oss-cn-shanghai.aliyuncs.com/upload/153388657502410777.jpg"},{"id":169,"imgpath":"http://oss-aliyun-liululu.oss-cn-shanghai.aliyuncs.com/upload/153388657518210096.jpg"},{"id":170,"imgpath":"http://oss-aliyun-liululu.oss-cn-shanghai.aliyuncs.com/upload/153388657532910633.jpg"},{"id":171,"imgpath":"http://oss-aliyun-liululu.oss-cn-shanghai.aliyuncs.com/upload/153388657544810248.jpg"},{"id":172,"imgpath":"http://oss-aliyun-liululu.oss-cn-shanghai.aliyuncs.com/upload/153388657559110528.jpg"},{"id":173,"imgpath":"http://oss-aliyun-liululu.oss-cn-shanghai.aliyuncs.com/upload/153388657574010248.jpg"},{"id":181,"imgpath":"http://oss-aliyun-liululu.oss-cn-shanghai.aliyuncs.com/upload/153388797131110513.jpg"},{"id":182,"imgpath":"http://oss-aliyun-liululu.oss-cn-shanghai.aliyuncs.com/upload/153388797147310045.jpg"},{"id":183,"imgpath":"http://oss-aliyun-liululu.oss-cn-shanghai.aliyuncs.com/upload/153388797162510569.jpg"},{"id":184,"imgpath":"http://oss-aliyun-liululu.oss-cn-shanghai.aliyuncs.com/upload/153388797174910832.jpg"},{"id":185,"imgpath":"http://oss-aliyun-liululu.oss-cn-shanghai.aliyuncs.com/upload/153388823703310437.jpg"},{"id":188,"imgpath":"http://oss-aliyun-liululu.oss-cn-shanghai.aliyuncs.com/upload/153388901659410891.jpg"},{"id":191,"imgpath":"http://oss-aliyun-liululu.oss-cn-shanghai.aliyuncs.com/upload/153412856730110343.jpg"}],"usablemoney":"1000","userInfostatus":3302,"waitactivemoney":"0"}
     */

    private DataBean data;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * belongcode : uM4ClHbL
         * channelid : 0
         * coldmoney : 0
         * creattime : 2018-07-16 10:13:24
         * fetchmoney : 0
         * friendStatus : 6001
         * headimage : http://p6nlllxof.bkt.clouddn.com/upload/20180705/3f668359c2354f2298f978c6266b560d.jpg
         * id : 30
         * investmoney : 0
         * inviterid : 0
         * isf : 50
         * level : 0
         * loginnum : 0
         * logintime : 1531707204
         * name : 15013554615
         * passwd : 079362f9fa1b7c78a6e408916c90bbd6
         * phone : 15013554615
         * rapnum : 0
         * registermoney : 10
         * remark : 无
         * returnmoney : 0
         * status : 1
         * summoney : 10
         * token : DCRd05FU/mhfGGsCdllSg3XBKsKU1/gRTOL7CDCNxZimIrYg/qfTOPdAKpHeXEfAbGVvcoTJxGnU1X6bDu+ZvA==
         * trendsImg : [{"id":146,"imgpath":"http://oss-aliyun-liululu.oss-cn-shanghai.aliyuncs.com/upload/153379936857710272.jpg"},{"id":147,"imgpath":"http://oss-aliyun-liululu.oss-cn-shanghai.aliyuncs.com/upload/153379941170510870.jpg"},{"id":148,"imgpath":"http://oss-aliyun-liululu.oss-cn-shanghai.aliyuncs.com/upload/153379957718410031.jpg"},{"id":151,"imgpath":"http://oss-aliyun-liululu.oss-cn-shanghai.aliyuncs.com/upload/153380304613410415.jpg"},{"id":152,"imgpath":"http://oss-aliyun-liululu.oss-cn-shanghai.aliyuncs.com/upload/153380753977610110.jpg"},{"id":153,"imgpath":"http://oss-aliyun-liululu.oss-cn-shanghai.aliyuncs.com/upload/153380753990610656.jpg"},{"id":154,"imgpath":"http://oss-aliyun-liululu.oss-cn-shanghai.aliyuncs.com/upload/153386419099010812.jpg"},{"id":160,"imgpath":"http://oss-aliyun-liululu.oss-cn-shanghai.aliyuncs.com/upload/153388235840710637.jpg"},{"id":168,"imgpath":"http://oss-aliyun-liululu.oss-cn-shanghai.aliyuncs.com/upload/153388657502410777.jpg"},{"id":169,"imgpath":"http://oss-aliyun-liululu.oss-cn-shanghai.aliyuncs.com/upload/153388657518210096.jpg"},{"id":170,"imgpath":"http://oss-aliyun-liululu.oss-cn-shanghai.aliyuncs.com/upload/153388657532910633.jpg"},{"id":171,"imgpath":"http://oss-aliyun-liululu.oss-cn-shanghai.aliyuncs.com/upload/153388657544810248.jpg"},{"id":172,"imgpath":"http://oss-aliyun-liululu.oss-cn-shanghai.aliyuncs.com/upload/153388657559110528.jpg"},{"id":173,"imgpath":"http://oss-aliyun-liululu.oss-cn-shanghai.aliyuncs.com/upload/153388657574010248.jpg"},{"id":181,"imgpath":"http://oss-aliyun-liululu.oss-cn-shanghai.aliyuncs.com/upload/153388797131110513.jpg"},{"id":182,"imgpath":"http://oss-aliyun-liululu.oss-cn-shanghai.aliyuncs.com/upload/153388797147310045.jpg"},{"id":183,"imgpath":"http://oss-aliyun-liululu.oss-cn-shanghai.aliyuncs.com/upload/153388797162510569.jpg"},{"id":184,"imgpath":"http://oss-aliyun-liululu.oss-cn-shanghai.aliyuncs.com/upload/153388797174910832.jpg"},{"id":185,"imgpath":"http://oss-aliyun-liululu.oss-cn-shanghai.aliyuncs.com/upload/153388823703310437.jpg"},{"id":188,"imgpath":"http://oss-aliyun-liululu.oss-cn-shanghai.aliyuncs.com/upload/153388901659410891.jpg"},{"id":191,"imgpath":"http://oss-aliyun-liululu.oss-cn-shanghai.aliyuncs.com/upload/153412856730110343.jpg"}]
         * usablemoney : 1000
         * userInfostatus : 3302
         * waitactivemoney : 0
         */

        private String belongcode;
        private int channelid;
        private String coldmoney;
        private String creattime;
        private String fetchmoney;
        private int friendStatus;
        private String headimage;
        private int id;
        private String investmoney;
        private int inviterid;
        private int isf;
        private int level;
        private int loginnum;
        private String logintime;
        private String name;
        private String passwd;
        private String phone;
        private String rapnum;
        private String registermoney;
        private String remark;
        private String returnmoney;
        private int status;
        private String summoney;
        private String token;
        private String usablemoney;
        private int userInfostatus;
        private String waitactivemoney;
        private List<TrendsImgBean> trendsImg;

        public String getBelongcode() {
            return belongcode;
        }

        public void setBelongcode(String belongcode) {
            this.belongcode = belongcode;
        }

        public int getChannelid() {
            return channelid;
        }

        public void setChannelid(int channelid) {
            this.channelid = channelid;
        }

        public String getColdmoney() {
            return coldmoney;
        }

        public void setColdmoney(String coldmoney) {
            this.coldmoney = coldmoney;
        }

        public String getCreattime() {
            return creattime;
        }

        public void setCreattime(String creattime) {
            this.creattime = creattime;
        }

        public String getFetchmoney() {
            return fetchmoney;
        }

        public void setFetchmoney(String fetchmoney) {
            this.fetchmoney = fetchmoney;
        }

        public int getFriendStatus() {
            return friendStatus;
        }

        public void setFriendStatus(int friendStatus) {
            this.friendStatus = friendStatus;
        }

        public String getHeadimage() {
            return headimage;
        }

        public void setHeadimage(String headimage) {
            this.headimage = headimage;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getInvestmoney() {
            return investmoney;
        }

        public void setInvestmoney(String investmoney) {
            this.investmoney = investmoney;
        }

        public int getInviterid() {
            return inviterid;
        }

        public void setInviterid(int inviterid) {
            this.inviterid = inviterid;
        }

        public int getIsf() {
            return isf;
        }

        public void setIsf(int isf) {
            this.isf = isf;
        }

        public int getLevel() {
            return level;
        }

        public void setLevel(int level) {
            this.level = level;
        }

        public int getLoginnum() {
            return loginnum;
        }

        public void setLoginnum(int loginnum) {
            this.loginnum = loginnum;
        }

        public String getLogintime() {
            return logintime;
        }

        public void setLogintime(String logintime) {
            this.logintime = logintime;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getPasswd() {
            return passwd;
        }

        public void setPasswd(String passwd) {
            this.passwd = passwd;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getRapnum() {
            return rapnum;
        }

        public void setRapnum(String rapnum) {
            this.rapnum = rapnum;
        }

        public String getRegistermoney() {
            return registermoney;
        }

        public void setRegistermoney(String registermoney) {
            this.registermoney = registermoney;
        }

        public String getRemark() {
            return remark;
        }

        public void setRemark(String remark) {
            this.remark = remark;
        }

        public String getReturnmoney() {
            return returnmoney;
        }

        public void setReturnmoney(String returnmoney) {
            this.returnmoney = returnmoney;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public String getSummoney() {
            return summoney;
        }

        public void setSummoney(String summoney) {
            this.summoney = summoney;
        }

        public String getToken() {
            return token;
        }

        public void setToken(String token) {
            this.token = token;
        }

        public String getUsablemoney() {
            return usablemoney;
        }

        public void setUsablemoney(String usablemoney) {
            this.usablemoney = usablemoney;
        }

        public int getUserInfostatus() {
            return userInfostatus;
        }

        public void setUserInfostatus(int userInfostatus) {
            this.userInfostatus = userInfostatus;
        }

        public String getWaitactivemoney() {
            return waitactivemoney;
        }

        public void setWaitactivemoney(String waitactivemoney) {
            this.waitactivemoney = waitactivemoney;
        }

        public List<TrendsImgBean> getTrendsImg() {
            return trendsImg;
        }

        public void setTrendsImg(List<TrendsImgBean> trendsImg) {
            this.trendsImg = trendsImg;
        }

        public static class TrendsImgBean {
            /**
             * id : 146
             * imgpath : http://oss-aliyun-liululu.oss-cn-shanghai.aliyuncs.com/upload/153379936857710272.jpg
             */

            private int id;
            private String imgpath;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getImgpath() {
                return imgpath;
            }

            public void setImgpath(String imgpath) {
                this.imgpath = imgpath;
            }
        }
    }
}
