package cn.rongcloud.im.server;

import android.content.Context;
import android.text.TextUtils;

import com.vise.utils.assist.StringUtil;

import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.entity.StringEntity;
import org.apache.http.message.BasicNameValuePair;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import cn.rongcloud.im.server.network.http.HttpException;
import cn.rongcloud.im.server.network.http.RequestParams;
import cn.rongcloud.im.server.request.AgreeFriendsRequest;
import cn.rongcloud.im.server.request.FriendInvitationRequest;
import cn.rongcloud.im.server.request.LoginRequest;
import cn.rongcloud.im.server.request.PhoneContactRequest;
import cn.rongcloud.im.server.request.RecordCommunicationRequest;
import cn.rongcloud.im.server.request.RegisterRequest;
import cn.rongcloud.im.server.request.SendCodeRequest;
import cn.rongcloud.im.server.request.base.BaseRequest;
import cn.rongcloud.im.server.response.AgreeFriendsResponse;
import cn.rongcloud.im.server.response.AppVersionResponse;
import cn.rongcloud.im.server.response.BaseResponse;
import cn.rongcloud.im.server.response.ChatGroupDetailResponse;
import cn.rongcloud.im.server.response.CoinCountByUserResponse;
import cn.rongcloud.im.server.response.CoinListResponse;
import cn.rongcloud.im.server.response.CreateGroupResponse;
import cn.rongcloud.im.server.response.FriendInvitationResponse;
import cn.rongcloud.im.server.response.FriendRemarkResponse;
import cn.rongcloud.im.server.response.GetUserInfoByPhoneResponse;
import cn.rongcloud.im.server.response.GroupListResponse;
import cn.rongcloud.im.server.response.LoginResponse;
import cn.rongcloud.im.server.response.MachineUserListResponse;
import cn.rongcloud.im.server.response.PeopleNearbyResponse;
import cn.rongcloud.im.server.response.PhoneContactResponse;
import cn.rongcloud.im.server.response.RecordBehaviorResponse;
import cn.rongcloud.im.server.response.RecordCommunicationResponse;
import cn.rongcloud.im.server.response.RecordFileResponse;
import cn.rongcloud.im.server.response.RecordFinancialResponse;
import cn.rongcloud.im.server.response.RegisterResponse;
import cn.rongcloud.im.server.response.SellMachineListResponse;
import cn.rongcloud.im.server.response.SendCodeResponse;
import cn.rongcloud.im.server.response.UserInfoResponse;
import cn.rongcloud.im.server.response.UserRelationshipResponse;
import cn.rongcloud.im.server.response.friendcircle.FriendBackgroundResponse;
import cn.rongcloud.im.server.response.friendcircle.MomentsInfo;
import cn.rongcloud.im.server.utils.NLog;
import cn.rongcloud.im.server.utils.json.JsonMananger;

/**
 * Created by AMing on 16/1/14.
 * Company RongCloud
 */
@SuppressWarnings("deprecation")
public class SealAction extends NoUseAction {
    private final String CONTENT_TYPE = "application/json";
    private final String ENCODING = "utf-8";

    /**
     * 构造方法
     *
     * @param context 上下文
     */
    public SealAction(Context context) {
        super(context);
    }


    /**
     * 处理请求entity
     *
     * @param baseRequest
     * @return
     */
    private UrlEncodedFormEntity getRequestEntity(BaseRequest baseRequest) {
        UrlEncodedFormEntity entity1 = null;
        try {
            Map<String, String> mapParams = baseRequest.getMapParams();

            List<NameValuePair> list = new ArrayList<NameValuePair>();
            if (mapParams != null && !mapParams.isEmpty()) {
                for (Map.Entry<String, String> entry : mapParams.entrySet()) {
                    // 解析Map传递的参数，使用一个键值对对象BasicNameValuePair保存。
                    if (!StringUtil.isNullString(entry.getValue()))
                        list.add(new BasicNameValuePair(entry.getKey(), entry.getValue()));
                }
            }
            // 实现将请求 的参数封装封装到HttpEntity中。
            entity1 = new UrlEncodedFormEntity(list, ENCODING);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return entity1;
    }

    /**
     * 发送验证码
     *
     * @param region 国家码
     * @param phone  手机号
     * @throws HttpException
     */
    public SendCodeResponse sendCode(String region, String phone) throws HttpException {
        String url = getURL("register/get-check-code");
        String json = JsonMananger.beanToJson(new SendCodeRequest(region, phone, 1, true));
        StringEntity entity = null;
        try {
            entity = new StringEntity(json, ENCODING);
            entity.setContentType(CONTENT_TYPE);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        SendCodeResponse response = null;
        UrlEncodedFormEntity requestEntity = getRequestEntity(new SendCodeRequest(region, phone, 1, true));

//            String result = httpManager.post(mContext, url, entity, CONTENT_TYPE);
        String result = httpManager.post(mContext, url, requestEntity, null, null);
        if (!TextUtils.isEmpty(result)) {
            response = JsonMananger.jsonToBean(result, SendCodeResponse.class);
        }
        return response;
    }

    /**
     * 注册
     *
     * @param phoneNum 手机号码
     * @param password 密码
     * @param passsure 确认密码
     * @param vcode    验证码
     * @throws HttpException
     */
    public RegisterResponse register(String phoneNum, String password, String passsure, String vcode,String type) throws HttpException {
        String url = getURL("register/user-register");
        StringEntity entity = null;
        try {
            entity = new StringEntity(JsonMananger.beanToJson(new RegisterRequest(phoneNum, password, passsure, vcode,type)), ENCODING);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        RegisterResponse response = null;
        UrlEncodedFormEntity requestEntity = getRequestEntity(new RegisterRequest(phoneNum, password, passsure, vcode,type));
        String result = httpManager.post(mContext, url, requestEntity, null, null);
        if (!TextUtils.isEmpty(result)) {
            NLog.e("RegisterResponse", result);
            response = jsonToBean(result, RegisterResponse.class);
        }
        return response;
    }

    /**
     * 登录: 登录成功后，会设置 Cookie，后续接口调用需要登录的权限都依赖于 Cookie。
     *
     * @param region   国家码
     * @param phone    手机号
     * @param password 密码
     * @throws HttpException
     */
    public LoginResponse login(String region, String phone, String password) throws HttpException {
        String uri = getURL("login/user-login");
        String json = JsonMananger.beanToJson(new LoginRequest(region, phone, password, ""));
        StringEntity entity = null;
        try {
            entity = new StringEntity(json, ENCODING);
            entity.setContentType(CONTENT_TYPE);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        UrlEncodedFormEntity requestEntity = getRequestEntity(new LoginRequest(region, phone, password, "/user/index"));
        String result = httpManager.post(mContext, uri, requestEntity, null, null);
        LoginResponse response = null;
        if (!TextUtils.isEmpty(result)) {
            NLog.e("LoginResponse", result);
            response = JsonMananger.jsonToBean(result, LoginResponse.class);
        }
        return response;
    }

    /**
     * 通过手机号查询用户信息
     *
     * @param region 国家码
     * @param phone  手机号
     * @throws HttpException
     */
    public GetUserInfoByPhoneResponse getUserInfoFromPhone(String region, String phone) throws HttpException {
        String url = getURL("user/findUser");
        RequestParams params = new RequestParams();
        params.put("con", phone);
        String result = httpManager.get(url, params, mHeadToken);
        GetUserInfoByPhoneResponse response = null;
        if (!TextUtils.isEmpty(result)) {
            response = jsonToBean(result, GetUserInfoByPhoneResponse.class);
        }
        return response;
    }


    /**
     * 发送好友邀请
     *
     * @param userid           好友id
     * @param addFriendMessage 添加好友的信息
     * @throws HttpException
     */
    public FriendInvitationResponse sendFriendInvitation(String userid, String addFriendMessage) throws HttpException {
        String url = getURL("friend/addFriend");
        String json = JsonMananger.beanToJson(new FriendInvitationRequest(userid, addFriendMessage));
        StringEntity entity = null;
        try {
            entity = new StringEntity(json, ENCODING);
            entity.setContentType(CONTENT_TYPE);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        UrlEncodedFormEntity requestEntity = getRequestEntity(new FriendInvitationRequest(userid, addFriendMessage));
        String result = httpManager.post(mContext, url, requestEntity, mHeadToken, null);
        FriendInvitationResponse response = null;
        if (!TextUtils.isEmpty(result)) {
            response = jsonToBean(result, FriendInvitationResponse.class);
        }
        return response;
    }


    /**
     * 查看好友请求列表接口 （分页）-----------获取发生过用户关系的列表
     *
     * @throws HttpException
     */
    public UserRelationshipResponse getAllUserRelationship() throws HttpException {
        String url = getURL("friend/listFriendRequestByPage");
        RequestParams params = new RequestParams();
        params.put("pageNum", "1");
        String result = httpManager.get(url, params, mHeadToken);
        UserRelationshipResponse response = null;
        if (!TextUtils.isEmpty(result)) {
            response = jsonToBean(result, UserRelationshipResponse.class);
        }
        return response;
    }

    /**
     * 好友通讯录列表
     *
     * @throws HttpException
     */
    public UserRelationshipResponse getAllUserFriend() throws HttpException {
        mHeadToken = getToken(mContext);
        String url = getURL("friend/listFrient");
        String result = httpManager.get(url, mHeadToken);
        UserRelationshipResponse response = null;
        if (!TextUtils.isEmpty(result)) {
            response = jsonToBean(result, UserRelationshipResponse.class);
        }
        return response;
    }

    /**
     * 同意添加好友请求接口------------同意对方好友邀请
     *
     * @param friendId id=查看好友请求列表接口 （分页）返回的requestId
     * @throws HttpException
     */
    public AgreeFriendsResponse agreeFriends(String friendId) throws HttpException {
        String url = getURL("friend/addFriendSuccess");
        String json = JsonMananger.beanToJson(new AgreeFriendsRequest(friendId));
        StringEntity entity = null;
        try {
            entity = new StringEntity(json, ENCODING);
            entity.setContentType(CONTENT_TYPE);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        UrlEncodedFormEntity requestEntity = getRequestEntity(new AgreeFriendsRequest(friendId));
        String result = httpManager.post(mContext, url, requestEntity, mHeadToken, null);
        AgreeFriendsResponse response = null;
        if (!TextUtils.isEmpty(result)) {
            response = jsonToBean(result, AgreeFriendsResponse.class);
        }
        return response;
    }

    /**
     * 查询好友备注
     * <p>
     * 请求类型：post/get
     *
     * @param tuserid 要查询好友的id
     * @throws HttpException
     * @return响应主体属性： 返回你对该用户设置的备注，该用户的头像和id
     */
    public FriendRemarkResponse selectFriendRemark(String tuserid) throws HttpException {
        String url = getURL("friend/selectFriendRemark");
        RequestParams params = new RequestParams();
        params.put("tuserid", tuserid);
        String result = httpManager.get(url, params, mHeadToken);
        FriendRemarkResponse response = null;
        if (!TextUtils.isEmpty(result)) {
            response = jsonToBean(result, FriendRemarkResponse.class);
        }
        return response;
    }

    /**
     * 设置好友备注
     * <p>
     * 请求类型：post/get
     *
     * @param tuserid      设置备注好友的id
     * @param friendremark 备注
     * @throws HttpException
     * @return响应主体属性：
     */
    public BaseResponse setFriendRemark(String tuserid, String friendremark) throws HttpException {
        String url = getURL("friend/setFriendRemark");
        RequestParams params = new RequestParams();
        params.put("tuserid", tuserid);
        params.put("friendremark", friendremark);
        String result = httpManager.get(url, params, mHeadToken);
        BaseResponse response = null;
        if (!TextUtils.isEmpty(result)) {
            response = jsonToBean(result, BaseResponse.class);
        }
        return response;
    }

    /**
     * 手机联系人（查询通讯录好友信息）
     * <p>
     * 请求类型：post/get
     * 响应主体属性说明：根据isf判断是否为好友关系，如果isf有值就说明他们是好友关系
     *
     * @param phone phone要查询的手机号（支持多个查询，分割）
     * @return
     * @throws HttpException
     */
    public PhoneContactResponse linkman(String phone) throws HttpException {
        String url = getURL("user/linkman");
//        RequestParams params = new RequestParams();
//        params.put("phone", phone);
//        String result = httpManager.get(url, params, mHeadToken);
//        BaseResponse response = null;
//        if (!TextUtils.isEmpty(result)) {
//            response = jsonToBean(result, BaseResponse.class);
//        }

        PhoneContactResponse response = null;
        UrlEncodedFormEntity requestEntity = getRequestEntity(new PhoneContactRequest(phone));
        String result = httpManager.post(mContext, url, requestEntity, mHeadToken, null);
        if (!TextUtils.isEmpty(result)) {
            NLog.e("RegisterResponse", result);
            response = jsonToBean(result, PhoneContactResponse.class);
        }
        return response;
    }


    /**
     * 手机联系人（短信邀请好友注册）
     * 请求类型：post/get
     *
     * @param phone phone邀请用户手机号（不支持多个）
     * @return
     * @throws HttpException
     */
    public BaseResponse sendInviteUser(String phone) throws HttpException {
        String url = getURL("user/inviteUser");
        RequestParams params = new RequestParams();
        params.put("phone", phone);
        String result = httpManager.get(url, params, mHeadToken);
        BaseResponse response = null;
        if (!TextUtils.isEmpty(result)) {
            response = jsonToBean(result, BaseResponse.class);
        }
        return response;
    }

    /*##########################朋友圈相关##########################*/

    /**
     * 朋友圈动态（分页）
     * 请求类型：post/get
     *
     * @param pageNum  pageNum当前页数（默认为1）
     * @param pageSize pageSize每页显示条数（默认5）
     * @throws HttpException
     */
    public MomentsInfo getListFriendByPage(int pageNum, int pageSize) throws HttpException {
        String url = getURL("trends/listFriendByPage");
        if (pageNum <= 0)
            pageNum = 1;
        RequestParams params = new RequestParams();
        params.put("pageNum", pageNum + "");
        params.put("pageSize", 20 + "");
        String result = httpManager.get(url, params, mHeadToken);
        MomentsInfo response = null;
        if (!TextUtils.isEmpty(result)) {
            response = jsonToBean(result, MomentsInfo.class);
        }
        return response;
    }

    /**
     * 朋友圈动态评论
     * 请求类型：post/get
     *
     * @param trendsid  动态详情id
     * @param commentid 评论的id(可为空)
     * @param content   评论内容
     * @throws HttpException
     */
    public MomentsInfo addComment(String trendsid, String commentid, String content) throws HttpException {
        String url = getURL("trends/addComment");
        RequestParams params = new RequestParams();
        params.put("trendsid", trendsid);
        params.put("commentid", commentid);
        params.put("content", content);
        String result = httpManager.get(url, params, mHeadToken);
        MomentsInfo response = null;
        if (!TextUtils.isEmpty(result)) {
            response = jsonToBean(result, MomentsInfo.class);
        }
        return response;
    }

    /**
     * 朋友圈动态评论删除
     * 请求类型：post/get
     *
     * @param ids 为整型数组，可传多个，分割     ids=1,2,3
     * @throws HttpException
     */
    public MomentsInfo deleteComment(ArrayList<String> ids) throws HttpException {
        String url = getURL("trends/deleteComment");
        String listId = getIds(ids);
        RequestParams params = new RequestParams();
        params.put("ids", listId);
        String result = httpManager.get(url, params, mHeadToken);
        MomentsInfo response = null;
        if (!TextUtils.isEmpty(result)) {
            response = jsonToBean(result, MomentsInfo.class);
        }
        return response;
    }

    /**
     * 朋友圈动态点赞/取消点赞
     * 请求类型：post/get
     *
     * @param trendsid 动态详情id
     * @throws HttpException
     */
    public BaseResponse trendsCommentLike(String trendsid) throws HttpException {
        String url = getURL("trends/trendsCommentLike");
        RequestParams params = new RequestParams();
        params.put("trendsid", trendsid);
        String result = httpManager.get(url, params, mHeadToken);
        BaseResponse response = null;
        if (!TextUtils.isEmpty(result)) {
            response = jsonToBean(result, BaseResponse.class);
        }
        return response;
    }

    /**
     * 朋友圈发布动态（图文格式）content=123&imgId=1,2,3
     * 请求类型：post/get
     *
     * @param content 动态的文本内容
     * @param imgId   上传图片返回的id（可传多个，分割）
     * @throws HttpException
     */
    public MomentsInfo addTrends(String content, String imgId) throws HttpException {
        String url = getURL("trends/addTrends");
        RequestParams params = new RequestParams();
        params.put("content", content);
        params.put("imgId", imgId);
        String result = httpManager.get(url, params, mHeadToken);
        MomentsInfo response = null;
        if (!TextUtils.isEmpty(result)) {
            response = jsonToBean(result, MomentsInfo.class);
        }
        return response;
    }

    /**
     * 朋友圈删除动态
     * 请求类型：post/get
     *
     * @param ids 为整型数组，可传多个，分割
     * @throws HttpException
     */
    public MomentsInfo deleteTrends(String ids) throws HttpException {
        String url = getURL("trends/deleteTrends");
        RequestParams params = new RequestParams();
        params.put("ids", ids);
        String result = httpManager.get(url, params, mHeadToken);
        MomentsInfo response = null;
        if (!TextUtils.isEmpty(result)) {
            response = jsonToBean(result, MomentsInfo.class);
        }
        return response;
    }

    /**
     * 删除图片
     * 请求类型：post/get
     *
     * @param ids ids为整型数组，可传多个，分割
     * @throws HttpException
     */
    public MomentsInfo deleteImg(String ids) throws HttpException {
        String url = getURL("trends/deleteImg");
        RequestParams params = new RequestParams();
        params.put("ids", ids);
        String result = httpManager.get(url, params, mHeadToken);
        MomentsInfo response = null;
        if (!TextUtils.isEmpty(result)) {
            response = jsonToBean(result, MomentsInfo.class);
        }
        return response;
    }

    /**
     * 朋友圈背景图查询
     * 请求类型：post/get
     *
     * @param userId 要查询用户的id
     * @throws HttpException
     */
    public FriendBackgroundResponse friendTrendsInfo(String userId) throws HttpException {
        String url = getURL("trends/friendTrendsInfo");
        RequestParams params = new RequestParams();
        params.put("userId", userId);
        String result = httpManager.get(url, params, mHeadToken);
        FriendBackgroundResponse response = null;
        if (!TextUtils.isEmpty(result)) {
            response = jsonToBean(result, FriendBackgroundResponse.class);
        }
        return response;
    }

    /**
     * 朋友圈背景图--上传图片
     * 请求类型：post/get
     *
     * @param file 选择的图片        参数描述：   file为表单上传（不支持多张上传）
     * @throws HttpException
     */
    public MomentsInfo addFriendTrendsInfo(String file) throws HttpException {
        String url = getURL("trends/addFriendTrendsInfo");
        RequestParams params = new RequestParams();
        params.put("file", file);
        String result = httpManager.get(url, params, mHeadToken);
        MomentsInfo response = null;
        if (!TextUtils.isEmpty(result)) {
            response = jsonToBean(result, MomentsInfo.class);
        }
        return response;
    }

        /*##########################矿机##########################*/

    /**
     * 用户矿机列表（分页）
     * 请求类型：post/get
     * 响应主体属性描述：coinMinute矿机工作时间（分钟）。 计算剩余多少分钟 可根据manhour（小时）* 60 -  coinMinute
     *
     * @param pageNum pageNum当前页数（默认为1），pageSize每页显示条数（默认5）
     * @throws HttpException
     */
    public MachineUserListResponse listMachineUserByPage(String pageNum, String pageSize) throws HttpException {
        String url = getURL("machine/listMachineUserByPage");
        RequestParams params = new RequestParams();
        params.put("pageNum", pageNum);
        params.put("pageSize", pageSize);
        String result = httpManager.get(url, params, mHeadToken);
        MachineUserListResponse response = null;
        if (!TextUtils.isEmpty(result)) {
            response = jsonToBean(result, MachineUserListResponse.class);
        }
        return response;
    }

    /**
     * 出售的矿机列表
     * 请求类型：post/get
     *
     * @param pageNum pageNum当前页数（默认为1），pageSize每页显示条数（默认5）
     * @throws HttpException
     */
    public SellMachineListResponse sellMachineList(String pageNum, String pageSize) throws HttpException {
        String url = getURL("machine/listByPage");
        RequestParams params = new RequestParams();
        params.put("pageNum", pageNum);
        params.put("pageSize", pageSize);
        String result = httpManager.get(url, params, mHeadToken);
        SellMachineListResponse response = null;
        if (!TextUtils.isEmpty(result)) {
            response = jsonToBean(result, SellMachineListResponse.class);
        }
        return response;
    }

    /**
     * 购买矿机
     * 请求类型：post/get
     *
     * @param machineid 矿机id
     * @throws HttpException
     */
    public SellMachineListResponse machineBuy(String machineid) throws HttpException {
        String url = getURL("machine/buy");
        RequestParams params = new RequestParams();
        params.put("machineid", machineid);
        String result = httpManager.get(url, params, mHeadToken);
        SellMachineListResponse response = null;
        if (!TextUtils.isEmpty(result)) {
            response = jsonToBean(result, SellMachineListResponse.class);
        }
        return response;
    }

    /**
     * 开启矿机
     * 请求类型：post/get
     *
     * @param id 用户矿机列表muid
     * @throws HttpException
     */
    public SellMachineListResponse machineOpen(String id) throws HttpException {
        String url = getURL("machine/open");
        RequestParams params = new RequestParams();
        params.put("id", id);
        String result = httpManager.get(url, params, mHeadToken);
        SellMachineListResponse response = null;
        if (!TextUtils.isEmpty(result)) {
            response = jsonToBean(result, SellMachineListResponse.class);
        }
        return response;
    }

    /**
     * 关闭矿机
     * 请求类型：post/get
     *
     * @param id 用户矿机列表muid
     * @throws HttpException
     */
    public SellMachineListResponse machineClose(String id) throws HttpException {
        String url = getURL("machine/close");
        RequestParams params = new RequestParams();
        params.put("id", id);
        String result = httpManager.get(url, params, mHeadToken);
        SellMachineListResponse response = null;
        if (!TextUtils.isEmpty(result)) {
            response = jsonToBean(result, SellMachineListResponse.class);
        }
        return response;
    }




    /*##########################用户##########################*/

    /**
     * 当前登录用户详情
     * 请求类型：post/get
     *
     * @throws HttpException
     */
    public UserInfoResponse getUser() throws HttpException {
        String url = getURL("user/getUser");
        RequestParams params = new RequestParams();
        String result = httpManager.get(url, params, mHeadToken);
        UserInfoResponse response = null;
        if (!TextUtils.isEmpty(result)) {
            response = jsonToBean(result, UserInfoResponse.class);
        }
        return response;
    }



    /*##########################交易##########################*/

    /**
     * 查询货币列表
     * 请求类型：post/get
     *
     * @throws HttpException
     */
    public CoinListResponse coinList() throws HttpException {
        String url = getURL("coin/list");
        RequestParams params = new RequestParams();
        String result = httpManager.get(url, params, mHeadToken);
        CoinListResponse response = null;
        if (!TextUtils.isEmpty(result)) {
            response = jsonToBean(result, CoinListResponse.class);
        }
        return response;
    }

    /**
     * 查询用户所持有货币数量
     * 请求类型：post/get
     *
     * @param coinid=货币id
     * @throws HttpException
     */
    public CoinCountByUserResponse getCoinCountByUser(String coinid) throws HttpException {
        String url = getURL("coin/getCoinCountByUser");
        RequestParams params = new RequestParams();
        params.put("coinid", coinid);
        String result = httpManager.get(url, params, mHeadToken);
        CoinCountByUserResponse response = null;
        if (!TextUtils.isEmpty(result)) {
            response = jsonToBean(result, CoinCountByUserResponse.class);
        }
        return response;
    }


    /*##########################群聊##########################*/

    /**
     * 创建群组
     *
     * @param name      群组名称
     * @param code      群号（融云code）
     * @param memberIds 群组成员id      uid=群成员id(数组：例 12,34,45)
     * @throws HttpException
     */
    public CreateGroupResponse createChatGroup(String name, String code, List<String> memberIds) throws HttpException {
        String url = getURL("chatGroup/add");
        RequestParams params = new RequestParams();
        params.put("name", name);
        params.put("code", code);
        params.put("uid", getIds(memberIds));
        String result = httpManager.get(url, params, mHeadToken);
        CreateGroupResponse response = null;
        if (!TextUtils.isEmpty(result)) {
            response = jsonToBean(result, CreateGroupResponse.class);
        }
        return response;
    }

    /**
     * 群组详情
     * 请求类型：post/get
     *
     * @param groupCode code=群组code&limit=用户个数（默认50）
     * @throws HttpException
     */
    public ChatGroupDetailResponse chatGroupDetail(String groupCode) throws HttpException {
        String url = getURL("chatGroup/detail");
        RequestParams params = new RequestParams();
        params.put("code", groupCode);
        String result = httpManager.get(url, params, mHeadToken);
        ChatGroupDetailResponse response = null;
        if (!TextUtils.isEmpty(result)) {
            response = jsonToBean(result, ChatGroupDetailResponse.class);
        }
        return response;
    }

    /**
     * 群组全部成员
     * 请求类型：post/get
     *
     * @param id 群组id
     * @throws HttpException
     */
    public BaseResponse chatGroupListUser(String id) throws HttpException {
        String url = getURL("chatGroup/listUser");
        RequestParams params = new RequestParams();
        params.put("id", id);
        String result = httpManager.get(url, params, mHeadToken);
        BaseResponse response = null;
        if (!TextUtils.isEmpty(result)) {
            response = jsonToBean(result, BaseResponse.class);
        }
        return response;
    }

    /**
     * 修改群名称
     * 请求类型：post/get
     *
     * @param id   群组id
     * @param name 名称
     * @throws HttpException
     */
    public BaseResponse chatGroupEditGroupName(String id, String name) throws HttpException {
        String url = getURL("chatGroup/editGroupName");
        RequestParams params = new RequestParams();
        params.put("id", id);
        params.put("name", name);
        String result = httpManager.get(url, params, mHeadToken);
        BaseResponse response = null;
        if (!TextUtils.isEmpty(result)) {
            response = jsonToBean(result, BaseResponse.class);
        }
        return response;
    }

    /**
     * 加入群聊
     * 请求类型：post/get        groupid必填，uid选填（只创建群）
     *
     * @param groupid 群组id(后台id)
     * @param uid     群成员id(数组：例 12,34,45)
     * @return
     * @throws HttpException
     */
    public BaseResponse chatGroupAddUser(String groupid, List<String> uid) throws HttpException {
        String url = getURL("chatGroup/addUser");
        RequestParams params = new RequestParams();
        params.put("groupid", groupid);
        params.put("uid", getIds(uid));
        String result = httpManager.get(url, params, mHeadToken);
        BaseResponse response = null;
        if (!TextUtils.isEmpty(result)) {
            response = jsonToBean(result, BaseResponse.class);
        }
        return response;
    }

    /**
     * 踢出群聊
     * 请求类型：post/get
     *
     * @param groupid  群组id(后台id)
     * @param userid   踢出的用户id
     * @param prohibit prohibit=8101   （选填）踢出后不允许咋再进群
     * @return
     * @throws HttpException
     */
    public BaseResponse chatGroupKickUser(String groupid, List<String> userid, String prohibit) throws HttpException {
        String url = getURL("chatGroup/kickUser");
        RequestParams params = new RequestParams();
        params.put("groupid", groupid);
        params.put("userid", getIds(userid));
        params.put("prohibit", prohibit);
        String result = httpManager.get(url, params, mHeadToken);
        BaseResponse response = null;
        if (!TextUtils.isEmpty(result)) {
            response = jsonToBean(result, BaseResponse.class);
        }
        return response;
    }

    /**
     * 退出群聊
     * 请求类型：post/get
     *
     * @param id 群组id(后台id)
     * @return
     * @throws HttpException
     */
    public BaseResponse chatGroupQuit(String id) throws HttpException {
        String url = getURL("chatGroup/quit");
        RequestParams params = new RequestParams();
        params.put("id", id);
        String result = httpManager.get(url, params, mHeadToken);
        BaseResponse response = null;
        if (!TextUtils.isEmpty(result)) {
            response = jsonToBean(result, BaseResponse.class);
        }
        return response;
    }

    /**
     * 查询加入的群组列表
     * 请求类型：post/get
     *
     * @param pageNum  当前页数（默认为1）
     * @param pageSize 每页显示条数（默认5）
     * @return
     * @throws HttpException
     */
    public GroupListResponse chatGroupListByPage(int pageNum, int pageSize) throws HttpException {
        String url = getURL("chatGroup/listByPage");
        RequestParams params = new RequestParams();
        params.put("pageNum", pageNum + "");
        params.put("pageSize", pageSize + "");
        String result = httpManager.get(url, params, mHeadToken);
        GroupListResponse response = null;
        if (!TextUtils.isEmpty(result)) {
            response = jsonToBean(result, GroupListResponse.class);
        }
        return response;
    }

    /*##########################社区##########################*/

    /**
     * 社区（查询我邀请注册用户列表 和 邀请我的用户）
     * <p>
     * 请求类型：post/get
     * 响应主体属性：{resultCode:附录1，msg：附录1， data：{ children : [{邀请的注册用户}]，parent：{邀请我的用户} } }
     * 响应主体属性说明：childrend为当前用户邀请的用户列表，递归循环（后台已经组装好，循环出来就行）
     *
     * @return
     * @throws HttpException
     */
    public BaseResponse listCommunityUser() throws HttpException {
        String url = getURL("user/listCommunityUser");
        RequestParams params = new RequestParams();
        String result = httpManager.get(url, params, mHeadToken);
        BaseResponse response = null;
        if (!TextUtils.isEmpty(result)) {
            response = jsonToBean(result, BaseResponse.class);
        }
        return response;
    }

    /**
     * 社区动态 （分页）（只查询社区用户动态，（评论，点赞与朋友圈接口一样））
     * <p>
     * 请求类型：post/get
     * 响应主体属性：{resultCode:附录1，msg：附录1，count：总条数， data：{ 附录，trendsImgCount:图片总数，
     * trendsCommentCount:评论数数量，trendsCommentLikeCount:点赞数量，trendsImg:[{图片列表}]，trendsComment:
     * [{评论列表}]，trendsCommentLike:[{点赞列表}]  }}
     *
     * @param pageNum  当前页数（默认为1）
     * @param pageSize 每页显示条数（默认5）
     * @return
     * @throws HttpException
     */
    public MomentsInfo listCommunityByPage(int pageNum, int pageSize) throws HttpException {
        String url = getURL("trends/listCommunityByPage");
        RequestParams params = new RequestParams();
        params.put("pageNum", pageNum + "");
        params.put("pageSize", pageSize + "");
        String result = httpManager.get(url, params, mHeadToken);
        MomentsInfo response = null;
        if (!TextUtils.isEmpty(result)) {
            response = jsonToBean(result, MomentsInfo.class);
        }
        return response;
    }

    /*##########################版本更新##########################*/

    /**
     * 获取最新app版本
     * 请求类型：post/get
     *
     * @param appType （ios，android）
     * @return
     * @throws HttpException
     */
    public AppVersionResponse appVersion(String appType) throws HttpException {
        String url = getURL("app/version");
        RequestParams params = new RequestParams();
        params.put("appType", appType);
        String result = httpManager.get(url, params, mHeadToken);
        AppVersionResponse response = null;
        if (!TextUtils.isEmpty(result)) {
            response = jsonToBean(result, AppVersionResponse.class);
        }
        return response;
    }

    /*##########################附近的人##########################*/

    /**
     * 开启定位
     *
     * @param lat lat经度
     * @param lon lon=纬度
     * @return
     * @throws HttpException
     */
    public BaseResponse userLocationOpen(String lat, String lon) throws HttpException {
        String url = getURL("userLocation/open");
        RequestParams params = new RequestParams();
        params.put("lat", lat);
        params.put("lon", lon);
        String result = httpManager.get(url, params, mHeadToken);
        BaseResponse response = null;
        if (!TextUtils.isEmpty(result)) {
            response = jsonToBean(result, BaseResponse.class);
        }
        return response;
    }

    /**
     * 清除定位
     *
     * @return
     * @throws HttpException
     */
    public BaseResponse userLocationClose() throws HttpException {
        String url = getURL("userLocation/close");
        RequestParams params = new RequestParams();
        String result = httpManager.get(url, params, mHeadToken);
        BaseResponse response = null;
        if (!TextUtils.isEmpty(result)) {
            response = jsonToBean(result, BaseResponse.class);
        }
        return response;
    }

    /**
     * 附近的人列表（分页）
     *
     * @param pageNum  当前页数（默认为1）
     * @param pageSize 每页显示条数（默认5）
     * @return
     * @throws HttpException
     */
    public PeopleNearbyResponse userLocationListByPage(int pageNum, int pageSize) throws HttpException {
        String url = getURL("userLocation/listByPage");
        RequestParams params = new RequestParams();
        params.put("pageNum", pageNum + "");
        params.put("pageSize", pageSize + "");
        String result = httpManager.get(url, params, mHeadToken);
        PeopleNearbyResponse response = null;
        if (!TextUtils.isEmpty(result)) {
            response = jsonToBean(result, PeopleNearbyResponse.class);
        }
        return response;
    }

    /*##########################重要文件记录##########################*/

    /**
     * 重要文件列表（分页）
     *
     * @param pageNum  当前页数（默认为1）
     * @param pageSize 每页显示条数（默认5）
     * @return
     * @throws HttpException
     */
    public RecordFileResponse fileListByPage(int pageNum, int pageSize) throws HttpException {
        String url = getURL("file/listByPage");
        RequestParams params = new RequestParams();
        params.put("pageNum", pageNum + "");
        params.put("pageSize", pageSize + "");
        String result = httpManager.get(url, params, mHeadToken);
        RecordFileResponse response = null;
        if (!TextUtils.isEmpty(result)) {
            response = jsonToBean(result, RecordFileResponse.class);
        }
        return response;
    }

    /**
     * 重要文件删除
     *
     * @param ids 可传多个，分割
     * @return
     * @throws HttpException
     */
    public BaseResponse fileDelete(String ids) throws HttpException {
        String url = getURL("file/delete");
        RequestParams params = new RequestParams();
        params.put("ids", ids);
        String result = httpManager.get(url, params, mHeadToken);
        BaseResponse response = null;
        if (!TextUtils.isEmpty(result)) {
            response = jsonToBean(result, BaseResponse.class);
        }
        return response;
    }

    /**
     * 新增、修改重要文件
     *
     * @param id 如上（修改需传数据id）
     * @return
     * @throws HttpException
     */
    public PeopleNearbyResponse fileEdit(String id) throws HttpException {
        String url = getURL("file/edit");
        RequestParams params = new RequestParams();
        params.put("id", id);
        String result = httpManager.get(url, params, mHeadToken);
        PeopleNearbyResponse response = null;
        if (!TextUtils.isEmpty(result)) {
            response = jsonToBean(result, PeopleNearbyResponse.class);
        }
        return response;
    }

    /*##########################交易记录##########################*/

    /**
     * 交易记录删除
     *
     * @param ids 可传多个，分割
     * @return
     * @throws HttpException
     */
    public PeopleNearbyResponse userDealDelete(String ids) throws HttpException {
        String url = getURL("userDeal/delete");
        RequestParams params = new RequestParams();
        params.put("ids", ids);
        String result = httpManager.get(url, params, mHeadToken);
        PeopleNearbyResponse response = null;
        if (!TextUtils.isEmpty(result)) {
            response = jsonToBean(result, PeopleNearbyResponse.class);
        }
        return response;
    }

    /**
     * 新增、修改交易记录
     *
     * @param id 如上（修改需传数据id）
     * @return
     * @throws HttpException
     */
    public PeopleNearbyResponse userDealEdit(String id) throws HttpException {
        String url = getURL("userDeal/edit");
        RequestParams params = new RequestParams();
        params.put("id", id);
        String result = httpManager.get(url, params, mHeadToken);
        PeopleNearbyResponse response = null;
        if (!TextUtils.isEmpty(result)) {
            response = jsonToBean(result, PeopleNearbyResponse.class);
        }
        return response;
    }

    /*##########################行为记录##########################*/

    /**
     * 行为记录列表（分页）
     *
     * @param pageNum  当前页数（默认为1）
     * @param pageSize 每页显示条数（默认5）
     * @return
     * @throws HttpException
     */
    public RecordBehaviorResponse recordActionListByPage(int pageNum, int pageSize) throws HttpException {
        String url = getURL("recordAction/listByPage");
        RequestParams params = new RequestParams();
        params.put("pageNum", pageNum + "");
        params.put("pageSize", pageSize + "");
        String result = httpManager.get(url, params, mHeadToken);
        RecordBehaviorResponse response = null;
        if (!TextUtils.isEmpty(result)) {
            response = jsonToBean(result, RecordBehaviorResponse.class);
        }
        return response;
    }

    /**
     * 行为记录删除
     *
     * @param ids 可传多个，分割
     * @return
     * @throws HttpException
     */
    public BaseResponse recordActionDelete(String ids) throws HttpException {
        String url = getURL("recordAction/delete");
        RequestParams params = new RequestParams();
        params.put("ids", ids);
        String result = httpManager.get(url, params, mHeadToken);
        BaseResponse response = null;
        if (!TextUtils.isEmpty(result)) {
            response = jsonToBean(result, BaseResponse.class);
        }
        return response;
    }

    /**
     * 新增、修改行为记录
     *
     * @param id        如上（修改需传数据id）
     * @param content
     * @param timestart
     * @param timeend
     * @param remark
     * @return
     * @throws HttpException
     */
    public BaseResponse recordActionEdit(String id, String type, String content, String timestart, String timeend, String remark) throws HttpException {
        String url = getURL("recordAction/edit");
        RequestParams params = new RequestParams();
        if (!TextUtils.isEmpty(id)) {
            params.put("id", id);
        } else {
            params.put("type", type);
            params.put("content", content);
            params.put("timestart", timestart);
            params.put("timeend", timeend);
            params.put("remark", remark);
        }

        String result = httpManager.get(url, params, mHeadToken);
        BaseResponse response = null;
        if (!TextUtils.isEmpty(result)) {
            response = jsonToBean(result, BaseResponse.class);
        }
        return response;
    }

    /*##########################交流列表##########################*/

    /**
     * 交流列表（分页）
     *
     * @param pageNum  当前页数（默认为1）
     * @param pageSize 每页显示条数（默认5）
     * @return
     * @throws HttpException
     */
    public RecordCommunicationResponse recordExchangeListByPage(int pageNum, int pageSize) throws HttpException {
        String url = getURL("recordExchange/listByPage");
        RequestParams params = new RequestParams();
        params.put("pageNum", pageNum + "");
        params.put("pageSize", pageSize + "");
        String result = httpManager.get(url, params, mHeadToken);
        RecordCommunicationResponse response = null;
        if (!TextUtils.isEmpty(result)) {
            response = jsonToBean(result, RecordCommunicationResponse.class);
        }
        return response;
    }

    /**
     * 交流记录删除
     *
     * @param ids 可传多个，分割
     * @return
     * @throws HttpException
     */
    public BaseResponse recordExchangeDelete(String ids) throws HttpException {
        String url = getURL("recordExchange/delete");
        RequestParams params = new RequestParams();
        params.put("ids", ids);
        String result = httpManager.get(url, params, mHeadToken);
        BaseResponse response = null;
        if (!TextUtils.isEmpty(result)) {
            response = jsonToBean(result, BaseResponse.class);
        }
        return response;
    }

    /**
     * 新增、修改交流记录
     *
     * @param id 如上（修改需传数据id）
     * @return
     * @throws HttpException
     */
    public PeopleNearbyResponse recordExchangeEdit(String id, String type, String fuserid, String tuserid, String content) throws HttpException {
        String url = getURL("recordExchange/edit");
        if (TextUtils.isEmpty(id))
            id = "";
        UrlEncodedFormEntity requestEntity = getRequestEntity(new RecordCommunicationRequest(id, type, fuserid, tuserid, content));
        String result = httpManager.post(mContext, url, requestEntity, mHeadToken, null);
        PeopleNearbyResponse response = null;
        if (!TextUtils.isEmpty(result)) {
            response = jsonToBean(result, PeopleNearbyResponse.class);
        }
        return response;
    }

    /*##########################财务记录##########################*/

    /**
     * 财务记录（分页）
     *
     * @param pageNum  当前页数（默认为1）
     * @param pageSize 每页显示条数（默认5）
     * @return
     * @throws HttpException
     */
    public RecordFinancialResponse recordFinanceListByPage(int pageNum, int pageSize) throws HttpException {
        String url = getURL("recordFinance/listByPage");
        RequestParams params = new RequestParams();
        params.put("pageNum", pageNum + "");
        params.put("pageSize", pageSize + "");
        String result = httpManager.get(url, params, mHeadToken);
        RecordFinancialResponse response = null;
        if (!TextUtils.isEmpty(result)) {
            response = jsonToBean(result, RecordFinancialResponse.class);
        }
        return response;
    }

    /**
     * 财务记录删除
     *
     * @param ids 可传多个，分割
     * @return
     * @throws HttpException
     */
    public BaseResponse recordFinanceDelete(String ids) throws HttpException {
        String url = getURL("recordFinance/delete");
        RequestParams params = new RequestParams();
        params.put("ids", ids);
        String result = httpManager.get(url, params, mHeadToken);
        BaseResponse response = null;
        if (!TextUtils.isEmpty(result)) {
            response = jsonToBean(result, BaseResponse.class);
        }
        return response;
    }

    /**
     * 新增、修改财务记录
     *
     * @param id 如上（修改需传数据id）
     * @return
     * @throws HttpException
     */
    public BaseResponse recordFinanceEdit(String id, String type, String num, String content) throws HttpException {
        String url = getURL("recordFinance/edit");
        RequestParams params = new RequestParams();
        if (!TextUtils.isEmpty(id)) {
            params.put("id", id);
        } else {
            params.put("type", type);
            params.put("num", num);
            params.put("content", content);
        }
        String result = httpManager.get(url, params, mHeadToken);
        BaseResponse response = null;
        if (!TextUtils.isEmpty(result)) {
            response = jsonToBean(result, BaseResponse.class);
        }
        return response;
    }
}
