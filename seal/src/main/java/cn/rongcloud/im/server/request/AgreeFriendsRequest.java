package cn.rongcloud.im.server.request;


import cn.rongcloud.im.server.request.base.BaseRequest;

/**
 * Created by AMing on 16/1/8.
 * Company RongCloud
 */
public class AgreeFriendsRequest extends BaseRequest{

    private String id;

    public AgreeFriendsRequest(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
