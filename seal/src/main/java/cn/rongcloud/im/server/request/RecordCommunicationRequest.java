package cn.rongcloud.im.server.request;


import cn.rongcloud.im.server.request.base.BaseRequest;

/**
 *
 */
public class RecordCommunicationRequest extends BaseRequest{

    private String id;
    private String type;
    private String fuserid;
    private String tuserid;
    private String content;

    public RecordCommunicationRequest(String id, String type, String fuserid, String tuserid, String content) {
        this.id = id;
        this.type = type;
        this.fuserid = fuserid;
        this.tuserid = tuserid;
        this.content = content;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getFuserid() {
        return fuserid;
    }

    public void setFuserid(String fuserid) {
        this.fuserid = fuserid;
    }

    public String getTuserid() {
        return tuserid;
    }

    public void setTuserid(String tuserid) {
        this.tuserid = tuserid;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
