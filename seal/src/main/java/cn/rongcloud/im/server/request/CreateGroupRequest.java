package cn.rongcloud.im.server.request;

import java.util.List;

/**
 * 创建群组
 */
public class CreateGroupRequest {

    private String name;

    private List<String> uid;

    private String code;

    public CreateGroupRequest(String name, List<String> uid, String code) {
        this.name = name;
        this.uid = uid;
        this.code = code;
    }

    public CreateGroupRequest(String name, List<String> memberIds) {
        this.name = name;
        this.uid = memberIds;
    }

    public List<String> getUid() {
        return uid;
    }

    public void setUid(List<String> uid) {
        this.uid = uid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
