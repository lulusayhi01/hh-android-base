package cn.rongcloud.im.server.response.friendcircle;

import android.text.TextUtils;

import com.sj.friendcircle.ui.widget.commentwidget.IComment;

import cn.rongcloud.im.server.response.BaseResponse;
import cn.rongcloud.im.server.response.user.UserInfo;


/**
 * 评论
 */

public class CommentInfo extends BaseResponse implements IComment<CommentInfo> {

    public interface CommentFields {
        String REPLY_USER = "reply";
        String MOMENT = "moment";
        String CONTENT = "content";
        String AUTHOR_USER = "author";
    }


    private int commentid;//评论id

    private MomentsInfo moment;
    private String content;
    private String trendsid;//动态详情id
    private UserInfo author;
    private UserInfo reply;

    public String getTrendsid() {
        return trendsid;
    }

    public void setTrendsid(String trendsid) {
        this.trendsid = trendsid;
    }

    public MomentsInfo getMoment() {
        return moment;
    }

    public void setMoment(MomentsInfo moment) {
        this.moment = moment;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getCommentid() {
        return commentid;
    }

    public void setCommentid(int commentid) {
        this.commentid = commentid;
    }

    //    public String getCommentid() {
//        return getObjectId();
//    }

    public UserInfo getAuthor() {
        return author;
    }

    public void setAuthor(UserInfo author) {
        this.author = author;
    }

    public UserInfo getReply() {
        return reply;
    }

    public void setReply(UserInfo reply) {
        this.reply = reply;
    }

    public boolean canDelete(String userid) {
        return author != null && TextUtils.equals(author.getUserid(), userid);
    }

    @Override
    public String getCommentCreatorName() {
        return author == null ? "" : author.getNick();
    }

    @Override
    public String getReplyerName() {
        return reply == null ? "" : reply.getNick();
    }

//    @Override
//    public String getCommentCreatorName() {
//        return null;
//    }
//
//    @Override
//    public String getReplyerName() {
//        return null;
//    }

    @Override
    public String getCommentContent() {
        return content;
    }

    @Override
    public CommentInfo getData() {
        return this;
    }


//    @Override
//    public String toString() {
//        return "CommentInfo{" +
//                "moment=" + moment +
//                ", content='" + content + '\'' +
//                ", author=" + author.toString() + '\n' +
//                ", reply=" + (reply == null ? "null" : reply.toString()) + '\n' +
//                '}';
//    }
}
