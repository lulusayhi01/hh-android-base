package cn.rongcloud.im.ui.fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.alibaba.android.arouter.launcher.ARouter;
import com.mbt.deal.util.ToastUtil;
import com.mbt.im.R;
import com.sj.friendcircle.common.router.RouterList;

import cn.rongcloud.im.SealAppContext;
import cn.rongcloud.im.server.broadcast.BroadcastManager;
import cn.rongcloud.im.utils.ShareUtil;

/**
 * 动态   Fragment  tab4
 */
public class DynamicFragment extends Fragment implements View.OnClickListener {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;
    private RelativeLayout mRlMoments;//朋友圈
    private RelativeLayout mRlMachine;//矿机
    private RelativeLayout mRlCommunity;
    private RelativeLayout mRlRichScan;
    private RelativeLayout mRlShops;
    private RelativeLayout mRlNearby;
    private View ivFootprint;

    public DynamicFragment() {
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment DynamicFragment.
     */
    public static DynamicFragment newInstance(String param1, String param2) {
        DynamicFragment fragment = new DynamicFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_dynamic, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initView();
    }

    private void initView() {
        ivFootprint = getActivity().findViewById(R.id.iv_footprint);
        mRlMoments = getActivity().findViewById(R.id.rl_moments);
        mRlMachine = getActivity().findViewById(R.id.rl_machine);
        mRlCommunity = getActivity().findViewById(R.id.rl_community);
        mRlRichScan = getActivity().findViewById(R.id.rl_rich_scan);
        mRlShops = getActivity().findViewById(R.id.rl_shops);
        mRlNearby = getActivity().findViewById(R.id.rl_nearby);
        mRlMoments.setOnClickListener(this);
        mRlCommunity.setOnClickListener(this);
        mRlMachine.setOnClickListener(this);
        mRlRichScan.setOnClickListener(this);
        mRlShops.setOnClickListener(this);
        mRlNearby.setOnClickListener(this);
        updateFootPrint();

        BroadcastManager.getInstance(getActivity()).addAction(SealAppContext.UPDATE_FOOTPRINT, new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String command = intent.getAction();
                if (!TextUtils.isEmpty(command)) {
                    updateFootPrint();
                }
            }
        });
    }

    private void updateFootPrint() {
        boolean isPeopleNearby = ShareUtil.getBoolean(getActivity(), "isPeopleNearby");
        if (isPeopleNearby){
            ivFootprint.setVisibility(View.VISIBLE);
        }else {
            ivFootprint.setVisibility(View.GONE);
        }
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.rl_moments){
            ARouter.getInstance().build(RouterList.FriendCircleActivity.path).navigation();
        }else if (v.getId() == R.id.rl_machine){
            ARouter.getInstance().build(RouterList.MachineListActivity.path).navigation();
        }else if (v.getId() == R.id.rl_community){
            ARouter.getInstance().build(RouterList.CommunityActivity.path).navigation();
        }else if (v.getId() == R.id.rl_rich_scan){
            ToastUtil.makeToast(getContext(),"正在开发中");
        }else if (v.getId() == R.id.rl_shops){
            ToastUtil.makeToast(getContext(),"正在开发中");
        }else if (v.getId() == R.id.rl_nearby){
            ARouter.getInstance().build(RouterList.PeopleNearbyActivity.path).navigation();

        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
