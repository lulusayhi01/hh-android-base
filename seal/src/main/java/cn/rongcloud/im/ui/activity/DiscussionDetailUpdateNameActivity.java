package cn.rongcloud.im.ui.activity;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.mbt.im.R;

import java.io.Serializable;

import cn.rongcloud.im.server.network.http.HttpException;
import cn.rongcloud.im.server.response.BaseResponse;
import cn.rongcloud.im.server.utils.NToast;
import cn.rongcloud.im.ui.activity.base.BaseActivity;
import io.rong.imkit.RongIM;
import io.rong.imlib.RongIMClient;
import io.rong.imlib.model.Discussion;

/**
 * 群聊名称修改
 */
public class DiscussionDetailUpdateNameActivity extends BaseActivity implements View.OnClickListener {

    private static final int CHAT_GROUP_EDIT_GROUP_NAME = 1;


    private EditText editUpdateName;
    private Button btnUpdateName;

    private String groupId;
    private String groupName;
    private Discussion mDiscussion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_discussion_detail_update_name);
        setTitle("群聊名片");

        initView();
        initData();
    }

    private void initView() {
        editUpdateName = (EditText) findViewById(R.id.edit_update_name);
        btnUpdateName = (Button) findViewById(R.id.btn_update_name);
        btnUpdateName.setOnClickListener(this);
    }

    private void initData() {
        groupId = getIntent().getStringExtra("groupId");
        mDiscussion = (Discussion) getIntent().getExtras().get("Discussion");
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btn_update_name) {
            groupName = editUpdateName.getText().toString().trim();
            if (TextUtils.isEmpty(groupName)) {
                NToast.shortToast(mContext, "群名称不能为空！");
                return;
            }
            request(CHAT_GROUP_EDIT_GROUP_NAME);
        }
    }

    @Override
    public Object doInBackground(int requestCode, String id) throws HttpException {
        switch (requestCode) {
            case CHAT_GROUP_EDIT_GROUP_NAME:
                return action.chatGroupEditGroupName(groupId, groupName);
        }
        return super.doInBackground(requestCode, id);
    }

    @Override
    public void onSuccess(int requestCode, Object result) {
        if (result != null) {
            switch (requestCode) {
                case CHAT_GROUP_EDIT_GROUP_NAME:
                    BaseResponse scrres = (BaseResponse) result;
                    if (scrres.getResultCode() == 0) {
                        if (mDiscussion == null) return;
                        RongIM.getInstance().setDiscussionName(mDiscussion.getId(), groupName, new RongIMClient.OperationCallback() {
                            @Override
                            public void onSuccess() {
                                mDiscussion.setName(groupName);
                                RongIM.getInstance().refreshDiscussionCache(mDiscussion);
                            }

                            @Override
                            public void onError(RongIMClient.ErrorCode errorCode) {

                            }
                        });

                        setResult(RESULT_OK);
                        this.finish();
                    } else {
                        NToast.shortToast(mContext, scrres.getMsg());
                    }
                    break;
            }
        }
    }

    @Override
    public void onFailure(int requestCode, int state, Object result) {
        switch (requestCode) {
            case CHAT_GROUP_EDIT_GROUP_NAME:
                NToast.shortToast(mContext, "请求失败！");
                break;
        }
    }

}
