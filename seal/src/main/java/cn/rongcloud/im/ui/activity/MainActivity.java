package cn.rongcloud.im.ui.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.mbt.deal.ui.fragment.DealbuinessFragment2;
import com.mbt.im.R;
import com.rong.mbt.personal.ActivityManager;
import com.rong.mbt.personal.fragment.DoctorIndexFragment;
import com.rong.mbt.personal.fragment.DoctorPersonalFragment;
import com.rong.mbt.personal.fragment.HomeDoctorFragment;
import com.rong.mbt.personal.fragment.PatientIndexFragment;
import com.rong.mbt.personal.fragment.PatientPersonalFragment;
import com.vector.update_app.UpdateAppBean;
import com.vector.update_app.UpdateAppManager;
import com.vector.update_app.UpdateCallback;
import com.vector.update_app.utils.AppUpdateUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.rongcloud.im.server.BaseAction;
import cn.rongcloud.im.server.HomeWatcherReceiver;
import cn.rongcloud.im.server.broadcast.BroadcastManager;
import cn.rongcloud.im.server.network.http.HttpException;
import cn.rongcloud.im.server.response.AppVersionResponse;
import cn.rongcloud.im.server.utils.NToast;
import cn.rongcloud.im.server.utils.json.JsonMananger;
import cn.rongcloud.im.server.widget.LoadDialog;
import cn.rongcloud.im.ui.activity.base.BaseActivity;
import cn.rongcloud.im.ui.adapter.ConversationListAdapterEx;
import cn.rongcloud.im.ui.fragment.ContactsFragment;
import cn.rongcloud.im.ui.fragment.MineFragment;
import cn.rongcloud.im.ui.widget.DragPointView;
import cn.rongcloud.im.ui.widget.MorePopWindow;
import cn.rongcloud.im.utils.update.UpdateAppHttpUtil;
import cn.rongcloud.im.utils.update.VersionUtils;
import io.rong.common.RLog;
import io.rong.imkit.RongContext;
import io.rong.imkit.RongIM;
import io.rong.imkit.fragment.ConversationListFragment;
import io.rong.imkit.manager.IUnReadMessageObserver;
import io.rong.imlib.RongIMClient;
import io.rong.imlib.model.Conversation;
import io.rong.message.ContactNotificationMessage;
//import io.rong.toolkit.TestActivity;

@SuppressWarnings("deprecation")
@Route(path = "/activity/main")
public class MainActivity extends BaseActivity implements
        ViewPager.OnPageChangeListener,
        View.OnClickListener,
        DragPointView.OnDragListencer,
        IUnReadMessageObserver {

    private static final int APP_VERSION = 1;

    public static ViewPager mViewPager;
    private List<Fragment> mFragment = new ArrayList<>();
    private ImageView moreImage, mImageChats, mImageContact, mImageTransaction, mImageFind, mImageMe, mMineRed;
    private TextView mTextChats, mTextContact, mTextTransaction, mTextFind, mTextMe;
    private DragPointView mUnreadNumView;
    private ImageView mSearchImageView;
    /**
     * 会话列表的fragment
     */
    private ConversationListFragment mConversationListFragment = null;
    private boolean isDebug;
    private Context mContext;
    private Conversation.ConversationType[] mConversationsTypes = null;
    private RelativeLayout mTitle;
    private DealbuinessFragment2 dealbuinessFragmet;
    private String type;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_seal_main);
        mContext = this;
        setHeadVisibility(View.GONE);
        ActivityManager.addActivity(this);
        SharedPreferences config = getSharedPreferences("config", MODE_PRIVATE);
        isDebug = config.getBoolean("isDebug", false);
        type = config.getString("type", "");
        initViews();
        changeTextViewColor();
        changeSelectedTabState(0);
        initMainViewPager();
        registerHomeKeyReceiver(this);
        updateApp();
    }


    private void initViews() {
        RelativeLayout chatRLayout = (RelativeLayout) findViewById(R.id.seal_chat);
        RelativeLayout contactRLayout = (RelativeLayout) findViewById(R.id.seal_contact_list);
        RelativeLayout transactionRLayout = (RelativeLayout) findViewById(R.id.seal_transaction);
        RelativeLayout foundRLayout = (RelativeLayout) findViewById(R.id.seal_find);
        RelativeLayout mineRLayout = (RelativeLayout) findViewById(R.id.seal_me);

        mTitle = (RelativeLayout) findViewById(R.id.title);/**顶部导航*/

        mImageChats = (ImageView) findViewById(R.id.tab_img_chats);
        mImageContact = (ImageView) findViewById(R.id.tab_img_contact);
        mImageTransaction = (ImageView) findViewById(R.id.tab_img_transaction);
        mImageFind = (ImageView) findViewById(R.id.tab_img_find);
        mImageMe = (ImageView) findViewById(R.id.tab_img_me);
        mTextChats = (TextView) findViewById(R.id.tab_text_chats);
        mTextContact = (TextView) findViewById(R.id.tab_text_contact);
        mTextTransaction = (TextView) findViewById(R.id.tab_text_transaction);
        mTextFind = (TextView) findViewById(R.id.tab_text_find);
        mTextMe = (TextView) findViewById(R.id.tab_text_me);

        mMineRed = (ImageView) findViewById(R.id.mine_red);
        moreImage = (ImageView) findViewById(R.id.seal_more);
        mSearchImageView = (ImageView) findViewById(R.id.ac_iv_search);

        chatRLayout.setOnClickListener(this);
        contactRLayout.setOnClickListener(this);
        transactionRLayout.setOnClickListener(this);
        foundRLayout.setOnClickListener(this);
        mineRLayout.setOnClickListener(this);
        moreImage.setOnClickListener(this);
        mSearchImageView.setOnClickListener(this);
        BroadcastManager.getInstance(mContext).addAction(MineFragment.SHOW_RED, new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                mMineRed.setVisibility(View.VISIBLE);
            }
        });
    }


    private void initMainViewPager() {
        Fragment conversationList = initConversationList();
        mViewPager = (ViewPager) findViewById(R.id.main_viewpager);

        mUnreadNumView = (DragPointView) findViewById(R.id.seal_num);
        mUnreadNumView.setOnClickListener(this);
        mUnreadNumView.setDragListencer(this);
        dealbuinessFragmet = new DealbuinessFragment2();


//        mFragment.add(dealbuinessFragmet);//交易
//        mFragment.add(new DynamicFragment());//动态

//        mFragment.add(new DiscoverFragment());
//        mFragment.add(new MineFragment());
        if (TextUtils.isEmpty(type) || "0".equals(type)) {
            mTitle.setVisibility(View.GONE);
            mFragment.add(new PatientIndexFragment());
            mFragment.add(conversationList);
//            mFragment.add(new ContactsFragment());
            mFragment.add(new PatientPersonalFragment());

        } else if ("1".equals(type)) {
            mTitle.setVisibility(View.GONE);
            mFragment.add(new DoctorIndexFragment());
            mFragment.add(conversationList);
            mFragment.add(new DoctorPersonalFragment());
        } else if ("2".equals(type)) {
            mTitle.setVisibility(View.GONE);
            mFragment.add(conversationList);
            mFragment.add(new ContactsFragment());
            mFragment.add(new PatientIndexFragment());
        } else {
            mTitle.setVisibility(View.VISIBLE);
            mFragment.add(new PatientIndexFragment());
            mFragment.add(conversationList);
//            mFragment.add(new ContactsFragment());
            mFragment.add(new PatientPersonalFragment());
        }
        FragmentPagerAdapter fragmentPagerAdapter = new FragmentPagerAdapter(getSupportFragmentManager()) {
            @Override
            public Fragment getItem(int position) {
                return mFragment.get(position);
            }

            @Override
            public int getCount() {
                return mFragment.size();
            }
        };
        mViewPager.setAdapter(fragmentPagerAdapter);
        mViewPager.setOffscreenPageLimit(3);
        mViewPager.setOnPageChangeListener(this);
        initData();
    }


    private Fragment initConversationList() {
        if (mConversationListFragment == null) {
            ConversationListFragment listFragment = new ConversationListFragment();
            listFragment.setAdapter(new ConversationListAdapterEx(RongContext.getInstance()));
            Uri uri;
            if (isDebug) {
                uri = Uri.parse("rong://" + getApplicationInfo().packageName).buildUpon()
                        .appendPath("conversationlist")
                        .appendQueryParameter(Conversation.ConversationType.PRIVATE.getName(), "true") //设置私聊会话是否聚合显示
                        .appendQueryParameter(Conversation.ConversationType.GROUP.getName(), "true")//群组
                        .appendQueryParameter(Conversation.ConversationType.PUBLIC_SERVICE.getName(), "false")//公共服务号
                        .appendQueryParameter(Conversation.ConversationType.APP_PUBLIC_SERVICE.getName(), "false")//订阅号
                        .appendQueryParameter(Conversation.ConversationType.SYSTEM.getName(), "true")//系统
                        .appendQueryParameter(Conversation.ConversationType.DISCUSSION.getName(), "true")
                        .build();
                mConversationsTypes = new Conversation.ConversationType[]{Conversation.ConversationType.PRIVATE,
                        Conversation.ConversationType.GROUP,
                        Conversation.ConversationType.PUBLIC_SERVICE,
                        Conversation.ConversationType.APP_PUBLIC_SERVICE,
                        Conversation.ConversationType.SYSTEM,
                        Conversation.ConversationType.DISCUSSION
                };

            } else {
                uri = Uri.parse("rong://" + getApplicationInfo().packageName).buildUpon()
                        .appendPath("conversationlist")
                        .appendQueryParameter(Conversation.ConversationType.PRIVATE.getName(), "false") //设置私聊会话是否聚合显示
                        .appendQueryParameter(Conversation.ConversationType.GROUP.getName(), "false")//群组
                        .appendQueryParameter(Conversation.ConversationType.PUBLIC_SERVICE.getName(), "false")//公共服务号
                        .appendQueryParameter(Conversation.ConversationType.APP_PUBLIC_SERVICE.getName(), "false")//订阅号
                        .appendQueryParameter(Conversation.ConversationType.SYSTEM.getName(), "true")//系统
                        .appendQueryParameter(Conversation.ConversationType.DISCUSSION.getName(), "false")//设置讨论组会话非聚合显示
                        .build();
                mConversationsTypes = new Conversation.ConversationType[]{Conversation.ConversationType.PRIVATE,
                        Conversation.ConversationType.GROUP,
                        Conversation.ConversationType.PUBLIC_SERVICE,
                        Conversation.ConversationType.APP_PUBLIC_SERVICE,
                        Conversation.ConversationType.SYSTEM,
                        Conversation.ConversationType.DISCUSSION
                };
            }
            listFragment.setUri(uri);
            mConversationListFragment = listFragment;
            return listFragment;
        } else {
            return mConversationListFragment;
        }
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        changeTextViewColor();
        changeSelectedTabState(position);
    }

    private void changeTextViewColor() {
        mImageChats.setBackgroundDrawable(getResources().getDrawable(R.drawable.tab_chat));
        mImageContact.setBackgroundDrawable(getResources().getDrawable(R.drawable.tab_contacts));
        mImageTransaction.setBackgroundDrawable(getResources().getDrawable(R.drawable.tab_transaction));
        mImageFind.setBackgroundDrawable(getResources().getDrawable(R.drawable.tab_dynamic));
        mImageMe.setBackgroundDrawable(getResources().getDrawable(R.drawable.p_setting_unselector));
        mTextChats.setTextColor(getResources().getColor(R.color.c_6));
        mTextContact.setTextColor(getResources().getColor(R.color.c_6));
        mTextTransaction.setTextColor(getResources().getColor(R.color.c_6));
        mTextFind.setTextColor(getResources().getColor(R.color.c_6));
        mTextMe.setTextColor(getResources().getColor(R.color.c_6));
    }

    private void changeSelectedTabState(int position) {
        switch (position) {
            case 0:
                mTitle.setVisibility(View.GONE);
                mTextChats.setTextColor(getResources().getColor(R.color.tab_select));
                mImageChats.setBackgroundDrawable(getResources().getDrawable(R.drawable.tab_chat_hover));
                break;
            case 1:
                mTitle.setVisibility(View.VISIBLE);
                mTextContact.setTextColor(getResources().getColor(R.color.tab_select));
                mImageContact.setBackgroundDrawable(getResources().getDrawable(R.drawable.tab_contacts_hover));
                break;
//            case 2:
//                mTitle.setVisibility(View.GONE);
//                mTextTransaction.setTextColor(getResources().getColor(R.color.tab_select));
//                mImageTransaction.setBackgroundDrawable(getResources().getDrawable(R.drawable.tab_transaction_hover));
//                break;
//            case 3:
//                mTitle.setVisibility(View.GONE);
//                mTextFind.setTextColor(getResources().getColor(R.color.tab_select));
//                mImageFind.setBackgroundDrawable(getResources().getDrawable(R.drawable.tab_dynamic_hover));
//                break;
            case 2:
                mTitle.setVisibility(View.GONE);
                mTextMe.setTextColor(getResources().getColor(R.color.tab_select));
                mImageMe.setBackgroundDrawable(getResources().getDrawable(R.drawable.p_setting_selector));
                break;
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }


    long firstClick = 0;
    long secondClick = 0;

    @Override
    public void onClick(View v) {
        int i = v.getId();
        if (i == R.id.seal_chat) {
            if (mViewPager.getCurrentItem() == 0) {
                if (firstClick == 0) {
                    firstClick = System.currentTimeMillis();
                } else {
                    secondClick = System.currentTimeMillis();
                }
                RLog.i("MainActivity", "time = " + (secondClick - firstClick));
                if (secondClick - firstClick > 0 && secondClick - firstClick <= 800) {
                    mConversationListFragment.focusUnreadItem();
                    firstClick = 0;
                    secondClick = 0;
                } else if (firstClick != 0 && secondClick != 0) {
                    firstClick = 0;
                    secondClick = 0;
                }
            }
            mViewPager.setCurrentItem(0, false);

        } else if (i == R.id.seal_contact_list) {
            mViewPager.setCurrentItem(1, false);

        }
//        else if (i == R.id.seal_transaction) {
//            mViewPager.setCurrentItem(2, false);
//
//        } else if (i == R.id.seal_find) {
//            mViewPager.setCurrentItem(3, false);
//
//        }
        else if (i == R.id.seal_me) {
            mViewPager.setCurrentItem(2, false);
            mMineRed.setVisibility(View.GONE);

        } else if (i == R.id.seal_more) {
            MorePopWindow morePopWindow = new MorePopWindow(MainActivity.this);
            morePopWindow.showPopupWindow(moreImage);

        } else if (i == R.id.ac_iv_search) {
            startActivity(new Intent(MainActivity.this, SealSearchActivity.class));

        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (intent.getBooleanExtra("systemconversation", false)) {
            mViewPager.setCurrentItem(0, false);
        }
    }

    protected void initData() {

        final Conversation.ConversationType[] conversationTypes = {
                Conversation.ConversationType.PRIVATE,
                Conversation.ConversationType.GROUP, Conversation.ConversationType.SYSTEM,
                Conversation.ConversationType.PUBLIC_SERVICE, Conversation.ConversationType.APP_PUBLIC_SERVICE,
                Conversation.ConversationType.DISCUSSION
        };

        RongIM.getInstance().addUnReadMessageCountChangedObserver(this, conversationTypes);
        getConversationPush();// 获取 push 的 id 和 target
        getPushMessage();
    }

    private void getConversationPush() {
        if (getIntent() != null && getIntent().hasExtra("PUSH_CONVERSATIONTYPE") && getIntent().hasExtra("PUSH_TARGETID")) {

            final String conversationType = getIntent().getStringExtra("PUSH_CONVERSATIONTYPE");
            final String targetId = getIntent().getStringExtra("PUSH_TARGETID");


            RongIM.getInstance().getConversation(Conversation.ConversationType.valueOf(conversationType), targetId, new RongIMClient.ResultCallback<Conversation>() {
                @Override
                public void onSuccess(Conversation conversation) {

                    if (conversation != null) {

                        if (conversation.getLatestMessage() instanceof ContactNotificationMessage) { //好友消息的push
                            startActivity(new Intent(MainActivity.this, NewFriendListActivity.class));
                        } else {
                            Uri uri = Uri.parse("rong://" + getApplicationInfo().packageName).buildUpon().appendPath("conversation")
                                    .appendPath(conversationType).appendQueryParameter("targetId", targetId).build();
                            Intent intent = new Intent(Intent.ACTION_VIEW);
                            intent.setData(uri);
                            startActivity(intent);
                        }
                    }
                }

                @Override
                public void onError(RongIMClient.ErrorCode e) {

                }
            });
        }
    }

    /**
     * 得到不落地 push 消息
     */
    private void getPushMessage() {
        Intent intent = getIntent();
        if (intent != null && intent.getData() != null && intent.getData().getScheme().equals("rong")) {
            String path = intent.getData().getPath();
            if (path.contains("push_message")) {
                SharedPreferences sharedPreferences = getSharedPreferences("config", MODE_PRIVATE);
                String cacheToken = sharedPreferences.getString("loginToken", "");
                if (TextUtils.isEmpty(cacheToken)) {
                    startActivity(new Intent(MainActivity.this, LoginActivity.class));
                } else {
                    if (!RongIM.getInstance().getCurrentConnectionStatus().equals(RongIMClient.ConnectionStatusListener.ConnectionStatus.CONNECTED)) {
                        LoadDialog.show(mContext);
                        RongIM.connect(cacheToken, new RongIMClient.ConnectCallback() {
                            @Override
                            public void onTokenIncorrect() {
                                LoadDialog.dismiss(mContext);
                            }

                            @Override
                            public void onSuccess(String s) {
                                LoadDialog.dismiss(mContext);
                            }

                            @Override
                            public void onError(RongIMClient.ErrorCode e) {
                                LoadDialog.dismiss(mContext);
                            }
                        });
                    }
                }
            }
        }
    }

    @Override
    public void onCountChanged(int count) {
        if (count == 0) {
            mUnreadNumView.setVisibility(View.GONE);
        } else if (count > 0 && count < 100) {
            mUnreadNumView.setVisibility(View.VISIBLE);
            mUnreadNumView.setText(String.valueOf(count));
        } else {
            mUnreadNumView.setVisibility(View.VISIBLE);
            mUnreadNumView.setText(R.string.no_read_message);
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            moveTaskToBack(false);
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }


    private void hintKbTwo() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm.isActive() && getCurrentFocus() != null) {
            if (getCurrentFocus().getWindowToken() != null) {
                imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
            }
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (null != this.getCurrentFocus() && event.getAction() == MotionEvent.ACTION_UP) {
            InputMethodManager mInputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            return mInputMethodManager.hideSoftInputFromWindow(this.getCurrentFocus().getWindowToken(), 0);
        }
        return super.onTouchEvent(event);
    }

    @Override
    protected void onDestroy() {
        RongIM.getInstance().removeUnReadMessageCountChangedObserver(this);
        if (mHomeKeyReceiver != null)
            this.unregisterReceiver(mHomeKeyReceiver);
        super.onDestroy();
        ActivityManager.removeActivity(this);
    }

    @Override
    public void onDragOut() {
        mUnreadNumView.setVisibility(View.GONE);
        NToast.shortToast(mContext, getString(R.string.clear_success));
        RongIM.getInstance().getConversationList(new RongIMClient.ResultCallback<List<Conversation>>() {
            @Override
            public void onSuccess(List<Conversation> conversations) {
                if (conversations != null && conversations.size() > 0) {
                    for (Conversation c : conversations) {
                        RongIM.getInstance().clearMessagesUnreadStatus(c.getConversationType(), c.getTargetId(), null);
                    }
                }
            }

            @Override
            public void onError(RongIMClient.ErrorCode e) {

            }
        }, mConversationsTypes);

    }

    private HomeWatcherReceiver mHomeKeyReceiver = null;

    //如果遇见 Android 7.0 系统切换到后台回来无效的情况 把下面注册广播相关代码注释或者删除即可解决。下面广播重写 home 键是为了解决三星 note3 按 home 键花屏的一个问题
    private void registerHomeKeyReceiver(Context context) {
        if (mHomeKeyReceiver == null) {
            mHomeKeyReceiver = new HomeWatcherReceiver();
            final IntentFilter homeFilter = new IntentFilter(Intent.ACTION_CLOSE_SYSTEM_DIALOGS);
            try {
                context.registerReceiver(mHomeKeyReceiver, homeFilter);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public Object doInBackground(int requestCode, String id) throws HttpException {
        switch (requestCode) {
            case APP_VERSION:
                return action.appVersion("android");
        }
        return super.doInBackground(requestCode, id);
    }

    @Override
    public void onSuccess(int requestCode, Object result) {
        if (result != null) {
            switch (requestCode) {
                case APP_VERSION:
                    AppVersionResponse response = (AppVersionResponse) result;
                    if (response.getResultCode() == 0) {
                        String mUpdateUrl = response.getData().getAppUrl();
                    } else {
//                        NToast.shortToast(mContext, response.getMsg());
                    }
                    break;
            }
        }
    }

    @Override
    public void onFailure(int requestCode, int state, Object result) {
        switch (requestCode) {
            default:
//                NToast.shortToast(mContext, "请求失败！");
                break;
        }
    }

    /**
     * 版本更新
     */
    private void updateApp() {
        Map<String, String> params = new HashMap<String, String>();
        params.put("appType", "android");
        params.put("mbtToken", action.getToken(mContext));
        new UpdateAppManager
                .Builder()
                .setActivity(this)
                .setParams(params)
                .setUpdateUrl(BaseAction.DOMAIN + "/app/check")
                .setTopPic(R.drawable.top_3)
//                .hideDialogOnDownloading()
                .setThemeColor(0xff3498DB)
                .setHttpManager(new UpdateAppHttpUtil())
                .build()
                .checkNewApp(new UpdateCallback() {
                    /**
                     * 解析json,自定义协议
                     *
                     * @param json 服务器返回的json
                     * @return UpdateAppBean
                     */
                    @Override
                    protected UpdateAppBean parseJson(String json) {
                        UpdateAppBean updateAppBean = new UpdateAppBean();
                        try {
                            AppVersionResponse response = JsonMananger.jsonToBean(json, AppVersionResponse.class);
                            if (response == null) return updateAppBean;
                            String versionName = AppUpdateUtils.getVersionName(mContext);
                            final String newVersion = response.getData().getAppVersion();
                            if (VersionUtils.compareVersion(versionName, newVersion) == -1) {
                                updateAppBean
                                        //（必须）是否更新Yes,No
                                        .setUpdate("Yes")
                                        //（必须）新版本号，
                                        .setNewVersion(newVersion)
                                        .setTargetSize(response.getData().getAppSize())
                                        //（必须）下载地址
                                        .setApkFileUrl(response.getData().getAppUrl())
                                        .setUpdateLog(response.getData().getRemarker())
                                        //是否强制更新
                                        .setConstraint(response.getData().getConstraint() == 1);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        return updateAppBean;
                    }

                    @Override
                    protected void hasNewApp(UpdateAppBean updateApp, UpdateAppManager updateAppManager) {
                        updateAppManager.showDialogFragment();
                    }

                    /**
                     * 没有新版本
                     */
                    @Override
                    public void noNewApp(String error) {
                    }
                });
    }

}
