package cn.rongcloud.im.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * @Author: huluhong
 * @Time: 2018/10/10 0010
 * @Description：This is ShareUtil
 */

public class ShareUtil {

    private static SharedPreferences sharedPreferences = null;

    /**
     * 构造函数
     */
    public ShareUtil(Context context) {
        sharedPreferences = context.getSharedPreferences("SP_mbt_data", Context.MODE_PRIVATE);

    }

    public static SharedPreferences getInstance(Context context) {
        if (sharedPreferences == null) {
            new ShareUtil(context);
        }
        return sharedPreferences;
    }

    /**
     * 保存数据
     *
     * @param context
     * @param key
     * @param value
     */
    public static void putData(Context context,String key, int value) {
        getInstance(context).edit().putInt(key, value).apply();
    }

    public static void putData(Context context,String key, long value) {
        getInstance(context).edit().putLong(key, value).apply();
    }

    public static void putData(Context context,String key, String value) {
        getInstance(context).edit().putString(key, value).apply();
    }

    public static void putData(Context context,String key, boolean value) {
        getInstance(context).edit().putBoolean(key, value).apply();
    }

    /**
     * 获取数据
     *
     * @param context
     * @param key
     */
    public static String getString(Context context,String key) {
        return getInstance(context).getString(key, "");
    }

    public static int getInt(Context context,String key) {
        return getInstance(context).getInt(key, -1);
    }

    public static long getLong(Context context,String key) {
        return getInstance(context).getLong(key, 0L);
    }

    /**
     * 没有值默认是false
     *
     * @param context
     * @param key
     * @return
     */
    public static boolean getBoolean(Context context,String key) {
        return getInstance(context).getBoolean(key, false);
    }

    /**
     * 没有值默认是 true
     *
     * @param context
     * @param key
     * @return
     */
    public static boolean getBooleanTrue(Context context,String key) {
        return getInstance(context).getBoolean(key, true);
    }

    public static void clear(Context context) {
        getInstance(context).edit().clear().apply();
    }
}
