package com.rong.mbt.personal.bean.order;

import java.io.Serializable;

public class OrderPublishBean implements Serializable {


    /**
     * data : {"age":"3","content":"dndndnndnxnn","feed":"3","headimg":"https://hh-liululu.oss-cn-beijing.aliyuncs.com/default/user-header-dafault.png","id":36,"name":"15013554615","sex":2,"userId":86}
     * msg : 请求成功
     * resultCode : 0
     */

    private DataBean data;
    private String msg;
    private int resultCode;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public int getResultCode() {
        return resultCode;
    }

    public void setResultCode(int resultCode) {
        this.resultCode = resultCode;
    }

    public static class DataBean {
        /**
         * age : 3
         * content : dndndnndnxnn
         * feed : 3
         * headimg : https://hh-liululu.oss-cn-beijing.aliyuncs.com/default/user-header-dafault.png
         * id : 36
         * name : 15013554615
         * sex : 2
         * userId : 86
         */

        private String age;
        private String content;
        private String feed;
        private String headimg;
        private int id;
        private String name;
        private int sex;
        private int userId;

        public String getAge() {
            return age;
        }

        public void setAge(String age) {
            this.age = age;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public String getFeed() {
            return feed;
        }

        public void setFeed(String feed) {
            this.feed = feed;
        }

        public String getHeadimg() {
            return headimg;
        }

        public void setHeadimg(String headimg) {
            this.headimg = headimg;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getSex() {
            return sex;
        }

        public void setSex(int sex) {
            this.sex = sex;
        }

        public int getUserId() {
            return userId;
        }

        public void setUserId(int userId) {
            this.userId = userId;
        }
    }
}
