package com.rong.mbt.personal.activity;


import android.app.ProgressDialog;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.callback.StringCallback;
import com.lzy.okgo.model.Response;
import com.rong.mbt.personal.R;
import com.rong.mbt.personal.activity.base.PBaseActivity;
import com.rong.mbt.personal.bean.base.BaseBean;
import com.rong.mbt.personal.bean.PersonalDetailInfoBean;
import com.rong.mbt.personal.dialog.pickerDialog.OptionPicker;
import com.rong.mbt.personal.utils.DlgUtil;

import java.util.ArrayList;

public class PersonalDetailInfoActivity extends PBaseActivity {


    private TextView tvXueli;
    private EditText etXueXiao;
    private TextView tvHunYin;
    private EditText etAddress;
    private TextView tvHangYe;
    private TextView tvGuimo;
    private EditText etZhiWei;
    private TextView tvShouRu;
    private TextView tvGouFang;
    private TextView tvGouChe;
    private EditText etLianxiren;
    private EditText etLianxirenPhone;
    private TextView tvGuanxi;

    private ArrayList<String> mList = new ArrayList<>();
    private String mbtToken;
    private ProgressDialog pdlg;

    @Override
    public int getLayoutId() {
        return R.layout.activity_personal_detail_info;
    }

    @Override
    public void initView() {
        super.initView();
        mbtToken = getIntent().getStringExtra("mbtToken");
        find();
        listener();
        getExtra();
    }

    private void listener() {
        findViewById(R.id.head_while_iv_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        findViewById(R.id.act_line_xueli).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mList.clear();
                mList.add("高中或以下");
                mList.add("大专");
                mList.add("本科");
                mList.add("博士");
                mList.add("博士后");
                selectorItem(tvXueli, mList);

            }
        });
        findViewById(R.id.act_line_hunyin).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mList.clear();
                mList.add("已婚");
                mList.add("未婚");
                selectorItem(tvHunYin, mList);
            }
        });
        findViewById(R.id.act_line_hanye).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mList.clear();
                mList.add("互联网");
                mList.add("制造业");
                mList.add("其他");
                selectorItem(tvHangYe, mList);
            }
        });
        findViewById(R.id.act_line_guimo).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mList.clear();
                mList.add("0-20");
                mList.add("20-99");
                mList.add("100-499");
                mList.add("500-999");
                mList.add("1000-9999");
                mList.add("10000以上");
                selectorItem(tvGuimo, mList);
            }
        });
        findViewById(R.id.act_line_shouru).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mList.clear();
                mList.add("3000以下");
                mList.add("3000-4500");
                mList.add("4500-7000");
                mList.add("7000-10000");
                mList.add("10000以上");
                selectorItem(tvShouRu, mList);
            }
        });
        findViewById(R.id.act_line_goufang).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mList.clear();
                mList.add("是");
                mList.add("否");
                selectorItem(tvGouFang, mList);
            }
        });
        findViewById(R.id.act_line_gouche).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mList.clear();
                mList.add("是");
                mList.add("否");
                selectorItem(tvGouChe, mList);
            }
        });
        findViewById(R.id.act_line_guanxi).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mList.clear();
                mList.add("家人");
                mList.add("同事");
                mList.add("朋友");
                selectorItem(tvGuanxi, mList);
            }
        });


        //保存
        findViewById(R.id.act_pdi_tv_submit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String xueli = tvXueli.getText().toString();
                String xuexiao = etXueXiao.getText().toString();
                String hunyin = tvHunYin.getText().toString();
                String address = etAddress.getText().toString();
                String hangye = tvHangYe.getText().toString();
                String guimo = tvGuimo.getText().toString();
                String zhiwei = etZhiWei.getText().toString();
                String shouru = tvShouRu.getText().toString();
                String goufang = tvGouFang.getText().toString();
                String gouche = tvGouChe.getText().toString();
                String liangxiren = etLianxiren.getText().toString();
                String liangxirenPhone = etLianxirenPhone.getText().toString();
                String guanxi = tvGuanxi.getText().toString();
                if (xueli.isEmpty() || xuexiao.isEmpty() || hunyin.isEmpty() || address.isEmpty() ||
                        hangye.isEmpty() || guimo.isEmpty() || zhiwei.isEmpty() || shouru.isEmpty()
                        || goufang.isEmpty() || gouche.isEmpty() || liangxiren.isEmpty() || liangxirenPhone.isEmpty() || guanxi.isEmpty()) {
                    Toast.makeText(PersonalDetailInfoActivity.this, "请全部选择并填写", Toast.LENGTH_SHORT).show();
                    return;
                } else {
                    pdlg = DlgUtil.showProgressDlg(PersonalDetailInfoActivity.this, "正在保存");
                    //http://service.jiangaifen.com:38082/userInfo/edit
                    OkGo.<String>get("http://service.jiangaifen.com:38082/userInfo/edit")
                            .tag(this)
//                            .isMultipart(true)
                            .headers("mbtToken", mbtToken)
                            .params("education", xueli)
                            .params("academy", xuexiao)
                            .params("marriage", hunyin)
                            .params("address", address)
                            .params("business", hangye)
                            .params("scale", guimo)
                            .params("position", zhiwei)
                            .params("income", shouru)
                            .params("purchasehouse", goufang)
                            .params("purchasecar", gouche)
                            .params("urgencylinkman", liangxiren)
                            .params("urgencyphone", liangxirenPhone)
                            .params("urgencyrelation", guanxi)
                            .execute(new StringCallback() {
                                @Override
                                public void onSuccess(Response<String> response) {
                                    Log.i("test", "onSuccess  " + response.body());

                                    BaseBean baseBean = JSON.parseObject(response.body(), BaseBean.class);
                                    if (pdlg != null) {
                                        pdlg.cancel();
                                    }
                                    Toast.makeText(PersonalDetailInfoActivity.this, baseBean.getMsg(), Toast.LENGTH_SHORT).show();
                                }

                                @Override
                                public void onError(Response<String> response) {
                                    super.onError(response);
                                    Log.i("test", "onError  " + response.message());
                                    if (pdlg != null) {
                                        pdlg.cancel();
                                    }
                                }
                            });
                }
            }
        });
    }

    private void find() {
        tvXueli = findViewById(R.id.act_tv_xueli);
        etXueXiao = findViewById(R.id.act_et_xuexiao);
        tvHunYin = findViewById(R.id.act_tv_hunyin);
        etAddress = findViewById(R.id.act_et_address);
        tvHangYe = findViewById(R.id.act_tv_hanye);
        tvGuimo = findViewById(R.id.act_tv_guimo);
        etZhiWei = findViewById(R.id.act_et_zhiwei);
        tvShouRu = findViewById(R.id.act_tv_shouru);
        tvGouFang = findViewById(R.id.act_tv_goufang);
        tvGouChe = findViewById(R.id.act_tv_gouche);
        etLianxiren = findViewById(R.id.act_et_lianxiren);
        etLianxirenPhone = findViewById(R.id.act_et_lianxirenPhone);
        tvGuanxi = findViewById(R.id.act_tv_guanxi);
    }

    public void selectorItem(final TextView tv, ArrayList<String> list) {
        if (tv == null) {
            return;
        }
        OptionPicker picker = new OptionPicker(this, list); //list为选择器中的选项
        picker.setOffset(2);
        picker.setSelectedIndex(1); //默认选中项
        picker.setTextSize(18);
        picker.setCycleDisable(true); //选项不循环滚动
        picker.setOnOptionPickListener(new OptionPicker.OnOptionPickListener() {
            @Override
            public void onOptionPicked(int position, String option) {
                tv.setText(option);
            }
        });
        picker.show();
    }

    public void getExtra() {
        PersonalDetailInfoBean.DataBean data = (PersonalDetailInfoBean.DataBean) getIntent().getSerializableExtra("data");
        if (data != null) {
            String education = data.getEducation();
            String academy = data.getAcademy();
            String marriage = data.getMarriage();
            String address = data.getAddress();
            String business = data.getBusiness();
            String scale = data.getScale();
            String position = data.getPosition();
            String income = data.getIncome();
            String purchasehouse = data.getPurchasehouse();
            String purchasecar = data.getPurchasecar();
            String urgencylinkman = data.getUrgencylinkman();
            String urgencyphone = data.getUrgencyphone();
            String urgencyrelation = data.getUrgencyrelation();
            tvXueli.setText(education);
            etXueXiao.setText(academy);
            tvHunYin.setText(marriage);
            etAddress.setText(address);
            tvHangYe.setText(business);
            tvGuimo.setText(scale);
            etZhiWei.setText(position);
            tvShouRu.setText(income);
            tvGouFang.setText(purchasehouse);
            tvGouChe.setText(purchasecar);
            etLianxiren.setText(urgencylinkman);
            etLianxirenPhone.setText(urgencyphone);
            tvGuanxi.setText(urgencyrelation);
        }
    }
}