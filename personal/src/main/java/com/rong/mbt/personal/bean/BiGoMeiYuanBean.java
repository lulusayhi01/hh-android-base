package com.rong.mbt.personal.bean;

/**
 * Created by jianfei on 2018/9/25.
 */

public class BiGoMeiYuanBean {

    /**
     * data : {"pricenow":1.2}
     * msg : 请求成功
     * resultCode : 0
     */

    private DataBean data;
    private String msg;
    private int resultCode;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public int getResultCode() {
        return resultCode;
    }

    public void setResultCode(int resultCode) {
        this.resultCode = resultCode;
    }

    public static class DataBean {
        /**
         * pricenow : 1.2
         */

        private double pricenow;

        public double getPricenow() {
            return pricenow;
        }

        public void setPricenow(double pricenow) {
            this.pricenow = pricenow;
        }
    }
}
