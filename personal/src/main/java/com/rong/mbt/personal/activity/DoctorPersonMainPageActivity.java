package com.rong.mbt.personal.activity;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.flyco.tablayout.SlidingTabLayout;
import com.rong.mbt.personal.R;
import com.rong.mbt.personal.activity.base.PBaseActivity;
import com.rong.mbt.personal.adapter.TabPagerAdapter;
import com.rong.mbt.personal.adapter.UserViewLikeCommentAdapter;
import com.rong.mbt.personal.fragment.CommentItemFragment;
import com.rong.mbt.personal.fragment.article.ArticleItemFragment;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * 更多文章列表
 */
public class DoctorPersonMainPageActivity extends PBaseActivity {


    private LinearLayout mLlBack;
    private TextView mTxtTitle;
    private View mIvRight;
    private LinearLayout mLlRightMenu;
    private TextView mTxtRight;
    private LinearLayout mLlRightText;
    private SlidingTabLayout mSlidingTabLayout;
    private ViewPager mViewPager;
    private TextView tvDoctorMore;

    private int categoryId = 0;
    private String[] mTitles = {"评论", "动态"};//1  快速回答2  精选讲堂3  育婴护理4  精神健康
    private Integer[] codes = {0, 1};
    private ArrayList<Fragment> mFragments = new ArrayList<>();

    @Override
    public int getLayoutId() {
        return R.layout.activity_doctor_person_main_page;
    }

    @Override
    public void initView() {
        super.initView();
        mLlBack = findViewById(R.id.ll_back);
        mTxtTitle = findViewById(R.id.txt_title);
        mIvRight = findViewById(R.id.iv_right);
        mLlRightMenu = findViewById(R.id.ll_right_menu);
        mTxtRight = findViewById(R.id.txt_right);
        mLlRightText = findViewById(R.id.ll_right_text);
        mSlidingTabLayout = findViewById(R.id.sliding_tab_layout);
        mViewPager = findViewById(R.id.view_pager);
        mTxtTitle.setText("");
        tvDoctorMore = findViewById(R.id.tv_doctor_more);

        categoryId = getIntent().getIntExtra("categoryId", 0);

        initTabLayout();
        initListener();
    }

    private void initTabLayout() {
        mFragments.add(CommentItemFragment.getInstance(mTitles[1], codes[1]));
        mFragments.add(ArticleItemFragment.getInstance(mTitles[0], codes[0]));

        TabPagerAdapter tabPagerAdapter = new TabPagerAdapter(getSupportFragmentManager(), Arrays.asList(mTitles), mFragments);
        mViewPager.setAdapter(tabPagerAdapter);
        mViewPager.setCurrentItem(categoryId);
        mSlidingTabLayout.setViewPager(mViewPager);
    }

    private void initListener() {

        mLlBack.setOnClickListener(v -> finish());
        tvDoctorMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DoctorPersonMainPageActivity.this, DoctorInfomationActivity.class);
                startActivity(intent);
            }
        });
    }


}
