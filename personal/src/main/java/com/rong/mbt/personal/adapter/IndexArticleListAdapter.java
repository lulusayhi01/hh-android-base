package com.rong.mbt.personal.adapter;

import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.rong.mbt.personal.R;
import com.rong.mbt.personal.bean.article.ArticleItemBean;

/**
 * 文章列表
 */
public class IndexArticleListAdapter extends BaseQuickAdapter<ArticleItemBean, BaseViewHolder> {


    public IndexArticleListAdapter() {
        super(R.layout.item_patient_index_article);
    }

    @Override
    protected void convert(BaseViewHolder helper, ArticleItemBean item) {

        try {
            ImageView ivHead = helper.getView(R.id.iv_head);
            Glide.with(helper.itemView.getContext()).load(item.getArticle().getPageImg()).into(ivHead);

            TextView tvArticleTitle = helper.getView(R.id.tv_article_title);
            tvArticleTitle.setText(item.getArticle().getTitle());

            TextView tvDoctorName = helper.getView(R.id.tv_doctor_name);
            tvDoctorName.setText(item.getUser().getName());

            TextView tvReadingNumber = helper.getView(R.id.tv_reading_number);
            tvReadingNumber.setText(item.getUserViewLikeCommentListSize() + "");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
