package com.rong.mbt.personal.activity;

import android.content.Intent;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.model.Response;
import com.rong.mbt.personal.R;
import com.rong.mbt.personal.activity.base.PBaseActivity;
import com.rong.mbt.personal.adapter.SelectDepartmentListAdapter;
import com.rong.mbt.personal.bean.doctor.OfficesFindBean;
import com.rong.mbt.personal.http.UrlApi;
import com.rong.mbt.personal.http.callback.JsonCallback;
import com.rong.mbt.personal.utils.SpUtils;

/**
 * 选择科室
 */
public class SelectDepartmentListActivity extends PBaseActivity {

    @Override
    public void initView() {
        super.initView();

        RecyclerView recyclerView = findViewById(R.id.recycler_view);
        TextView textTitle = findViewById(R.id.txt_title);
        textTitle.setText("选择科室");

        LinearLayout back = findViewById(R.id.ll_back);
        back.setOnClickListener(v -> finish());

        recyclerView.setLayoutManager(new GridLayoutManager(getApplication(), 2));
        SelectDepartmentListAdapter doctorListAdapter = new SelectDepartmentListAdapter();
        doctorListAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                OfficesFindBean.DataBean item = (OfficesFindBean.DataBean) adapter.getItem(position);
                startActivity(new Intent(getBaseContext(),FindDoctorListActivity.class).putExtra("officesId",item.getId()));
            }
        });
        recyclerView.setAdapter(doctorListAdapter);

        OkGo.<OfficesFindBean>get(UrlApi.officesFind)
                .tag(this)
                .headers("mbtToken", SpUtils.getToken(this))
                .execute(new JsonCallback<OfficesFindBean>() {
                    @Override
                    public void onSuccess(Response<OfficesFindBean> response) {
                        if (response != null && response.body() != null) {
                            OfficesFindBean body = response.body();
                            if (body.getResultCode() == 0) {
                                doctorListAdapter.setNewData(body.getData());
                            }
                        }
                    }
                });
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_recycle_view;
    }

}
