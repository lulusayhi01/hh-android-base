package com.rong.mbt.personal.adapter;

import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.rong.mbt.personal.R;
import com.rong.mbt.personal.bean.doctor.OfficesFindBean;

/**
 * 文章列表
 */
public class SelectDepartmentListAdapter extends BaseQuickAdapter<OfficesFindBean.DataBean, BaseViewHolder> {


    public SelectDepartmentListAdapter() {
        super(R.layout.item_select_department);
    }

    @Override
    protected void convert(BaseViewHolder helper, OfficesFindBean.DataBean item) {

        try {
            TextView tvName = helper.getView(R.id.tv_name);
            tvName.setText(item.getName());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
