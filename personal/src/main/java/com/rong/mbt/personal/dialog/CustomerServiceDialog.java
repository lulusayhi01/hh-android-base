package com.rong.mbt.personal.dialog;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;

import com.rong.mbt.personal.R;

public class CustomerServiceDialog extends Dialog {

    private Context context;


    public CustomerServiceDialog(Context context) {
        super(context, R.style.dialog_home_event);
        this.context = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_cs);

        findViewById(R.id.dialog_iv_shutdown).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
        findViewById(R.id.dialog_bt_call).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + "440-9879-0989"));
                context.startActivity(intent);
            }
        });

        setCanceledOnTouchOutside(false);
        setCancelable(false);
    }

//    public interface CustomerServiceDialogInter {
//        void dismiss();
//
//        void call();
//    }
//
//    private CustomerServiceDialogInter listener;
//
//    public void CustomerServiceDialogInterListener(CustomerServiceDialogInter listener) {
//        this.listener = listener;
//    }
}
