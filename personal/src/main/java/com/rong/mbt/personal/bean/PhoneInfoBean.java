package com.rong.mbt.personal.bean;

public class PhoneInfoBean {


    /**
     * msg : 手机号码已注册
     * resultCode : 200
     */

    private String msg;
    private int resultCode;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public int getResultCode() {
        return resultCode;
    }

    public void setResultCode(int resultCode) {
        this.resultCode = resultCode;
    }
}
