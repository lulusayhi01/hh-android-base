package com.rong.mbt.personal.activity;


import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.alibaba.fastjson.JSON;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.callback.StringCallback;
import com.lzy.okgo.model.Response;
import com.rong.mbt.personal.R;
import com.rong.mbt.personal.activity.base.PBaseActivity;
import com.rong.mbt.personal.adapter.MyCommunityActAdapter;
import com.rong.mbt.personal.bean.ChilrenBean;
import com.rong.mbt.personal.bean.MyCommunityBean;

import java.util.ArrayList;
import java.util.List;

public class MyCommunityActivity extends PBaseActivity {


    private MyCommunityActAdapter adapter;
    private String mbtToken;
    private TextView tvLeft;
    private TextView tvRight;

    @Override
    public int getLayoutId() {
        return R.layout.activity_my_community;
    }

    @Override
    public void initView() {
        super.initView();
        mbtToken = getIntent().getStringExtra("mbtToken");
        find();
        listener();
        initnet();
    }

    private void initnet() {
        OkGo.<String>post("http://service.jiangaifen.com:38082/user/listCommunityUser")
                .tag(this)
//                            .isMultipart(true)
                .headers("mbtToken", mbtToken)
                .execute(new StringCallback() {
                    @Override
                    public void onSuccess(Response<String> response) {
                        Log.i("test", "onSuccess  " + response.body());


                        try {
                            MyCommunityBean myCommunityBean = JSON.parseObject(response.body(), MyCommunityBean.class);
                            String jsonString = JSON.toJSONString(myCommunityBean.getData().getChildren());
                            forChilren(jsonString);

                            Log.i("test", "chilrenBeanList " + chilrenBeanList.size());
                            if (myCommunityBean.getResultCode() == 0) {
                                MyCommunityBean.DataBean.ParentBean parent = myCommunityBean.getData().getParent();
                                adapter.setChild(chilrenBeanList);
                                adapter.setParent(parent);
                                if (!chilrenBeanList.isEmpty()) {
                                    tvRight.setText("我的社区(" + chilrenBeanList.size() + ")人");
                                }
                                if (parent != null) {
                                    tvLeft.setText("社区直推(1)人");
                                }
                            }
                        } catch (Exception e) {

                        }
                    }

                    @Override
                    public void onError(Response<String> response) {
                        super.onError(response);
                        Log.i("test", "onError  " + response.message());
                    }
                });

    }

    private List<ChilrenBean> chilrenBeanList = new ArrayList<>();

    private void forChilren(String string) {
        List<ChilrenBean> chilrenBeans = JSON.parseArray(string, ChilrenBean.class);
        for (int i = 0; i < chilrenBeans.size(); i++) {
            ChilrenBean myCommunityBean = JSON.parseObject(JSON.toJSONString(chilrenBeans.get(i)), ChilrenBean.class);
            chilrenBeanList.add(myCommunityBean);
            if (myCommunityBean.getChildren().size() > 0) {
                String jsonString = JSON.toJSONString(myCommunityBean.getChildren());
                forChilren(jsonString);

            }
        }
    }

    private void find() {
        tvLeft = findViewById(R.id.head_blue_tv_center);
        tvRight = findViewById(R.id.head_blue_tv_right);

        RecyclerView mRecycler = findViewById(R.id.act_mc_recycler);
        LinearLayoutManager manager = new LinearLayoutManager(this);
        mRecycler.setLayoutManager(manager);

        adapter = new MyCommunityActAdapter(MyCommunityActivity.this);
        mRecycler.setAdapter(adapter);
    }

    private void listener() {
        findViewById(R.id.head_blue_iv_left).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

    }
}
