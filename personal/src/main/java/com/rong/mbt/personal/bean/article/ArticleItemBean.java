package com.rong.mbt.personal.bean.article;

import java.io.Serializable;
import java.util.List;

/**
 * 文章实体
 */
public class ArticleItemBean implements Serializable {

    /**
     * imgUrlList : [{"creattime":1567258860000,"id":17,"imgUrl":"http://oss-aliyun-liululu.oss-cn-shanghai.aliyuncs.com/upload/156725886425010133.jpg","objId":15,"objType":1,"status":1,"type":1},{"creattime":1567258860000,"id":18,"imgUrl":"http://oss-aliyun-liululu.oss-cn-shanghai.aliyuncs.com/upload/156725886499110249.jpg","objId":15,"objType":1,"status":1,"type":1},{"creattime":1567258860000,"id":19,"imgUrl":"http://oss-aliyun-liululu.oss-cn-shanghai.aliyuncs.com/upload/156725886504710083.jpg","objId":15,"objType":1,"status":1,"type":1}]
     * userViewLikeCommentList : [{"content":"测试评论呢看附近啊是打开了附近的时刻了饭看电视了附近开了电视减肥看电视了就分开了电视减肥看啦都是减肥里看电视放进口袋上了饭就打开了谁附近的时刻了附近的时刻了奋斗开始了就分开了电视见风使舵","creattime":1567259992000,"id":8,"objId":15,"objType":1,"status":1,"type":2,"userId":82}]
     * imgUrlListSize : 3
     * article : {"categoryId":1,"content":"放大了空间饭都快累死饭看电视了饭的时间看了饭的时刻了饭就打开了谁附近的时刻了饭的时刻了附近开了电视就分开了电视饭就困了电视附近开了电视饭就困了电视分开了电视饭都快累死饭都快累死姐姐分开的零食附近开了电视放进口袋零食附近开了电视就分开了电视放的路上看见放看电视了附近里看电视分开的零食附近的森林\n\n\n附近的时刻了附近的时刻了饭的时间看了饭的时刻了附近的快累死饭的就是看了饭的时间看了饭的时刻了饭就打开死了附近的时刻了减肥的开始了减肥的时刻了饭的时间看了饭的时刻了饭都结束了饭的时间里看放电视了饭的时刻了饭的时间都是电视了饭","creattime":1567258860000,"id":15,"pageImg":"http://oss-aliyun-liululu.oss-cn-shanghai.aliyuncs.com/upload/156725886425010133.jpg","status":1,"summary":"https://hh-liululu.oss-cn-beijing.aliyuncs.com/img-dafault%20.png","title":"养生。我们都都在为你付出的人和你一起去努力争取的人和你一起努力一起来支持","userid":82}
     * userViewLikeCommentListSize : 1
     * user : {"belongcode":"s3u5CJgt","channelid":0,"coinstatus":1,"coldmoney":"0","creattime":"2019-07-24 14:31:54","fetchmoney":"0","headimage":"https://hh-liululu.oss-cn-beijing.aliyuncs.com/default/default-ys001.jpg","id":82,"investmoney":"0","inviterid":0,"level":0,"loginnum":0,"logintime":"1563949914","name":"蓝二哥哥","passwd":"662919d207123aa79147b5fc3d54831f","phone":"13501031194","rapnum":"0","registermoney":"10","remark":"","returnmoney":"0","sex":0,"status":1,"summoney":"10","token":"1T27m3FUOURGljkNtHDiyVGFpQlNjXOfWZ2l03ptw05nufSd7dM0JuLP8awIQJPQHsl1uCyG/7wHloUAU5sRyw==","type":1,"waitactivemoney":"0"}
     */

    private int imgUrlListSize;
    private ArticleBean article;
    private int userViewLikeCommentListSize;
    private UserBean user;
    private List<ImgUrlListBean> imgUrlList;
    private List<UserViewLikeCommentListBean> userViewLikeCommentList;

    public int getImgUrlListSize() {
        return imgUrlListSize;
    }

    public void setImgUrlListSize(int imgUrlListSize) {
        this.imgUrlListSize = imgUrlListSize;
    }

    public ArticleBean getArticle() {
        return article;
    }

    public void setArticle(ArticleBean article) {
        this.article = article;
    }

    public int getUserViewLikeCommentListSize() {
        return userViewLikeCommentListSize;
    }

    public void setUserViewLikeCommentListSize(int userViewLikeCommentListSize) {
        this.userViewLikeCommentListSize = userViewLikeCommentListSize;
    }

    public UserBean getUser() {
        return user;
    }

    public void setUser(UserBean user) {
        this.user = user;
    }

    public List<ImgUrlListBean> getImgUrlList() {
        return imgUrlList;
    }

    public void setImgUrlList(List<ImgUrlListBean> imgUrlList) {
        this.imgUrlList = imgUrlList;
    }

    public List<UserViewLikeCommentListBean> getUserViewLikeCommentList() {
        return userViewLikeCommentList;
    }

    public void setUserViewLikeCommentList(List<UserViewLikeCommentListBean> userViewLikeCommentList) {
        this.userViewLikeCommentList = userViewLikeCommentList;
    }

    public static class ArticleBean implements Serializable{
        /**
         * categoryId : 1
         * content : 放大了空间饭都快累死饭看电视了饭的时间看了饭的时刻了饭就打开了谁附近的时刻了饭的时刻了附近开了电视就分开了电视饭就困了电视附近开了电视饭就困了电视分开了电视饭都快累死饭都快累死姐姐分开的零食附近开了电视放进口袋零食附近开了电视就分开了电视放的路上看见放看电视了附近里看电视分开的零食附近的森林
         * <p>
         * <p>
         * 附近的时刻了附近的时刻了饭的时间看了饭的时刻了附近的快累死饭的就是看了饭的时间看了饭的时刻了饭就打开死了附近的时刻了减肥的开始了减肥的时刻了饭的时间看了饭的时刻了饭都结束了饭的时间里看放电视了饭的时刻了饭的时间都是电视了饭
         * creattime : 1567258860000
         * id : 15
         * pageImg : http://oss-aliyun-liululu.oss-cn-shanghai.aliyuncs.com/upload/156725886425010133.jpg
         * status : 1
         * summary : https://hh-liululu.oss-cn-beijing.aliyuncs.com/img-dafault%20.png
         * title : 养生。我们都都在为你付出的人和你一起去努力争取的人和你一起努力一起来支持
         * userid : 82
         */

        private int categoryId;
        private String content;
        private long creattime;
        private int id;
        private String pageImg;
        private int status;
        private String summary;
        private String title;
        private int userid;

        public int getCategoryId() {
            return categoryId;
        }

        public void setCategoryId(int categoryId) {
            this.categoryId = categoryId;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public long getCreattime() {
            return creattime;
        }

        public void setCreattime(long creattime) {
            this.creattime = creattime;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getPageImg() {
            return pageImg;
        }

        public void setPageImg(String pageImg) {
            this.pageImg = pageImg;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public String getSummary() {
            return summary;
        }

        public void setSummary(String summary) {
            this.summary = summary;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public int getUserid() {
            return userid;
        }

        public void setUserid(int userid) {
            this.userid = userid;
        }
    }

    public static class UserBean implements Serializable{
        /**
         * belongcode : s3u5CJgt
         * channelid : 0
         * coinstatus : 1
         * coldmoney : 0
         * creattime : 2019-07-24 14:31:54
         * fetchmoney : 0
         * headimage : https://hh-liululu.oss-cn-beijing.aliyuncs.com/default/default-ys001.jpg
         * id : 82
         * investmoney : 0
         * inviterid : 0
         * level : 0
         * loginnum : 0
         * logintime : 1563949914
         * name : 蓝二哥哥
         * passwd : 662919d207123aa79147b5fc3d54831f
         * phone : 13501031194
         * rapnum : 0
         * registermoney : 10
         * remark :
         * returnmoney : 0
         * sex : 0
         * status : 1
         * summoney : 10
         * token : 1T27m3FUOURGljkNtHDiyVGFpQlNjXOfWZ2l03ptw05nufSd7dM0JuLP8awIQJPQHsl1uCyG/7wHloUAU5sRyw==
         * type : 1
         * waitactivemoney : 0
         */

        private String belongcode;
        private int channelid;
        private int coinstatus;
        private String coldmoney;
        private String creattime;
        private String fetchmoney;
        private String headimage;
        private int id;
        private String investmoney;
        private int inviterid;
        private int level;
        private int loginnum;
        private String logintime;
        private String name;
        private String passwd;
        private String phone;
        private String rapnum;
        private String registermoney;
        private String remark;
        private String returnmoney;
        private int sex;
        private int status;
        private String summoney;
        private String token;
        private int type;
        private String waitactivemoney;

        public String getBelongcode() {
            return belongcode;
        }

        public void setBelongcode(String belongcode) {
            this.belongcode = belongcode;
        }

        public int getChannelid() {
            return channelid;
        }

        public void setChannelid(int channelid) {
            this.channelid = channelid;
        }

        public int getCoinstatus() {
            return coinstatus;
        }

        public void setCoinstatus(int coinstatus) {
            this.coinstatus = coinstatus;
        }

        public String getColdmoney() {
            return coldmoney;
        }

        public void setColdmoney(String coldmoney) {
            this.coldmoney = coldmoney;
        }

        public String getCreattime() {
            return creattime;
        }

        public void setCreattime(String creattime) {
            this.creattime = creattime;
        }

        public String getFetchmoney() {
            return fetchmoney;
        }

        public void setFetchmoney(String fetchmoney) {
            this.fetchmoney = fetchmoney;
        }

        public String getHeadimage() {
            return headimage;
        }

        public void setHeadimage(String headimage) {
            this.headimage = headimage;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getInvestmoney() {
            return investmoney;
        }

        public void setInvestmoney(String investmoney) {
            this.investmoney = investmoney;
        }

        public int getInviterid() {
            return inviterid;
        }

        public void setInviterid(int inviterid) {
            this.inviterid = inviterid;
        }

        public int getLevel() {
            return level;
        }

        public void setLevel(int level) {
            this.level = level;
        }

        public int getLoginnum() {
            return loginnum;
        }

        public void setLoginnum(int loginnum) {
            this.loginnum = loginnum;
        }

        public String getLogintime() {
            return logintime;
        }

        public void setLogintime(String logintime) {
            this.logintime = logintime;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getPasswd() {
            return passwd;
        }

        public void setPasswd(String passwd) {
            this.passwd = passwd;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getRapnum() {
            return rapnum;
        }

        public void setRapnum(String rapnum) {
            this.rapnum = rapnum;
        }

        public String getRegistermoney() {
            return registermoney;
        }

        public void setRegistermoney(String registermoney) {
            this.registermoney = registermoney;
        }

        public String getRemark() {
            return remark;
        }

        public void setRemark(String remark) {
            this.remark = remark;
        }

        public String getReturnmoney() {
            return returnmoney;
        }

        public void setReturnmoney(String returnmoney) {
            this.returnmoney = returnmoney;
        }

        public int getSex() {
            return sex;
        }

        public void setSex(int sex) {
            this.sex = sex;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public String getSummoney() {
            return summoney;
        }

        public void setSummoney(String summoney) {
            this.summoney = summoney;
        }

        public String getToken() {
            return token;
        }

        public void setToken(String token) {
            this.token = token;
        }

        public int getType() {
            return type;
        }

        public void setType(int type) {
            this.type = type;
        }

        public String getWaitactivemoney() {
            return waitactivemoney;
        }

        public void setWaitactivemoney(String waitactivemoney) {
            this.waitactivemoney = waitactivemoney;
        }
    }

    public static class ImgUrlListBean implements Serializable{
        /**
         * creattime : 1567258860000
         * id : 17
         * imgUrl : http://oss-aliyun-liululu.oss-cn-shanghai.aliyuncs.com/upload/156725886425010133.jpg
         * objId : 15
         * objType : 1
         * status : 1
         * type : 1
         */

        private long creattime;
        private int id;
        private String imgUrl;
        private int objId;
        private int objType;
        private int status;
        private int type;

        public long getCreattime() {
            return creattime;
        }

        public void setCreattime(long creattime) {
            this.creattime = creattime;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getImgUrl() {
            return imgUrl;
        }

        public void setImgUrl(String imgUrl) {
            this.imgUrl = imgUrl;
        }

        public int getObjId() {
            return objId;
        }

        public void setObjId(int objId) {
            this.objId = objId;
        }

        public int getObjType() {
            return objType;
        }

        public void setObjType(int objType) {
            this.objType = objType;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public int getType() {
            return type;
        }

        public void setType(int type) {
            this.type = type;
        }
    }

    public static class UserViewLikeCommentListBean implements Serializable{
        /**
         * content : 测试评论呢看附近啊是打开了附近的时刻了饭看电视了附近开了电视减肥看电视了就分开了电视减肥看啦都是减肥里看电视放进口袋上了饭就打开了谁附近的时刻了附近的时刻了奋斗开始了就分开了电视见风使舵
         * creattime : 1567259992000
         * id : 8
         * objId : 15
         * objType : 1
         * status : 1
         * type : 2
         * userId : 82
         */

        private String content;
        private long creattime;
        private int id;
        private int objId;
        private int objType;
        private int status;
        private int type;
        private int userId;

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public long getCreattime() {
            return creattime;
        }

        public void setCreattime(long creattime) {
            this.creattime = creattime;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getObjId() {
            return objId;
        }

        public void setObjId(int objId) {
            this.objId = objId;
        }

        public int getObjType() {
            return objType;
        }

        public void setObjType(int objType) {
            this.objType = objType;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public int getType() {
            return type;
        }

        public void setType(int type) {
            this.type = type;
        }

        public int getUserId() {
            return userId;
        }

        public void setUserId(int userId) {
            this.userId = userId;
        }
    }
}
