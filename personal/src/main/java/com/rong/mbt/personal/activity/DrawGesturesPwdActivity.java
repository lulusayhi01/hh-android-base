package com.rong.mbt.personal.activity;

import android.content.SharedPreferences;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.android.arouter.launcher.ARouter;
import com.rong.mbt.personal.R;
import com.rong.mbt.personal.activity.base.PBaseActivity;
import com.rong.mbt.personal.view.GestureLockViewGroup;

import java.util.ArrayList;
import java.util.List;

//绘制手势密码
public class DrawGesturesPwdActivity extends PBaseActivity {

    private GestureLockViewGroup gestureLock;
    private TextView tvInfo;

    private List<Integer> mChoose = new ArrayList<Integer>();
    private String goWhygo;
    private String gesturesPwd; //手势密码
    private TextView tvLogin;
    private SharedPreferences.Editor edit;
    private SharedPreferences sp;

    @Override
    public int getLayoutId() {
        return R.layout.activity_draw_gestures_pwd;
    }

    @Override
    public void initView() {
        super.initView();
        sp = getSharedPreferences("config", MODE_PRIVATE);
        edit = sp.edit();
        boolean geSturesLineShow = getIntent().getBooleanExtra("geSturesLineShow", false);

        gesturesPwd = sp.getString("gesturesPwd", "");


        goWhygo = getIntent().getStringExtra("goWhygo");  //loginGoMain


        find();
        listener();
        if (goWhygo != null) {
            tvInfo.setText("请绘制解锁密码");
            tvLogin.setVisibility(View.VISIBLE);
        }
        gestureLock.setLineIsShow(geSturesLineShow);
    }

    private void listener() {
        findViewById(R.id.act_dgp_iv).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        gestureLock.setOnGestureLockViewListener(new GestureLockViewGroup.OnGestureLockViewListener() {
            @Override
            public void onBlockSelected(int cId) {
            }

            @Override
            public void onGestureEvent(boolean matched, List<Integer> choose) {


                if (!gesturesPwd.isEmpty() && !choose.toString().isEmpty() && choose.toString().equals(gesturesPwd)) {
                    if (goWhygo != null && goWhygo.equals("loginGoMain")) {
                        // cn.rongcloud.im.ui.activity.MainActivity
                        //跳转到mainactivity
                        ARouter.getInstance().build("/activity/main").navigation();
                        finish();
                        return;
                    }
                }

                if (goWhygo == null) {
                    if (mChoose.isEmpty()) {
                        mChoose.addAll(choose);
                        tvInfo.setText("再次输入以确认");
                    } else {
                        if (listCompare(mChoose, choose)) {
                            edit.putString("gesturesPwd", choose.toString());
                            edit.commit();
                            finish();
                        } else {
                            mChoose.clear();
                            tvInfo.setText("请绘制新的手势密码");
                        }
                    }
                }
            }

            @Override
            public void onUnmatchedExceedBoundary() {
            }
        });

        //goLogin
        tvLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (sp != null && edit != null) {
                    edit.putBoolean("GestureIsOpen", false);
                    edit.commit();
                    ARouter.getInstance().build("/acitvity/login").navigation();
                    finish();
                } else {
                    Toast.makeText(DrawGesturesPwdActivity.this, "请重新登录", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void find() {
        gestureLock = findViewById(R.id.act_dgp_gesturelock);
        tvInfo = findViewById(R.id.act_dgp_tv);

        tvLogin = findViewById(R.id.act_dgp_tv_login);
    }


    private boolean listCompare(List<Integer> i1, List<Integer> i2) {
        if (i1.size() != i2.size()) {
            return false;
        }

        for (int i = 0; i < i1.size(); i++) {
            if (i1.get(i) != i2.get(i)) {
                return false;
            }
        }
        return true;
    }
}
