package com.rong.mbt.personal.activity;


import android.content.Intent;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.TextAppearanceSpan;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.alibaba.android.arouter.launcher.ARouter;
import com.alibaba.fastjson.JSON;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.callback.StringCallback;
import com.lzy.okgo.model.Response;
import com.rong.mbt.personal.R;
import com.rong.mbt.personal.activity.base.PBaseActivity;
import com.rong.mbt.personal.bean.base.BaseBean;
import com.rong.mbt.personal.bean.PersonalInfoBean;
import com.rong.mbt.personal.http.UrlApi;
import com.rong.mbt.personal.utils.StringUtil;

public class MonkeyPackageActivity extends PBaseActivity {


    private PersonalInfoBean infoBean;
    private String mbtToken;
    private TextView tvMoney;
    private RelativeLayout rtRecharge;
    private RelativeLayout rtWithdrawa;
    private TextView tvMoneyOther;
    private String phone;
    private String name;

    @Override
    public int getLayoutId() {
        return R.layout.activity_monkey_package;
    }

    @Override
    public void initView() {
        super.initView();
        mbtToken = getIntent().getStringExtra("mbtToken");
        phone = getIntent().getStringExtra("phone");
        name = getIntent().getStringExtra("name");
        init();
        listener();
    }

    @Override
    public void onStart() {
        super.onStart();
        initNet();
    }

    private void initNet() {
        OkGo.<String>get(UrlApi.userGetUser)
                .tag(this)
                .headers("mbtToken", mbtToken)
                .execute(new StringCallback() {
                    @Override
                    public void onSuccess(Response<String> response) {
                        Log.i("test", "onSuccess getUser  " + response.body());
                        if (!response.body().isEmpty()) {
                            infoBean = JSON.parseObject(response.body(), PersonalInfoBean.class);
                            if (infoBean == null || infoBean.getData() == null) {
                                return;
                            }

                            SpannableString spannableString = null;
                            if (infoBean.getData().getUsablemoney() != null) {
                                String str = StringUtil.formatString(Double.parseDouble(infoBean.getData().getUsablemoney()));
                                spannableString = new SpannableString("余额\r\n" + str);
                            } else {
                                spannableString = new SpannableString("余额\r\n0.00");
                            }
                            spannableString.setSpan(new TextAppearanceSpan(MonkeyPackageActivity.this, R.style.dp_13), 0, 2, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                            spannableString.setSpan(new TextAppearanceSpan(MonkeyPackageActivity.this, R.style.dp_20), spannableString.length() - 3, spannableString.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                            tvMoney.setText(spannableString);

                        }
                    }

                    @Override
                    public void onError(Response<String> response) {
                        super.onError(response);
                        Log.i("test", "onError  " + response.body());
                    }
                });


        OkGo.<String>get(UrlApi.coinGetCoinCountByUser)
                .tag(this)
                .headers("mbtToken", mbtToken)
                .params("coinid", "1")
                .execute(new StringCallback() {
                    @Override
                    public void onSuccess(Response<String> response) {
                        try {
                            BaseBean baseBean = JSON.parseObject(response.body(), BaseBean.class);
                            if (baseBean.getResultCode() == 0) {
                                SpannableString spannableStringother = null;
                                if (infoBean != null && baseBean.getData() != null && infoBean.getData().getWaitactivemoney() != null) {
                                    String str = StringUtil.formatString(Double.parseDouble(baseBean.getData()));
                                    spannableStringother = new SpannableString("SDA币\r\n" + str);
                                } else {
                                    spannableStringother = new SpannableString("SDA币\r\n0.00");
                                }
                                spannableStringother.setSpan(new TextAppearanceSpan(MonkeyPackageActivity.this, R.style.dp_13), 0, 4, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                                spannableStringother.setSpan(new TextAppearanceSpan(MonkeyPackageActivity.this, R.style.dp_20), spannableStringother.length() - 3, spannableStringother.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                                tvMoneyOther.setText(spannableStringother);
                            }
                        } catch (Exception e) {
                        }
                    }
                });
    }

    public void init() {
        tvMoney = findViewById(R.id.tv_money);
        tvMoneyOther = findViewById(R.id.tv_money_other);

        rtRecharge = findViewById(R.id.fragment_personal_relative_recharge);
        rtWithdrawa = findViewById(R.id.fragment_personal_relative_withdrawal);

    }

    private void listener() {

        //充值
        rtRecharge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Intent intent = new Intent(MonkeyPackageActivity.this, RechargeActivity.class);
                Intent intent = new Intent(MonkeyPackageActivity.this, Recharge2CopyUserActivity.class);
                intent.putExtra("mbtToken", mbtToken);
                startActivity(intent);
            }
        });
        //提现
        rtWithdrawa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MonkeyPackageActivity.this, WithdrawalActivityAList.class);
                intent.putExtra("mbtToken", mbtToken);
                startActivity(intent);
            }
        });
        //账单
        findViewById(R.id.fragment_personal_line_bill).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MonkeyPackageActivity.this, BillActivity.class);
                intent.putExtra("mbtToken", mbtToken);
                startActivity(intent);
            }
        });
        findViewById(R.id.act_mp_iv_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        //交易
        findViewById(R.id.fragment_personal_line_trading).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ARouter.getInstance().build("/deal/TansActionRecordActivity").navigation();
            }
        });
        //币转美元
        findViewById(R.id.fragment_personal_line_btomy).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MonkeyPackageActivity.this, CoinToDollarActivity.class);
                intent.putExtra("mbtToken", mbtToken);
                startActivity(intent);
            }
        });
        //体现账户
        findViewById(R.id.fragment_personal_line_tixian).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MonkeyPackageActivity.this, CashAccountListActivity.class);
                intent.putExtra("mbtToken", mbtToken);
                intent.putExtra("phone",phone);
                intent.putExtra("name",name);
                startActivity(intent);
            }
        });
    }
}
