package com.rong.mbt.personal.activity;

import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.fastjson.JSON;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.callback.StringCallback;
import com.lzy.okgo.model.Response;
import com.rong.mbt.personal.R;
import com.rong.mbt.personal.activity.base.PBaseActivity;
import com.rong.mbt.personal.bean.base.BaseBean;
import com.rong.mbt.personal.bean.QRCodeBean;
import com.rong.mbt.personal.utils.ImageUtil;

//充值
@Route(path = "/personal/RechargeActivity")

public class RechargeActivity extends PBaseActivity {


    private String mbtToken;
    private EditText etMonkey;
    private ImageView ivQrcode;
    private TextView tvRight;

//    @Override
//    protected void onCreate(@Nullable Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_recharge);
//
//    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_recharge_2;
    }

    @Override
    public void initView() {
        super.initView();
        mbtToken = getIntent().getStringExtra("mbtToken");

        find();
        listener();
    }

    private void listener() {
        findViewById(R.id.head_blue_iv_left).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }

        });
        //充值记录
        tvRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(RechargeActivity.this, RechargeRecordActivity.class);
                intent.putExtra("mbtToken",mbtToken);
                startActivity(intent);
            }
        });

        findViewById(R.id.act_r_bt_withdrawal).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (etMonkey.getText().toString().isEmpty()) {
                    return;
                }
                OkGo.<String>post("http://service.jiangaifen.com:38082/user/addMon")
                        .tag(this)
                        .headers("mbtToken", mbtToken)
                        .params("price", etMonkey.getText().toString())
                        .execute(new StringCallback() {
                            @Override
                            public void onSuccess(Response<String> response) {
                                BaseBean baseBean = JSON.parseObject(response.body(), BaseBean.class);
                                if (baseBean.isSuccess()) {
                                    rechargeQrCode();
                                }
                            }

                            @Override
                            public void onError(Response<String> response) {
                                super.onError(response);
                                Log.i("test", "onError  " + response.body());
                            }
                        });
            }
        });
    }

    //充值二维码
    private void rechargeQrCode() {
        OkGo.<String>post("http://service.jiangaifen.com:38082/user/showPriceQrCode")
                .tag(this)
                .headers("mbtToken", mbtToken)
                .params("price", etMonkey.getText().toString())
                .execute(new StringCallback() {
                    @Override
                    public void onSuccess(Response<String> response) {
                        Log.i("test", "onSuccess    " + response.body());
                        QRCodeBean baseBean = JSON.parseObject(response.body(), QRCodeBean.class);
                        if (baseBean.getResultCode() == 0 && !baseBean.getData().isEmpty()) {
                            ImageUtil.loadImage(RechargeActivity.this
                                    , baseBean.getData(), ivQrcode);
                        }
                    }

                    @Override
                    public void onError(Response<String> response) {
                        super.onError(response);
                        Log.i("test", "onError  " + response.body());
                    }
                });
    }

    private void find() {
        etMonkey = findViewById(R.id.act_r_et_mokey);
        ivQrcode = findViewById(R.id.act_r_iv_qrcode);

        TextView tvCenter = findViewById(R.id.head_blue_tv_center);
        tvCenter.setText("账户充值");
        findViewById(R.id.head_blue_iv_right).setVisibility(View.GONE);

        tvRight = findViewById(R.id.head_blue_tv_right);
        tvRight.setText("充值记录");
    }
}
