package com.rong.mbt.personal.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.alibaba.fastjson.JSONObject;
import com.calanger.hh.entity.ResultView;
import com.calanger.hh.model.Doctor;
import com.calanger.hh.model.Notice;
import com.calanger.hh.model.OrderPre;
import com.calanger.hh.model.User;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.model.Response;
import com.rong.mbt.personal.R;
import com.rong.mbt.personal.activity.ArticleDetailsActivity;
import com.rong.mbt.personal.activity.PatientDetailActivity;
import com.rong.mbt.personal.adapter.FindDoctorListAdapter;
import com.rong.mbt.personal.adapter.IndexArticleListAdapter;
import com.rong.mbt.personal.adapter.IndexOrderListAdapter;
import com.rong.mbt.personal.adapter.NoticeBySystemListAdapter;
import com.rong.mbt.personal.adapter.NoticeByUserListAdapter;
import com.rong.mbt.personal.bean.article.ArticleItemBean;
import com.rong.mbt.personal.bean.article.ArticleListPageBean;
import com.rong.mbt.personal.http.UrlApi;
import com.rong.mbt.personal.http.callback.JsonCallback;
import com.rong.mbt.personal.utils.SpUtils;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshLoadMoreListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 文章列表
 */
public class NoticeItemFragment extends Fragment implements OnRefreshLoadMoreListener {

    private String mTitle;
    private Integer code;
    private RecyclerView mRecyclerView;
    private SmartRefreshLayout mRefreshLayout;
    private NoticeBySystemListAdapter noticeBySystemListAdapter;
    private NoticeByUserListAdapter noticeByUserListAdapter;
    private int pageNumber;
    private boolean freshOrMore = false;
    private List<Map<String,Object>> initData = new ArrayList<Map<String,Object>>();

    private int total_count = 0;
    private int total_count_doctor = 0;

    public static NoticeItemFragment getInstance(String title, Integer code) {
        NoticeItemFragment sf = new NoticeItemFragment();
        sf.mTitle = title;
        sf.code = code;
        return sf;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        return super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_smart_refresh_layout, null);
        initView(view);
        return view;
    }

    private void initView(View view) {
        mRecyclerView = view.findViewById(R.id.recyclerView);
        mRefreshLayout = view.findViewById(R.id.refreshLayout);

        mRefreshLayout.setOnRefreshLoadMoreListener(this);
        noticeBySystemListAdapter = new NoticeBySystemListAdapter();
        noticeByUserListAdapter = new NoticeByUserListAdapter(getActivity());

        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        noticeBySystemListAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                Map<String,Object> item = (Map<String,Object>) adapter.getItem(position);
                JSONObject jo = new JSONObject(item);
                Notice notice = jo.getObject("notice", Notice.class);
                User user = jo.getObject("user", User.class);
                Doctor doctor = jo.getObject("doctor", Doctor.class);
                Integer userViewLikeCommentListSize = jo.getObject("userViewLikeCommentListSize", Integer.class);
//                startActivity(new Intent(getActivity(), NoticleActionActivity.class).putExtra("id", notice));
            }
        });
        noticeByUserListAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                Map<String,Object> item = (Map<String,Object>) adapter.getItem(position);
                JSONObject jo = new JSONObject(item);
                Notice notice = jo.getObject("notice", Notice.class);
                User user = jo.getObject("user", User.class);
                Doctor doctor = jo.getObject("doctor", Doctor.class);
//                startActivity(new Intent(getActivity(), NoticleActionActivity.class).putExtra("id", notice));
            }
        });

        if(code==0){
            mRecyclerView.setAdapter(noticeBySystemListAdapter);
        }else{
            mRecyclerView.setAdapter(noticeByUserListAdapter);
        }

        requestArticle();

    }

    private void requestArticle() {
            OkGo.<ResultView>get(UrlApi.noticeList)
                    .tag(this)
                    .headers("mbtToken", SpUtils.getToken(getActivity()))
                    .params("type",code)
                    .params("pageNumber", pageNumber)
                    .execute(new JsonCallback<ResultView>() {
                        @Override
                        public void onSuccess(Response<ResultView> response) {
                            if (response != null && response.body() != null) {
                                ResultView resultView = response.body();
                                total_count = resultView.getCount();
                                if (resultView.getData() != null) {
                                    List<Map<String,Object>> data = (List<Map<String,Object>>) resultView.getData();
                                    if (freshOrMore) {
                                        if(initData.size()>= total_count){
                                            mRefreshLayout.finishLoadMore();
                                        }else{
                                            initData.addAll(data);
                                        }
                                        freshOrMore = false;
                                        mRefreshLayout.finishLoadMore();
                                    } else {
                                        initData = data;
                                        mRefreshLayout.finishRefresh();
                                    }

                                    if(code==0){
                                        noticeBySystemListAdapter.setNewData(initData);
                                    }else{
                                        noticeByUserListAdapter.setNewData(initData);
                                    }
                                }
                                mRefreshLayout.finishRefresh();
                                mRefreshLayout.finishLoadMore();
                            }
                        }
                    });

        }

    @Override
    public void onLoadMore(@NonNull RefreshLayout refreshLayout) {
        pageNumber++;
        freshOrMore = true;
        requestArticle();

    }

    @Override
    public void onRefresh(@NonNull RefreshLayout refreshLayout) {
        pageNumber = 1;
        requestArticle();
    }
}
