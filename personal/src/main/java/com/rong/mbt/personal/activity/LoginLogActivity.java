package com.rong.mbt.personal.activity;


import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.alibaba.fastjson.JSON;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.callback.StringCallback;
import com.lzy.okgo.model.Response;
import com.rong.mbt.personal.R;
import com.rong.mbt.personal.activity.base.PBaseActivity;
import com.rong.mbt.personal.adapter.LoginLogAdapter;
import com.rong.mbt.personal.bean.LoginLogBean;

//登录日志
public class LoginLogActivity extends PBaseActivity {

    private RecyclerView mRecycler;
    private LoginLogAdapter adapter;
    private String mbtToken;
    private String phone;

    private int PAGENUM = 1;
    private int PAGESIZE = 20;
    private LinearLayoutManager manager;
    private boolean loading;

    @Override
    public int getLayoutId() {
        return R.layout.activity_login_log;
    }

    @Override
    public void initView() {
        super.initView();
        mbtToken = getIntent().getStringExtra("mbtToken");
        phone = getIntent().getStringExtra("phone");
        find();
        listener();
        net();
    }

    private void net() {
        OkGo.<String>post("http://service.jiangaifen.com:38082/user/loginLog")
                .tag(this)
//                            .isMultipart(true)
                .headers("mbtToken", mbtToken)
                .params("pageNum", PAGENUM)
                .params("pageSize", PAGESIZE)
                .execute(new StringCallback() {
                    @Override
                    public void onSuccess(Response<String> response) {

                        Log.i("test", "onSuccess  " + response.body());
                        LoginLogBean logbean = JSON.parseObject(response.body(), LoginLogBean.class);
                        if (logbean.getResultCode() == 0 && !logbean.getData().isEmpty()) {
                            adapter.setData(logbean.getData());
                            loading = false;
                        }
                    }

                    @Override
                    public void onError(Response<String> response) {
                        super.onError(response);
                        Log.i("test", "onError  " + response.message());
                    }
                });
    }

    private void listener() {
        findViewById(R.id.head_blue_iv_left).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        //加载
        mRecycler.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0) {
                    int visibleItemCount = manager.getChildCount();
                    int totalItemCount = manager.getItemCount();
                    int pastVisiblesItems = manager.findFirstVisibleItemPosition();

                    if (!loading && (visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                        loading = !loading;
                        PAGENUM++;
                        net();
                    }
                }
            }
        });
    }

    private void find() {
        TextView tvTitle = findViewById(R.id.head_blue_tv_center);
        tvTitle.setText("登录日志");
        findViewById(R.id.head_blue_iv_right).setVisibility(View.GONE);
        mRecycler = findViewById(R.id.act_ll_recycler);

        manager = new LinearLayoutManager(this);
        mRecycler.setLayoutManager(manager);

        adapter = new LoginLogAdapter(this);
        mRecycler.setAdapter(adapter);

    }
}
