package com.rong.mbt.personal.activity;

import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.callback.StringCallback;
import com.lzy.okgo.model.Response;
import com.rong.mbt.personal.R;
import com.rong.mbt.personal.activity.base.PBaseActivity;
import com.rong.mbt.personal.bean.base.BaseBean;

//提现
public class WithdrawalActivity extends PBaseActivity {

    private String mbtToken;
    private EditText etMonkey;
    private TextView tvRight;
    private int dealtypeId;

    @Override
    public int getLayoutId() {
        return R.layout.activity_withdrawal;
    }

    @Override
    public void initView() {
        super.initView();
        mbtToken = getIntent().getStringExtra("mbtToken");
        dealtypeId = getIntent().getIntExtra("dealtypeId", -1);


        find();
        listener();
    }

    private void listener() {

        findViewById(R.id.head_blue_iv_left).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        tvRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(WithdrawalActivity.this, WithdrawalRecordActivity.class);
                intent.putExtra("mbtToken", mbtToken);
                startActivity(intent);
            }
        });
        findViewById(R.id.act_p_bt_withdrawal).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (dealtypeId == -1) {
                    return;
                }
                OkGo.<String>post("http://service.jiangaifen.com:38082/user/essayMon")
                        .tag(this)
                        .headers("mbtToken", mbtToken)
                        .params("price", etMonkey.getText().toString())
                        .params("dealtypeid", dealtypeId)
                        .execute(new StringCallback() {
                            @Override
                            public void onSuccess(Response<String> response) {
                                Log.i("test", "essayMon  " + response.body());
                                BaseBean baseBean = JSON.parseObject(response.body(), BaseBean.class);
                                if (baseBean.isSuccess()) {
                                    Toast.makeText(WithdrawalActivity.this, baseBean.getMsg(), Toast.LENGTH_SHORT).show();
                                    finish();
                                } else {
                                    Toast.makeText(WithdrawalActivity.this, baseBean.getMsg(), Toast.LENGTH_SHORT).show();
                                }
                            }

                            @Override
                            public void onError(Response<String> response) {
                                super.onError(response);
                                Log.i("test", "essayMon onError  " + response.body());
                            }
                        });
            }
        });
    }

    private void find() {
        etMonkey = findViewById(R.id.act_p_et);
        TextView tvCenter = findViewById(R.id.head_blue_tv_center);
        tvCenter.setText("账户提现");
        findViewById(R.id.head_blue_iv_right).setVisibility(View.GONE);

        tvRight = findViewById(R.id.head_blue_tv_right);
        tvRight.setText("提现记录");
        tvRight.setVisibility(View.GONE);
    }
}
