package com.rong.mbt.personal.activity;

import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.flyco.tablayout.SlidingTabLayout;
import com.rong.mbt.personal.R;
import com.rong.mbt.personal.activity.base.PBaseActivity;
import com.rong.mbt.personal.adapter.TabPagerAdapter;
import com.rong.mbt.personal.fragment.OrderPreItemFragment;
import com.rong.mbt.personal.fragment.article.ArticleItemFragment;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * 更多文章列表
 */
public class OrderPreListOnPatientActivity extends PBaseActivity {


    private LinearLayout mLlBack;
    private TextView mTxtTitle;
    private View mIvRight;
    private LinearLayout mLlRightMenu;
    private TextView mTxtRight;
    private LinearLayout mLlRightText;
    private SlidingTabLayout mSlidingTabLayout;
    private ViewPager mViewPager;

    private int mode=0;
    private String[] mTitles = {"全部","预约订单","完成订单", "评价订单"};
    private Integer[] codes = {-1,0,1,2};
    private ArrayList<Fragment> mFragments = new ArrayList<>();

    @Override
    public int getLayoutId() {
        return R.layout.activity_patient_order_pre;
    }

    @Override
    public void initView() {
        super.initView();
        mLlBack = findViewById(R.id.ll_back);
        mTxtTitle = findViewById(R.id.txt_title);
        mIvRight = findViewById(R.id.iv_right);
        mLlRightMenu = findViewById(R.id.ll_right_menu);
        mTxtRight = findViewById(R.id.txt_right);
        mLlRightText = findViewById(R.id.ll_right_text);
        mSlidingTabLayout = findViewById(R.id.sliding_tab_layout);
        mViewPager = findViewById(R.id.view_pager);
        mTxtTitle.setText("我的订单");

        mode = getIntent().getIntExtra("mode", -1);

        initTabLayout();
        initListener();
    }

    private void initTabLayout() {
        for(int i=0;i<mTitles.length;i++){
            mFragments.add(OrderPreItemFragment.getInstance(mTitles[i],codes[i]));
        }
        TabPagerAdapter tabPagerAdapter = new TabPagerAdapter(getSupportFragmentManager(), Arrays.asList(mTitles), mFragments);
        mViewPager.setAdapter(tabPagerAdapter);
        mViewPager.setCurrentItem(mode);
        mSlidingTabLayout.setViewPager(mViewPager);
    }

    private void initListener() {
        mLlBack.setOnClickListener(v -> finish());
    }

}
