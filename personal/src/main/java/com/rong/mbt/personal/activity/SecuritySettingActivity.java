package com.rong.mbt.personal.activity;


import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.view.View;
import android.widget.TextView;

import com.rong.mbt.personal.R;
import com.rong.mbt.personal.activity.base.PBaseActivity;

//安全设置
public class SecuritySettingActivity extends PBaseActivity {


    private String mbtToken;
    private String phone;
    private TextView tvGesturesPwd;
    private TextView tvRenLian;

    @Override
    public int getLayoutId() {
        return R.layout.activity_security_setting;

    }

    @Override
    protected void onStart() {
        super.onStart();
        SharedPreferences sp = getSharedPreferences("config", MODE_PRIVATE);
        boolean gestureIsOpen = sp.getBoolean("GestureIsOpen", false);
        if (gestureIsOpen) {
            tvGesturesPwd.setText("已开启");
            tvGesturesPwd.setTextColor(Color.parseColor("#7282FB"));
        } else {
            tvGesturesPwd.setText("未开启");
            tvGesturesPwd.setTextColor(Color.parseColor("#999999"));
        }

        boolean faceIsOpen   = sp.getBoolean("faceIsOpen", false);
        if (faceIsOpen) {
            tvRenLian.setText("已开启");
            tvRenLian.setTextColor(Color.parseColor("#7282FB"));
        } else {
            tvRenLian.setText("未开启");
            tvRenLian.setTextColor(Color.parseColor("#999999"));
        }
    }

    @Override
    public void initView() {
        super.initView();
        mbtToken = getIntent().getStringExtra("mbtToken");
        phone = getIntent().getStringExtra("phone");
        find();
        listener();
    }

    private void listener() {
        findViewById(R.id.head_while_iv_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        //登录密码
        findViewById(R.id.act_ss_line_loginPwd).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(SecuritySettingActivity.this, UpdateLoginPwdActivity.class);
                intent.putExtra("mbtToken", mbtToken);
                intent.putExtra("phone", phone);
                intent.putExtra("startAct", "Security");
                startActivity(intent);
            }
        });

        //手势
        tvGesturesPwd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(SecuritySettingActivity.this, GesturesPwdActivity.class);
                startActivity(intent);
            }
        });
        //交易密码
        findViewById(R.id.act_ss_line_tradingPwd).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(SecuritySettingActivity.this, TradingPwdSettingActivity.class);
                intent.putExtra("mbtToken", mbtToken);
                intent.putExtra("phone", phone);
                startActivity(intent);
            }
        });

        //人脸
        tvRenLian.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(SecuritySettingActivity.this, FaceRecognitionSettingActivity.class);
                intent.putExtra("mbtToken", mbtToken);
                startActivity(intent);
            }
        });
    }

    private void find() {
        TextView tvTitle = findViewById(R.id.head_while_tv_center);
        tvTitle.setText("安全设置");

        tvGesturesPwd = findViewById(R.id.act_ss_shou_gestures_pwd);

        tvRenLian = findViewById(R.id.act_ss_shou_renlian);
    }
}
