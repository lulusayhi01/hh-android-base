package com.rong.mbt.personal.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.just.agentweb.AgentWeb;
import com.rong.mbt.personal.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class HomeDoctorFragment extends Fragment {

//    @BindView(R.id.head_blue_iv_left)
//    ImageView headBlueIvLeft;
//    @BindView(R.id.head_blue_tv_center)
//    TextView headBlueTvCenter;
//    @BindView(R.id.head_blue_iv_right)
//    ImageView headBlueIvRight;
//    @BindView(R.id.head_blue_tv_right)
//    TextView headBlueTvRight;
//    @BindView(R.id.web_view)
//    LinearLayout webView;
//    Unbinder unbinder;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = inflater.inflate(R.layout.fragment_home_doctor, container, false);
        initView(rootView);
        listener(rootView);
        return rootView;
    }

    private void listener(View rootView) {

    }

    private void initView(View rootView) {
        rootView.findViewById(R.id.head_blue_iv_left).setVisibility(View.GONE);
        TextView tvTitle = rootView.findViewById(R.id.head_blue_tv_center);
        tvTitle.setText("首页");
        rootView.findViewById(R.id.head_blue_iv_right).setVisibility(View.GONE);

        LinearLayout webview = rootView.findViewById(R.id.web_view);
        AgentWeb mAgentWeb = AgentWeb.with(getActivity())
                .setAgentWebParent(webview, new LinearLayout.LayoutParams(-1, -1))
                .useDefaultIndicator()
                .createAgentWeb()
                .ready()
                .go("http://www.kantingyong.com:8084/third/11");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }
}
