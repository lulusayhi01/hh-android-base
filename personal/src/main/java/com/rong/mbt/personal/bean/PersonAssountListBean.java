package com.rong.mbt.personal.bean;

import java.util.List;

public class PersonAssountListBean {


    /**
     * count : 2
     * data : [{"account":"6228486958369525","bank":"ABC","createtime":"2018-09-10 17:25:12","id":22,"name":"简飞","status":1,"type":4,"userid":48},{"account":"13922245256","createtime":"2018-09-10 17:12:54","id":21,"name":"简飞","status":1,"type":1,"userid":48}]
     * msg : 请求成功
     * resultCode : 0
     */

    private int count;
    private String msg;
    private int resultCode;
    private List<DataBean> data;

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public int getResultCode() {
        return resultCode;
    }

    public void setResultCode(int resultCode) {
        this.resultCode = resultCode;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * account : 6228486958369525
         * bank : ABC
         * createtime : 2018-09-10 17:25:12
         * id : 22
         * name : 简飞
         * status : 1
         * type : 4
         * userid : 48
         */

        private String account;
        private String bank;
        private String createtime;
        private int id;
        private String name;
        private int status;
        private int type;
        private int userid;

        public String getAccount() {
            return account;
        }

        public void setAccount(String account) {
            this.account = account;
        }

        public String getBank() {
            return bank;
        }

        public void setBank(String bank) {
            this.bank = bank;
        }

        public String getCreatetime() {
            return createtime;
        }

        public void setCreatetime(String createtime) {
            this.createtime = createtime;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public int getType() {
            return type;
        }

        public void setType(int type) {
            this.type = type;
        }

        public int getUserid() {
            return userid;
        }

        public void setUserid(int userid) {
            this.userid = userid;
        }
    }
}
