package com.rong.mbt.personal.bean.order;

import com.rong.mbt.personal.bean.base.BaseBeanVo;
import com.rong.mbt.personal.bean.doctor.DoctorBean;
import com.rong.mbt.personal.bean.user.UserBean;

import java.io.Serializable;
import java.util.List;

/**
 * 订单列表
 */
public class OrderPreListBean extends BaseBeanVo implements Serializable {


    /**
     * count : 2
     * data : [{"orderPre":{"age":"20","content":"如果你已经下定决心, 准备学习和安装 TensorFlow, 你可以略过这些文字, 直接阅读 后面的章节. 不用担心, 你仍然会看到 MNIST -- 在阐述 TensorFlow 的特性时, 我们还会使用 MNIST 作为一个样例.","creattime":1567245699000,"feed":"3","headimg":"http://oss-aliyun-liululu.oss-cn-shanghai.aliyuncs.com/upload/156404160692410487.jpg","id":28,"name":"蓝二哥哥","orderStatus":0,"sex":1}},{"orderPre":{"age":"80","content":"放大了附近的开始了疯狂了多少疯狂了多少家疯狂了多少疯狂了倒计时份健康的零食风口浪尖的时间疯狂了多少份健康的零食飞机离开的时间疯狂了多少积分离开的房间看了多少","creattime":1567246950000,"feed":"3","headimg":"http://oss-aliyun-liululu.oss-cn-shanghai.aliyuncs.com/upload/156404160692410487.jpg","id":29,"name":"蓝二哥哥","orderStatus":0,"sex":1,"userId":82},"user":{"belongcode":"s3u5CJgt","channelid":0,"coinstatus":1,"coldmoney":"0","creattime":"2019-07-24 14:31:54","fetchmoney":"0","headimage":"https://hh-liululu.oss-cn-beijing.aliyuncs.com/default/default-ys001.jpg","id":82,"investmoney":"0","inviterid":0,"level":0,"loginnum":0,"logintime":"1563949914","name":"蓝二哥哥","passwd":"662919d207123aa79147b5fc3d54831f","phone":"13501031194","rapnum":"0","registermoney":"10","remark":"","returnmoney":"0","sex":0,"status":1,"summoney":"10","token":"1T27m3FUOURGljkNtHDiyVGFpQlNjXOfWZ2l03ptw05nufSd7dM0JuLP8awIQJPQHsl1uCyG/7wHloUAU5sRyw==","type":1,"waitactivemoney":"0"}}]
     * msg : 请求成功
     * resultCode : 0
     */

    private List<DataBean> data;

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean implements Serializable{
        /**
         * orderPre : {"age":"44","content":"Fdsalfjdls fjksd Fidel\u2019s forsaking Fisk\u2019s flasks fjdsf day fds","creattime":1567337726000,"feed":"8","headimg":"https://hh-liululu.oss-cn-beijing.aliyuncs.com/default/user-header-dafault.png","id":42,"name":"17631330812","objId":83,"orderStatus":2,"sex":1,"userId":87}
         * userViewLikeCommentListSize : 2
         * user : {"belongcode":"QDSEFw1w","channelid":0,"coinstatus":1,"coldmoney":"0","creattime":"2019-08-02 15:28:55","fetchmoney":"0","headimage":"https://hh-liululu.oss-cn-beijing.aliyuncs.com/default/user-header-dafault.png","id":87,"investmoney":"0","inviterid":0,"level":0,"loginnum":0,"logintime":"1564730935","name":"17631330812","passwd":"662919d207123aa79147b5fc3d54831f","phone":"17631330812","rapnum":"0","registermoney":"10","remark":"","returnmoney":"0","sex":0,"status":1,"summoney":"10","token":"LmcEB0ATXU0fQadUf/chF0yGNZGdjsClwXid56lRYen6qh8SPZ+DT5KcdZGSk9qj08w48h423/8bnlrI7lq8HA==","type":0,"waitactivemoney":"0"}
         * doctor : {"certifiedImg":"https://hh-liululu.oss-cn-beijing.aliyuncs.com/zy2.jpg","creattime":1566885162000,"doctorTitle":1,"headerimg":"https://hh-liululu.oss-cn-beijing.aliyuncs.com/default/default-ys001.jpg","hospitalId":1,"id":83,"idCardBackImg":"https://hh-liululu.oss-cn-beijing.aliyuncs.com/sfz/sfzb0.jpeg","idCardFrontImg":"https://hh-liululu.oss-cn-beijing.aliyuncs.com/sfz/sfz2.jpg","idCardNo":"362323199011070066","level":5,"name":"刘主任","officesId":2,"remark":"","status":1,"summary":"擅长外壳各种突发症状1","userId":82}
         */

        private OrderPreBean orderPre;
        private int userViewLikeCommentListSize;
        private UserBean user;
        private DoctorBean doctor;

        public OrderPreBean getOrderPre() {
            return orderPre;
        }

        public void setOrderPre(OrderPreBean orderPre) {
            this.orderPre = orderPre;
        }

        public int getUserViewLikeCommentListSize() {
            return userViewLikeCommentListSize;
        }

        public void setUserViewLikeCommentListSize(int userViewLikeCommentListSize) {
            this.userViewLikeCommentListSize = userViewLikeCommentListSize;
        }

        public UserBean getUser() {
            return user;
        }

        public void setUser(UserBean user) {
            this.user = user;
        }

        public DoctorBean getDoctor() {
            return doctor;
        }

        public void setDoctor(DoctorBean doctor) {
            this.doctor = doctor;
        }
    }
}
