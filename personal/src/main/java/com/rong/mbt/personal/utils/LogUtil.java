package com.rong.mbt.personal.utils;

import android.util.Log;



/**
 * 日志工具类
 * */
public class LogUtil {
    public static final int VERBOSE = 1;
    public static final int DEBUG = VERBOSE + 1;
    public static final int INFO = DEBUG + 1;
    public static final int WARN = INFO + 1;
    public static final int ERROR = WARN + 1;
    public static final int NOTHING = ERROR + 1;

    private static int level = VERBOSE;
    private static String tag = "MBT";

    public static void setLevel(int level) {
        LogUtil.level = level;
    }

    public static void setTag(String tag) {
        LogUtil.tag = tag;
    }

    public static void v(String tag, String msg) {
        if (level <= VERBOSE) {
            Log.v(tag, "" + msg);
        }
    }

    public static void v(String msg) {
        v(tag, msg);
    }

    public static void d(String tag, String msg) {
        if (level <= DEBUG) {
            Log.d(tag, "" + msg);
        }
    }

    public static void d(String msg) {
        d(tag, msg);
    }

    public static void i(String tag, String msg) {
        if (level <= INFO) {
            Log.i(tag, "" + msg);
        }
    }

    public static void i_all(String tag, String msg) {
        if (tag == null || tag.length() == 0
                || msg == null || msg.length() == 0)
            return;

        int segmentSize = 3 * 1024;
        long length = msg.length();
        if (length <= segmentSize) {// 长度小于等于限制直接打印
            Log.e(tag, msg);
        } else {
            while (msg.length() > segmentSize) {// 循环分段打印日志
                String logContent = msg.substring(0, segmentSize);
                msg = msg.replace(logContent, "");
                Log.e(tag, logContent);
            }
            Log.e(tag, msg);// 打印剩余日志
        }
    }

    public static void i_all(String msg) {
        if (tag == null || tag.length() == 0
                || msg == null || msg.length() == 0)
            return;

        int segmentSize = 3 * 1024;
        long length = msg.length();
        if (length <= segmentSize) {// 长度小于等于限制直接打印
            Log.e(tag, msg);
        } else {
            while (msg.length() > segmentSize) {// 循环分段打印日志
                String logContent = msg.substring(0, segmentSize);
                msg = msg.replace(logContent, "");
                Log.e(tag, logContent);
            }
            Log.e(tag, msg);// 打印剩余日志
        }
    }


    public static void i(String msg) {
        i(tag, msg);
    }

    public static void w(String tag, String msg) {
        if (level <= WARN) {
            Log.w(tag, "" + msg);
        }
    }

    public static void w(String msg) {
        w(tag, msg);
    }

    public static void e(String tag, String msg) {
        if (level <= ERROR) {
            Log.e(tag, "" + msg);
        }
    }

    public static void e(String msg) {
        e(tag, msg);
    }

}
