package com.rong.mbt.personal.bean.doctor;

import java.io.Serializable;
import java.util.List;

public class OfficesFindBean implements Serializable {


    /**
     * count : 7
     * data : [{"creattime":1566229410000,"id":1,"level":1,"name":"口腔科","status":1},{"creattime":1566229415000,"id":2,"level":1,"name":"外科","status":1},{"creattime":1566229420000,"id":3,"level":1,"name":"内科","status":1},{"creattime":1566229429000,"id":4,"level":1,"name":"耳鼻喉科","status":1},{"creattime":1566229433000,"id":5,"level":1,"name":"皮肤科","status":1},{"creattime":1566229437000,"id":6,"level":1,"name":"肿瘤科","status":1},{"creattime":1566229462000,"id":7,"level":1,"name":"内分泌科","status":1}]
     * msg : 请求成功
     * resultCode : 0
     */

    private int count;
    private String msg;
    private int resultCode;
    private List<DataBean> data;

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public int getResultCode() {
        return resultCode;
    }

    public void setResultCode(int resultCode) {
        this.resultCode = resultCode;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean implements Serializable {
        /**
         * creattime : 1566229410000
         * id : 1
         * level : 1
         * name : 口腔科
         * status : 1
         */

        private long creattime;
        private int id;
        private int level;
        private String name;
        private int status;

        public long getCreattime() {
            return creattime;
        }

        public void setCreattime(long creattime) {
            this.creattime = creattime;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getLevel() {
            return level;
        }

        public void setLevel(int level) {
            this.level = level;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }
    }
}
