package com.rong.mbt.personal;


import android.app.Activity;

import java.util.Stack;

/**
 * 所有活动的管理器
 */
public class ActivityManager {

    public static Stack<Activity> activityStack = new Stack<>();

    public static void addActivity(Activity activity) {
        activityStack.add(activity);
    }

    public static void removeActivity(Activity activity) {
        if (activityStack.size() > 0) {
            if (activity != null) {
                activityStack.remove(activity);
            }
        }
    }

    public static void exit() {
        for (Activity activity : activityStack) {
            if (!activity.isFinishing()) {
                activity.finish();
            }
        }
    }

}
