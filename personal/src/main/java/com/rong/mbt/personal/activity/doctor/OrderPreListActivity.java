package com.rong.mbt.personal.activity.doctor;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.lzy.okgo.model.HttpParams;
import com.lzy.okgo.model.Response;
import com.rong.mbt.personal.R;
import com.rong.mbt.personal.activity.PatientDetailsActivity;
import com.rong.mbt.personal.activity.base.PBaseActivity;
import com.rong.mbt.personal.adapter.IndexOrderListAdapter;
import com.rong.mbt.personal.bean.order.OrderPreBean;
import com.rong.mbt.personal.bean.order.OrderPreListBean;
import com.rong.mbt.personal.http.OkHttpManager;
import com.rong.mbt.personal.http.UrlApi;
import com.rong.mbt.personal.http.callback.JsonCallback;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshLoadMoreListener;

import java.util.ArrayList;
import java.util.List;


/**
 * 订单列表（医生）
 */
public class OrderPreListActivity extends PBaseActivity implements OnRefreshLoadMoreListener {

    private LinearLayout mLlBack;
    private TextView mTxtTitle;
    private View mIvRight;
    private LinearLayout mLlRightMenu;
    private TextView mTxtRight;
    private LinearLayout mLlRightText;
    private RecyclerView mRecyclerView;
    private SmartRefreshLayout mRefreshLayout;

    private IndexOrderListAdapter adapter;
    private int pageNum = 1;

    @Override
    public void initView() {
        super.initView();
        mLlBack = findViewById(R.id.ll_back);
        mTxtTitle = findViewById(R.id.txt_title);
        mIvRight = findViewById(R.id.iv_right);
        mLlRightMenu = findViewById(R.id.ll_right_menu);
        mTxtRight = findViewById(R.id.txt_right);
        mLlRightText = findViewById(R.id.ll_right_text);
        mRecyclerView = findViewById(R.id.recyclerView);
        mRefreshLayout = findViewById(R.id.refreshLayout);
        mTxtTitle.setText("订单列表");
        mLlBack.setOnClickListener(v -> finish());

        initData();

    }

    private void initData() {
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRefreshLayout.setOnRefreshLoadMoreListener(this);
        adapter = new IndexOrderListAdapter();
        mRecyclerView.setAdapter(adapter);


        adapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                OrderPreBean item = (OrderPreBean) adapter.getItem(position);
                startActivity(new Intent(getApplicationContext(), PatientDetailsActivity.class).putExtra("OrderPreBean", item));
            }
        });

        HttpParams httpParams = OkHttpManager.getListParams(pageNum);
        httpParams.put("orderStatus", 0);//0未接单  1沟通中  2完成
        OkHttpManager.getInstance(this).get(UrlApi.orderPreListByPage, httpParams, new JsonCallback<OrderPreListBean>() {
            @Override
            public void onSuccess(Response<OrderPreListBean> response) {
                if (response != null && response.body() != null) {
                    OrderPreListBean body = response.body();
                    if (body.getData() != null) {
                        List<OrderPreListBean.DataBean> data = body.getData();
                        List<OrderPreBean> preBeanList = new ArrayList<>();
                        for (OrderPreListBean.DataBean dataBean : data) {
                            OrderPreBean orderPre = dataBean.getOrderPre();
                            if (dataBean.getUser() != null){
                                orderPre.setName(dataBean.getUser().getName());
                                orderPre.setHeadimg(dataBean.getUser().getHeadimage());
                            }
                            preBeanList.add(orderPre);
                        }
                        adapter.setNewData(preBeanList);
                    }
                }
            }
        });
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_smart_refresh_layout;
    }

    @Override
    public void onLoadMore(@NonNull RefreshLayout refreshLayout) {
        pageNum++;
        mRefreshLayout.finishLoadMore();
    }

    @Override
    public void onRefresh(@NonNull RefreshLayout refreshLayout) {
        pageNum = 1;
        mRefreshLayout.finishRefresh();
    }
}
