package com.rong.mbt.personal.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.alibaba.android.arouter.launcher.ARouter;
import com.bumptech.glide.Glide;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.model.Response;
import com.rong.mbt.personal.R;
import com.rong.mbt.personal.activity.ArticleDetailsActivity;
import com.rong.mbt.personal.activity.ArticleListActivity;
import com.rong.mbt.personal.activity.NoticeActivity;
import com.rong.mbt.personal.activity.SearchActivity;
import com.rong.mbt.personal.activity.SelectDepartmentListActivity;
import com.rong.mbt.personal.activity.doctor.OrderPreListActivity;
import com.rong.mbt.personal.activity.PatientDetailsActivity;
import com.rong.mbt.personal.activity.WebViewAcitvity;
import com.rong.mbt.personal.adapter.IndexArticleListAdapter;
import com.rong.mbt.personal.adapter.IndexOrderListAdapter;
import com.rong.mbt.personal.bean.BannerListBean;
import com.rong.mbt.personal.bean.article.ArticleItemBean;
import com.rong.mbt.personal.bean.article.ArticleListPageBean;
import com.rong.mbt.personal.bean.info.BannerInfo;
import com.rong.mbt.personal.bean.order.OrderPreBean;
import com.rong.mbt.personal.bean.order.OrderPreListBean;
import com.rong.mbt.personal.http.UrlApi;
import com.rong.mbt.personal.http.callback.JsonCallback;
import com.rong.mbt.personal.utils.SpUtils;
import com.stx.xhb.xbanner.XBanner;

import java.util.ArrayList;
import java.util.List;

/**
 * 医生模式首页
 */
public class DoctorIndexFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    private XBanner xBanner;
    private RecyclerView orderRecyclerList;
    private IndexOrderListAdapter adapter;
    private IndexArticleListAdapter articleListAdapter;
    private SwipeRefreshLayout swipeRefresh;
    private LinearLayout llSearch;
    private LinearLayout llNotice;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = inflater.inflate(R.layout.fragment_doctor_index, container, false);
        initView(rootView);
        listener(rootView);
        return rootView;
    }

    private void listener(View rootView) {
        llSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), SearchActivity.class));
            }
        });
        llNotice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), NoticeActivity.class));
            }
        });
    }

    private void initView(View rootView) {
        llSearch = rootView.findViewById(R.id.ll_search);
        llNotice = rootView.findViewById(R.id.ll_notice);

        rootView.findViewById(R.id.head_blue_iv_left).setVisibility(View.GONE);
        TextView tvTitle = rootView.findViewById(R.id.head_blue_tv_center);
        tvTitle.setText("首页");
        rootView.findViewById(R.id.head_blue_iv_right).setVisibility(View.GONE);
        xBanner = rootView.findViewById(R.id.xbanner);
        swipeRefresh = rootView.findViewById(R.id.swipe_refresh);
        swipeRefresh.setOnRefreshListener(this);
        orderRecyclerList = rootView.findViewById(R.id.order_recycler_list);
        orderRecyclerList.setLayoutManager(new LinearLayoutManager(getContext()));
        RecyclerView articleRcyclerList = rootView.findViewById(R.id.article_recycler_list);
        articleRcyclerList.setLayoutManager(new LinearLayoutManager(getContext()));
        articleListAdapter = new IndexArticleListAdapter();
        articleRcyclerList.setAdapter(articleListAdapter);

        /**发布订单*/
        rootView.findViewById(R.id.rl_publish_orders).setOnClickListener(v -> {
            ARouter.getInstance().build("/order/OrderPreActivity").navigation();
        });

        rootView.findViewById(R.id.rl_seek_doctor).setOnClickListener(v -> {
            startActivity(new Intent(getActivity(), SelectDepartmentListActivity.class));
        });


        //更多订单
        rootView.findViewById(R.id.ll_Order).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), OrderPreListActivity.class));
            }
        });

        //更多美文
        rootView.findViewById(R.id.ll_more_article).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), ArticleListActivity.class).putExtra("categoryId", 2));
            }
        });

        //加载广告图片
        xBanner.loadImage((banner, model, view, position) -> {
            //在此处使用图片加载框架加载图片，demo中使用glide加载，可替换成自己项目中的图片加载框架
            BannerInfo listBean = (BannerInfo) model;
            String url = listBean.getXBannerUrl();
            Glide.with(getActivity()).load(url).into((ImageView) view);
        });
        xBanner.setOnItemClickListener((banner, model, view, position) -> {
            BannerInfo listBean = (BannerInfo) model;
            startActivity(new Intent(getContext(), WebViewAcitvity.class).putExtra("url", listBean.getUrl()));
        });

        adapter = new IndexOrderListAdapter();
        orderRecyclerList.setAdapter(adapter);

        setOnClick();

        initBanner();

        initData();

    }

    private void initBanner() {
        OkGo.<List<BannerListBean>>get(UrlApi.bannerListByPage)
                .tag(this)
                .headers("mbtToken", SpUtils.getToken(getActivity()))
                .execute(new JsonCallback<List<BannerListBean>>() {
                    @Override
                    public void onSuccess(Response<List<BannerListBean>> response) {
                        if (response != null && response.body() != null) {
                            List<BannerListBean> body = response.body();
                            List<BannerInfo> images = new ArrayList<>();
                            for (BannerListBean bean : body) {
                                BannerInfo info = new BannerInfo();
                                info.setUrl(bean.getUrl());
                                info.setImageUrl(bean.getImg());
                                images.add(info);
                            }
                            xBanner.setBannerData(images);
                        }
                    }
                });
    }

    private void setOnClick() {
        adapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                OrderPreBean item = (OrderPreBean) adapter.getItem(position);
                startActivity(new Intent(getActivity(), PatientDetailsActivity.class).putExtra("OrderPreBean", item));
            }
        });

        articleListAdapter.setOnItemClickListener((adapter, view, position) -> {
            ArticleItemBean item = (ArticleItemBean) adapter.getItem(position);
            startActivity(new Intent(getActivity(), ArticleDetailsActivity.class).putExtra("ArticleItemBean", item));
        });
    }

    private void initData() {


        //订单列表请求
        OkGo.<OrderPreListBean>get(UrlApi.orderPreListByPage)
                .tag(this)
                .headers("mbtToken", SpUtils.getToken(getActivity()))
                .params("orderStatus", 0)//0未接单  1沟通中  2完成
                .params("pageSize", 5)
                .params("pageNumber", 1)
                .execute(new JsonCallback<OrderPreListBean>() {
                    @Override
                    public void onSuccess(Response<OrderPreListBean> response) {
                        if (response != null && response.body() != null) {
                            OrderPreListBean body = response.body();
                            if (body.getData() != null) {
                                List<OrderPreListBean.DataBean> data = body.getData();
                                List<OrderPreBean> preBeanList = new ArrayList<>();
                                for (OrderPreListBean.DataBean dataBean : data) {
                                    OrderPreBean orderPre = dataBean.getOrderPre();
                                    if (dataBean.getUser() != null){
                                        orderPre.setName(dataBean.getUser().getName());
                                        orderPre.setHeadimg(dataBean.getUser().getHeadimage());
                                    }
                                    preBeanList.add(orderPre);
                                }
                                adapter.setNewData(preBeanList);
                            }
                        }
                    }
                });


        //文章列表
        OkGo.<ArticleListPageBean>get(UrlApi.articleListByPage)
                .tag(this)
                .headers("mbtToken", SpUtils.getToken(getActivity()))
//                .params("categoryId", 1)//范畴：1  快速回答2  精选讲堂3  育婴护理4  精神健康
                .params("pageSize", 5)
                .params("pageNumber", 1)
                .execute(new JsonCallback<ArticleListPageBean>() {
                    @Override
                    public void onSuccess(Response<ArticleListPageBean> response) {
                        if (response != null && response.body() != null) {
                            ArticleListPageBean body = response.body();
                            if (body.getData() != null) {
                                List<ArticleItemBean> data = body.getData();
                                articleListAdapter.setNewData(data);
                            }
                        }
                    }
                });
    }

    @Override
    public void onRefresh() {
        initData();
        swipeRefresh.setRefreshing(false);
    }
}
