package com.rong.mbt.personal.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.alibaba.android.arouter.launcher.ARouter;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.model.Response;
import com.rong.mbt.personal.R;
import com.rong.mbt.personal.R2;
import com.rong.mbt.personal.activity.ArticleDetailsActivity;
import com.rong.mbt.personal.activity.ArticleListActivity;
import com.rong.mbt.personal.activity.DoctorDetailsActivity;
import com.rong.mbt.personal.activity.SelectDepartmentListActivity;
import com.rong.mbt.personal.adapter.DoctorListAdapter;
import com.rong.mbt.personal.adapter.IndexArticleListAdapter;
import com.rong.mbt.personal.bean.BannerListBean;
import com.rong.mbt.personal.bean.article.ArticleItemBean;
import com.rong.mbt.personal.bean.article.ArticleListPageBean;
import com.rong.mbt.personal.bean.doctor.DoctorIndexBean;
import com.rong.mbt.personal.bean.doctor.DoctorListBean;
import com.rong.mbt.personal.bean.info.BannerInfo;
import com.rong.mbt.personal.http.UrlApi;
import com.rong.mbt.personal.http.callback.JsonCallback;
import com.rong.mbt.personal.utils.SpUtils;
import com.stx.xhb.xbanner.XBanner;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * 病人模式首页
 */
public class PatientIndexFragment extends Fragment {
    private XBanner xBanner;
    private IndexArticleListAdapter adapterKt;
    private IndexArticleListAdapter adapterWd;
    private DoctorListAdapter doctorListAdapter;
    private RelativeLayout mRlMoreArticle1;
    private RelativeLayout mRlMoreArticle2;

    //    @BindView(R2.id.ll_search)
    LinearLayout llSearch;
    //    @BindView(R2.id.ll_notice)
    LinearLayout llNotice;

    private Unbinder unbinder;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = inflater.inflate(R.layout.fragment_patient_index, container, false);
        initView(rootView);
        listener(rootView);
        unbinder = ButterKnife.bind(this, rootView);
        return rootView;
    }

//    @OnClick(R2.id.ll_search)
    public void search() {
        ARouter.getInstance().build("/search/SearchActivity").navigation();
    }

//    @OnClick(R2.id.ll_notice)
    public void notice() {
        ARouter.getInstance().build("/notice/NoticeActivity").navigation();
    }

    private void listener(View rootView) {

    }

    private void initView(View rootView) {
        llSearch = rootView.findViewById(R.id.ll_search);
        llNotice = rootView.findViewById(R.id.ll_notice);
        llSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                search();
            }
        });
        llNotice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                notice();
            }
        });

        rootView.findViewById(R.id.head_blue_iv_left).setVisibility(View.GONE);
        TextView tvTitle = rootView.findViewById(R.id.head_blue_tv_center);
        tvTitle.setText("首页");
        rootView.findViewById(R.id.head_blue_iv_right).setVisibility(View.GONE);
        mRlMoreArticle1 = rootView.findViewById(R.id.rl_more_article1);
        mRlMoreArticle2 = rootView.findViewById(R.id.rl_more_article2);
//        xBanner = rootView.findViewById(R.id.xbanner);
//        //加载广告图片
//        xBanner.loadImage((banner, model, view, position) -> {
//            //在此处使用图片加载框架加载图片，demo中使用glide加载，可替换成自己项目中的图片加载框架
//            BannerInfo listBean = (BannerInfo) model;
//            String url = listBean.getXBannerUrl();
//            Glide.with(getActivity()).load(url).into((ImageView) view);
//        });
//        xBanner.setOnItemClickListener((banner, model, view, position) -> {
//            BannerInfo listBean = (BannerInfo) model;
//            startActivity(new Intent(getContext(), WebViewAcitvity.class).putExtra("url", listBean.getUrl()));
//        });

        /**发布订单*/
        rootView.findViewById(R.id.rl_publish_orders).setOnClickListener(v -> {
            ARouter.getInstance().build("/order/OrderPreActivity").navigation();
        });

        rootView.findViewById(R.id.rl_seek_doctor).setOnClickListener(v -> {
            startActivity(new Intent(getActivity(), SelectDepartmentListActivity.class));
        });

        //精选讲堂
        mRlMoreArticle1.setOnClickListener(v -> {
            startActivity(new Intent(getActivity(), ArticleListActivity.class).putExtra("categoryId", 2));
        });

        //快速问答
        mRlMoreArticle2.setOnClickListener(v -> {
            startActivity(new Intent(getActivity(), ArticleListActivity.class).putExtra("categoryId", 1));
        });

//        initData();

        RecyclerView rcyclerKt = rootView.findViewById(R.id.recycler_view_kt);
        rcyclerKt.setLayoutManager(new LinearLayoutManager(getActivity()));
        adapterKt = new IndexArticleListAdapter();
        rcyclerKt.setAdapter(adapterKt);
        reuestData(2);

        RecyclerView rcyclerWd = rootView.findViewById(R.id.recycler_view_wd);
        rcyclerWd.setLayoutManager(new LinearLayoutManager(getActivity()));
        adapterWd = new IndexArticleListAdapter();
        rcyclerWd.setAdapter(adapterWd);
        reuestData(1);


        RecyclerView rcyclerzx = rootView.findViewById(R.id.recycler_view_zx);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        rcyclerzx.setLayoutManager(layoutManager);
        doctorListAdapter = new DoctorListAdapter();
        rcyclerzx.setAdapter(doctorListAdapter);
        doctorListAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                DoctorListBean item = (DoctorListBean) adapter.getItem(position);
//                startActivity(new Intent(getActivity(), DoctorDetailsActivity.class).putExtra("DoctorListBean", item));
                startActivity(new Intent(getActivity(), DoctorDetailsActivity.class).putExtra("id", item.getDoctor().getId()));
            }
        });

        adapterKt.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                ArticleItemBean item = (ArticleItemBean) adapter.getItem(position);
                startActivity(new Intent(getActivity(), ArticleDetailsActivity.class).putExtra("ArticleItemBean", item));
            }
        });

        adapterWd.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                ArticleItemBean item = (ArticleItemBean) adapter.getItem(position);
                startActivity(new Intent(getActivity(), ArticleDetailsActivity.class).putExtra("ArticleItemBean", item));
            }
        });
        reuestDoctor();
    }

    private void reuestDoctor() {
        OkGo.<DoctorIndexBean>get(UrlApi.userIndexDoctorList)
                .tag(this)
                .headers("mbtToken", SpUtils.getToken(getActivity()))
                .execute(new JsonCallback<DoctorIndexBean>() {
                    @Override
                    public void onSuccess(Response<DoctorIndexBean> response) {
                        if (response != null && response.body() != null) {
                            DoctorIndexBean body = response.body();
                            if (body.getData() != null) {
                                List<DoctorListBean> data = body.getData();
                                doctorListAdapter.setNewData(data);
                            }
                        }
                    }
                });
    }

    private void reuestData(int categoryId) {
        //文章列表
        OkGo.<ArticleListPageBean>get(UrlApi.articleListByPage)
                .tag(this)
                .headers("mbtToken", SpUtils.getToken(getActivity()))
                .params("categoryId", categoryId)//范畴：1  快速回答2  精选讲堂3  育婴护理4  精神健康
                .params("pageSize", 10)
                .params("pageNumber", 1)
                .execute(new JsonCallback<ArticleListPageBean>() {
                    @Override
                    public void onSuccess(Response<ArticleListPageBean> response) {
                        if (response != null && response.body() != null) {
                            ArticleListPageBean body = response.body();
                            if (body.getData() != null) {
                                List<ArticleItemBean> data = body.getData();
                                if (categoryId == 2) {
                                    adapterKt.setNewData(data);
                                } else if (categoryId == 1) {
                                    adapterWd.setNewData(data);
                                }
                            }
                        }
                    }
                });


    }

    private void initData() {
        OkGo.<List<BannerListBean>>get(UrlApi.bannerListByPage)
                .tag(this)
                .headers("mbtToken", SpUtils.getToken(getActivity()))
                .execute(new JsonCallback<List<BannerListBean>>() {
                    @Override
                    public void onSuccess(Response<List<BannerListBean>> response) {
                        if (response != null && response.body() != null) {
                            List<BannerListBean> body = response.body();
                            List<BannerInfo> images = new ArrayList<>();
                            for (BannerListBean bean :
                                    body) {
                                BannerInfo info = new BannerInfo();
                                info.setImageUrl(bean.getImg());
                                images.add(info);
                            }
                            xBanner.setBannerData(images);
                        }
                    }
                });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
