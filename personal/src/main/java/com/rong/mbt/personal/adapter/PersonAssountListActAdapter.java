package com.rong.mbt.personal.adapter;

import android.widget.TextView;

import com.chad.library.adapter.base.BaseItemDraggableAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.rong.mbt.personal.R;
import com.rong.mbt.personal.bean.PersonAssountListBean;

public class PersonAssountListActAdapter extends BaseItemDraggableAdapter<PersonAssountListBean.DataBean, BaseViewHolder> {


    public PersonAssountListActAdapter() {
        super(R.layout.item_person_assount_list,null);
    }

    @Override
    protected void convert(BaseViewHolder helper, PersonAssountListBean.DataBean item) {

        helper.setText(R.id.item_tv_name, item.getName());
        helper.addOnClickListener(R.id.ipal_relative_right);


        TextView tv = helper.getView(R.id.item_tv_info);
        switch (item.getType()) {
            case 1:
                tv.setText("支付宝账号：" + item.getAccount());
                break;
            case 2:
                tv.setText("新浪账号：" + item.getAccount());
                break;
            case 3:
                tv.setText("微信账号：" + item.getAccount());
                break;
            case 4:
                tv.setText("银行卡账号：" + item.getAccount());
                break;
            case 5:
                tv.setText("比特币账号：" + item.getAccount());
                break;
            case 6:
                tv.setText("莱特币账号：" + item.getAccount());
                break;
            case 7:
                tv.setText("以太坊账号：" + item.getAccount());
                break;
                default:
                    tv.setText(item.getAccount());
                    break;
        }
    }
}
