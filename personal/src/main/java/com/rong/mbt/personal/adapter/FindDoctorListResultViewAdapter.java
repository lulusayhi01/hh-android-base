package com.rong.mbt.personal.adapter;

import android.widget.ImageView;
import android.widget.TextView;

import com.alibaba.fastjson.JSONObject;
import com.bumptech.glide.Glide;
import com.calanger.hh.model.Doctor;
import com.calanger.hh.model.User;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.rong.mbt.personal.R;
import com.rong.mbt.personal.bean.doctor.DoctorListBean;

import java.util.Map;

/**
 * 文章列表
 */
public class FindDoctorListResultViewAdapter extends BaseQuickAdapter<Map<String,Object>, BaseViewHolder> {


    public FindDoctorListResultViewAdapter() {
        super(R.layout.item_find_doctor);
    }

    @Override
    protected void convert(BaseViewHolder helper, Map<String,Object> item) {

        try {
            JSONObject jo = new JSONObject(item);
            User user = jo.getObject("user", User.class);
            Doctor doctor = jo.getObject("doctor", Doctor.class);

            TextView tvName = helper.getView(R.id.tv_name);
            tvName.setText(user.getName());

            ImageView ivHead = helper.getView(R.id.iv_head);
            Glide.with(helper.itemView.getContext()).load(doctor.getHeaderimg()).into(ivHead);

            String doctorTypeName = "";
            int doctorTitle = doctor.getDoctorTitle();
            if (doctorTitle == 1) {
                doctorTypeName = "助理医生";
            } else {
                doctorTypeName = "执业医生";
            }
            TextView tvDoctorType = helper.getView(R.id.tv_doctor_type);
            tvDoctorType.setText(doctorTypeName);//医生头衔(1.助理医生 2.执业医生)


        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
