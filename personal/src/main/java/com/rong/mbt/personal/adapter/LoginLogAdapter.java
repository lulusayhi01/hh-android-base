package com.rong.mbt.personal.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.rong.mbt.personal.R;
import com.rong.mbt.personal.bean.LoginLogBean;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;


public class LoginLogAdapter extends RecyclerView.Adapter {
    private List<LoginLogBean.DataBean> data = new ArrayList<>();
    private Context context;
    private final SimpleDateFormat sd;


    public LoginLogAdapter(Context context) {
        this.context = context;
        sd = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_loginlog,parent,false);
        return new LoginLogAdapterHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        LoginLogAdapterHolder h = (LoginLogAdapterHolder) holder;
        if(sd !=null){
        h.time.setText(sd.format(data.get(position).getCreatetime()));
        }

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public void setData(List<LoginLogBean.DataBean> data) {
        this.data.addAll(data);
        notifyDataSetChanged();
    }

    class LoginLogAdapterHolder extends RecyclerView.ViewHolder{

        private TextView time;
        public LoginLogAdapterHolder(View itemView) {
            super(itemView);
            time =  itemView.findViewById(R.id.item_loginlog_time);
        }
    }
}
