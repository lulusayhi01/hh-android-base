package com.rong.mbt.personal.activity;


import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.callback.StringCallback;
import com.lzy.okgo.model.Response;
import com.rong.mbt.personal.R;
import com.rong.mbt.personal.activity.base.PBaseActivity;
import com.rong.mbt.personal.bean.base.BaseBean;

public class IndividualitySignActivity extends PBaseActivity {


    private String mbtToken;

    @Override
    public int getLayoutId() {
        return R.layout.activity_individuality_sign;
    }

    @Override
    public void initView() {
        super.initView();
        mbtToken = getIntent().getStringExtra("mbtToken");
        String signature = getIntent().getStringExtra("signature");

        findViewById(R.id.head_blue_iv_left).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        TextView tvTitle = findViewById(R.id.head_blue_tv_center);
        tvTitle.setText("个性签名");
        findViewById(R.id.head_blue_iv_right).setVisibility(View.GONE);

        final EditText et = findViewById(R.id.act_is_et);

        if (signature != null) {
            et.setText(signature);
        }

        findViewById(R.id.act_is_bt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String s = et.getText().toString();
                if (!s.isEmpty() && s.length() > 1 && s.length() < 128) {
                    OkGo.<String>post("http://service.jiangaifen.com:38082/user/editUserSignature")
                            .tag(this)
//                            .isMultipart(true)
                            .headers("mbtToken", mbtToken)
                            .params("signature", s)
                            .execute(new StringCallback() {
                                @Override
                                public void onSuccess(Response<String> response) {
                                    Log.i("test", "onSuccess  " + response.body());

                                    BaseBean baseBean = JSON.parseObject(response.body(), BaseBean.class);
                                    Toast.makeText(IndividualitySignActivity.this, baseBean.getMsg(), Toast.LENGTH_SHORT).show();
                                }

                                @Override
                                public void onError(Response<String> response) {
                                    super.onError(response);
                                    Log.i("test", "onError  " + response.message());
                                }
                            });
                } else {
                    Toast.makeText(IndividualitySignActivity.this, "长度为1~128个字符", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

}
