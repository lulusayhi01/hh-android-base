package com.rong.mbt.personal.bean;

import com.rong.mbt.personal.activity.SecuritySettingActivity;

import java.io.Serializable;
import java.util.List;

public class PersonalInfoBean implements Serializable{



    private DataBean data;
    private String msg;
    private int resultCode;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public int getResultCode() {
        return resultCode;
    }

    public void setResultCode(int resultCode) {
        this.resultCode = resultCode;
    }

    public static class DataBean implements Serializable{

        private String belongcode;
        private int channelid;
        private String coldmoney;
        private String creattime;
        private String fetchmoney;
        private String headimage;
        private int id;
        private String investmoney;
        private String invitecode;
        private int inviterid;
        private int level;
        private int loginnum;
        private String logintime;
        private String name;
        private String passwd;
        private String phone;
        private String rapnum;
        private String registermoney;
        private String remark;
        private String returnmoney;
        private String signature;
        private int status;
        private String summoney;
        private String accountMoney;
        private String token;
        private String usablemoney;
        private int userInfostatus;
        private String waitactivemoney;
        private List<TrendsImgBean> trendsImg;

        public String getAccountMoney() {
            return accountMoney;
        }

        public void setAccountMoney(String accountMoney) {
            this.accountMoney = accountMoney;
        }

        public String getBelongcode() {
            return belongcode;
        }

        public void setBelongcode(String belongcode) {
            this.belongcode = belongcode;
        }

        public int getChannelid() {
            return channelid;
        }

        public void setChannelid(int channelid) {
            this.channelid = channelid;
        }

        public String getColdmoney() {
            return coldmoney;
        }

        public void setColdmoney(String coldmoney) {
            this.coldmoney = coldmoney;
        }

        public String getCreattime() {
            return creattime;
        }

        public void setCreattime(String creattime) {
            this.creattime = creattime;
        }

        public String getFetchmoney() {
            return fetchmoney;
        }

        public void setFetchmoney(String fetchmoney) {
            this.fetchmoney = fetchmoney;
        }

        public String getHeadimage() {
            return headimage;
        }

        public void setHeadimage(String headimage) {
            this.headimage = headimage;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getInvestmoney() {
            return investmoney;
        }

        public void setInvestmoney(String investmoney) {
            this.investmoney = investmoney;
        }

        public String getInvitecode() {
            return invitecode;
        }

        public void setInvitecode(String invitecode) {
            this.invitecode = invitecode;
        }

        public int getInviterid() {
            return inviterid;
        }

        public void setInviterid(int inviterid) {
            this.inviterid = inviterid;
        }

        public int getLevel() {
            return level;
        }

        public void setLevel(int level) {
            this.level = level;
        }

        public int getLoginnum() {
            return loginnum;
        }

        public void setLoginnum(int loginnum) {
            this.loginnum = loginnum;
        }

        public String getLogintime() {
            return logintime;
        }

        public void setLogintime(String logintime) {
            this.logintime = logintime;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getPasswd() {
            return passwd;
        }

        public void setPasswd(String passwd) {
            this.passwd = passwd;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getRapnum() {
            return rapnum;
        }

        public void setRapnum(String rapnum) {
            this.rapnum = rapnum;
        }

        public String getRegistermoney() {
            return registermoney;
        }

        public void setRegistermoney(String registermoney) {
            this.registermoney = registermoney;
        }

        public String getRemark() {
            return remark;
        }

        public void setRemark(String remark) {
            this.remark = remark;
        }

        public String getReturnmoney() {
            return returnmoney;
        }

        public void setReturnmoney(String returnmoney) {
            this.returnmoney = returnmoney;
        }

        public String getSignature() {
            return signature;
        }

        public void setSignature(String signature) {
            this.signature = signature;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public String getSummoney() {
            return summoney;
        }

        public void setSummoney(String summoney) {
            this.summoney = summoney;
        }

        public String getToken() {
            return token;
        }

        public void setToken(String token) {
            this.token = token;
        }

        public String getUsablemoney() {
            return usablemoney;
        }

        public void setUsablemoney(String usablemoney) {
            this.usablemoney = usablemoney;
        }

        public int getUserInfostatus() {
            return userInfostatus;
        }

        public void setUserInfostatus(int userInfostatus) {
            this.userInfostatus = userInfostatus;
        }

        public String getWaitactivemoney() {
            return waitactivemoney;
        }

        public void setWaitactivemoney(String waitactivemoney) {
            this.waitactivemoney = waitactivemoney;
        }

        public List<TrendsImgBean> getTrendsImg() {
            return trendsImg;
        }

        public void setTrendsImg(List<TrendsImgBean> trendsImg) {
            this.trendsImg = trendsImg;
        }

        public static class TrendsImgBean implements Serializable{

            private int id;
            private String imgpath;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getImgpath() {
                return imgpath;
            }

            public void setImgpath(String imgpath) {
                this.imgpath = imgpath;
            }
        }
    }
}
