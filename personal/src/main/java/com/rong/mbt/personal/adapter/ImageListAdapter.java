package com.rong.mbt.personal.adapter;

import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.rong.mbt.personal.R;
import com.rong.mbt.personal.bean.image.ImageBean;

/**
 * 文章列表
 */
public class ImageListAdapter extends BaseQuickAdapter<ImageBean, BaseViewHolder> {


    public ImageListAdapter() {
        super(R.layout.item_image_list);
    }

    @Override
    protected void convert(BaseViewHolder helper, ImageBean item) {
        ImageView mIvImage = helper.getView(R.id.iv_image);
        Glide.with(helper.itemView.getContext()).load(item.getUrl()).into(mIvImage);
    }
}
