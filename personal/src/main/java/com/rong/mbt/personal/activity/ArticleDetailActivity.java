package com.rong.mbt.personal.activity;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.bumptech.glide.Glide;
import com.calanger.hh.entity.ResultView;
import com.calanger.hh.model.Article;
import com.calanger.hh.model.Doctor;
import com.calanger.hh.model.Hospital;
import com.calanger.hh.model.ImgUrl;
import com.calanger.hh.model.OrderPre;
import com.calanger.hh.model.User;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.model.Response;
import com.rong.mbt.personal.R;
import com.rong.mbt.personal.activity.base.PBaseActivity;
import com.rong.mbt.personal.adapter.ImageListAdapter;
import com.rong.mbt.personal.bean.article.ArticleItemBean;
import com.rong.mbt.personal.bean.image.ImageBean;
import com.rong.mbt.personal.http.UrlApi;
import com.rong.mbt.personal.http.callback.JsonCallback;
import com.rong.mbt.personal.utils.SpUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import io.rong.imkit.RongIM;

/**
 * 文章详情
 */
public class ArticleDetailActivity extends PBaseActivity {


    private LinearLayout mLlBack;
    private TextView mTxtTitle;
    private View mIvRight;
    private LinearLayout mLlRightMenu;
    private TextView mTxtRight;
    private LinearLayout mLlRightText;
    private TextView mTvTitle;
    private RecyclerView mRecyclerImageView;
    private TextView mTvContent;


    private ImageListAdapter imageListAdapter;

    @Override
    public void initView() {
        super.initView();
        mLlBack = findViewById(R.id.ll_back);
        mTxtTitle = findViewById(R.id.txt_title);
        mIvRight = findViewById(R.id.iv_right);
        mLlRightMenu = findViewById(R.id.ll_right_menu);
        mTxtRight = findViewById(R.id.txt_right);
        mLlRightText = findViewById(R.id.ll_right_text);
        mTvTitle = findViewById(R.id.tv_title);
        mRecyclerImageView = findViewById(R.id.recycler_image_view);
        mTvContent = findViewById(R.id.tv_content);

        mLlBack.setOnClickListener(v -> finish());
        initData();
    }

    private void initData() {
        mTxtTitle.setText("文章详情");
        mRecyclerImageView.setLayoutManager(new LinearLayoutManager(this));
        imageListAdapter = new ImageListAdapter();
        mRecyclerImageView.setAdapter(imageListAdapter);

        ArticleItemBean articleItemBean = (ArticleItemBean) getIntent().getSerializableExtra("ArticleItemBean");

        if (articleItemBean != null) {
            setData(articleItemBean);

        }else{
            Integer id = (Integer) getIntent().getIntExtra("id",0);

            OkGo.<ResultView>get(UrlApi.articleGetSingle)
                    .tag(this)
                    .headers("mbtToken", SpUtils.getToken(this))
                    .params("id", id)
                    .execute(new JsonCallback<ResultView>() {
                        @Override
                        public void onSuccess(Response<ResultView> response) {
                            if (response != null && response.body() != null) {
                                ResultView resultView = response.body();
                                if (resultView.getResultCode() == 0) {
//                                        requestSelectFriendRemark(orderPreBean.getUserId()+"",orderPreBean.getName());
                                    Map<String,Object> map = (Map<String, Object>) resultView.getData();
                                    JSONObject jo = new JSONObject(map);
                                    Article article = (Article) jo.getObject("article",Article.class);

                                    List<ImageBean> imageBeans = new ArrayList<>();
                                    JSONArray ja = jo.getJSONArray("imgUrlList");
                                    for(int i=0;i<ja.size();i++){
                                        JSONObject single = ja.getJSONObject(i);
                                        ImgUrl bean = single.toJavaObject(ImgUrl.class);
                                        ImageBean imageBean = new ImageBean();
                                        imageBean.setUrl(bean.getImgUrl());
                                        imageBeans.add(imageBean);
                                    }

                                    imageListAdapter.setNewData(imageBeans);
                                    mTvTitle.setText(article.getTitle());
                                    mTvContent.setText(article.getContent());
                                } else {
                                    Toast.makeText(getApplicationContext(), resultView.getMsg(), Toast.LENGTH_SHORT);
                                }
                            }
                        }
                    });

        }
    }

    private void setData(ArticleItemBean articleItemBean){
        List<ArticleItemBean.ImgUrlListBean> imgUrlList = articleItemBean.getImgUrlList();
        List<ImageBean> imageBeans = new ArrayList<>();
        for (ArticleItemBean.ImgUrlListBean bean : imgUrlList) {
            ImageBean imageBean = new ImageBean();
            imageBean.setUrl(bean.getImgUrl());
            imageBeans.add(imageBean);
        }

        imageListAdapter.setNewData(imageBeans);
        mTvTitle.setText(articleItemBean.getArticle().getTitle());
        mTvContent.setText(articleItemBean.getArticle().getContent());
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_article_details;
    }
}
