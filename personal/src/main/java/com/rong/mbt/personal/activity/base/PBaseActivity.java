package com.rong.mbt.personal.activity.base;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;

import com.rong.mbt.personal.ActivityManager;
import com.rong.mbt.personal.R;
import com.umeng.message.PushAgent;

import butterknife.ButterKnife;
import okhttp3.OkHttpClient;

public class PBaseActivity extends FragmentActivity {


    protected LayoutInflater inflater;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        PushAgent.getInstance(this).onAppStart();
        setContentView(getLayoutId());
        ButterKnife.bind(this);
        // 禁止屏幕旋转
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        ActivityManager.addActivity(this);
        inflater = LayoutInflater.from(this);
        initView();
    }


    public void initView() {

    }

    public int getLayoutId() {
        return R.layout.activity_base;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ActivityManager.removeActivity(this);
    }

    public void startAty(Class<?> cls) {
        startActivity(new Intent(this, cls));
    }
}

