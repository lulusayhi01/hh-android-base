package com.rong.mbt.personal.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.calanger.hh.entity.ResultView;
import com.calanger.hh.model.UserViewLikeComment;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.model.HttpParams;
import com.lzy.okgo.model.Response;
import com.rong.mbt.personal.R;
import com.rong.mbt.personal.activity.ArticleDetailsActivity;
import com.rong.mbt.personal.adapter.IndexArticleListAdapter;
import com.rong.mbt.personal.adapter.UserViewLikeCommentAdapter;
import com.rong.mbt.personal.bean.article.ArticleItemBean;
import com.rong.mbt.personal.bean.article.ArticleListPageBean;
import com.rong.mbt.personal.http.UrlApi;
import com.rong.mbt.personal.http.callback.JsonCallback;
import com.rong.mbt.personal.utils.SpUtils;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshLoadMoreListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 文章列表
 */
public class CommentItemFragment extends Fragment implements OnRefreshLoadMoreListener {

    private String mTitle;
    private Integer code = 0;
    private Integer mode = 0;
    private RecyclerView mRecyclerView;
    private SmartRefreshLayout mRefreshLayout;
    private UserViewLikeCommentAdapter userViewLikeCommentAdapter;
    private int pageNumber = 1;
    private List<Map<String, Object>> initData = new ArrayList<Map<String, Object>>();
    private boolean freshOrMore = false;
    private int total_count = 0;

    public static CommentItemFragment getInstance(String title, Integer code) {
        CommentItemFragment sf = new CommentItemFragment();
        sf.mTitle = title;
        sf.code = code;
        return sf;
    }

    public static CommentItemFragment getInstance(String title, Integer code, Integer mode) {
        CommentItemFragment sf = new CommentItemFragment();
        sf.mTitle = title;
        sf.code = code;
        sf.mode = mode;
        return sf;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        return super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_smart_refresh_layout, null);
        initView(view);
        return view;
    }

    private void initView(View view) {
        mRecyclerView = view.findViewById(R.id.recyclerView);
        mRefreshLayout = view.findViewById(R.id.refreshLayout);

        mRefreshLayout.setOnRefreshLoadMoreListener(this);
        userViewLikeCommentAdapter = new UserViewLikeCommentAdapter();
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerView.setAdapter(userViewLikeCommentAdapter);
        userViewLikeCommentAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                ArticleItemBean item = (ArticleItemBean) adapter.getItem(position);
                startActivity(new Intent(getActivity(), ArticleDetailsActivity.class).putExtra("ArticleItemBean", item));
            }
        });
        requestArticle();

    }

    private void requestArticle() {
        int categoryId = code;
        OkGo okGo = OkGo.getInstance();
        HttpParams commonParams = new HttpParams();
        if (code != 0) {
            okGo.addCommonParams(commonParams);
        }

        okGo.<ResultView>get(UrlApi.userViewLikeCommentList)
                .tag(this)
                .headers("mbtToken", SpUtils.getToken(getActivity()))
                .params("type", 2)
                .params("objId", SpUtils.getDoctorId(getActivity()))
                .params("objType", 6)
                .params("pageNumber", pageNumber)
                .execute(new JsonCallback<ResultView>() {
                    @Override
                    public void onSuccess(Response<ResultView> response) {
                        if (response != null && response.body() != null) {
                            ResultView resultView = response.body();
                            if (resultView.getData() != null) {
                                total_count = resultView.getCount();
                                List<Map<String, Object>> data = (List<Map<String, Object>>) resultView.getData();

                                if (freshOrMore) {
                                    if (initData.size() >= total_count) {
                                        mRefreshLayout.finishLoadMore();
                                    } else {
                                        initData.addAll(data);
                                    }
                                    freshOrMore = false;
                                    userViewLikeCommentAdapter.setNewData(initData);
                                    mRefreshLayout.finishLoadMore();
                                } else {
                                    initData = data;
                                    userViewLikeCommentAdapter.setNewData(initData);
                                    mRefreshLayout.finishRefresh();
                                }
                            }
                            mRefreshLayout.finishRefresh();
                            mRefreshLayout.finishLoadMore();
                        }
                    }
                });


    }

    @Override
    public void onLoadMore(@NonNull RefreshLayout refreshLayout) {
        pageNumber++;
        freshOrMore = true;
        requestArticle();
    }

    @Override
    public void onRefresh(@NonNull RefreshLayout refreshLayout) {
        pageNumber = 1;
        requestArticle();
    }
}
