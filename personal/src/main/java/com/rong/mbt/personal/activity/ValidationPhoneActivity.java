package com.rong.mbt.personal.activity;


import android.content.Intent;
import android.graphics.Color;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.callback.StringCallback;
import com.lzy.okgo.model.Response;
import com.rong.mbt.personal.R;
import com.rong.mbt.personal.activity.base.PBaseActivity;
import com.rong.mbt.personal.bean.base.BaseBean;
import com.rong.mbt.personal.utils.PhoneUtil;

//验证手机号
public class ValidationPhoneActivity extends PBaseActivity {


    private CountDownTimer timer;
    private TextView tvGetCode;
    private String mbtToken;
    private String phone;

    @Override
    public int getLayoutId() {
        return R.layout.activity_validation_phone;
    }

    @Override
    public void initView() {
        super.initView();

        phone = getIntent().getStringExtra("phone");
        mbtToken = getIntent().getStringExtra("mbtToken");
        findViewById(R.id.head_while_iv_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        TextView tvTitle = findViewById(R.id.head_while_tv_center);
        tvTitle.setText("更换手机号码");
        TextView tvPhone = findViewById(R.id.act_vp_tv_phone);
        tvGetCode = findViewById(R.id.tv_get_code);
        final EditText etCode = findViewById(R.id.act_validtion_phone_et);
        Button btNext = findViewById(R.id.act_validtion_phone_bt);


        tvPhone.setText("原手机号码：" + PhoneUtil.phoneShowStar(phone));


        tvGetCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                timer.start();
                if (!phone.isEmpty()) {
                    OkGo.<String>post("http://service.jiangaifen.com:38082/register/get-check-code")
                            .tag(this)
//                            .isMultipart(true)
                            .headers("mbtToken", mbtToken)
                            .params("mobile", phone)
                            .params("action", "11")
                            .params("isChannel", true)
                            .execute(new StringCallback() {
                                @Override
                                public void onSuccess(Response<String> response) {
                                    Log.i("test", "onSuccess  " + response.body());
                                }

                                @Override
                                public void onError(Response<String> response) {
                                    super.onError(response);
                                    Log.i("test", "onError  " + response.message());
                                }
                            });
                }
            }
        });


        btNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!etCode.getText().toString().isEmpty()) {
                    OkGo.<String>post("http://service.jiangaifen.com:38082/user/checkPhone")
                            .tag(this)
//                            .isMultipart(true)
                            .headers("mbtToken", mbtToken)
                            .params("phoneNum", phone)
                            .params("vcode", etCode.getText().toString())
                            .execute(new StringCallback() {
                                @Override
                                public void onSuccess(Response<String> response) {
                                    //{"msg":"验证码错误或失效","resultCode":200}
//                                    {"msg":"请求成功","resultCode":0}
                                    try {
                                        BaseBean baseBean = JSON.parseObject(response.body(), BaseBean.class);
                                        if (baseBean.getResultCode() == 0) {
                                            Intent intent = new Intent(ValidationPhoneActivity.this, UpdateBindingPhoneActivity.class);
                                            intent.putExtra("mbtToken", mbtToken);
                                            startActivity(intent);
                                            finish();
                                        } else {
                                            Toast.makeText(ValidationPhoneActivity.this, baseBean.getMsg(), Toast.LENGTH_SHORT).show();
                                        }
                                    } catch (Exception e) {
                                    }

                                }

                                @Override
                                public void onError(Response<String> response) {
                                    super.onError(response);
                                    Log.i("test", "onError  " + response.message());
                                }
                            });
                }

//                Intent intent = new Intent(ValidationPhoneActivity.this, UpdateBindingPhoneActivity.class);
//                intent.putExtra("mbtToken", mbtToken);
//                startActivity(intent);
            }
        });
        downTimer();
    }

    private void downTimer() {
        timer = new CountDownTimer(60000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                tvGetCode.setText(((millisUntilFinished + 1000) / 1000) + "s");
                tvGetCode.setEnabled(false);
                tvGetCode.setTextColor(Color.parseColor("#999999"));
            }

            @Override
            public void onFinish() {
                tvGetCode.setEnabled(true);
                tvGetCode.setText("重新发送");
                tvGetCode.setTextColor(Color.parseColor("#7282FB"));
            }
        };

    }

    @Override
    protected void onStop() {
        super.onStop();
        if (timer != null) {
            timer.cancel();
        }
    }

}
