package com.rong.mbt.personal.adapter;

import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.rong.mbt.personal.R;
import com.rong.mbt.personal.bean.doctor.DoctorListBean;

/**
 * 文章列表
 */
public class DoctorListAdapter extends BaseQuickAdapter<DoctorListBean, BaseViewHolder> {


    public DoctorListAdapter() {
        super(R.layout.item_patient_index_consultation);
    }

    @Override
    protected void convert(BaseViewHolder helper, DoctorListBean item) {

        try {
            ImageView ivHead = helper.getView(R.id.iv_head);
            Glide.with(helper.itemView.getContext()).load(item.getDoctor().getHeaderimg()).into(ivHead);

            TextView tvName = helper.getView(R.id.tv_name);
            tvName.setText(item.getUser().getName());

            String doctorTypeName = "";
            int doctorTitle = item.getDoctor().getDoctorTitle();
            if (doctorTitle == 1) {
                doctorTypeName = "助理医生";
            } else {
                doctorTypeName = "执业医生";
            }
            TextView tvDoctorType = helper.getView(R.id.tv_doctor_type);
            tvDoctorType.setText(doctorTypeName);//医生头衔(1.助理医生 2.执业医生)


            TextView tvHospitalName = helper.getView(R.id.tv_hospital_name);
            tvHospitalName.setText(item.getHospital().getName());

            TextView tvAddress = helper.getView(R.id.tv_address);
            tvAddress.setText(item.getHospital().getAddress());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
