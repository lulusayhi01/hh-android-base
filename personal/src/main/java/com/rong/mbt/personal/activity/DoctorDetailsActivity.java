package com.rong.mbt.personal.activity;

import android.content.Intent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.fastjson.JSONObject;
import com.bumptech.glide.Glide;
import com.calanger.hh.entity.ResultView;
import com.calanger.hh.model.Doctor;
import com.calanger.hh.model.Hospital;
import com.calanger.hh.model.OrderPre;
import com.calanger.hh.model.User;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.model.Response;
import com.rong.mbt.personal.R;
import com.rong.mbt.personal.activity.base.PBaseActivity;
import com.rong.mbt.personal.bean.doctor.DoctorListBean;
import com.rong.mbt.personal.http.UrlApi;
import com.rong.mbt.personal.http.callback.JsonCallback;
import com.rong.mbt.personal.utils.SpUtils;
import com.shehuan.niv.NiceImageView;

import java.util.Map;

import io.rong.imkit.RongIM;

/**
 * 医生详情（咨询医生页面）
 */
public class DoctorDetailsActivity extends PBaseActivity {


    NiceImageView ivHead;
    TextView tvName;
    TextView tvDoctorType;
    TextView tvHospitalName;
    TextView tvAddress;
    ImageView ivCard1;
    ImageView ivCard2;
    TextView tvNotice;

    TextView tvConsultation;
    TextView tvFollow;
    TextView tvMOre;
    TextView txtTitle;
    LinearLayout back;

    private Doctor doctor;
    private User user;
    private Hospital hospital;
    private boolean follow;
    private int followStatus = 0;

    @Override
    public void initView() {
        super.initView();
        DoctorListBean orderPreBean = (DoctorListBean) getIntent().getSerializableExtra("DoctorListBean");

        ivHead = findViewById(R.id.iv_head);
        tvName = findViewById(R.id.tv_name);
        tvDoctorType = findViewById(R.id.tv_doctor_type);
        tvHospitalName = findViewById(R.id.tv_hospital_name);
        tvAddress = findViewById(R.id.tv_address);
        ivCard1 = findViewById(R.id.iv_Card1);
        ivCard2 = findViewById(R.id.iv_Card2);
        tvNotice = findViewById(R.id.tv_notice);
        tvConsultation = findViewById(R.id.tv_Consultation);
        tvFollow = findViewById(R.id.tv_follow);
        tvMOre = findViewById(R.id.tv_more);
        txtTitle = findViewById(R.id.txt_title);
        back = findViewById(R.id.ll_back);

        txtTitle.setText("医师信息");
        tvFollow.setText("关注他");

        listener();

        if (orderPreBean != null) {
            //doctor
            doctor.setId(orderPreBean.getDoctor().getId());

            Glide.with(this).load(orderPreBean.getDoctor().getHeaderimg()).into(ivHead);
            Glide.with(this).load(orderPreBean.getDoctor().getIdCardBackImg()).into(ivCard1);
            Glide.with(this).load(orderPreBean.getDoctor().getIdCardFrontImg()).into(ivCard2);

            tvName.setText(orderPreBean.getUser().getName());

            String doctorTypeName = "";
            int doctorTitle = orderPreBean.getDoctor().getDoctorTitle();
            if (doctorTitle == 1) {
                doctorTypeName = "助理医生";
            } else {
                doctorTypeName = "执业医生";
            }
            tvDoctorType.setText(doctorTypeName);//医生头衔(1.助理医生 2.执业医生)
            tvHospitalName.setText(orderPreBean.getHospital().getName());

            tvAddress.setText(orderPreBean.getHospital().getAddress());

            tvConsultation.setOnClickListener(v -> {
                RongIM.getInstance().startPrivateChat(this, orderPreBean.getDoctor().getUserId() + "", orderPreBean.getUser().getName());

            });
        } else {
            Integer id = (Integer) getIntent().getIntExtra("id", 0);

            OkGo.<ResultView>post(UrlApi.doctorGetSingle)
                    .tag(this)
                    .headers("mbtToken", SpUtils.getToken(this))
                    .params("id", id)
                    .execute(new JsonCallback<ResultView>() {
                        @Override
                        public void onSuccess(Response<ResultView> response) {
                            if (response != null && response.body() != null) {
                                ResultView resultView = response.body();
                                if (resultView.getResultCode() == 0) {
//                                        requestSelectFriendRemark(orderPreBean.getUserId()+"",orderPreBean.getName());
                                    Map<String, Object> map = (Map<String, Object>) resultView.getData();
                                    JSONObject jo = new JSONObject(map);
                                    doctor = (Doctor) jo.getObject("doctor", Doctor.class);
                                    user = (User) jo.getObject("user", User.class);
                                    hospital = (Hospital) jo.getObject("hospital", Hospital.class);
                                    if(SpUtils.getUserType(DoctorDetailsActivity.this)==0){
                                        follow = jo.getObject("follow", Boolean.class);
                                        if(follow){
                                            tvFollow.setText("已关注");
                                            followStatus = 1;
                                        }else{
                                            tvFollow.setText("关注他");
                                            followStatus = 0;
                                        }
                                    }

                                    Glide.with(DoctorDetailsActivity.this).load(doctor.getHeaderimg()).into(ivHead);
                                    Glide.with(DoctorDetailsActivity.this).load(doctor.getIdCardBackImg()).into(ivCard1);
                                    Glide.with(DoctorDetailsActivity.this).load(doctor.getIdCardFrontImg()).into(ivCard2);

                                    tvName.setText(user.getName());

                                    String doctorTypeName = "";
                                    int doctorTitle = doctor.getDoctorTitle();
                                    if (doctorTitle == 1) {
                                        doctorTypeName = "助理医生";
                                    } else {
                                        doctorTypeName = "执业医生";
                                    }
                                    tvDoctorType.setText(doctorTypeName);//医生头衔(1.助理医生 2.执业医生)
                                    tvHospitalName.setText(hospital.getName());

                                    tvAddress.setText(hospital.getAddress());

                                    tvConsultation.setOnClickListener(v -> {
//                                        缺少了一个支付费用的弹窗

                                        RongIM.getInstance().startPrivateChat(DoctorDetailsActivity.this, doctor.getUserId() + "", user.getName());

                                    });
                                } else {
                                    Toast.makeText(getApplicationContext(), resultView.getMsg(), Toast.LENGTH_SHORT);
                                }
                            }
                        }
                    });
        }
    }


    public void listener() {
        back.setOnClickListener(v -> finish());
        tvFollow.setOnClickListener(v -> initDataByUrl(followStatus));

        tvNotice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(DoctorDetailsActivity.this, "还未完成调试", Toast.LENGTH_SHORT);
            }
        });

        tvMOre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(DoctorDetailsActivity.this, DoctorPersonMainPageActivity.class).putExtra("id", doctor.getId()));
            }
        });
    }

    public void initDataByUrl(int action) {

        switch (action) {
            // 网络不可用给出提示
            case 1:
                OkGo.<ResultView>get(UrlApi.followCancel)
                        .tag(this)
                        .headers("mbtToken", SpUtils.getToken(getApplication()))
                        .params("followId", doctor.getId())//被关注的医生的id
                        .params("objType", 1)//关注对象的类型 码值1:医生 2：文章
                        .execute(new JsonCallback<ResultView>() {
                            @Override
                            public void onSuccess(Response<ResultView> response) {
                                if (response != null && response.body() != null) {
                                    ResultView resultView = response.body();
                                    if (resultView.getResultCode() == 0) {
                                        Toast.makeText(getApplicationContext(), resultView.getMsg(), Toast.LENGTH_SHORT);
                                        tvFollow.setText("关注他");
                                        followStatus = 0;
                                    }
                                }
                            }
                        });
                break;
            case 0:
                OkGo.<ResultView>get(UrlApi.followInsert)
                        .tag(this)
                        .headers("mbtToken", SpUtils.getToken(getApplication()))
                        .params("followId", doctor.getId())//被关注的医生的id
                        .params("objType", 1)//关注对象的类型 码值1:医生 2：文章
                        .execute(new JsonCallback<ResultView>() {
                            @Override
                            public void onSuccess(Response<ResultView> response) {
                                if (response != null && response.body() != null) {
                                    ResultView resultView = response.body();
                                    if (resultView.getResultCode() == 0) {
                                        Toast.makeText(getApplicationContext(), resultView.getMsg(), Toast.LENGTH_SHORT);
                                        tvFollow.setText("已关注");
                                        followStatus = 1;
                                    }
                                }
                            }
                        });
                break;
        }


    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_doctor_details;
    }

}
