package com.rong.mbt.personal.activity;


import android.graphics.Color;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.callback.StringCallback;
import com.lzy.okgo.model.Response;
import com.rong.mbt.personal.R;
import com.rong.mbt.personal.activity.base.PBaseActivity;
import com.rong.mbt.personal.bean.base.BaseBean;
import com.rong.mbt.personal.bean.PhoneInfoBean;

//更换绑定手机号码， 绑定新手机号码
public class UpdateBindingPhoneActivity extends PBaseActivity {


    private CountDownTimer timer;
    private TextView tvGetVcode;
    private EditText etPhone;
    private EditText etVcode;
    private String mbtToken;

    @Override
    public int getLayoutId() {
        return R.layout.activity_update_binding_phone;
    }

    @Override
    public void initView() {
        super.initView();

        mbtToken = getIntent().getStringExtra("mbtToken");

        //新手机号
        etPhone = findViewById(R.id.act_ubp_et_phone);
        tvGetVcode = findViewById(R.id.act_ubp_tv_vcode);
        etVcode = findViewById(R.id.act_ubp_et_vcode);
        Button button = findViewById(R.id.act_ubp_bt);

        tvGetVcode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!etPhone.getText().toString().isEmpty()) {
                    //获取验证码
                    timer.start();
                    if (!etPhone.getText().toString().isEmpty()) {
                        OkGo.<String>post("http://service.jiangaifen.com:38082/register/get-check-code")
                                .tag(this)
//                            .isMultipart(true)
                                .headers("mbtToken", mbtToken)
                                .params("mobile", etPhone.getText().toString())
                                .params("action", "12")
                                .params("isChannel", true)
                                .execute(new StringCallback() {
                                    @Override
                                    public void onSuccess(Response<String> response) {
                                        try {
                                            String body = response.body();

                                            PhoneInfoBean phoneInfoBean = JSON.parseObject(body, PhoneInfoBean.class);
                                            if (phoneInfoBean.getResultCode() != 0) {
                                                Toast.makeText(UpdateBindingPhoneActivity.this, phoneInfoBean.getMsg(), Toast.LENGTH_SHORT).show();
                                            }
                                        }catch (Exception e){}
                                    }

                                    @Override
                                    public void onError(Response<String> response) {
                                        super.onError(response);
                                        Log.i("test", "onError  " + response.message());
                                        Toast.makeText(UpdateBindingPhoneActivity.this, response.message(), Toast.LENGTH_SHORT).show();
                                    }
                                });
                    }
                }
            }
        });
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!etPhone.getText().toString().isEmpty() && !etVcode.getText().toString().isEmpty()) {
                    //完成
                    OkGo.<String>post("http://service.jiangaifen.com:38082/user/changePhone")
                            .tag(this)
//                            .isMultipart(true)
                            .headers("mbtToken", mbtToken)
                            .params("phoneNum", etPhone.getText().toString())
                            .params("vcode", etVcode.getText().toString())
                            .execute(new StringCallback() {
                                @Override
                                public void onSuccess(Response<String> response) {
                                    Log.i("test", "onSuccess  " + response.body());
                                    try {
                                        BaseBean baseBean = JSON.parseObject(response.body(), BaseBean.class);
                                        if (baseBean.getResultCode() == 0) {
                                            finish();
                                        } else {
                                            Toast.makeText(UpdateBindingPhoneActivity.this, baseBean.getMsg(), Toast.LENGTH_SHORT).show();
                                        }
                                    }catch (Exception e){}
                                }

                                @Override
                                public void onError(Response<String> response) {
                                    super.onError(response);
                                    Toast.makeText(UpdateBindingPhoneActivity.this, response.message(), Toast.LENGTH_SHORT).show();
                                }
                            });
                }
            }
        });
        downTimer();
    }

    private void downTimer() {
        timer = new CountDownTimer(60000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                tvGetVcode.setText(((millisUntilFinished + 1000) / 1000) + "s");
                tvGetVcode.setEnabled(false);
                tvGetVcode.setTextColor(Color.parseColor("#999999"));
            }

            @Override
            public void onFinish() {
                tvGetVcode.setEnabled(true);
                tvGetVcode.setText("重新发送");
                tvGetVcode.setTextColor(Color.parseColor("#7282FB"));
            }
        };
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (timer != null) {
            timer.cancel();
        }
    }
}
