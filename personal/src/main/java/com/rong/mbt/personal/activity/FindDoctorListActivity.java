package com.rong.mbt.personal.activity;

import android.content.Intent;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.model.Response;
import com.rong.mbt.personal.R;
import com.rong.mbt.personal.activity.base.PBaseActivity;
import com.rong.mbt.personal.adapter.FindDoctorListAdapter;
import com.rong.mbt.personal.bean.doctor.DoctorIndexBean;
import com.rong.mbt.personal.bean.doctor.DoctorListBean;
import com.rong.mbt.personal.http.UrlApi;
import com.rong.mbt.personal.http.callback.JsonCallback;
import com.rong.mbt.personal.utils.SpUtils;

import java.util.List;

/**
 * 找医生
 */
public class FindDoctorListActivity extends PBaseActivity {

    @Override
    public void initView() {
        super.initView();

        String officesId = getIntent().getStringExtra("officesId");

        RecyclerView recyclerView = findViewById(R.id.recycler_view);
        TextView textTitle = findViewById(R.id.txt_title);
        textTitle.setText("找医生");

        LinearLayout back = findViewById(R.id.ll_back);
        back.setOnClickListener(v -> finish());

        recyclerView.setLayoutManager(new GridLayoutManager(getApplication(), 1));
        FindDoctorListAdapter doctorListAdapter = new FindDoctorListAdapter();
        doctorListAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                DoctorListBean item = (DoctorListBean) adapter.getItem(position);
                startActivity(new Intent(FindDoctorListActivity.this,DoctorDetailsActivity.class).putExtra("id",item.getDoctor().getId()));
            }
        });
        recyclerView.setAdapter(doctorListAdapter);

        OkGo.<DoctorIndexBean>get(UrlApi.userIndexDoctorList)
                .tag(this)
                .headers("mbtToken", SpUtils.getToken(getApplication()))
                .params("officesId",officesId)//科室
                .execute(new JsonCallback<DoctorIndexBean>() {
                    @Override
                    public void onSuccess(Response<DoctorIndexBean> response) {
                        if (response != null && response.body() != null) {
                            DoctorIndexBean body = response.body();
                            if (body.getData() != null) {
                                List<DoctorListBean> data = body.getData();
                                doctorListAdapter.setNewData(data);
                            }
                        }
                    }
                });
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_recycle_view;
    }

}
