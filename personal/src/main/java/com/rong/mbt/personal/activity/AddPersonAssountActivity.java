package com.rong.mbt.personal.activity;

import android.graphics.Color;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.callback.StringCallback;
import com.lzy.okgo.model.Response;
import com.rong.mbt.personal.R;
import com.rong.mbt.personal.activity.base.PBaseActivity;
import com.rong.mbt.personal.bean.base.BaseBean;
import com.rong.mbt.personal.dialog.pickerDialog.OptionPicker;
import com.rong.mbt.personal.utils.PhoneUtil;

import java.util.ArrayList;

public class AddPersonAssountActivity extends PBaseActivity {


    private String phone;
    private TextView tvYzm;
    private CountDownTimer timer;
    private String mbtToken;


    private boolean isBank = false; //true 银行卡  false 支付宝
    private LinearLayout linearZhsyr;
    private LinearLayout linearZfbzh;
    private LinearLayout linearCkrxm;
    private LinearLayout linearSsyh;
    private LinearLayout linearyhkkh;
    private EditText et1Yzm;
    private EditText et2Zmxm;
    private EditText et3Zfbzh;
    private EditText et4Yhkxm;
    private TextView et5Ssyh;
    private EditText et6yhkh;

    private String[] banks = {"CDB", "ICBC", "ABC", "BOC", "CCB", "PSBC", "COMM", "CMB", "SPDB", "CIB", "HXBANK"
            , "GDB", "CMBC", "CITIC", "CEB", "EGBANK", "CZBANK", "BOHAIB", "SPABANK", "SHRCB", "YXCCB", "YDRCB", "BJBANK", "SHBANK"};
    private String bankString;
    private ImageView imgCheckAli;
    private ImageView imgCheckBank;

    @Override
    public int getLayoutId() {
        return R.layout.activity_add_person_assount;
    }

    @Override
    public void initView() {
        super.initView();
        phone = getIntent().getStringExtra("phone");
        mbtToken = getIntent().getStringExtra("mbtToken");
        find();
        listener();
        downTimer();
    }

    public void addpersonOk(View view) {
        String et1 = et1Yzm.getText().toString();
        String et2 = et2Zmxm.getText().toString();
        String et3 = et3Zfbzh.getText().toString();
        String et4 = et4Yhkxm.getText().toString();
        String et5 = et5Ssyh.getText().toString();
        String et6 = et6yhkh.getText().toString();

        if (!isBank) {
            if (et1.isEmpty() || et2.isEmpty() || et3.isEmpty()) {
                return;
            }
            addAliPay(et1, et2, et3);
        } else {
            if (et1.isEmpty() || et4.isEmpty() || et5.isEmpty() || et6.isEmpty()) {
                return;
            }
            addBank(et1, et4, et5, et6);
        }
    }

    private void find() {
        TextView tvTitle = findViewById(R.id.head_blue_tv_center);
        tvTitle.setText("添加账户");
        findViewById(R.id.head_blue_iv_right).setVisibility(View.GONE);

        TextView tvPhoneInfo = findViewById(R.id.act_apa_phone_info);
        tvPhoneInfo.setText("手机验证码将发送到" + PhoneUtil.phoneShowStar(phone) + "手机上，请注意查收");

        tvYzm = findViewById(R.id.act_apa_tv_yzm);

        linearZhsyr = findViewById(R.id.linear_zhsyr);
        linearZfbzh = findViewById(R.id.linear_zfbzh);
        linearCkrxm = findViewById(R.id.linear_ckrxm);
        linearSsyh = findViewById(R.id.linear_ssyh);
        linearyhkkh = findViewById(R.id.yhkkh);

        et1Yzm = findViewById(R.id.act_et1_yzm);
        et2Zmxm = findViewById(R.id.act_et2_zmxm);
        et3Zfbzh = findViewById(R.id.act_et3_zfbzh);
        et4Yhkxm = findViewById(R.id.act_et4_yhkxm);
        et5Ssyh = findViewById(R.id.act_e5_ssyh);
        et6yhkh = findViewById(R.id.act_et6_yhkh);

        imgCheckAli = findViewById(R.id.act_apa_check_alipay);
        imgCheckBank = findViewById(R.id.act_apa_check_bank);

    }

    private void listener() {
        findViewById(R.id.head_blue_iv_left).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        //支付宝
        findViewById(R.id.act_apa_tv_zfb).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isBank = false;
                linearZhsyr.setVisibility(View.VISIBLE);
                linearZfbzh.setVisibility(View.VISIBLE);
                linearCkrxm.setVisibility(View.GONE);
                linearSsyh.setVisibility(View.GONE);
                linearyhkkh.setVisibility(View.GONE);
                imgCheckAli.setBackgroundResource(R.drawable.check_box_select);
                imgCheckBank.setBackgroundResource(R.drawable.check_box_unselect);
            }
        });
        //银行卡
        findViewById(R.id.act_apa_tv_yhk).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isBank = true;
                linearZhsyr.setVisibility(View.GONE);
                linearZfbzh.setVisibility(View.GONE);
                linearCkrxm.setVisibility(View.VISIBLE);
                linearSsyh.setVisibility(View.VISIBLE);
                linearyhkkh.setVisibility(View.VISIBLE);
                imgCheckBank.setBackgroundResource(R.drawable.check_box_select);
                imgCheckAli.setBackgroundResource(R.drawable.check_box_unselect);
            }
        });

        et5Ssyh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                optionPick();
            }
        });
        tvYzm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                timer.start();
                if (!phone.isEmpty()) {
                    OkGo.<String>post("http://service.jiangaifen.com:38082/register/get-check-code")
                            .tag(this)
//                            .isMultipart(true)
                            .headers("mbtToken", mbtToken)
                            .params("mobile", phone)
                            .params("action", "14")
                            .params("isChannel", true)
                            .execute(new StringCallback() {
                                @Override
                                public void onSuccess(Response<String> response) {
                                    Log.i("test", "onSuccess  " + response.body());
                                    JSONObject jsonObject = JSON.parseObject(response.body());
                                    if(jsonObject.getIntValue("resultCode") !=0){
                                        Toast.makeText(AddPersonAssountActivity.this, jsonObject.getString("msg"), Toast.LENGTH_SHORT).show();
                                    }
                                }

                                @Override
                                public void onError(Response<String> response) {
                                    super.onError(response);
                                    Log.i("test", "onError  " + response.message());
                                }

                            });
                }
            }

        });
    }

    private void downTimer() {
        timer = new CountDownTimer(60000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                tvYzm.setText(((millisUntilFinished + 1000) / 1000) + "s");
                tvYzm.setEnabled(false);
                tvYzm.setTextColor(Color.parseColor("#999999"));
            }

            @Override
            public void onFinish() {
                tvYzm.setEnabled(true);
                tvYzm.setText("重新发送");
                tvYzm.setTextColor(Color.parseColor("#7282FB"));
            }
        };
    }

    public void optionPick() {
        ArrayList<String> list = new ArrayList<>();
        list.add("国家开发银行");
        list.add("中国工商银行");
        list.add("中国农业银行");
        list.add("中国银行");
        list.add("中国建设银行");
        list.add("中国邮政储蓄银行");
        list.add("交通银行");
        list.add("招商银行");
        list.add("上海浦东发展银行");
        list.add("兴业银行");
        list.add("华夏银行");
        list.add("广东发展银行");
        list.add("中国民生银行");
        list.add("中信银行");
        list.add("中国光大银行");
        list.add("恒丰银行");
        list.add("浙商银行");
        list.add("渤海银行");
        list.add("平安银行");
        list.add("上海农村商业银行");
        list.add("玉溪市商业银行");
        list.add("尧都农商行");
        list.add("北京银行");
        list.add("上海银行");

        OptionPicker picker = new OptionPicker(this, list); //list为选择器中的选项
        picker.setOffset(2);
        picker.setSelectedIndex(1); //默认选中项
        picker.setTextSize(18);
        picker.setCycleDisable(true); //选项不循环滚动
        picker.setOnOptionPickListener(new OptionPicker.OnOptionPickListener() {
            @Override
            public void onOptionPicked(int position, String option) {
                et5Ssyh.setText(option);
                bankString = banks[position];
            }
        });
        picker.show();
    }

    public void addAliPay(String yzm, String zhxm, String zhbzh) {
        OkGo.<String>post("http://service.jiangaifen.com:38082/userAccount/edit")
                .tag(this)
                .headers("mbtToken", mbtToken)
                .params("type", "1")
                .params("phoneNum", phone)
                .params("vcode", yzm)
                .params("account", zhbzh)
                .params("name", zhxm)
                .params("dealtype","4201")
                .execute(new StringCallback() {
                    @Override
                    public void onSuccess(Response<String> response) {
                        Log.i("test", "onSuccess  " + response.body());
                        BaseBean baseBean = JSON.parseObject(response.body(), BaseBean.class);
                        if (baseBean.getResultCode() == 0) {
                            finish();
                        } else {
                            Toast.makeText(AddPersonAssountActivity.this, baseBean.getMsg(), Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onError(Response<String> response) {
                        super.onError(response);
                        Toast.makeText(AddPersonAssountActivity.this, response.message(), Toast.LENGTH_SHORT).show();
                    }

                });
    }

    public void addBank(String yzm, String cyrxm, String ssyh, String yhkh) {
        OkGo.<String>post("http://service.jiangaifen.com:38082/userAccount/edit")
                .tag(this)
                .headers("mbtToken", mbtToken)
                .params("type", "4")
                .params("phoneNum", phone)
                .params("vcode", yzm)
                .params("name", cyrxm)
                .params("bank", bankString)
                .params("account", yhkh)
                .params("dealtype","4201")
                .execute(new StringCallback() {
                    @Override
                    public void onSuccess(Response<String> response) {
                        Log.i("test", "onSuccess  " + response.body());
                        BaseBean baseBean = JSON.parseObject(response.body(), BaseBean.class);
                        if (baseBean.getResultCode() == 0) {
                            finish();
                        } else {
                            Toast.makeText(AddPersonAssountActivity.this, baseBean.getMsg(), Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onError(Response<String> response) {
                        super.onError(response);
                        Log.i("test", "onError  " + response.message());
                        Toast.makeText(AddPersonAssountActivity.this, response.message(), Toast.LENGTH_SHORT).show();
                    }

                });
    }
}