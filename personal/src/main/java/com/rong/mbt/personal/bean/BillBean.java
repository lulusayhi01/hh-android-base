package com.rong.mbt.personal.bean;

import java.util.List;

public class BillBean {


    private int count;
    private String msg;
    private int resultCode;
    private List<DataBean> data;

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public int getResultCode() {
        return resultCode;
    }

    public void setResultCode(int resultCode) {
        this.resultCode = resultCode;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * createtime : 2018-08-16 19:36:20
         * dealstatus : 4201
         * dealtype : 4102
         * id : 95
         * price : 200.0
         * status : 1
         * tuserid : 36
         * typedetailid : 4304
         * userHeadImage : http://oss-aliyun-liululu.oss-cn-shanghai.aliyuncs.com/upload/153429626057510425.jpg
         * userId : 36
         * userName : jinyawen
         * userPhone : 15888561823
         */

        private String createtime;
        private int dealstatus;
        private int dealtype;
        private int id;
        private double price;
        private int status;
        private int tuserid;
        private int typedetailid;
        private String userHeadImage;
        private int userId;
        private String userName;
        private String userPhone;

        public String getCreatetime() {
            return createtime;
        }

        public void setCreatetime(String createtime) {
            this.createtime = createtime;
        }

        public int getDealstatus() {
            return dealstatus;
        }

        public void setDealstatus(int dealstatus) {
            this.dealstatus = dealstatus;
        }

        public int getDealtype() {
            return dealtype;
        }

        public void setDealtype(int dealtype) {
            this.dealtype = dealtype;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public double getPrice() {
            return price;
        }

        public void setPrice(double price) {
            this.price = price;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public int getTuserid() {
            return tuserid;
        }

        public void setTuserid(int tuserid) {
            this.tuserid = tuserid;
        }

        public int getTypedetailid() {
            return typedetailid;
        }

        public void setTypedetailid(int typedetailid) {
            this.typedetailid = typedetailid;
        }

        public String getUserHeadImage() {
            return userHeadImage;
        }

        public void setUserHeadImage(String userHeadImage) {
            this.userHeadImage = userHeadImage;
        }

        public int getUserId() {
            return userId;
        }

        public void setUserId(int userId) {
            this.userId = userId;
        }

        public String getUserName() {
            return userName;
        }

        public void setUserName(String userName) {
            this.userName = userName;
        }

        public String getUserPhone() {
            return userPhone;
        }

        public void setUserPhone(String userPhone) {
            this.userPhone = userPhone;
        }
    }
}
