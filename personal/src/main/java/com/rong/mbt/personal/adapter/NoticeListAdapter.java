package com.rong.mbt.personal.adapter;

import android.widget.ImageView;
import android.widget.TextView;

import com.alibaba.fastjson.JSONObject;
import com.calanger.hh.model.MedicalRecord;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.rong.mbt.personal.R;

import java.text.SimpleDateFormat;
import java.util.Map;

/**
 * 文章列表
 */
public class NoticeListAdapter extends BaseQuickAdapter<Map<String,Object>, BaseViewHolder> {


    public NoticeListAdapter() {
        super(R.layout.item_medical_record);
    }

    @Override
    protected void convert(BaseViewHolder helper,  Map<String,Object> item) {

        try {

            JSONObject jsonMap = new JSONObject(item);

            TextView tvName = helper.getView(R.id.tv_name);
            MedicalRecord medicalRecord =  jsonMap.getObject("medicalRecord",MedicalRecord.class);
            tvName.setText(medicalRecord.getTitle());

            ImageView ivHead = helper.getView(R.id.iv_head);
//            Glide.with(helper.itemView.getContext()).load(item.getDoctor().getHeaderimg()).into(ivHead);

            TextView tvSecond = helper.getView(R.id.tv_second);
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            tvSecond.setText(sdf.format(medicalRecord.getCreattime()));

            helper.addOnClickListener(R.id.tv_remove);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
