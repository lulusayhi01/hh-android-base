package com.rong.mbt.personal.bean;

import java.util.List;

/**
 * Created by jianfei on 2018/9/25.
 */

public class CashAccountListBean {

    /**
     * count : 2
     * data : [{"account":"jianfeiltb","createtime":"2018-09-25 17:42:30","dealtype":4202,"id":34,"name":"哈哈哈","status":1,"type":6,"userid":48},{"account":"jianfeibtb","createtime":"2018-09-25 17:38:46","dealtype":4202,"id":33,"name":"哈哈哈","status":1,"type":5,"userid":48}]
     * msg : 请求成功
     * resultCode : 0
     */

    private int count;
    private String msg;
    private int resultCode;
    private List<DataBean> data;

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public int getResultCode() {
        return resultCode;
    }

    public void setResultCode(int resultCode) {
        this.resultCode = resultCode;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * account : jianfeiltb
         * createtime : 2018-09-25 17:42:30
         * dealtype : 4202
         * id : 34
         * name : 哈哈哈
         * status : 1
         * type : 6
         * userid : 48
         */

        private String account;
        private String createtime;
        private int dealtype;
        private int id;
        private String name;
        private int status;
        private int type;
        private int userid;
        private boolean isSelector;

        public boolean isSelector() {
            return isSelector;
        }

        public void setSelector(boolean selector) {
            isSelector = selector;
        }

        public String getAccount() {
            return account;
        }

        public void setAccount(String account) {
            this.account = account;
        }

        public String getCreatetime() {
            return createtime;
        }

        public void setCreatetime(String createtime) {
            this.createtime = createtime;
        }

        public int getDealtype() {
            return dealtype;
        }

        public void setDealtype(int dealtype) {
            this.dealtype = dealtype;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public int getType() {
            return type;
        }

        public void setType(int type) {
            this.type = type;
        }

        public int getUserid() {
            return userid;
        }

        public void setUserid(int userid) {
            this.userid = userid;
        }
    }
}
