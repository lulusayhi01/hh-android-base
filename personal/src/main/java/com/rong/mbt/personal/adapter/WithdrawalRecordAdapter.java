package com.rong.mbt.personal.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.rong.mbt.personal.R;
import com.rong.mbt.personal.activity.WithdrawalRecordActivity;
import com.rong.mbt.personal.bean.RechargeRecordInfoBean;
import com.rong.mbt.personal.utils.StringUtil;

import java.util.ArrayList;
import java.util.List;

public class WithdrawalRecordAdapter extends RecyclerView.Adapter {
    private Context context;
    private List<RechargeRecordInfoBean.DataBean> data = new ArrayList<>();

    public WithdrawalRecordAdapter(Context context) {
        this.context = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_recharge_record_item, parent, false);
        return new RechargeRecordAdapterHoder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        RechargeRecordAdapterHoder h = (RechargeRecordAdapterHoder) holder;

        RechargeRecordInfoBean.DataBean dataBean = data.get(position);
//        "dealstatus":4201,
//                "dealtype":4102,
//        "status":1,
//                "tuserid":36,
//                "typedetailid":4304,
//         'dealstatus' '交易状态 4201交易中 4202交易成功 4203 交易失败'
        h.tvMoney.setText(StringUtil.formatString(dataBean.getPrice()) + "元");
        h.tvDate.setText(dataBean.getCreatetime());
        switch (dataBean.getDealstatus()) {
            case 4201:
                h.tvInfo.setText("待确认到账");
                h.tvInfo.setTextColor(Color.parseColor("#FFF6592B"));
                break;
            case 4202:
                h.tvInfo.setText("提现成功");
                h.tvInfo.setTextColor(Color.parseColor("#FF333333"));
                break;
            case 4203:
                h.tvInfo.setText("提现失败");
                h.tvInfo.setTextColor(Color.parseColor("#FF333333"));
                break;
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public void setData(List<RechargeRecordInfoBean.DataBean> data) {
        for (int i = 0; i < data.size(); i++) {
            if (data.get(i).getTypedetailid() == 4303) {
                this.data.add(data.get(i));
            }
        }
        notifyDataSetChanged();
    }

    class RechargeRecordAdapterHoder extends RecyclerView.ViewHolder {

        private TextView tvMoney;
        private TextView tvDate;
        private TextView tvInfo;

        public RechargeRecordAdapterHoder(View itemView) {
            super(itemView);
            tvMoney = itemView.findViewById(R.id.item_rri_money);
            tvDate = itemView.findViewById(R.id.item_rri_date);
            tvInfo = itemView.findViewById(R.id.item_rri_info);
        }
    }
}
