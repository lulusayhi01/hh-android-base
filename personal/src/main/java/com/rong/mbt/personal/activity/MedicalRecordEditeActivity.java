package com.rong.mbt.personal.activity;


import android.app.Dialog;
import android.content.Context;
import android.os.Build;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.calanger.hh.entity.ResultView;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.model.Response;
import com.rong.mbt.personal.R;
import com.rong.mbt.personal.activity.base.PBaseActivity;
import com.rong.mbt.personal.dialog.pickerDialog.GlideImageLoader;
import com.rong.mbt.personal.http.UrlApi;
import com.rong.mbt.personal.http.callback.JsonCallback;
import com.rong.mbt.personal.utils.SpUtils;
import com.yancy.gallerypick.config.GalleryConfig;
import com.yancy.gallerypick.config.GalleryPick;
import com.yancy.gallerypick.inter.IHandlerCallBack;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class MedicalRecordEditeActivity extends PBaseActivity {

    protected Context mContext;
    private TextView tvYzm;


    private EditText etMedicalRecordTitle;
    private EditText etMedicalRecordInfo;
    private ImageView commonIvImgAdd;
    private LinearLayout llWindLayout;
    private Button btMedicalRecordSubmit;

    private String title;
    private String info;
    private List<File> fileList = new ArrayList<File>();;
    private GalleryConfig galleryConfig;
    private List<String> path = new ArrayList<>();
    private Dialog dialog_photo;

    @Override
    public int getLayoutId() {
        return R.layout.activity_edit_medical_record;
    }

    @Override
    public void initView() {
        super.initView();
//        phone = getIntent().getStringExtra("phone");
//        mbtToken = getIntent().getStringExtra("mbtToken");
        mContext = this;
        findView();
        initData();
        setListener();
//        listener();
//        downTimer();
    }

    public void checkOk(View view) {
        title = etMedicalRecordTitle.getText().toString();
        info = etMedicalRecordInfo.getText().toString();

    }

    private void findView() {
        TextView tvTitle = findViewById(R.id.header_center_text);
        tvTitle.setText("添加病历");
        commonIvImgAdd = findViewById(R.id.common_iv_img_add);
        llWindLayout  = findViewById(R.id.iiiimmmm);
//        findViewById(R.id.head_blue_iv_right).setVisibility(View.GONE);

        galleryConfig = new GalleryConfig.Builder()
                .imageLoader(new GlideImageLoader())    // ImageLoader 加载框架（必填）
                .iHandlerCallBack(iHandlerCallBack)     // 监听接口（必填）
                .provider("com.sj.hh.FileProvider")   // provider(必填)
                .pathList(path)                         // 记录已选的图片
//                .multiSelect(false)                      // 是否多选   默认：false
                .multiSelect(true, 9)                   // 配置是否多选的同时 配置多选数量   默认：false ， 9
//                .maxSize(9)                             // 配置多选时 的多选数量。    默认：9
                .crop(false)                             // 快捷开启裁剪功能，仅当单选 或直接开启相机时有效
                .crop(false, 1, 1, 500, 500)             // 配置裁剪功能的参数，   默认裁剪比例 1:1
                .isShowCamera(true)                     // 是否现实相机按钮  默认：false
                .filePath("/Gallery/Pictures")          // 图片存放路径
                .build();

    }


    private void initData(){

    }


    private void setListener() {
        findViewById(R.id.head_blue_iv_left).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        findViewById(R.id.common_iv_img_add).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                photoDialog();
            }
        });


        findViewById(R.id.bt_medical_record_submit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                OkGo.<ResultView>post(UrlApi.medicalRecordAdd)
                        .tag(this)
                        .headers("mbtToken", SpUtils.getToken(getApplicationContext()))
                        .params("title", title)
                        .params("info", info)
                        .params("dealtype","4201")
                        .addFileParams("file",fileList)
                        .execute(new JsonCallback<ResultView>(){
                            @Override
                            public void onSuccess(Response<ResultView> response) {
                                Log.i("test", "onSuccess  " + response.body());
                                ResultView resultView = response.body();
                                if (resultView.getResultCode() == 0) {
                                    Toast.makeText(MedicalRecordEditeActivity.this, "添加成功", Toast.LENGTH_SHORT).show();
                                    finish();
                                } else {
                                    Toast.makeText(MedicalRecordEditeActivity.this, resultView.getMsg(), Toast.LENGTH_SHORT).show();
                                }
                            }

                            @Override
                            public void onError(Response<ResultView> response) {
                                super.onError(response);
                                Log.i("test", "onError  " + response.message());
                                Toast.makeText(MedicalRecordEditeActivity.this, response.message(), Toast.LENGTH_SHORT).show();
                            }

                        });
            }
        });


    }

    private IHandlerCallBack iHandlerCallBack = new IHandlerCallBack() {

        @Override
        public void onStart() {

        }

        @Override
        public void onSuccess(List<String> photoList) {
            path = photoList;
            if (photoList != null && photoList.size() == 1) {
//                ImageView imageView = new ImageView(this);
                Glide.with(mContext).load(photoList.get(0)).into(commonIvImgAdd);
            }
            for(String fileName:photoList){
                Log.i("fileName",fileName);
                File file = new File(fileName);

                ImageView imageView = new ImageView(MedicalRecordEditeActivity.this);
                imageView.setLayoutParams(new WindowManager.LayoutParams(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT));  //设置图片宽高
//                imageView.setImageDrawable(getResources().getDrawable(R.drawable.ic_search));
                Glide.with(mContext).load(fileName).into(imageView);
                llWindLayout.addView(imageView);

                fileList.add(file);
            }

        }

        @Override
        public void onCancel() {

        }

        @Override
        public void onFinish() {

        }

        @Override
        public void onError() {

        }
    };


    private void photoDialog() {
        dialog_photo = new Dialog(this, R.style.photo_dialog);
        View view = LayoutInflater.from(this).inflate(R.layout.dialog_take_photo, null);
        dialog_photo.setContentView(view);
        Window window = dialog_photo.getWindow();
        WindowManager.LayoutParams lp = window.getAttributes();
        window.setGravity(Gravity.BOTTOM);
        window.setWindowAnimations(R.style.bottomDialogStyle);
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
        if (!dialog_photo.isShowing()) {
            dialog_photo.show();
        }
        view.findViewById(R.id.tv_open_camera).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog_photo.dismiss();
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

                } else {
                }
                GalleryPick.getInstance().setGalleryConfig(galleryConfig).open(MedicalRecordEditeActivity.this);
            }
        });
        view.findViewById(R.id.tv_choose_img).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog_photo.dismiss();
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                } else {
                }
                GalleryPick.getInstance().setGalleryConfig(galleryConfig).open(MedicalRecordEditeActivity.this);
            }
        });
        view.findViewById(R.id.tv_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dialog_photo.isShowing()) {
                    dialog_photo.hide();
                }
            }
        });
    }

}
