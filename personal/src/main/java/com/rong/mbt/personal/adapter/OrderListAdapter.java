package com.rong.mbt.personal.adapter;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.alibaba.fastjson.JSONObject;
import com.calanger.hh.model.Doctor;
import com.calanger.hh.model.Order;
import com.calanger.hh.model.OrderPre;
import com.calanger.hh.model.User;
import com.calanger.hh.util.DateUtils;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.rong.mbt.personal.R;

import java.text.SimpleDateFormat;
import java.util.Map;

import butterknife.BindView;

/**
 * 文章列表
 */
public class OrderListAdapter extends BaseQuickAdapter<Map<String, Object>, BaseViewHolder> {



    public OrderListAdapter() {
        super(R.layout.item_ot_order);
    }

    @Override
    protected void convert(BaseViewHolder helper, Map<String, Object> item) {

        try {
            JSONObject jsonMap = new JSONObject(item);
            Order order = jsonMap.getObject("order", Order.class);
            User user = jsonMap.getObject("user", User.class);

            TextView tvName = helper.getView(R.id.tv_name);
            TextView tvSecond = helper.getView(R.id.tv_second);
            TextView rightTvTag = helper.getView(R.id.right_tv_tag);

//            (1:订单 2：咨询 3：文章)
            if(order.getProductType()==1){
                tvName.setText("订单");
            }else if(order.getProductType()==2){
                tvName.setText("咨询");
            }else if(order.getProductType()==3){
                tvName.setText("文章");
            }else{
                tvName.setText("其他");
            }
            tvSecond.setText(DateUtils.dateToString(order.getCreattime()));
            rightTvTag.setText(order.getTradeMoney()+"元");


        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
