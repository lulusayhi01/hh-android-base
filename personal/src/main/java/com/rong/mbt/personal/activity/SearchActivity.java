package com.rong.mbt.personal.activity;

import android.annotation.SuppressLint;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.flyco.tablayout.SlidingTabLayout;
import com.rong.mbt.personal.R;
import com.rong.mbt.personal.R2;
import com.rong.mbt.personal.activity.base.PBaseActivity;
import com.rong.mbt.personal.adapter.TabPagerAdapter;
import com.rong.mbt.personal.fragment.SearchItemFragment;
import com.rong.mbt.personal.fragment.article.ArticleItemFragment;
import com.rong.mbt.personal.utils.SpUtils;
import com.sj.friendcircle.common.router.RouterList;

import java.util.ArrayList;
import java.util.Arrays;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * 更多文章列表
 */
@Route(path = RouterList.SearchActivity.path)
@SuppressLint("NewApi")
public class SearchActivity extends PBaseActivity {


    private SlidingTabLayout mSlidingTabLayout;
    private ViewPager mViewPager;

    private LinearLayout llSearchSubmit;
    private EditText etSearchContent;
    private String searchContent;


    private String[] mTitlesPatient = {"医生","文章"};
    private String[] mTitlesDoctor = {"患者", "文章"};
    private ArrayList<Fragment> mFragments = new ArrayList<>();

    @Override
    public int getLayoutId(){
        return R.layout.activity_search_common;
    }

    @Override
    public void initView() {
        super.initView();
        mSlidingTabLayout = findViewById(R.id.sliding_tab_layout);
        mViewPager = findViewById(R.id.view_pager);
        llSearchSubmit = findViewById(R.id.ll_search_submit);
        etSearchContent = findViewById(R.id.et_search_content);

        initListener();
    }

    private void initTabLayout() {
        mFragments.clear();
        int userType = SpUtils.getUserType(this);
        if(userType==0){
            for(int i=0;i<mTitlesPatient.length;i++){
                mFragments.add(SearchItemFragment.getInstance(mTitlesPatient[i],userType,searchContent));
                TabPagerAdapter tabPagerAdapter = new TabPagerAdapter(getSupportFragmentManager(), Arrays.asList(mTitlesPatient), mFragments);
                mViewPager.setAdapter(tabPagerAdapter);
            }
        }else{
            for(int i=0;i<mTitlesPatient.length;i++){
                mFragments.add(SearchItemFragment.getInstance(mTitlesPatient[i],userType,searchContent));
                TabPagerAdapter tabPagerAdapter = new TabPagerAdapter(getSupportFragmentManager(), Arrays.asList(mTitlesDoctor), mFragments);
                mViewPager.setAdapter(tabPagerAdapter);
            }
        }

        mSlidingTabLayout.setViewPager(mViewPager);
    }

    private void initListener() {
//        mLlBack.setOnClickListener(v -> finish());
        llSearchSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchContent = etSearchContent.getText().toString().trim();
                initTabLayout();
            }
        });


    }

}
