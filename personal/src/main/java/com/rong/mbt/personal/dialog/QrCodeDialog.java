package com.rong.mbt.personal.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.rong.mbt.personal.R;
import com.rong.mbt.personal.bean.PersonalInfoBean;
import com.rong.mbt.personal.utils.ImageUtil;
import com.rong.mbt.personal.utils.ScreenUtils;
import com.rong.mbt.personal.utils.ZxingUtil;

public class QrCodeDialog extends Dialog {

    private Context context;
    private PersonalInfoBean infoBean;

    public QrCodeDialog(Context context, PersonalInfoBean infoBean) {
        super(context, R.style.dialog_home_event);
        this.context = context;
        this.infoBean = infoBean;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_qrcode);

        ImageView iv = findViewById(R.id.dialog_qrcode_iv);
        TextView phone = findViewById(R.id.dialog_qrcode_tv_phone);
        ImageView tvQr = findViewById(R.id.dialog_qrcode_iv_qr);
        TextView location = findViewById(R.id.dialog_qrcode_tv_location);

        if (infoBean.getData().getHeadimage() != null) {
            ImageUtil.loadCircleImage(context, infoBean.getData().getHeadimage(), iv);
        }
        phone.setText(infoBean.getData().getPhone());

        tvQr.setImageBitmap(ZxingUtil.createBitmap(infoBean.getData().getPhone(), (int) (ScreenUtils.getScreenWidth(context)/3),(int) (ScreenUtils.getScreenHeight(context)/3)));
    }
}
