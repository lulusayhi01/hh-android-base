package com.rong.mbt.personal.fragment;

import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.android.arouter.launcher.ARouter;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.calanger.hh.entity.ResultView;
import com.calanger.hh.model.Account;
import com.calanger.hh.model.CustomerService;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.callback.StringCallback;
import com.lzy.okgo.model.Response;
import com.rong.mbt.personal.R;
import com.rong.mbt.personal.activity.ArticleListActivity;
import com.rong.mbt.personal.activity.ArticleListFollowActivity;
import com.rong.mbt.personal.activity.DoctorListSimpleActivity;
import com.rong.mbt.personal.activity.MedicalRecordListActivity;
import com.rong.mbt.personal.activity.MonkeyPackageActivity;
import com.rong.mbt.personal.activity.MyCommunityActivity;
import com.rong.mbt.personal.activity.MyQRcodeActivity;
import com.rong.mbt.personal.activity.OrderListActivity;
import com.rong.mbt.personal.activity.OrderPreListOnPatientActivity;
import com.rong.mbt.personal.activity.PersonalInfoActivity;
import com.rong.mbt.personal.activity.SettingActivity;
import com.rong.mbt.personal.bean.PersonalInfoBean;
import com.rong.mbt.personal.http.UrlApi;
import com.rong.mbt.personal.http.callback.JsonCallback;
import com.rong.mbt.personal.utils.ImageUtil;
import com.shehuan.niv.NiceImageView;
import com.tencent.mm.opensdk.modelmsg.SendMessageToWX;
import com.tencent.mm.opensdk.modelmsg.WXMediaMessage;
import com.tencent.mm.opensdk.modelmsg.WXTextObject;
import com.tencent.mm.opensdk.openapi.IWXAPI;
import com.tencent.mm.opensdk.openapi.WXAPIFactory;

import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import io.rong.imkit.RongIM;

/**
 * 病人模式-个人中心
 */


public class PatientPersonalFragment extends Fragment {

    private String mbtToken;
    private PersonalInfoBean infoBean;
    private NiceImageView ivHead;
    private TextView tvName;
    private TextView tvPhone;
    private TextView tvUserAccountMoney;

    private TextView tvInvitaion;
    private SharedPreferences sp;

    private LinearLayout llMedicalRecords;
    private RelativeLayout rlOrder;
    private LinearLayout llFollowArticle;
    private LinearLayout llFollowDoctor;

    private Unbinder unbinder;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_patient_personal, container, false);
        unbinder = ButterKnife.bind(this,view);
        initView(view);
        listener(view);
        return view;
    }

//    @OnClick(R2.id.ll_customer_service)
    public void contectCustomerService(){
    }

    private void listener(View view) {




        //订单-预约
        view.findViewById(R.id.ll_patient_top1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), OrderPreListOnPatientActivity.class);
                intent.putExtra("mode",0);
                startActivity(intent);
            }
        });
        //订单-沟通中
        view.findViewById(R.id.ll_patient_top2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), OrderPreListOnPatientActivity.class);
                intent.putExtra("mode",1);
                startActivity(intent);
            }
        });
        //订单-评价
        view.findViewById(R.id.ll_patient_top3).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), OrderPreListOnPatientActivity.class);
                intent.putExtra("mode",2);
                startActivity(intent);
            }
        });

        view.findViewById(R.id.ll_customer_service).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OkGo.<ResultView>get(UrlApi.customerServiceGetSingle)
                        .tag(this)
                        .headers("mbtToken", mbtToken)
                        .execute(new JsonCallback<ResultView>() {
                            @Override
                            public void onSuccess(Response<ResultView> response) {
                                ResultView resultView = response.body();
                                if (resultView.getData() != null) {
                                    Map<String,Object> map = (Map<String, Object>) resultView.getData();
                                    JSONObject jo = new JSONObject(map);
                                    CustomerService customerService = jo.getObject("customerService", CustomerService.class);
                                    RongIM.getInstance().startPrivateChat(getActivity(), customerService.getObjId() + "", "客服");

                                }
                            }
                            @Override
                            public void onError(Response<ResultView> response) {
                                super.onError(response);
                                Log.i("test", "onError  " + response.body());
                            }
                        });
            }
        });



        //收藏-文章管理
        llFollowArticle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), ArticleListFollowActivity.class);
                startActivity(intent);
            }
        });
        //关注-医生管理
        llFollowDoctor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), DoctorListSimpleActivity.class);
                startActivity(intent);
            }
        });
        //花费管理
        rlOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), OrderListActivity.class);
                startActivity(intent);
            }
        });

        //病历管理
        llMedicalRecords.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), MedicalRecordListActivity.class);
                intent.putExtra("infoBean", infoBean);
                intent.putExtra("mbtToken", mbtToken);
                startActivity(intent);
            }
        });

        //二维码
        view.findViewById(R.id.fnp_head_relative).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (infoBean == null || infoBean.getData() == null) {
                    return;
                }
                Intent intent = new Intent(getActivity(), MyQRcodeActivity.class);
                intent.putExtra("infoBean", infoBean);
                intent.putExtra("mbtToken", mbtToken);
                startActivity(intent);
            }
        });

        //个人信息
        view.findViewById(R.id.ll_patient_more1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), PersonalInfoActivity.class);
                if (infoBean != null) {
                    intent.putExtra("infoBean", infoBean);
                    intent.putExtra("mbtToken", mbtToken);
                }
                startActivity(intent);
            }
        });

        //设置
        view.findViewById(R.id.ll_patient_more7).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), SettingActivity.class);
                startActivity(intent);
            }
        });

        //发布文章-----只有医生有发布文字-----后期在考虑患者的文字发布。
//        view.findViewById(R.id.ll_patient_more6).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                ARouter.getInstance().build("/order/PublishArticlesActivity").navigation();
//            }
//        });


        //钱包
        view.findViewById(R.id.fnp_monkey_pack_linear).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), MedicalRecordListActivity.class);
                if (infoBean != null) {
                    intent.putExtra("phone", infoBean.getData().getPhone());
                    intent.putExtra("mbtToken", mbtToken);
                    intent.putExtra("name", infoBean.getData().getName());
                }
                startActivity(intent);
            }
        });
        //好友邀请
        view.findViewById(R.id.fnp_nvitation_linear).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (infoBean != null && infoBean.getData() != null && infoBean.getData().getBelongcode() != null) {
                    ClipboardManager cm = (ClipboardManager) getActivity().getSystemService(Context.CLIPBOARD_SERVICE);

                    cm.setText(infoBean.getData().getBelongcode());
                    Toast.makeText(getActivity(), "复制成功，赶紧邀请好友注册吧", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getActivity(), "复制失败", Toast.LENGTH_SHORT).show();
                }
            }
        });
        //我的矿机
        view.findViewById(R.id.ll_my_machine).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ARouter.getInstance().build("/acitvity/MachineListActivity").navigation();
            }
        });

        //社区
        view.findViewById(R.id.fnp_shequ_linear).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), MyCommunityActivity.class);
                intent.putExtra("mbtToken", mbtToken);
                startActivity(intent);

            }
        });

        //好友分享
        view.findViewById(R.id.fnp_linear_share).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                ARouter.getInstance().build("/app/WXEntryActivity").withString("code",infoBean.getData().getBelongcode()).navigation();
                api = WXAPIFactory.createWXAPI(getActivity(), "wxb0065d1cb10fd744", true);
                api.registerApp("wxb0065d1cb10fd744");

                WXTextObject textObj = new WXTextObject();
                textObj.text = "医疗\n我下在使用医疗APP，新型社区APP，你也来试试吧！邀请码：" + infoBean.getData().getBelongcode();

                WXMediaMessage msg = new WXMediaMessage();
                msg.mediaObject = textObj;
                msg.description = "医疗\n我下在使用医疗APP，新型社区APP，你也来试试吧！邀请码：" + infoBean.getData().getBelongcode();

                SendMessageToWX.Req req = new SendMessageToWX.Req();
                req.transaction = "text" + System.currentTimeMillis();
                req.message = msg;
                req.scene = SendMessageToWX.Req.WXSceneSession;
                api.sendReq(req);
            }
        });

        //黑匣子
        view.findViewById(R.id.ll_black_box).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ARouter.getInstance().build("/acitvity/BlackBoxActivity").navigation();
            }
        });
    }

    private IWXAPI api;


    @Override
    public void onStart() {
        super.onStart();
        initNet();
    }

    private void initNet() {
//        OkGo.<ResultView>get(UrlApi.accountGetSingle)
//                .tag(this)
//                .headers("mbtToken", mbtToken)
//                .execute(new JsonCallback<ResultView>() {
//                    @Override
//                    public void onSuccess(Response<ResultView> response) {
//                        ResultView resultView = response.body();
//                        if (resultView.getData() != null) {
//                            Map<String,Object> map = (Map<String, Object>) resultView.getData();
//                            JSONObject jo = new JSONObject(map);
//                            Account account = jo.getObject("account", Account.class);
//                            tvUserAccountMoney.setText(account.getMoney()+"元");
//                        }
//                    }
//                    @Override
//                    public void onError(Response<ResultView> response) {
//                        super.onError(response);
//                        Log.i("test", "onError  " + response.body());
//                    }
//                });


        OkGo.<String>get("http://service.jiangaifen.com:38082/user/getUser")
                .tag(this)
                .headers("mbtToken", mbtToken)
                .execute(new StringCallback() {
                    @Override
                    public void onSuccess(Response<String> response) {
                        if (!response.body().isEmpty()) {
                            infoBean = JSON.parseObject(response.body(), PersonalInfoBean.class);
                            if (infoBean == null || infoBean.getData() == null) {
                                return;
                            }
                            if (infoBean.getData().getHeadimage() != null) {
                                ImageUtil.loadRoundeImga(getActivity(), infoBean.getData().getHeadimage(), ivHead, 10);
                            }
                            tvUserAccountMoney.setText(infoBean.getData().getAccountMoney()+"元");
                            tvName.setText(infoBean.getData().getName());
                            tvPhone.setText("医疗账号：" + infoBean.getData().getPhone());
                            tvInvitaion.setText("好友邀请 (" + infoBean.getData().getBelongcode() + ")");
                            SharedPreferences.Editor editor = sp.edit().putString("belongcode", infoBean.getData().getBelongcode());
                            editor.apply();
                        }
                    }

                    @Override
                    public void onError(Response<String> response) {
                        super.onError(response);
                        Log.i("test", "onError  " + response.body());
                    }
                });
    }

    private void initView(View view) {
        sp = getActivity().getSharedPreferences("config", Context.MODE_PRIVATE);
        mbtToken = sp.getString("mbtToken", "");
        view.findViewById(R.id.head_blue_iv_left).setVisibility(View.GONE);
        TextView tvTitle = view.findViewById(R.id.head_blue_tv_center);
        tvTitle.setText("个人中心");
        view.findViewById(R.id.head_blue_iv_right).setVisibility(View.GONE);

        ivHead = view.findViewById(R.id.pf_np_iv_head);
        tvName = view.findViewById(R.id.pf_np_tv_name);
        tvPhone = view.findViewById(R.id.pf_np_tv_username);
        tvUserAccountMoney = view.findViewById(R.id.tv_user_account_money);
        tvInvitaion = view.findViewById(R.id.fnp_monkey_pack_tv_invitation);

        llMedicalRecords = view.findViewById(R.id.ll_medical_records);
        rlOrder = view.findViewById(R.id.rl_order);
        llFollowArticle = view.findViewById(R.id.ll_follow_article);
        llFollowDoctor = view.findViewById(R.id.ll_follow_doctor);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
