package com.rong.mbt.personal.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PhoneUtil {
    //手机号加***
    public static String phoneShowStar(String phone) {
        if (phone != null && !phone.isEmpty() && phone.length() >= 11) {
            return phone.substring(0, 3) + "****" + phone.substring(7, 11);
        }
        return "";
    }

    //检查是否是手机号
    public static boolean isMobileNum(String mobiles) {
        Pattern p = Pattern
                .compile("^(13[0-9]|14[579]|15[0-3,5-9]|16[6]|17[0135678]|18[0-9]|19[89])\\d{8}$");
        Matcher m = p.matcher(mobiles);
        return m.matches();
    }

    /**
     * 18位身份证校验,粗略的校验
     *
     * @param idCard
     * @return
     * @author lyl
     */
    public static boolean checkIdCardNo(String idCard) {
        Pattern pattern1 = Pattern.compile("^(\\d{6})(19|20)(\\d{2})(1[0-2]|0[1-9])(0[1-9]|[1-2][0-9]|3[0-1])(\\d{3})(\\d|X|x)?$"); //粗略的校验
        Matcher matcher = pattern1.matcher(idCard);
        if (matcher.matches()) {
            return true;
        }
        return false;
    }
}
