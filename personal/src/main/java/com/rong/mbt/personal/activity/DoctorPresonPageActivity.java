package com.rong.mbt.personal.activity;

import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.flyco.tablayout.SlidingTabLayout;
import com.rong.mbt.personal.R;
import com.rong.mbt.personal.activity.base.PBaseActivity;
import com.rong.mbt.personal.adapter.TabPagerAdapter;
import com.rong.mbt.personal.fragment.SearchItemFragment;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * 医生-个人主页
 */
public class DoctorPresonPageActivity extends PBaseActivity {

    private LinearLayout mLlBack;
    private TextView mTxtTitle;
    private View mIvRight;
    private LinearLayout mLlRightMenu;
    private TextView mTxtRight;
    private LinearLayout mLlRightText;
    private SlidingTabLayout mSlidingTabLayout;
    private ViewPager mViewPager;


    private String[] mTitlesPatient = {"医生","文章"};
    private String[] mTitlesDoctor = {"订单", "文章"};
    private Integer[] codes = {0,1};
    private ArrayList<Fragment> mFragments = new ArrayList<>();

    @Override
    public void initView() {
        super.initView();
        mLlBack = findViewById(R.id.ll_back);
        mTxtTitle = findViewById(R.id.txt_title);
        mIvRight = findViewById(R.id.iv_right);
        mLlRightMenu = findViewById(R.id.ll_right_menu);
        mTxtRight = findViewById(R.id.txt_right);
        mLlRightText = findViewById(R.id.ll_right_text);
        mSlidingTabLayout = findViewById(R.id.sliding_tab_layout);
        mViewPager = findViewById(R.id.view_pager);

        initTabLayout();
        initListener();
    }

    private void initTabLayout() {

        int userType = getIntent().getIntExtra("userType",0);
        if(userType==0){
            for(int i=0;i<codes.length;i++){
//                mFragments.add(SearchItemFragment.getInstance(mTitlesPatient[i],codes[i]));
                TabPagerAdapter tabPagerAdapter = new TabPagerAdapter(getSupportFragmentManager(), Arrays.asList(mTitlesPatient), mFragments);
                mViewPager.setAdapter(tabPagerAdapter);
            }
        }else{
            for(int i=0;i<codes.length;i++){
//                mFragments.add(SearchItemFragment.getInstance(mTitlesDoctor[i],codes[i]));
                TabPagerAdapter tabPagerAdapter = new TabPagerAdapter(getSupportFragmentManager(), Arrays.asList(mTitlesDoctor), mFragments);
                mViewPager.setAdapter(tabPagerAdapter);
            }
        }

        mSlidingTabLayout.setViewPager(mViewPager);
    }

    private void initListener() {
        mLlBack.setOnClickListener(v -> finish());
    }


    @Override
    public int getLayoutId() {
        return R.layout.activity_flyco_tablayout;
    }

}
