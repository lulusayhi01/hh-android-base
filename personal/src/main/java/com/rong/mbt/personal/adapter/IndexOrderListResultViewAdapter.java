package com.rong.mbt.personal.adapter;

import android.widget.TextView;

import com.alibaba.fastjson.JSONObject;
import com.bumptech.glide.Glide;
import com.calanger.hh.model.Article;
import com.calanger.hh.model.OrderPre;
import com.calanger.hh.model.User;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.rong.mbt.personal.R;
import com.rong.mbt.personal.bean.order.OrderPreBean;
import com.shehuan.niv.NiceImageView;

import java.util.Map;

public class IndexOrderListResultViewAdapter extends BaseQuickAdapter<Map<String,Object>, BaseViewHolder> {


    public IndexOrderListResultViewAdapter() {
        super(R.layout.item_patient_index_order);
    }

    @Override
    protected void convert(BaseViewHolder helper, Map<String,Object> item) {

        try {

            JSONObject jo = new JSONObject(item);
            OrderPre orderPre = jo.getObject("orderPre", OrderPre.class);

            NiceImageView ivHead = helper.getView(R.id.iv_head);
            Glide.with(helper.itemView.getContext()).load(orderPre.getHeadimg()).into(ivHead);

            TextView tvItem1 = helper.getView(R.id.tv_item1);
            tvItem1.setText(String.format("患者姓名：%s", orderPre.getName()));

            String sexName;//0未知  1男  2女
            if (orderPre.getSex() == 1) {
                sexName = "男";
            } else if (orderPre.getSex() == 2) {
                sexName = "女";
            } else {
                sexName = "未知";
            }
            TextView tvItem2 = helper.getView(R.id.tv_item2);
            tvItem2.setText(String.format("性别：%s", sexName));

            TextView tvItem3 = helper.getView(R.id.tv_item3);
            tvItem3.setText(String.format("年龄：%s", orderPre.getAge()));

            TextView tvItem4 = helper.getView(R.id.tv_item4);
            tvItem4.setText(String.format("咨询费用：%s", orderPre.getFeed()));

            TextView tvItem5 = helper.getView(R.id.tv_item5);
            tvItem5.setText(String.format("病情描述：%s", orderPre.getContent()));
            TextView tvReceipt = helper.getView(R.id.tv_receipt);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
