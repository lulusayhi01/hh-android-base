package com.rong.mbt.personal.bean.doctor;

import java.io.Serializable;
import java.util.List;

public class DoctorIndexBean implements Serializable {


    /**
     * count : 2
     * data : [{"user":{"belongcode":"wQ8iv4jR","channelid":0,"coinstatus":1,"coldmoney":"0","creattime":"2019-07-24 14:52:58","fetchmoney":"0","headimage":"https://hh-liululu.oss-cn-beijing.aliyuncs.com/default/default-ys001.jpg","id":83,"investmoney":"0","inviterid":0,"level":0,"loginnum":0,"logintime":"1563951178","name":"璐璐","passwd":"662919d207123aa79147b5fc3d54831f","phone":"13867017397","rapnum":"0","registermoney":"10","remark":"","returnmoney":"0","sex":1,"status":1,"summoney":"10","token":"Q10EPKSun1GUJvcJK0m291GFpQlNjXOfWZ2l03ptw05nufSd7dM0Jg2zldlm5QISwQ9V9TT5w1kHloUAU5sRyw==","type":1,"waitactivemoney":"0"},"hospital":{"address":"北京市中心南路101号","content":"优秀的医院优秀的医院优秀的医院优秀的医院","creattime":"2019-07-24 14:31:54","id":1,"level":5,"name":"第一医院","person":"刘医生","phone":"13867015689","status":1,"summary":"优秀的医院"},"doctor":{"certifiedImg":"https://hh-liululu.oss-cn-beijing.aliyuncs.com/zy1.jpg","creattime":1566885162000,"doctorTitle":2,"headerimg":"https://hh-liululu.oss-cn-beijing.aliyuncs.com/default/default-ys001.jpg","hospitalId":1,"id":82,"idCardBackImg":"https://hh-liululu.oss-cn-beijing.aliyuncs.com/sfz/sfzb0.jpeg","idCardFrontImg":"https://hh-liululu.oss-cn-beijing.aliyuncs.com/sfz/sfz1.jpg","idCardNo":"362323199011070056","level":5,"name":"王医生","officesId":1,"remark":"","status":1,"summary":"擅长口腔各种症状","userId":83}},{"user":{"belongcode":"s3u5CJgt","channelid":0,"coinstatus":1,"coldmoney":"0","creattime":"2019-07-24 14:31:54","fetchmoney":"0","headimage":"https://hh-liululu.oss-cn-beijing.aliyuncs.com/default/default-ys001.jpg","id":82,"investmoney":"0","inviterid":0,"level":0,"loginnum":0,"logintime":"1563949914","name":"蓝二哥哥","passwd":"662919d207123aa79147b5fc3d54831f","phone":"13501031194","rapnum":"0","registermoney":"10","remark":"","returnmoney":"0","sex":0,"status":1,"summoney":"10","token":"1T27m3FUOURGljkNtHDiyVGFpQlNjXOfWZ2l03ptw05nufSd7dM0JuLP8awIQJPQHsl1uCyG/7wHloUAU5sRyw==","type":1,"waitactivemoney":"0"},"hospital":{"address":"北京市中心南路101号","content":"优秀的医院优秀的医院优秀的医院优秀的医院","creattime":"2019-07-24 14:31:54","id":1,"level":5,"name":"第一医院","person":"刘医生","phone":"13867015689","status":1,"summary":"优秀的医院"},"doctor":{"certifiedImg":"https://hh-liululu.oss-cn-beijing.aliyuncs.com/zy2.jpg","creattime":1566885162000,"doctorTitle":1,"headerimg":"https://hh-liululu.oss-cn-beijing.aliyuncs.com/default/default-ys001.jpg","hospitalId":1,"id":83,"idCardBackImg":"https://hh-liululu.oss-cn-beijing.aliyuncs.com/sfz/sfzb0.jpeg","idCardFrontImg":"https://hh-liululu.oss-cn-beijing.aliyuncs.com/sfz/sfz2.jpg","idCardNo":"362323199011070066","level":5,"name":"刘主任","officesId":2,"remark":"","status":1,"summary":"擅长外壳各种突发症状","userId":82}}]
     * msg : 请求成功
     * resultCode : 0
     */

    private int count;
    private String msg;
    private int resultCode;
    private List<DoctorListBean> data;

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public int getResultCode() {
        return resultCode;
    }

    public void setResultCode(int resultCode) {
        this.resultCode = resultCode;
    }

    public List<DoctorListBean> getData() {
        return data;
    }

    public void setData(List<DoctorListBean> data) {
        this.data = data;
    }
}
