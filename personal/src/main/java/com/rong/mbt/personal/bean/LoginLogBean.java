package com.rong.mbt.personal.bean;

import java.util.List;

public class LoginLogBean {

    /**
     * count : 12
     * data : [{"createtime":1534399613000},{"createtime":1534400035000},{"createtime":1534400474000},{"createtime":1534401142000},{"createtime":1534401244000},{"createtime":1534401518000},{"createtime":1534401551000},{"createtime":1534401831000},{"createtime":1534401907000},{"createtime":1534402614000}]
     * msg : 请求成功
     * resultCode : 0
     */

    private int count;
    private String msg;
    private int resultCode;
    private List<DataBean> data;

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public int getResultCode() {
        return resultCode;
    }

    public void setResultCode(int resultCode) {
        this.resultCode = resultCode;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * createtime : 1534399613000
         */

        private long createtime;

        public long getCreatetime() {
            return createtime;
        }

        public void setCreatetime(long createtime) {
            this.createtime = createtime;
        }
    }
}
