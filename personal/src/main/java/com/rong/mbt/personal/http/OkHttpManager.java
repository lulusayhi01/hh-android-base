package com.rong.mbt.personal.http;

import android.content.Context;
import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.lzy.okgo.OkGo;
import com.lzy.okgo.callback.Callback;
import com.lzy.okgo.model.HttpParams;
import com.rong.mbt.personal.utils.SpUtils;

import java.io.File;
import java.util.List;

import okhttp3.MediaType;

public class OkHttpManager {

    private static final String TAG = OkHttpManager.class.getSimpleName();
    private static final MediaType MEDIA_TYPE_JSON = MediaType.parse("application/x-www-form-urlencoded; charset=utf-8");//mdiatype 这个需要和服务端保持一致
    //下面两个随便用哪个都可以
    //    private static final MediaType MEDIA_TYPE_FILE = MediaType.parse("application/octet-stream");
    private static final MediaType MEDIA_TYPE_FILE = MediaType.parse("image/jpg");
    private static volatile OkHttpManager mInstance;//单利引用
    private Context applicationContext;

    public OkHttpManager(Context applicationContext) {
        this.applicationContext = applicationContext;
    }

    /**
     * 获取单例引用 * * @return
     */
    public static OkHttpManager getInstance(Context context) {
        OkHttpManager inst = mInstance;
        if (inst == null) {
            synchronized (OkHttpManager.class) {
                inst = mInstance;
                if (inst == null) {
                    inst = new OkHttpManager(context.getApplicationContext());
                    mInstance = inst;
                }
            }
        }
        return inst;
    }


    /**
     * get
     *
     * @param <T>
     * @param url
     * @param httpParams
     * @param callBack
     */
    public <T> void get(String url, HttpParams httpParams, Callback<T> callBack) {
        OkGo.<T>get(urlReplace(url).toString())
                .tag(this)
                .headers("mbtToken", SpUtils.getToken(applicationContext)).params(httpParams).execute(callBack);
    }


    /**
     * post
     *
     * @param <T>
     * @param url
     * @param httpParams
     * @param callBack
     */
    public <T> void post(String url, HttpParams httpParams, Callback<T> callBack) {
        OkGo.<T>post(urlReplace(url).toString())
                .tag(this)
                .headers("mbtToken", SpUtils.getToken(applicationContext)).params(httpParams).execute(callBack);
    }

    /**
     * file
     *
     * @param <T>
     * @param url
     * @param httpParams
     * @param file
     * @param callBack
     */
    public <T> void file(String url, HttpParams httpParams, List<File> file, Callback<T> callBack) {
        OkGo.<T>post(urlReplace(url).toString())
                .tag(this)
                .isMultipart(true)
                .headers("mbtToken", SpUtils.getToken(applicationContext))
                .addFileParams("file", file).params(httpParams).execute(callBack);
    }

    @NonNull
    private StringBuffer urlReplace(String url) {
        StringBuffer stringBuffer = new StringBuffer();
        if (!TextUtils.isEmpty(url) && url.startsWith("http")) {
            stringBuffer.append(url);
        } else {
            stringBuffer.append(UrlApi.baseUrl);
            stringBuffer.append(url);
        }
        return stringBuffer;
    }

    /**
     *
     * @param pageNumber
     * @return
     */
    public static HttpParams getListParams(int pageNumber){
        HttpParams httpParams = new HttpParams();
        httpParams.put("pageSize", 10);
        httpParams.put("pageNumber", pageNumber);
        return httpParams;
    }

    public static HttpParams getParams(){
        HttpParams httpParams = new HttpParams();
        return httpParams;
    }
}
