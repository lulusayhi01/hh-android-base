package com.rong.mbt.personal.activity;

import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.callback.StringCallback;
import com.lzy.okgo.model.Response;
import com.rong.mbt.personal.R;
import com.rong.mbt.personal.activity.base.PBaseActivity;
import com.rong.mbt.personal.bean.BiGoMeiYuanBean;

public class CoinToDollarActivity extends PBaseActivity {

    private String mbtToken;
    private TextView tvInfo;
    private EditText etMonkey;
    private EditText etPwd;

    @Override
    public int getLayoutId() {
        return R.layout.activity_coin_to_dollar;
    }

    @Override
    public void initView() {
        super.initView();
        mbtToken = getIntent().getStringExtra("mbtToken");
        find();
        listener();
        setText();
        net();
    }

    private void net() {
        OkGo.<String>get("http://service.jiangaifen.com:38082/coinPrice/ctusdExchange")
                .tag(this)
                .headers("mbtToken", mbtToken)
                .execute(new StringCallback() {
                    @Override
                    public void onSuccess(Response<String> response) {
                        Log.i("test", "ctusdexchane  " + response.body()+"  mbtToken "+mbtToken);

                        BiGoMeiYuanBean biGoMeiYuanBean = JSON.parseObject(response.body(), BiGoMeiYuanBean.class);
                        if (biGoMeiYuanBean.getResultCode() == 0) {
                            tvInfo.setText("1币可兑换 " + biGoMeiYuanBean.getData().getPricenow() + " 美元");
                        }
                    }

                    @Override
                    public void onError(Response<String> response) {
                        super.onError(response);
                        Log.i("test", "ctusdexchane error " + response.body());
                    }
                });
    }

    private void setText() {
        TextView tvTitle = findViewById(R.id.head_blue_tv_center);
        tvTitle.setText("币转美元");
    }

    private void listener() {
        findViewById(R.id.head_blue_iv_left).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        findViewById(R.id.act_ctd_bt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String monkey = etMonkey.getText().toString();
                String pwd = etPwd.getText().toString();
                if (monkey.isEmpty() || pwd.isEmpty()) {
                    return;
                }
                OkGo.<String>get("http://service.jiangaifen.com:38082/coinPrice/ctusd")
                        .tag(this)
                        .headers("mbtToken", mbtToken)
                        .params("number", monkey)
                        .params("numberpwd", pwd)
                        .execute(new StringCallback() {
                            @Override
                            public void onSuccess(Response<String> response) {
                                Log.i("test", "ctusd  " + response.body());
                                JSONObject jsonObject = JSON.parseObject(response.body());
                                if (jsonObject.getIntValue("resultCode") == 0) {
                                    finish();
                                } else {
                                    Toast.makeText(CoinToDollarActivity.this, jsonObject.getString("msg"), Toast.LENGTH_SHORT).show();
                                }
                            }

                            @Override
                            public void onError(Response<String> response) {
                                super.onError(response);
                            }
                        });

            }
        });
    }

    private void find() {
        findViewById(R.id.head_blue_iv_right).setVisibility(View.GONE);
        tvInfo = findViewById(R.id.act_ctd_tv);
        etMonkey = findViewById(R.id.act_ctd_et_monkey);
        etPwd = findViewById(R.id.act_ctd_et_pwd);

    }
}
