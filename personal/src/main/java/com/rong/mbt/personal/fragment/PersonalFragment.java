package com.rong.mbt.personal.fragment;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.ImageSpan;
import android.text.style.TextAppearanceSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.alibaba.fastjson.JSON;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.callback.StringCallback;
import com.lzy.okgo.model.Response;
import com.rong.mbt.personal.R;
import com.rong.mbt.personal.SealConst;
import com.rong.mbt.personal.activity.BillActivity;
import com.rong.mbt.personal.activity.MyQRcodeActivity;
import com.rong.mbt.personal.activity.PersonalInfoActivity;
import com.rong.mbt.personal.activity.RechargeActivity;
import com.rong.mbt.personal.activity.WithdrawalActivity;
import com.rong.mbt.personal.bean.PersonalInfoBean;
import com.rong.mbt.personal.dialog.CustomerServiceDialog;
import com.rong.mbt.personal.dialog.QrCodeDialog;
import com.rong.mbt.personal.utils.ImageUtil;
import com.rong.mbt.personal.utils.SpUtils;
import com.rong.mbt.personal.utils.StringUtil;
import com.rong.mbt.personal.utils.ZxingUtil;


public class PersonalFragment extends Fragment {

    ImageView ivHeadProtrait;
    private String mbtToken;
    private TextView tvName;
    private TextView tvMoney;
    private ImageView ivCs;
    private ImageView ivSetting;
    private PersonalInfoBean infoBean;
    private RelativeLayout rtRecharge;
    private RelativeLayout rtWithdrawa;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_personal, container, false);
        initView(view);
        listener(view);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        initNet();
    }

    private void initNet() {

        OkGo.<String>get("http://service.jiangaifen.com:38082/user/getUser")
                .tag(this)
                .headers("mbtToken", mbtToken)
                .execute(new StringCallback() {
                    @Override
                    public void onSuccess(Response<String> response) {
                        Log.i("test", "onSuccess getUser  " + response.body());
                        if (!response.body().isEmpty()) {
                            infoBean = JSON.parseObject(response.body(), PersonalInfoBean.class);
                            if (infoBean == null || infoBean.getData() == null) {
                                return;
                            }
                            if (infoBean.getData().getHeadimage() != null) {
                                ImageUtil.loadCircleImage(getActivity(), infoBean.getData().getHeadimage(), ivHeadProtrait);
                            }

                            tvName.setText(infoBean.getData().getName());

                            SpannableString spannableString = null;
                            if (infoBean.getData().getUsablemoney() != null) {
                                String str = StringUtil.formatString(Double.parseDouble(infoBean.getData().getUsablemoney()));
                                spannableString = new SpannableString(str);
                            } else {
                                spannableString = new SpannableString("0.00");
                            }
                            spannableString.setSpan(new TextAppearanceSpan(getActivity(), R.style.dp_20), spannableString.length() - 3, spannableString.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                            tvMoney.setText(spannableString);
                        }
                    }

                    @Override
                    public void onError(Response<String> response) {
                        super.onError(response);
                        Log.i("test", "onError  " + response.body());
                    }
                });
    }

    private void initView(View view) {

        SharedPreferences sp = getActivity().getSharedPreferences("config", Context.MODE_PRIVATE);
        mbtToken = sp.getString("mbtToken", "");
        ivHeadProtrait = view.findViewById(R.id.fragment_personal_iv_head_portrait);
        tvName = view.findViewById(R.id.head_tv_name);
        tvMoney = view.findViewById(R.id.tv_money);
        ivCs = view.findViewById(R.id.head_iv_kf);
        ivSetting = view.findViewById(R.id.iv_setting);

        rtRecharge = view.findViewById(R.id.fragment_personal_relative_recharge);
        rtWithdrawa = view.findViewById(R.id.fragment_personal_relative_withdrawal);
    }


    private void listener(View view) {
        ivSetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), PersonalInfoActivity.class);
                if (infoBean != null) {
                    intent.putExtra("infoBean", infoBean);
                    intent.putExtra("mbtToken", mbtToken);
                }
                startActivity(intent);
            }
        });


        ivCs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CustomerServiceDialog csDialog = new CustomerServiceDialog(getActivity());
                csDialog.show();
            }
        });

        //充值
        rtRecharge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), RechargeActivity.class);
                intent.putExtra("mbtToken", mbtToken);
                startActivity(intent);
            }
        });
        //提现
        rtWithdrawa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), WithdrawalActivity.class);
                intent.putExtra("mbtToken", mbtToken);
                startActivity(intent);
            }
        });
        //账单
        view.findViewById(R.id.fragment_personal_line_bill).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), BillActivity.class);
                intent.putExtra("mbtToken", mbtToken);
                startActivity(intent);
            }
        });

        //二维码
        tvName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (infoBean == null || infoBean.getData() == null) {
                    return;
                }
                Intent intent = new Intent(getActivity(), MyQRcodeActivity.class);
                intent.putExtra("infoBean", infoBean);
                intent.putExtra("mbtToken", mbtToken);
                startActivity(intent);
            }
        });
        view.findViewById(R.id.fragment_personal_iv_qrcode).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (infoBean == null || infoBean.getData() == null) {
                    return;
                }
                Intent intent = new Intent(getActivity(), MyQRcodeActivity.class);
                intent.putExtra("infoBean", infoBean);
                intent.putExtra("mbtToken", mbtToken);
                startActivity(intent);
            }
        });
    }
}
