package com.rong.mbt.personal.activity;


import android.content.Intent;
import android.content.SharedPreferences;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.rong.mbt.personal.R;
import com.rong.mbt.personal.activity.base.PBaseActivity;
import com.rong.mbt.personal.dialog.CustomerServiceDialog;


//手势解锁
public class GesturesPwdActivity extends PBaseActivity {

    private ImageView ivRight;
    private LinearLayout lineUpdatePwd;
    private Switch switchShowLine;
    private boolean geSturesLineShow;
    private SharedPreferences.Editor edit;
    private Switch switchIsOpen;
    private boolean gestureIsOpen;
    private TextView tvIsOpen;
    private String gesturesPwd;
    private SharedPreferences sp;
    private boolean faceIsOpen;

    @Override
    public int getLayoutId() {
        return R.layout.activity_gestures_pwd;
    }

    @Override
    public void initView() {
        super.initView();
        sp = getSharedPreferences("config", MODE_PRIVATE);
        edit = sp.edit();
        geSturesLineShow = sp.getBoolean("geSturesLineShow", false);
        gestureIsOpen = sp.getBoolean("GestureIsOpen", false);

        find();
        listener();
        if(gestureIsOpen){
            tvIsOpen.setText("已开启");
        }else{
            tvIsOpen.setText("未开启");
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        //手势密码
        gesturesPwd = sp.getString("gesturesPwd", "");

        faceIsOpen = sp.getBoolean("faceIsOpen", false);
    }

    private void listener() {
        findViewById(R.id.head_while_iv_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        ivRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CustomerServiceDialog csDialog = new CustomerServiceDialog(GesturesPwdActivity.this);
                csDialog.show();
            }
        });
        switchShowLine.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                edit.putBoolean("geSturesLineShow", b);
                edit.commit();
            }
        });
        switchIsOpen.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                if(gesturesPwd.isEmpty()){
                    Toast.makeText(GesturesPwdActivity.this, "请先设置手势密码", Toast.LENGTH_SHORT).show();
                    switchIsOpen.setChecked(false);
                    return;
                }
                if(faceIsOpen){
                    Toast.makeText(GesturesPwdActivity.this, "请先关闭人脸解锁", Toast.LENGTH_SHORT).show();
                    switchIsOpen.setChecked(false);
                    return;
                }
                edit.putBoolean("GestureIsOpen", b);
                edit.commit();

                if(b){
                    tvIsOpen.setText("已开启");
                }else{
                    tvIsOpen.setText("未开启");
                }
            }
        });

        //修改手势密码
        lineUpdatePwd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(GesturesPwdActivity.this, DrawGesturesPwdActivity.class);
                intent.putExtra("geSturesLineShow",switchShowLine.isChecked());
                startActivity(intent);
            }
        });
    }

    private void find() {
        TextView tvTitle = findViewById(R.id.head_while_tv_center);
        tvTitle.setText("手势密码");

        ivRight = findViewById(R.id.head_while_iv_right);
        ivRight.setVisibility(View.VISIBLE);

        lineUpdatePwd = findViewById(R.id.act_gp_update_gesture_pwd_line);
        switchShowLine = findViewById(R.id.act_gp_switch_show);
        switchShowLine.setChecked(geSturesLineShow);

        switchIsOpen = findViewById(R.id.act_gp_switch_isopen);
        switchIsOpen.setChecked(gestureIsOpen);


        tvIsOpen = findViewById(R.id.act_ss_tv_isopen);
    }
}
