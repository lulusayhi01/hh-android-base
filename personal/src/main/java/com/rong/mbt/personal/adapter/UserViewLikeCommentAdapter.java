package com.rong.mbt.personal.adapter;

import android.widget.ImageView;
import android.widget.TextView;

import com.alibaba.fastjson.JSONObject;
import com.bumptech.glide.Glide;
import com.calanger.hh.model.Doctor;
import com.calanger.hh.model.Hospital;
import com.calanger.hh.model.User;
import com.calanger.hh.model.UserViewLikeComment;
import com.calanger.hh.util.DateUtils;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.rong.mbt.personal.R;

import java.util.Map;

/**
 * 医生列表-适配器-Simple模式
 */
public class UserViewLikeCommentAdapter extends BaseQuickAdapter<Map<String,Object>, BaseViewHolder> {

    public UserViewLikeCommentAdapter() {
        super(R.layout.item_user_view_like_comment);
    }

    @Override
    protected void convert(BaseViewHolder helper,  Map<String,Object> item) {

        try {
        	JSONObject jsonMap = new JSONObject(item);
            User user = jsonMap.getObject("user",User.class);
            UserViewLikeComment userViewLikeComment = jsonMap.getObject("userViewLikeComment",UserViewLikeComment.class);
        
            ImageView ivHead = helper.getView(R.id.iv_head);
            Glide.with(helper.itemView.getContext()).load(user.getHeadimage()).into(ivHead);

            TextView tvName = helper.getView(R.id.tv_name);
            tvName.setText(user.getName());

            TextView tvCommentCreattime = helper.getView(R.id.tv_comment_creattime);
            tvCommentCreattime.setText(DateUtils.dateToString(userViewLikeComment.getCreattime()));


            TextView tvCommentContent = helper.getView(R.id.tv_comment_content);
            tvCommentContent.setText(userViewLikeComment.getContent());

            helper.addOnClickListener(R.id.tv_user_like);
            helper.addOnClickListener(R.id.tv_user_comment);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
