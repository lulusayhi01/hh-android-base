package com.rong.mbt.personal.activity;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.fastjson.JSONObject;
import com.bumptech.glide.Glide;
import com.calanger.hh.entity.ResultView;
import com.calanger.hh.model.OrderPre;
import com.calanger.hh.model.User;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.model.Response;
import com.rong.mbt.personal.R;
import com.rong.mbt.personal.activity.base.PBaseActivity;
import com.rong.mbt.personal.bean.FriendRemarkBean;
import com.rong.mbt.personal.bean.order.ReceiptBean;
import com.rong.mbt.personal.http.UrlApi;
import com.rong.mbt.personal.http.callback.JsonCallback;
import com.rong.mbt.personal.utils.SpUtils;
import com.shehuan.niv.NiceImageView;


import java.util.Map;

import io.rong.imkit.RongIM;

/**
 * 患者详情（医生接单页面）
 */
public class PatientDetailActivity extends PBaseActivity {

    private OrderPre orderPre;
    private ImageView headBlueIvLeft;
    private NiceImageView ivHead;
    private TextView tvPatientName;
    private TextView tvSex;
    private TextView tvAge;
    private TextView tvPhone;
    private TextView tvCardNo;
    private TextView tvFeed;
    private TextView tvDescriptionIllness;
    private TextView isHasMedicalHistory;
    private TextView tvReceipt;
    private ImageView back;


    @Override
    public int getLayoutId() {
        return R.layout.activity_patient_details;
    }

    @Override
    public void initView() {
        super.initView();

        headBlueIvLeft = findViewById(R.id.head_blue_iv_left);
        ivHead = findViewById(R.id.iv_head);
        tvPatientName = findViewById(R.id.tv_patient_name);
        tvSex = findViewById(R.id.tv_sex);
        tvAge = findViewById(R.id.tv_age);
        tvPhone = findViewById(R.id.tv_phone);
        tvCardNo = findViewById(R.id.tv_card_no);
        tvFeed = findViewById(R.id.tv_feed);
        tvDescriptionIllness = findViewById(R.id.tv_description_illness);
        isHasMedicalHistory = findViewById(R.id.isHasMedical_history);
        tvReceipt = findViewById(R.id.tv_receipt);
        back = findViewById(R.id.head_blue_iv_left);

        tvReceipt.setVisibility(View.GONE);

        Integer orderPreId = (Integer) getIntent().getIntExtra("id",0);

        OkGo.<ResultView>post(UrlApi.orderPreGetSingle)
                .tag(this)
                .headers("mbtToken", SpUtils.getToken(this))
                .params("id", orderPreId)
                .execute(new JsonCallback<ResultView>() {
                    @Override
                    public void onSuccess(Response<ResultView> response) {
                        if (response != null && response.body() != null) {
                            ResultView resultView = response.body();
                            if (resultView.getResultCode() == 0) {
//                                        requestSelectFriendRemark(orderPreBean.getUserId()+"",orderPreBean.getName());
                                Map<String,Object> map = (Map<String, Object>) resultView.getData();
                                JSONObject jo = new JSONObject(map);
                                orderPre = (OrderPre) jo.getObject("orderPre",OrderPre.class);
                                User user = (User) jo.getObject("user", User.class);
                                if (orderPre != null) {
                                    Glide.with(PatientDetailActivity.this).load(user.getHeadimage()).into(ivHead);

                                    String sexName;//0未知  1男  2女
                                    if (orderPre.getSex() == 1) {
                                        sexName = "男";
                                    } else if (orderPre.getSex() == 2) {
                                        sexName = "女";
                                    } else {
                                        sexName = "未知";
                                    }
                                    tvSex.setText(sexName);

                                    tvAge.setText(orderPre.getAge() + "");
                                    tvCardNo.setText("暂无");
                                    tvPhone.setText(user.getPhone());
                                    tvFeed.setText(orderPre.getFeed());
                                    tvPatientName.setText(orderPre.getName());
                                    tvDescriptionIllness.setText(orderPre.getContent());

                                    tvReceipt.setVisibility(View.GONE);


                                }
                            } else {
                                Toast.makeText(getApplicationContext(), resultView.getMsg(), Toast.LENGTH_SHORT);
                            }
                        }
                    }
                });


        back.setOnClickListener(v -> finish());

    }

    public void setDataToView(){

    }
}
