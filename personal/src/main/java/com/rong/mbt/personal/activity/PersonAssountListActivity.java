package com.rong.mbt.personal.activity;

import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.callback.StringCallback;
import com.lzy.okgo.model.Response;
import com.rong.mbt.personal.R;
import com.rong.mbt.personal.activity.base.PBaseActivity;
import com.rong.mbt.personal.adapter.PersonAssountListActAdapter;
import com.rong.mbt.personal.bean.PersonAssountListBean;

@Route(path = "/personal/PersonAssountListActivity")
public class PersonAssountListActivity extends PBaseActivity {

    private TextView tvRight;
    private String mbtToken;
    private String phone;
    private PersonAssountListActAdapter adapter;

    @Override
    public int getLayoutId() {
        return R.layout.activity_person_assount_list;
    }

    @Override
    public void initView() {
        super.initView();
        Intent intent = getIntent();
        mbtToken = intent.getStringExtra("mbtToken");
        phone = intent.getStringExtra("phone");

        if (mbtToken == null || mbtToken.isEmpty()) {
            mbtToken = getIntent().getStringExtra("mbtToken");
        }
        if (phone == null || phone.isEmpty()) {
            phone = getIntent().getStringExtra("phone");
        }
        find();
        listener();
    }

    @Override
    protected void onStart() {
        super.onStart();
        initnet();
    }

    private void initnet() {
        OkGo.<String>get("http://service.jiangaifen.com:38082/userAccount/listByPage")
                .tag(this)
                .headers("mbtToken", mbtToken)
                .params("pageNum", "1")
                .params("pageSize", "30")
                .params("dealtype","4201")
                .execute(new StringCallback() {
                    @Override
                    public void onSuccess(Response<String> response) {
                        //{"count":1,"data":[{"account":"609053820@qq.com","createtime":"2018-09-10 10:09:55","id":19,"name":"赵飞超","status":1,"type":1,"userid":49}],"msg":"请求成功","resultCode":0}
                        //{"count":0,"data":[],"msg":"请求成功","resultCode":0}
                        Log.i("test", "onSuccess22222   " + response.body());

                        try {
                            PersonAssountListBean bean = JSON.parseObject(response.body(), PersonAssountListBean.class);
                            if (bean != null && !bean.getData().isEmpty()) {
                                adapter.setNewData(bean.getData());
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(Response<String> response) {
                        super.onError(response);
                        Log.i("test", "onError   " + response.body());
                    }
                });
    }

    private void find() {
        TextView tvTitle = findViewById(R.id.head_blue_tv_center);
        tvTitle.setText("个人账户");
        findViewById(R.id.head_blue_iv_right).setVisibility(View.GONE);

        tvRight = findViewById(R.id.head_blue_tv_right);
        tvRight.setText("添加");

        TextView tvInfo = findViewById(R.id.act_pal_tv);
        tvInfo.setText("右滑即可解绑账号\r\n添加支付宝账号将绑定最新添加的，所属银行同理");

        RecyclerView mRecycler = findViewById(R.id.act_pal_recycler);
        mRecycler.setLayoutManager(new LinearLayoutManager(PersonAssountListActivity.this));

        adapter = new PersonAssountListActAdapter();
        mRecycler.setAdapter(adapter);
    }

    private void listener() {
        findViewById(R.id.head_blue_iv_left).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        tvRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(PersonAssountListActivity.this, AddPersonAssountActivity.class);
                intent.putExtra("mbtToken", mbtToken);
                intent.putExtra("phone", phone);
                startActivity(intent);
            }
        });

        adapter.setOnItemChildClickListener(new BaseQuickAdapter.OnItemChildClickListener() {
            @Override
            public void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {
                PersonAssountListBean.DataBean dataBean = (PersonAssountListBean.DataBean) adapter.getItem(position);

                OkGo.<String>get("http://service.jiangaifen.com:38082/userAccount/delete")
                        .tag(this)
                        .headers("mbtToken", mbtToken)
                        .params("type", dataBean.getType())
                        .params("bank", dataBean.getBank())
                        .execute(new StringCallback() {
                            @Override
                            public void onSuccess(Response<String> response) {

                                Log.i("test", "delete   " + response.body());
                                JSONObject jsonObject = JSON.parseObject(response.body());
                                if (jsonObject.getIntValue("resultCode") == 0) {
                                    initnet();
                                } else {
                                    Toast.makeText(PersonAssountListActivity.this, jsonObject.getString("msg"), Toast.LENGTH_SHORT).show();
                                }
                            }

                            @Override
                            public void onError(Response<String> response) {
                                super.onError(response);
                                Log.i("test", " delete onError   " + response.body());
                            }
                        });
            }
        });
    }
}
