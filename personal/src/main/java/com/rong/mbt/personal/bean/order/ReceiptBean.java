package com.rong.mbt.personal.bean.order;

import java.io.Serializable;

public class ReceiptBean implements Serializable {


    /**
     * msg : 请求成功
     * resultCode : 0
     */

    private String msg;
    private int resultCode;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public int getResultCode() {
        return resultCode;
    }

    public void setResultCode(int resultCode) {
        this.resultCode = resultCode;
    }
}
