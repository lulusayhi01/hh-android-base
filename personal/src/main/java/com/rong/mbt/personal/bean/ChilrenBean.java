package com.rong.mbt.personal.bean;

import java.util.List;

/**
 * Created by jianfei on 2018/9/14.
 */

public class ChilrenBean {


    /**
     * headimage : http://p6nlllxof.bkt.clouddn.com/upload/20180705/3f668359c2354f2298f978c6266b560d.jpg
     * trendsImg : []
     * children : [{"headimage":"http://oss-aliyun-liululu.oss-cn-shanghai.aliyuncs.com/upload/153558497872210501.jpg","trendsImg":[],"children":[],"phone":"13165957673","creattime":"2018-08-02 17:24:26","name":"鑫哥","id":39,"machineCount":4,"childrenCount":0}]
     * phone : 13383305056
     * creattime : 2018-08-02 14:11:57
     * name : 13383305056
     * id : 37
     * machineCount : 3
     * childrenCount : 1
     */

    private String headimage;
    private String phone;
    private String creattime;
    private String name;
    private String friendRemark;
    private int id;
    private int machineCount;
    private int childrenCount;
    private List<?> trendsImg;
    private List<ChildrenBean> children;

    public String getFriendRemark() {
        return friendRemark;
    }

    public void setFriendRemark(String friendRemark) {
        this.friendRemark = friendRemark;
    }

    public String getHeadimage() {
        return headimage;
    }

    public void setHeadimage(String headimage) {
        this.headimage = headimage;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getCreattime() {
        return creattime;
    }

    public void setCreattime(String creattime) {
        this.creattime = creattime;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getMachineCount() {
        return machineCount;
    }

    public void setMachineCount(int machineCount) {
        this.machineCount = machineCount;
    }

    public int getChildrenCount() {
        return childrenCount;
    }

    public void setChildrenCount(int childrenCount) {
        this.childrenCount = childrenCount;
    }

    public List<?> getTrendsImg() {
        return trendsImg;
    }

    public void setTrendsImg(List<?> trendsImg) {
        this.trendsImg = trendsImg;
    }

    public List<ChildrenBean> getChildren() {
        return children;
    }

    public void setChildren(List<ChildrenBean> children) {
        this.children = children;
    }

    public static class ChildrenBean {
        /**
         * headimage : http://oss-aliyun-liululu.oss-cn-shanghai.aliyuncs.com/upload/153558497872210501.jpg
         * trendsImg : []
         * children : []
         * phone : 13165957673
         * creattime : 2018-08-02 17:24:26
         * name : 鑫哥
         * id : 39
         * machineCount : 4
         * childrenCount : 0
         */
        private String friendRemark;
        private String headimage;
        private String phone;
        private String creattime;
        private String name;
        private int id;
        private int machineCount;
        private int childrenCount;
        private List<?> trendsImg;
        private List<?> children;

        public String getFriendRemark() {
            return friendRemark;
        }

        public void setFriendRemark(String friendRemark) {
            this.friendRemark = friendRemark;
        }

        public String getHeadimage() {
            return headimage;
        }

        public void setHeadimage(String headimage) {
            this.headimage = headimage;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getCreattime() {
            return creattime;
        }

        public void setCreattime(String creattime) {
            this.creattime = creattime;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getMachineCount() {
            return machineCount;
        }

        public void setMachineCount(int machineCount) {
            this.machineCount = machineCount;
        }

        public int getChildrenCount() {
            return childrenCount;
        }

        public void setChildrenCount(int childrenCount) {
            this.childrenCount = childrenCount;
        }

        public List<?> getTrendsImg() {
            return trendsImg;
        }

        public void setTrendsImg(List<?> trendsImg) {
            this.trendsImg = trendsImg;
        }

        public List<?> getChildren() {
            return children;
        }

        public void setChildren(List<?> children) {
            this.children = children;
        }
    }
}
