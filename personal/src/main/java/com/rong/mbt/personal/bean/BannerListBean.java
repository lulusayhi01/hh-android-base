package com.rong.mbt.personal.bean;

import java.io.Serializable;

public class BannerListBean implements Serializable {


    /**
     * creattime : 2019-08-29 00:43:26.0
     * id : 5
     * img : https://hh-liululu.oss-cn-beijing.aliyuncs.com/banner/banner001.jpg
     * status : 1
     * title : 1
     */

    private String creattime;
    private int id;
    private String img;
    private int status;
    private String title;
    private String url;//轮播图跳转

    public String getCreattime() {
        return creattime;
    }

    public void setCreattime(String creattime) {
        this.creattime = creattime;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
