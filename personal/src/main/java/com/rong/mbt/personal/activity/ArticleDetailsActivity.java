package com.rong.mbt.personal.activity;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.rong.mbt.personal.R;
import com.rong.mbt.personal.activity.base.PBaseActivity;
import com.rong.mbt.personal.adapter.ImageListAdapter;
import com.rong.mbt.personal.bean.article.ArticleItemBean;
import com.rong.mbt.personal.bean.image.ImageBean;

import java.util.ArrayList;
import java.util.List;

/**
 * 文章详情
 */
public class ArticleDetailsActivity extends PBaseActivity {


    private LinearLayout mLlBack;
    private TextView mTxtTitle;
    private View mIvRight;
    private LinearLayout mLlRightMenu;
    private TextView mTxtRight;
    private LinearLayout mLlRightText;
    private TextView mTvTitle;
    private RecyclerView mRecyclerImageView;
    private TextView mTvContent;


    private ImageListAdapter imageListAdapter;

    @Override
    public void initView() {
        super.initView();
        mLlBack = findViewById(R.id.ll_back);
        mTxtTitle = findViewById(R.id.txt_title);
        mIvRight = findViewById(R.id.iv_right);
        mLlRightMenu = findViewById(R.id.ll_right_menu);
        mTxtRight = findViewById(R.id.txt_right);
        mLlRightText = findViewById(R.id.ll_right_text);
        mTvTitle = findViewById(R.id.tv_title);
        mRecyclerImageView = findViewById(R.id.recycler_image_view);
        mTvContent = findViewById(R.id.tv_content);

        mLlBack.setOnClickListener(v -> finish());
        inintData();
    }

    private void inintData() {
        mTxtTitle.setText("文章详情");
        mRecyclerImageView.setLayoutManager(new LinearLayoutManager(this));
        imageListAdapter = new ImageListAdapter();
        mRecyclerImageView.setAdapter(imageListAdapter);

        ArticleItemBean articleItemBean = (ArticleItemBean) getIntent().getSerializableExtra("ArticleItemBean");

        if (articleItemBean != null) {
            List<ArticleItemBean.ImgUrlListBean> imgUrlList = articleItemBean.getImgUrlList();
            List<ImageBean> imageBeans = new ArrayList<>();
            for (ArticleItemBean.ImgUrlListBean bean : imgUrlList) {
                ImageBean imageBean = new ImageBean();
                imageBean.setUrl(bean.getImgUrl());
                imageBeans.add(imageBean);
            }

            imageListAdapter.setNewData(imageBeans);
            mTvTitle.setText(articleItemBean.getArticle().getTitle());
            mTvContent.setText(articleItemBean.getArticle().getContent());

        }
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_article_details;
    }
}
