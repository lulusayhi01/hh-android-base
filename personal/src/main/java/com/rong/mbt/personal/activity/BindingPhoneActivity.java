package com.rong.mbt.personal.activity;


import android.content.Intent;
import android.view.View;
import android.widget.TextView;

import com.rong.mbt.personal.R;
import com.rong.mbt.personal.activity.base.PBaseActivity;

//绑定手机号
public class BindingPhoneActivity extends PBaseActivity {
    @Override
    public int getLayoutId() {
        return R.layout.activity_binding_phone;


    }

    @Override
    public void initView() {
        super.initView();
        findViewById(R.id.head_while_iv_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        TextView tvTitle = findViewById(R.id.head_while_tv_center);
        tvTitle.setText("绑定手机号码");



        final String phone = getIntent().getStringExtra("phone");
        final String mbtToken = getIntent().getStringExtra("mbtToken");
        TextView tvPhone = findViewById(R.id.act_bingding_phone_tv);
        tvPhone.setText(phone);


        findViewById(R.id.act_bingding_phone_relative).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(BindingPhoneActivity.this, ValidationPhoneActivity.class);
                intent.putExtra("phone", phone);
                intent.putExtra("mbtToken", mbtToken);
                startActivity(intent);
                finish();
            }
        });
    }


}

