package com.rong.mbt.personal.adapter;

import android.view.View;
import android.widget.TextView;

import com.alibaba.fastjson.JSONObject;
import com.calanger.hh.model.Doctor;
import com.calanger.hh.model.OrderPre;
import com.calanger.hh.model.User;
import com.calanger.hh.util.DateUtils;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.rong.mbt.personal.R;

import java.util.Map;

/**
 * 文章列表
 */
public class OrderPreListAdapter extends BaseQuickAdapter<Map<String, Object>, BaseViewHolder> {



    public OrderPreListAdapter() {
        super(R.layout.item_order_pre_list);
    }

    @Override
    protected void convert(BaseViewHolder helper, Map<String, Object> item) {

        try {
            JSONObject jsonMap = new JSONObject(item);
            OrderPre orderPre = jsonMap.getObject("orderPre", OrderPre.class);
            User user = jsonMap.getObject("user", User.class);
            Doctor doctor = jsonMap.getObject("doctor", Doctor.class);
            Integer userViewLikeCommentListSize = jsonMap.getObject("userViewLikeCommentListSize", Integer.class);


            TextView tvName = helper.getView(R.id.tv_name);
            TextView tvAge = helper.getView(R.id.tv_age);
            TextView tvTime = helper.getView(R.id.tv_time);
            TextView tvDescribe = helper.getView(R.id.tv_describe);
            TextView tvOrderStatus = helper.getView(R.id.tv_order_status);
            TextView tvConsultingFee = helper.getView(R.id.tv_consulting_fee);
            TextView tvCancelOrderPre = helper.getView(R.id.tv_cancel_order_pre);
            TextView tvQuickCommunication = helper.getView(R.id.tv_quick_communication);
            TextView tvConfirmCompletion = helper.getView(R.id.tv_confirm_completion);
            TextView tvComment = helper.getView(R.id.tv_comment);

            tvName.setText("患者姓名："+orderPre.getName());
            tvAge.setText("年龄："+orderPre.getAge());
            tvTime.setText("发布时间："+DateUtils.dateToString(orderPre.getCreattime()));
            tvDescribe.setText("病情描述："+orderPre.getContent());
            tvConsultingFee.setText("咨询费用："+orderPre.getFeed()+"元");

            tvCancelOrderPre.setVisibility(View.GONE);
            tvQuickCommunication.setVisibility(View.GONE);
            tvConfirmCompletion.setVisibility(View.GONE);
            tvComment.setVisibility(View.GONE);

//            (0未接单，1沟通中 2完成)
            if(orderPre.getOrderStatus()==0){
                tvOrderStatus.setText("订单状态："+"未接单");
                tvCancelOrderPre.setVisibility(View.VISIBLE);
            }
            if(orderPre.getOrderStatus()==1){
                tvOrderStatus.setText("订单状态："+"沟通中");
                tvQuickCommunication.setVisibility(View.VISIBLE);
                tvConfirmCompletion.setVisibility(View.VISIBLE);
            }
            if(orderPre.getOrderStatus()==2){
                tvOrderStatus.setText("订单状态："+"已完成");
                tvComment.setVisibility(View.VISIBLE);
            }

            helper.addOnClickListener(R.id.tv_cancel_order_pre);
            helper.addOnClickListener(R.id.tv_quick_communication);
            helper.addOnClickListener(R.id.tv_confirm_completion);
            helper.addOnClickListener(R.id.tv_comment);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
