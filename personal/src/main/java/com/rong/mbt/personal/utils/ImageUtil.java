package com.rong.mbt.personal.utils;

import android.content.Context;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;


public class ImageUtil {
    public static void loadImage(Context context, Object url, ImageView imageView) {
        Glide.with(context).load(url).into(imageView);
    }

    public static void loadCircleImage(Context context, Object url, ImageView imageView) {
        RequestOptions mRequestOptions = RequestOptions.circleCropTransform()
                .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                .skipMemoryCache(false);
        Glide.with(context).load(url).apply(mRequestOptions).into(imageView);

    }

    public static void loadRoundeImga(Context context, Object url, ImageView imageView, int round, int width, int height) {
        RoundedCorners roundedCorners = new RoundedCorners(round);
        RequestOptions options = RequestOptions.bitmapTransform(roundedCorners).override(width, height);
        Glide.with(context).load(url).apply(options).into(imageView);
    }
    public static void loadRoundeImga(Context context, Object url, ImageView imageView, int round) {
        RoundedCorners roundedCorners = new RoundedCorners(round);

        RequestOptions options = RequestOptions.bitmapTransform(roundedCorners);
        options.centerCrop();
        Glide.with(context).load(url).apply(options).into(imageView);
    }

    public static void loadImageCenterCrop(Context context, Object url, ImageView imageView) {

        RequestOptions requestOptions = new RequestOptions();

        requestOptions.centerCrop();
        Glide.with(context).load(url).apply(requestOptions).into(imageView);
    }
}
