package com.rong.mbt.personal.bean.user;

import java.io.Serializable;

public class UserBean implements Serializable {

    /**
     * belongcode : QDSEFw1w
     * channelid : 0
     * coinstatus : 1
     * coldmoney : 0
     * creattime : 2019-08-02 15:28:55
     * fetchmoney : 0
     * headimage : https://hh-liululu.oss-cn-beijing.aliyuncs.com/default/user-header-dafault.png
     * id : 87
     * investmoney : 0
     * inviterid : 0
     * level : 0
     * loginnum : 0
     * logintime : 1564730935
     * name : 17631330812
     * passwd : 662919d207123aa79147b5fc3d54831f
     * phone : 17631330812
     * rapnum : 0
     * registermoney : 10
     * remark :
     * returnmoney : 0
     * sex : 0
     * status : 1
     * summoney : 10
     * token : LmcEB0ATXU0fQadUf/chF0yGNZGdjsClwXid56lRYen6qh8SPZ+DT5KcdZGSk9qj08w48h423/8bnlrI7lq8HA==
     * type : 0
     * waitactivemoney : 0
     */

    private String belongcode;
    private int channelid;
    private int coinstatus;
    private String coldmoney;
    private String creattime;
    private String fetchmoney;
    private String headimage;
    private int id;
    private String investmoney;
    private int inviterid;
    private int level;
    private int loginnum;
    private String logintime;
    private String name;
    private String passwd;
    private String phone;
    private String rapnum;
    private String registermoney;
    private String remark;
    private String returnmoney;
    private int sex;
    private int status;
    private String summoney;
    private String token;
    private int type;
    private String waitactivemoney;

    public String getBelongcode() {
        return belongcode;
    }

    public void setBelongcode(String belongcode) {
        this.belongcode = belongcode;
    }

    public int getChannelid() {
        return channelid;
    }

    public void setChannelid(int channelid) {
        this.channelid = channelid;
    }

    public int getCoinstatus() {
        return coinstatus;
    }

    public void setCoinstatus(int coinstatus) {
        this.coinstatus = coinstatus;
    }

    public String getColdmoney() {
        return coldmoney;
    }

    public void setColdmoney(String coldmoney) {
        this.coldmoney = coldmoney;
    }

    public String getCreattime() {
        return creattime;
    }

    public void setCreattime(String creattime) {
        this.creattime = creattime;
    }

    public String getFetchmoney() {
        return fetchmoney;
    }

    public void setFetchmoney(String fetchmoney) {
        this.fetchmoney = fetchmoney;
    }

    public String getHeadimage() {
        return headimage;
    }

    public void setHeadimage(String headimage) {
        this.headimage = headimage;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getInvestmoney() {
        return investmoney;
    }

    public void setInvestmoney(String investmoney) {
        this.investmoney = investmoney;
    }

    public int getInviterid() {
        return inviterid;
    }

    public void setInviterid(int inviterid) {
        this.inviterid = inviterid;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getLoginnum() {
        return loginnum;
    }

    public void setLoginnum(int loginnum) {
        this.loginnum = loginnum;
    }

    public String getLogintime() {
        return logintime;
    }

    public void setLogintime(String logintime) {
        this.logintime = logintime;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPasswd() {
        return passwd;
    }

    public void setPasswd(String passwd) {
        this.passwd = passwd;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getRapnum() {
        return rapnum;
    }

    public void setRapnum(String rapnum) {
        this.rapnum = rapnum;
    }

    public String getRegistermoney() {
        return registermoney;
    }

    public void setRegistermoney(String registermoney) {
        this.registermoney = registermoney;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getReturnmoney() {
        return returnmoney;
    }

    public void setReturnmoney(String returnmoney) {
        this.returnmoney = returnmoney;
    }

    public int getSex() {
        return sex;
    }

    public void setSex(int sex) {
        this.sex = sex;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getSummoney() {
        return summoney;
    }

    public void setSummoney(String summoney) {
        this.summoney = summoney;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getWaitactivemoney() {
        return waitactivemoney;
    }

    public void setWaitactivemoney(String waitactivemoney) {
        this.waitactivemoney = waitactivemoney;
    }

}
