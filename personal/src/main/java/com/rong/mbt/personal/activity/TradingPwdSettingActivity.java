package com.rong.mbt.personal.activity;


import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.fastjson.JSON;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.callback.StringCallback;
import com.lzy.okgo.model.Response;
import com.rong.mbt.personal.R;
import com.rong.mbt.personal.activity.base.PBaseActivity;
import com.rong.mbt.personal.bean.base.BaseBean;
import com.rong.mbt.personal.dialog.CustomerServiceDialog;

@Route(path = "/personal/TradingPwdSettingActivity")
public class TradingPwdSettingActivity extends PBaseActivity {

    private ImageView ivRight;
    private EditText et1;
    private EditText et2;
    private EditText et3;
    private String mbtToken;
    private String phone;

    private boolean isSettingPwd;
    private LinearLayout linearlayout;

    @Override
    public int getLayoutId() {
        return R.layout.activity_trading_pwd_setting;
    }

    @Override
    public void initView() {
        super.initView();

        mbtToken = getIntent().getStringExtra("mbtToken");
        phone = getIntent().getStringExtra("phone");


        find();
        listener();
        boolean isNoShow = getIntent().getBooleanExtra("isNoShow", false);
        if (!isNoShow) {
            initnet();
        } else {
            linearlayout.setVisibility(View.GONE);
            isSettingPwd = false;
        }
    }

    private void initnet() {
        //检查是否设置过6位数字交易密码
        OkGo.<String>post("http://service.jiangaifen.com:38082/userPwd/checkUserNumberPwd")
                .tag(this)
//                            .isMultipart(true)
                .headers("mbtToken", mbtToken)
                .execute(new StringCallback() {
                    @Override
                    public void onSuccess(Response<String> response) {

                        Log.i("test", "onSuccess  " + response.body());
                        BaseBean baseBean = JSON.parseObject(response.body(), BaseBean.class);
                        if (baseBean.isSuccess()) {
                            isSettingPwd = true;
                            linearlayout.setVisibility(View.VISIBLE);
                            //{"msg":"请求成功","resultCode":0}
                        } else {
                            linearlayout.setVisibility(View.GONE);
                            isSettingPwd = false;
                            //{"msg":"暂未设置6位交易密码","resultCode":200}
                        }
                    }

                    @Override
                    public void onError(Response<String> response) {
                        super.onError(response);
                        Log.i("test", "onError  " + response.message());
                    }
                });
    }

    private void listener() {
        findViewById(R.id.head_while_iv_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        ivRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CustomerServiceDialog csDialog = new CustomerServiceDialog(TradingPwdSettingActivity.this);
                csDialog.show();
            }
        });
        findViewById(R.id.act_tps_bt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (et2.getText().toString().isEmpty() || et2.getText().toString().length() < 6
                        || et3.getText().toString().isEmpty() || et3.getText().toString().length() < 6) {
                    Toast.makeText(TradingPwdSettingActivity.this, "输入密码格式有误", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (!isSettingPwd) {
                    //设置新交易密码
                    OkGo.<String>post("http://service.jiangaifen.com:38082/userPwd/setNumberPwd")
                            .tag(this)
//                            .isMultipart(true)
                            .headers("mbtToken", mbtToken)
                            .params("numberpwd", et2.getText().toString())
                            .params("affirmPwd", et3.getText().toString())
                            .execute(new StringCallback() {
                                @Override
                                public void onSuccess(Response<String> response) {
                                    Log.i("test", "onSuccess  " + response.body());
                                    BaseBean baseBean = JSON.parseObject(response.body(), BaseBean.class);
                                    if (baseBean.isSuccess()) {
                                        Toast.makeText(TradingPwdSettingActivity.this, "新交易密码设置成功", Toast.LENGTH_SHORT).show();
                                        finish();
                                    } else {
                                        Toast.makeText(TradingPwdSettingActivity.this, baseBean.getMsg(), Toast.LENGTH_SHORT).show();
                                    }
                                }

                                @Override
                                public void onError(Response<String> response) {
                                    super.onError(response);
                                    Log.i("test", "onError  " + response.message());
                                }
                            });
                } else {
                    //旧换新
                    OkGo.<String>post("http://service.jiangaifen.com:38082/userPwd/editNumberPwd")
                            .tag(this)
//                            .isMultipart(true)
                            .headers("mbtToken", mbtToken)
                            .params("numberpwdure", et1.getText().toString())
                            .params("numberpwd", et2.getText().toString())
                            .params("affirmPwd", et3.getText().toString())
                            .execute(new StringCallback() {
                                @Override
                                public void onSuccess(Response<String> response) {
                                    Log.i("test", "onSuccess  " + response.body());
                                    BaseBean baseBean = JSON.parseObject(response.body(), BaseBean.class);
                                    if (baseBean.isSuccess()) {
                                        Toast.makeText(TradingPwdSettingActivity.this, "新交易密码设置成功", Toast.LENGTH_SHORT).show();
                                    } else {
                                        Toast.makeText(TradingPwdSettingActivity.this, baseBean.getMsg(), Toast.LENGTH_SHORT).show();
                                    }
                                }

                                @Override
                                public void onError(Response<String> response) {
                                    super.onError(response);
                                    Log.i("test", "onError  " + response.message());
                                }
                            });
                }
            }
        });

        findViewById(R.id.tv_passwrod).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                mbtToken = getIntent().getStringExtra("mbtToken");
//                phone = getIntent().getStringExtra("phone");
                Intent intent = new Intent(TradingPwdSettingActivity.this, UpdateLoginPwdActivity.class);
                intent.putExtra("mbtToken", mbtToken);
                intent.putExtra("phone", phone);
                intent.putExtra("startAct", "Trading");
                startActivity(intent);
                finish();
            }
        });
    }

    private void find() {
        TextView tvTitle = findViewById(R.id.head_while_tv_center);
        tvTitle.setText("修改交易密码");
        ivRight = findViewById(R.id.head_while_iv_right);
        ivRight.setVisibility(View.VISIBLE);

        et1 = findViewById(R.id.act_tps_et1);
        et2 = findViewById(R.id.act_tps_et2);
        et3 = findViewById(R.id.act_tps_et3);
        linearlayout = findViewById(R.id.act_tps_line);

    }
}
