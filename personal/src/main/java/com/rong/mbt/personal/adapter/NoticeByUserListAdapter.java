package com.rong.mbt.personal.adapter;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.alibaba.fastjson.JSONObject;
import com.bumptech.glide.Glide;
import com.calanger.hh.model.Doctor;
import com.calanger.hh.model.Notice;
import com.calanger.hh.model.User;
import com.calanger.hh.util.DateUtils;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.rong.mbt.personal.R;
import com.rong.mbt.personal.bean.doctor.DoctorListBean;
import com.rong.mbt.personal.utils.SpUtils;

import java.util.Map;

/**
 * 文章列表
 */
public class NoticeByUserListAdapter extends BaseQuickAdapter<Map<String, Object>, BaseViewHolder> {

    Context context;

    public NoticeByUserListAdapter(Context context) {
        super(R.layout.item_notice);
        context = context;
    }


    @Override
    protected void convert(BaseViewHolder helper, Map<String, Object> item) {

        JSONObject jo = new JSONObject(item);
        Notice notice = jo.getObject("notice", Notice.class);
        User user = jo.getObject("user", User.class);
        Doctor doctor = jo.getObject("doctor", Doctor.class);
        Integer userViewLikeCommentListSize = jo.getObject("userViewLikeCommentListSize", Integer.class);

        try {
            int userType = SpUtils.getUserType(context);

            if(userType==0){
                TextView tvName = helper.getView(R.id.tv_name);
                tvName.setText(doctor.getName());
                ImageView ivHead = helper.getView(R.id.iv_head);
                Glide.with(helper.itemView.getContext()).load(doctor.getHeaderimg()).into(ivHead);
            }else{
                TextView tvName = helper.getView(R.id.tv_name);
                tvName.setText(user.getName());
                ImageView ivHead = helper.getView(R.id.iv_head);
                Glide.with(helper.itemView.getContext()).load(user.getHeadimage()).into(ivHead);
            }


            TextView tvCreattime = helper.getView(R.id.tv_creattime);
            tvCreattime.setText(DateUtils.dateToString(notice.getCreattime()));
            TextView tvInfo = helper.getView(R.id.tv_info);
            tvInfo.setText(notice.getContent());
            TextView tvAction = helper.getView(R.id.tv_action);
            helper.addOnClickListener(R.id.tv_action);
            if (userViewLikeCommentListSize > 0) {
                tvAction.setText("已回复");
            } else {
                tvAction.setText("未回复");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
