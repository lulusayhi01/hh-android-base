package com.rong.mbt.personal.bean.base;

public class BaseBean {

    /**
     * msg : 设置成功
     * resultCode : 0
     */

    private String msg;
    private String data;
    private int resultCode;

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getMsg() {
        return msg;
    }

    public boolean isSuccess() {
        return resultCode == 0;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public int getResultCode() {
        return resultCode;
    }

    public void setResultCode(int resultCode) {
        this.resultCode = resultCode;
    }
}
