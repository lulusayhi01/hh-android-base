package com.rong.mbt.personal.activity;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.android.arouter.launcher.ARouter;
import com.alibaba.fastjson.JSON;
import com.kyleduo.switchbutton.SwitchButton;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.callback.StringCallback;
import com.lzy.okgo.model.Response;
import com.rong.mbt.personal.ActivityManager;
import com.rong.mbt.personal.R;
import com.rong.mbt.personal.activity.base.PBaseActivity;
import com.rong.mbt.personal.bean.base.BaseBean;
import com.rong.mbt.personal.http.UrlApi;
import com.rong.mbt.personal.utils.SpUtils;
import com.rong.mbt.personal.utils.UpdateAppHttpUtil;
import com.vector.update_app.UpdateAppBean;
import com.vector.update_app.UpdateAppManager;
import com.vector.update_app.UpdateCallback;

import java.util.HashMap;
import java.util.Map;

/**
 * 设置
 */
public class SettingActivity extends PBaseActivity {


    private LinearLayout mLlBack;
    private TextView mTxtTitle;
    private View mIvRight;
    private LinearLayout mLlRightMenu;
    private TextView mTxtRight;
    private LinearLayout mLlRightText;
    private SwitchButton mSwitchButton;
    private LinearLayout mLlCheckUpdate;
    private LinearLayout mLlLoginOut;

    @Override
    public void initView() {
        super.initView();
        mLlBack = findViewById(R.id.ll_back);
        mTxtTitle = findViewById(R.id.txt_title);
        mIvRight = findViewById(R.id.iv_right);
        mLlRightMenu = findViewById(R.id.ll_right_menu);
        mTxtRight = findViewById(R.id.txt_right);
        mLlRightText = findViewById(R.id.ll_right_text);
        mSwitchButton = findViewById(R.id.switch_button);
        mLlCheckUpdate = findViewById(R.id.ll_check_update);
        mLlLoginOut = findViewById(R.id.ll_login_out);
        mTxtTitle.setText("设置");
        mLlBack.setOnClickListener(v -> finish());


        mLlCheckUpdate.setOnClickListener(v -> updateApp());
        mLlLoginOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OkGo.<String>get("http://service.jiangaifen.com:38082/user/logout")
                        .tag(this)
                        .headers("mbtToken", SpUtils.getToken(getBaseContext()))
                        .execute(new StringCallback() {
                            @Override
                            public void onSuccess(Response<String> response) {
                                BaseBean baseBean = JSON.parseObject(response.body(), BaseBean.class);
                                if (baseBean.isSuccess()) {
                                    SharedPreferences config = getSharedPreferences("config", Context.MODE_PRIVATE);
                                    SharedPreferences.Editor edit = config.edit();
                                    edit.putInt("getAllUserInfoState", 0);
                                    edit.putBoolean("GestureIsOpen", false);
                                    edit.commit();

//                                    ARouter.getInstance().build("/personal/TradingPwdSettingActivity").navigation();
                                    ARouter.getInstance().build("/acitvity/login").navigation();
                                    ActivityManager.exit();
                                } else {
                                    Toast.makeText(SettingActivity.this, baseBean.getMsg(), Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
            }
        });
    }

    /**
     * 版本更新
     */
    private void updateApp() {
        Map<String, String> params = new HashMap<String, String>();
        params.put("appType", "android");
        params.put("mbtToken", SpUtils.getToken(this));
        new UpdateAppManager
                .Builder()
                .setActivity(this)
                .setParams(params)
                .setUpdateUrl(UrlApi.baseUrl + "/app/check")
                .setTopPic(R.drawable.top_3)
//                .hideDialogOnDownloading()
                .setThemeColor(0xff3498DB)
                .setHttpManager(new UpdateAppHttpUtil())
                .build()
                .checkNewApp(new UpdateCallback() {
                    /**
                     * 解析json,自定义协议
                     *
                     * @param json 服务器返回的json
                     * @return UpdateAppBean
                     */
                    @Override
                    protected UpdateAppBean parseJson(String json) {
                        UpdateAppBean updateAppBean = new UpdateAppBean();
                        try {
//                            AppVersionResponse response = JsonMananger.jsonToBean(json, AppVersionResponse.class);
//                            if (response == null) return updateAppBean;
//                            String versionName = AppUpdateUtils.getVersionName(mContext);
//                            final String newVersion = response.getData().getAppVersion();
//                            if (VersionUtils.compareVersion(versionName, newVersion) == -1) {
//                                updateAppBean
//                                        //（必须）是否更新Yes,No
//                                        .setUpdate("Yes")
//                                        //（必须）新版本号，
//                                        .setNewVersion(newVersion)
//                                        .setTargetSize(response.getData().getAppSize())
//                                        //（必须）下载地址
//                                        .setApkFileUrl(response.getData().getAppUrl())
//                                        .setUpdateLog(response.getData().getRemarker())
//                                        //是否强制更新
//                                        .setConstraint(response.getData().getConstraint() == 1);
//                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        return updateAppBean;
                    }

                    @Override
                    protected void hasNewApp(UpdateAppBean updateApp, UpdateAppManager updateAppManager) {
                        updateAppManager.showDialogFragment();
                    }

                    /**
                     * 没有新版本
                     */
                    @Override
                    public void noNewApp(String error) {
                    }
                });
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_setting;
    }
}
