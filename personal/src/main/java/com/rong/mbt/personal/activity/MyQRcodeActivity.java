package com.rong.mbt.personal.activity;

import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.lzy.okgo.OkGo;
import com.lzy.okgo.callback.StringCallback;
import com.lzy.okgo.model.Response;
import com.rong.mbt.personal.R;
import com.rong.mbt.personal.activity.base.PBaseActivity;
import com.rong.mbt.personal.bean.PersonalInfoBean;
import com.rong.mbt.personal.utils.ImageUtil;
import com.rong.mbt.personal.utils.ScreenUtils;
import com.rong.mbt.personal.utils.ZxingUtil;

public class MyQRcodeActivity extends PBaseActivity {


    private String mbtToken;

    @Override
    public int getLayoutId() {
        return R.layout.activity_my_qrcode;
    }

    @Override
    public void initView() {
        super.initView();
        mbtToken = getIntent().getStringExtra("mbtToken");

        net();
        listener();
        setText();

        PersonalInfoBean infoBean = (PersonalInfoBean) getIntent().getSerializableExtra("infoBean");

        ImageView iv = findViewById(R.id.dialog_qrcode_iv);
        TextView phone = findViewById(R.id.dialog_qrcode_tv_phone);
        ImageView tvQr = findViewById(R.id.dialog_qrcode_iv_qr);
        TextView location = findViewById(R.id.dialog_qrcode_tv_location);

        if (infoBean.getData().getHeadimage() != null) {
            ImageUtil.loadCircleImage(this, infoBean.getData().getHeadimage(), iv);
        }
        phone.setText(infoBean.getData().getPhone());

        tvQr.setImageBitmap(ZxingUtil.createBitmap(infoBean.getData().getPhone(), (int) (ScreenUtils.getScreenWidth(this) / 3), (int) (ScreenUtils.getScreenHeight(this) / 3)));
    }

    private void net() {
        OkGo.<String>get("http://service.jiangaifen.com:38082/user/showQrCode")
                .tag(this)
                .headers("mbtToken", mbtToken)
                .execute(new StringCallback() {
                    @Override
                    public void onSuccess(Response<String> response) {
                        Log.i("test","qrcode1 "+response.body());
                    }

                    @Override
                    public void onError(Response<String> response) {
                        super.onError(response);
                        Log.i("test","qrcode2 "+response.body());
                    }
                });
    }

    private void setText() {
        TextView tvTitle = findViewById(R.id.head_blue_tv_center);
        tvTitle.setText("我的二维码");
        findViewById(R.id.head_blue_iv_right).setVisibility(View.GONE);
    }

    private void listener() {
        findViewById(R.id.head_blue_iv_left).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
}
