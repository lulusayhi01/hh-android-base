package com.rong.mbt.personal.bean.article;

import java.io.Serializable;
import java.util.List;

public class ArticleListPageBean implements Serializable {


    /**
     * count : 1
     * data : [{"imgUrlList":[{"creattime":1567258860000,"id":17,"imgUrl":"http://oss-aliyun-liululu.oss-cn-shanghai.aliyuncs.com/upload/156725886425010133.jpg","objId":15,"objType":1,"status":1,"type":1},{"creattime":1567258860000,"id":18,"imgUrl":"http://oss-aliyun-liululu.oss-cn-shanghai.aliyuncs.com/upload/156725886499110249.jpg","objId":15,"objType":1,"status":1,"type":1},{"creattime":1567258860000,"id":19,"imgUrl":"http://oss-aliyun-liululu.oss-cn-shanghai.aliyuncs.com/upload/156725886504710083.jpg","objId":15,"objType":1,"status":1,"type":1}],"userViewLikeCommentList":[{"content":"测试评论呢看附近啊是打开了附近的时刻了饭看电视了附近开了电视减肥看电视了就分开了电视减肥看啦都是减肥里看电视放进口袋上了饭就打开了谁附近的时刻了附近的时刻了奋斗开始了就分开了电视见风使舵","creattime":1567259992000,"id":8,"objId":15,"objType":1,"status":1,"type":2,"userId":82}],"imgUrlListSize":3,"article":{"categoryId":1,"content":"放大了空间饭都快累死饭看电视了饭的时间看了饭的时刻了饭就打开了谁附近的时刻了饭的时刻了附近开了电视就分开了电视饭就困了电视附近开了电视饭就困了电视分开了电视饭都快累死饭都快累死姐姐分开的零食附近开了电视放进口袋零食附近开了电视就分开了电视放的路上看见放看电视了附近里看电视分开的零食附近的森林\n\n\n附近的时刻了附近的时刻了饭的时间看了饭的时刻了附近的快累死饭的就是看了饭的时间看了饭的时刻了饭就打开死了附近的时刻了减肥的开始了减肥的时刻了饭的时间看了饭的时刻了饭都结束了饭的时间里看放电视了饭的时刻了饭的时间都是电视了饭","creattime":1567258860000,"id":15,"pageImg":"http://oss-aliyun-liululu.oss-cn-shanghai.aliyuncs.com/upload/156725886425010133.jpg","status":1,"summary":"https://hh-liululu.oss-cn-beijing.aliyuncs.com/img-dafault%20.png","title":"养生。我们都都在为你付出的人和你一起去努力争取的人和你一起努力一起来支持","userid":82},"userViewLikeCommentListSize":1,"user":{"belongcode":"s3u5CJgt","channelid":0,"coinstatus":1,"coldmoney":"0","creattime":"2019-07-24 14:31:54","fetchmoney":"0","headimage":"https://hh-liululu.oss-cn-beijing.aliyuncs.com/default/default-ys001.jpg","id":82,"investmoney":"0","inviterid":0,"level":0,"loginnum":0,"logintime":"1563949914","name":"蓝二哥哥","passwd":"662919d207123aa79147b5fc3d54831f","phone":"13501031194","rapnum":"0","registermoney":"10","remark":"","returnmoney":"0","sex":0,"status":1,"summoney":"10","token":"1T27m3FUOURGljkNtHDiyVGFpQlNjXOfWZ2l03ptw05nufSd7dM0JuLP8awIQJPQHsl1uCyG/7wHloUAU5sRyw==","type":1,"waitactivemoney":"0"}}]
     * msg : 请求成功
     * resultCode : 0
     */

    private int count;
    private String msg;
    private int resultCode;
    private List<ArticleItemBean> data;

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public int getResultCode() {
        return resultCode;
    }

    public void setResultCode(int resultCode) {
        this.resultCode = resultCode;
    }

    public List<ArticleItemBean> getData() {
        return data;
    }

    public void setData(List<ArticleItemBean> data) {
        this.data = data;
    }
}
