package com.rong.mbt.personal.adapter;

import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.rong.mbt.personal.R;
import com.rong.mbt.personal.bean.order.OrderPreBean;
import com.rong.mbt.personal.utils.DateFormatUtil;

import java.text.MessageFormat;

/**
 * 我的订单
 */
public class MyOrderListAdapter extends BaseQuickAdapter<OrderPreBean, BaseViewHolder> {


    public MyOrderListAdapter() {
        super(R.layout.item_my_order_list);
    }

    @Override
    protected void convert(BaseViewHolder helper, OrderPreBean item) {

        try {
            TextView mTvName = helper.getView(R.id.tv_name);
            TextView mTvAge = helper.getView(R.id.tv_age);
            TextView mTvTime = helper.getView(R.id.tv_time);
            TextView mTvDescribe = helper.getView(R.id.tv_describe);
            TextView mTvOrderStatus = helper.getView(R.id.tv_order_status);
            TextView mTvConsultingFee = helper.getView(R.id.tv_consulting_fee);
            TextView mTvCommunication = helper.getView(R.id.tv_communication);
            TextView mTvDelete = helper.getView(R.id.tv_delete);
            mTvName.setText(String.format("患者姓名：%s", item.getName()));
            mTvAge.setText(MessageFormat.format("年龄：{0}", item.getAge()));
            mTvTime.setText(new StringBuilder().append("时间：").append(DateFormatUtil.transForDate2((int) item.getCreattime())).toString());
            mTvDescribe.setText(String.format("病情描述：%s", item.getContent()));
            int orderStatus = item.getOrderStatus();
            mTvOrderStatus.setText(String.format("订单状况：%s", orderStatus == 1 ? "沟通中" : "已解决"));//1:沟通中  2.已解决
            mTvCommunication.setText(orderStatus == 1 ? "沟通中" : "已解决");//1:沟通中  2.已解决
            mTvConsultingFee.setText(String.format("咨询费用：%s 元", item.getFeed()));


            helper.addOnClickListener(R.id.tv_communication).addOnClickListener(R.id.tv_delete);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
