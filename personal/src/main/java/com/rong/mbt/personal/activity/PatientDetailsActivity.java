package com.rong.mbt.personal.activity;

import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.model.Response;
import com.rong.mbt.personal.R;
import com.rong.mbt.personal.activity.base.PBaseActivity;
import com.rong.mbt.personal.bean.FriendRemarkBean;
import com.rong.mbt.personal.bean.order.OrderPreBean;
import com.rong.mbt.personal.bean.order.ReceiptBean;
import com.rong.mbt.personal.http.UrlApi;
import com.rong.mbt.personal.http.callback.JsonCallback;
import com.rong.mbt.personal.utils.SpUtils;
import com.shehuan.niv.NiceImageView;

import io.rong.imkit.RongIM;

/**
 * 患者详情（医生接单页面）
 */
public class PatientDetailsActivity extends PBaseActivity {

    @Override
    public void initView() {
        super.initView();
        OrderPreBean orderPreBean = (OrderPreBean) getIntent().getSerializableExtra("OrderPreBean");

        ImageView headBlueIvLeft = findViewById(R.id.head_blue_iv_left);
        NiceImageView ivHead = findViewById(R.id.iv_head);
        TextView tvPatientName = findViewById(R.id.tv_patient_name);
        TextView tvSex = findViewById(R.id.tv_sex);
        TextView tvAge = findViewById(R.id.tv_age);
        TextView tvPhone = findViewById(R.id.tv_phone);
        TextView tvCardNo = findViewById(R.id.tv_card_no);
        TextView tvFeed = findViewById(R.id.tv_feed);
        TextView tvDescriptionIllness = findViewById(R.id.tv_description_illness);
        TextView isHasMedicalHistory = findViewById(R.id.isHasMedical_history);
        TextView tvReceipt = findViewById(R.id.tv_receipt);
        ImageView back = findViewById(R.id.head_blue_iv_left);
        back.setOnClickListener(v -> finish());

        if (orderPreBean != null) {
            Glide.with(this).load(orderPreBean.getHeadimg()).into(ivHead);

            String sexName;//0未知  1男  2女
            if (orderPreBean.getSex() == 1) {
                sexName = "男";
            } else if (orderPreBean.getSex() == 2) {
                sexName = "女";
            } else {
                sexName = "未知";
            }
            tvSex.setText(sexName);

            tvAge.setText(orderPreBean.getAge() + "");
            tvCardNo.setText("无");
            tvFeed.setText(orderPreBean.getFeed());
            tvPatientName.setText(orderPreBean.getName());
            tvDescriptionIllness.setText(orderPreBean.getContent());

            tvReceipt.setOnClickListener(v -> {

                OkGo.<ReceiptBean>get(UrlApi.orderPreReceipt)
                        .tag(this)
                        .headers("mbtToken", SpUtils.getToken(this))
                        .params("id", orderPreBean.getId())
                        .execute(new JsonCallback<ReceiptBean>() {
                            @Override
                            public void onSuccess(Response<ReceiptBean> response) {
                                if (response != null && response.body() != null) {
                                    ReceiptBean body = response.body();
                                    if (body.getResultCode() == 0) {
//                                        requestSelectFriendRemark(orderPreBean.getUserId()+"",orderPreBean.getName());
                                        RongIM.getInstance().startPrivateChat(PatientDetailsActivity.this, orderPreBean.getUserId() + "", orderPreBean.getName());
                                        Toast.makeText(getApplicationContext(), "接单成功！", Toast.LENGTH_LONG);
                                        finish();
                                    } else {
                                        Toast.makeText(getApplicationContext(), "接单失败！", Toast.LENGTH_LONG);
                                    }
                                }
                            }
                        });

            });
        }else{




        }
    }

    private void requestSelectFriendRemark(String tuserid, String name) {
        OkGo.<FriendRemarkBean>get(UrlApi.friendSelectFriendRemark)
                .tag(this)
                .headers("mbtToken", SpUtils.getToken(this))
                .params("tuserid", tuserid)
                .execute(new JsonCallback<FriendRemarkBean>() {
                    @Override
                    public void onSuccess(Response<FriendRemarkBean> response) {
                        if (response != null && response.body() != null) {
                        }
                    }
                });
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_patient_details;
    }
}
