package com.rong.mbt.personal.utils;

import android.app.ProgressDialog;
import android.content.Context;

/**
 * Created by asus on 2017/8/18.
 */

public class DlgUtil {
    // 显示一个无法取消的进度条对话框，返回的对话框用来取消用
    public static ProgressDialog showProgressDlg(Context context, String msg) {
        ProgressDialog proDlg = new ProgressDialog(context);
        proDlg.setMessage(msg);
        proDlg.setCancelable(false);
        proDlg.setCanceledOnTouchOutside(false);
        proDlg.show();
        return proDlg;
    }
}
