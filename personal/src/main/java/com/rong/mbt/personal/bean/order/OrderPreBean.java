package com.rong.mbt.personal.bean.order;

import java.io.Serializable;

public class OrderPreBean implements Serializable {
    /**
     * age : 20
     * content : 如果你已经下定决心, 准备学习和安装 TensorFlow, 你可以略过这些文字, 直接阅读 后面的章节. 不用担心, 你仍然会看到 MNIST -- 在阐述 TensorFlow 的特性时, 我们还会使用 MNIST 作为一个样例.
     * creattime : 1567245699000
     * feed : 3
     * headimg : http://oss-aliyun-liululu.oss-cn-shanghai.aliyuncs.com/upload/156404160692410487.jpg
     * id : 28
     * name : 蓝二哥哥
     * orderStatus : 0
     * sex : 1
     */

    private String age;
    private String content;
    private long creattime;
    private String feed;
    private String headimg;
    private int id;
    private String name;
    private int orderStatus;
    private int sex;
    /**
     * userId : 86
     */
    private int userId;

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public long getCreattime() {
        return creattime;
    }

    public void setCreattime(long creattime) {
        this.creattime = creattime;
    }

    public String getFeed() {
        return feed;
    }

    public void setFeed(String feed) {
        this.feed = feed;
    }

    public String getHeadimg() {
        return headimg;
    }

    public void setHeadimg(String headimg) {
        this.headimg = headimg;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(int orderStatus) {
        this.orderStatus = orderStatus;
    }

    public int getSex() {
        return sex;
    }

    public void setSex(int sex) {
        this.sex = sex;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }
}