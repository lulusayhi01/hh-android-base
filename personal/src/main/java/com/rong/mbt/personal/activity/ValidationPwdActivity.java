package com.rong.mbt.personal.activity;


import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.lzy.okgo.OkGo;
import com.lzy.okgo.callback.StringCallback;
import com.lzy.okgo.model.Response;
import com.rong.mbt.personal.R;
import com.rong.mbt.personal.activity.base.PBaseActivity;
import com.rong.mbt.personal.dialog.CustomerServiceDialog;

//校验 两次的密码
public class ValidationPwdActivity extends PBaseActivity {


    private ImageView ivRight;
    private EditText et2;
    private EditText et1;
    private String mbtToken;
    private String phone;
    private String etVcode;

    @Override
    public int getLayoutId() {
        return R.layout.activity_validation_pwd;
    }

    @Override
    public void initView() {
        super.initView();
        mbtToken = getIntent().getStringExtra("mbtToken");
        phone = getIntent().getStringExtra("phone");
        etVcode = getIntent().getStringExtra("etVcode");


        find();
        listener();
    }

    private void find() {
        TextView tvTitle = findViewById(R.id.head_while_tv_center);
        tvTitle.setText("修改登录密码");
        ivRight = findViewById(R.id.head_while_iv_right);
        ivRight.setVisibility(View.VISIBLE);

        et1 = findViewById(R.id.act_vp_et1);
        et2 = findViewById(R.id.act_vp_et2);

    }

    private void listener() {
        findViewById(R.id.head_while_iv_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        ivRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CustomerServiceDialog csDialog = new CustomerServiceDialog(ValidationPwdActivity.this);
                csDialog.show();
            }
        });

        findViewById(R.id.act_vp_bt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String et1str = et1.getText().toString();
                String et2str = et2.getText().toString();
                if (et1str.length() >= 6 && et1str.length() <= 16 && et2str.length() >= 6 && et2str.length() < 16) {
                    if (et1str.equals(et2str)) {
                        OkGo.<String>post("http://service.jiangaifen.com:38082/user/findPwd")
                                .tag(this)
//                            .isMultipart(true)
                                .headers("mbtToken", mbtToken)
                                .params("phoneNum", phone)
                                .params("password", et1str)
                                .params("passsure", et2str)
                                .params("vcode", etVcode)
                                .execute(new StringCallback() {
                                    @Override
                                    public void onSuccess(Response<String> response) {
                                        Log.i("test", "onSuccess  " + response.message());
                                        Toast.makeText(ValidationPwdActivity.this, "新密码设置成功", Toast.LENGTH_SHORT).show();
                                        finish();
                                    }

                                    @Override
                                    public void onError(Response<String> response) {
                                        super.onError(response);
                                        Log.i("test", "onError  " + response.message());
                                    }
                                });
                    } else {
                        Toast.makeText(ValidationPwdActivity.this, "两次密码不一样", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(ValidationPwdActivity.this, "请输入6~16个字符", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
