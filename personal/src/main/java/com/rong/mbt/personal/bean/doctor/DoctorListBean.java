package com.rong.mbt.personal.bean.doctor;

import java.io.Serializable;

public class DoctorListBean implements Serializable {

    /**
     * user : {"belongcode":"wQ8iv4jR","channelid":0,"coinstatus":1,"coldmoney":"0","creattime":"2019-07-24 14:52:58","fetchmoney":"0","headimage":"https://hh-liululu.oss-cn-beijing.aliyuncs.com/default/default-ys001.jpg","id":83,"investmoney":"0","inviterid":0,"level":0,"loginnum":0,"logintime":"1563951178","name":"璐璐","passwd":"662919d207123aa79147b5fc3d54831f","phone":"13867017397","rapnum":"0","registermoney":"10","remark":"","returnmoney":"0","sex":1,"status":1,"summoney":"10","token":"Q10EPKSun1GUJvcJK0m291GFpQlNjXOfWZ2l03ptw05nufSd7dM0Jg2zldlm5QISwQ9V9TT5w1kHloUAU5sRyw==","type":1,"waitactivemoney":"0"}
     * hospital : {"address":"北京市中心南路101号","content":"优秀的医院优秀的医院优秀的医院优秀的医院","creattime":"2019-07-24 14:31:54","id":1,"level":5,"name":"第一医院","person":"刘医生","phone":"13867015689","status":1,"summary":"优秀的医院"}
     * doctor : {"certifiedImg":"https://hh-liululu.oss-cn-beijing.aliyuncs.com/zy1.jpg","creattime":1566885162000,"doctorTitle":2,"headerimg":"https://hh-liululu.oss-cn-beijing.aliyuncs.com/default/default-ys001.jpg","hospitalId":1,"id":82,"idCardBackImg":"https://hh-liululu.oss-cn-beijing.aliyuncs.com/sfz/sfzb0.jpeg","idCardFrontImg":"https://hh-liululu.oss-cn-beijing.aliyuncs.com/sfz/sfz1.jpg","idCardNo":"362323199011070056","level":5,"name":"王医生","officesId":1,"remark":"","status":1,"summary":"擅长口腔各种症状","userId":83}
     */

    private UserBean user;
    private HospitalBean hospital;
    private DoctorBean doctor;

    public UserBean getUser() {
        return user;
    }

    public void setUser(UserBean user) {
        this.user = user;
    }

    public HospitalBean getHospital() {
        return hospital;
    }

    public void setHospital(HospitalBean hospital) {
        this.hospital = hospital;
    }

    public DoctorBean getDoctor() {
        return doctor;
    }

    public void setDoctor(DoctorBean doctor) {
        this.doctor = doctor;
    }

    public static class UserBean implements Serializable{
        /**
         * belongcode : wQ8iv4jR
         * channelid : 0
         * coinstatus : 1
         * coldmoney : 0
         * creattime : 2019-07-24 14:52:58
         * fetchmoney : 0
         * headimage : https://hh-liululu.oss-cn-beijing.aliyuncs.com/default/default-ys001.jpg
         * id : 83
         * investmoney : 0
         * inviterid : 0
         * level : 0
         * loginnum : 0
         * logintime : 1563951178
         * name : 璐璐
         * passwd : 662919d207123aa79147b5fc3d54831f
         * phone : 13867017397
         * rapnum : 0
         * registermoney : 10
         * remark :
         * returnmoney : 0
         * sex : 1
         * status : 1
         * summoney : 10
         * token : Q10EPKSun1GUJvcJK0m291GFpQlNjXOfWZ2l03ptw05nufSd7dM0Jg2zldlm5QISwQ9V9TT5w1kHloUAU5sRyw==
         * type : 1
         * waitactivemoney : 0
         */

        private String belongcode;
        private int channelid;
        private int coinstatus;
        private String coldmoney;
        private String creattime;
        private String fetchmoney;
        private String headimage;
        private int id;
        private String investmoney;
        private int inviterid;
        private int level;
        private int loginnum;
        private String logintime;
        private String name;
        private String passwd;
        private String phone;
        private String rapnum;
        private String registermoney;
        private String remark;
        private String returnmoney;
        private int sex;
        private int status;
        private String summoney;
        private String token;
        private int type;
        private String waitactivemoney;

        public String getBelongcode() {
            return belongcode;
        }

        public void setBelongcode(String belongcode) {
            this.belongcode = belongcode;
        }

        public int getChannelid() {
            return channelid;
        }

        public void setChannelid(int channelid) {
            this.channelid = channelid;
        }

        public int getCoinstatus() {
            return coinstatus;
        }

        public void setCoinstatus(int coinstatus) {
            this.coinstatus = coinstatus;
        }

        public String getColdmoney() {
            return coldmoney;
        }

        public void setColdmoney(String coldmoney) {
            this.coldmoney = coldmoney;
        }

        public String getCreattime() {
            return creattime;
        }

        public void setCreattime(String creattime) {
            this.creattime = creattime;
        }

        public String getFetchmoney() {
            return fetchmoney;
        }

        public void setFetchmoney(String fetchmoney) {
            this.fetchmoney = fetchmoney;
        }

        public String getHeadimage() {
            return headimage;
        }

        public void setHeadimage(String headimage) {
            this.headimage = headimage;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getInvestmoney() {
            return investmoney;
        }

        public void setInvestmoney(String investmoney) {
            this.investmoney = investmoney;
        }

        public int getInviterid() {
            return inviterid;
        }

        public void setInviterid(int inviterid) {
            this.inviterid = inviterid;
        }

        public int getLevel() {
            return level;
        }

        public void setLevel(int level) {
            this.level = level;
        }

        public int getLoginnum() {
            return loginnum;
        }

        public void setLoginnum(int loginnum) {
            this.loginnum = loginnum;
        }

        public String getLogintime() {
            return logintime;
        }

        public void setLogintime(String logintime) {
            this.logintime = logintime;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getPasswd() {
            return passwd;
        }

        public void setPasswd(String passwd) {
            this.passwd = passwd;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getRapnum() {
            return rapnum;
        }

        public void setRapnum(String rapnum) {
            this.rapnum = rapnum;
        }

        public String getRegistermoney() {
            return registermoney;
        }

        public void setRegistermoney(String registermoney) {
            this.registermoney = registermoney;
        }

        public String getRemark() {
            return remark;
        }

        public void setRemark(String remark) {
            this.remark = remark;
        }

        public String getReturnmoney() {
            return returnmoney;
        }

        public void setReturnmoney(String returnmoney) {
            this.returnmoney = returnmoney;
        }

        public int getSex() {
            return sex;
        }

        public void setSex(int sex) {
            this.sex = sex;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public String getSummoney() {
            return summoney;
        }

        public void setSummoney(String summoney) {
            this.summoney = summoney;
        }

        public String getToken() {
            return token;
        }

        public void setToken(String token) {
            this.token = token;
        }

        public int getType() {
            return type;
        }

        public void setType(int type) {
            this.type = type;
        }

        public String getWaitactivemoney() {
            return waitactivemoney;
        }

        public void setWaitactivemoney(String waitactivemoney) {
            this.waitactivemoney = waitactivemoney;
        }
    }

    public static class HospitalBean implements Serializable{
        /**
         * address : 北京市中心南路101号
         * content : 优秀的医院优秀的医院优秀的医院优秀的医院
         * creattime : 2019-07-24 14:31:54
         * id : 1
         * level : 5
         * name : 第一医院
         * person : 刘医生
         * phone : 13867015689
         * status : 1
         * summary : 优秀的医院
         */

        private String address;
        private String content;
        private String creattime;
        private int id;
        private int level;
        private String name;
        private String person;
        private String phone;
        private int status;
        private String summary;

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public String getCreattime() {
            return creattime;
        }

        public void setCreattime(String creattime) {
            this.creattime = creattime;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getLevel() {
            return level;
        }

        public void setLevel(int level) {
            this.level = level;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getPerson() {
            return person;
        }

        public void setPerson(String person) {
            this.person = person;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public String getSummary() {
            return summary;
        }

        public void setSummary(String summary) {
            this.summary = summary;
        }
    }

    public static class DoctorBean implements Serializable{
        /**
         * certifiedImg : https://hh-liululu.oss-cn-beijing.aliyuncs.com/zy1.jpg
         * creattime : 1566885162000
         * doctorTitle : 2
         * headerimg : https://hh-liululu.oss-cn-beijing.aliyuncs.com/default/default-ys001.jpg
         * hospitalId : 1
         * id : 82
         * idCardBackImg : https://hh-liululu.oss-cn-beijing.aliyuncs.com/sfz/sfzb0.jpeg
         * idCardFrontImg : https://hh-liululu.oss-cn-beijing.aliyuncs.com/sfz/sfz1.jpg
         * idCardNo : 362323199011070056
         * level : 5
         * name : 王医生
         * officesId : 1
         * remark :
         * status : 1
         * summary : 擅长口腔各种症状
         * userId : 83
         */

        private String certifiedImg;
        private long creattime;
        private int doctorTitle;
        private String headerimg;
        private int hospitalId;
        private int id;
        private String idCardBackImg;
        private String idCardFrontImg;
        private String idCardNo;
        private int level;
        private String name;
        private int officesId;
        private String remark;
        private int status;
        private String summary;
        private int userId;

        public String getCertifiedImg() {
            return certifiedImg;
        }

        public void setCertifiedImg(String certifiedImg) {
            this.certifiedImg = certifiedImg;
        }

        public long getCreattime() {
            return creattime;
        }

        public void setCreattime(long creattime) {
            this.creattime = creattime;
        }

        public int getDoctorTitle() {
            return doctorTitle;
        }

        public void setDoctorTitle(int doctorTitle) {
            this.doctorTitle = doctorTitle;
        }

        public String getHeaderimg() {
            return headerimg;
        }

        public void setHeaderimg(String headerimg) {
            this.headerimg = headerimg;
        }

        public int getHospitalId() {
            return hospitalId;
        }

        public void setHospitalId(int hospitalId) {
            this.hospitalId = hospitalId;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getIdCardBackImg() {
            return idCardBackImg;
        }

        public void setIdCardBackImg(String idCardBackImg) {
            this.idCardBackImg = idCardBackImg;
        }

        public String getIdCardFrontImg() {
            return idCardFrontImg;
        }

        public void setIdCardFrontImg(String idCardFrontImg) {
            this.idCardFrontImg = idCardFrontImg;
        }

        public String getIdCardNo() {
            return idCardNo;
        }

        public void setIdCardNo(String idCardNo) {
            this.idCardNo = idCardNo;
        }

        public int getLevel() {
            return level;
        }

        public void setLevel(int level) {
            this.level = level;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getOfficesId() {
            return officesId;
        }

        public void setOfficesId(int officesId) {
            this.officesId = officesId;
        }

        public String getRemark() {
            return remark;
        }

        public void setRemark(String remark) {
            this.remark = remark;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public String getSummary() {
            return summary;
        }

        public void setSummary(String summary) {
            this.summary = summary;
        }

        public int getUserId() {
            return userId;
        }

        public void setUserId(int userId) {
            this.userId = userId;
        }
    }
}
