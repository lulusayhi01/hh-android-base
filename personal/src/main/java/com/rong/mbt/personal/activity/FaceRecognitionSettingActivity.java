package com.rong.mbt.personal.activity;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.rong.mbt.personal.R;
import com.rong.mbt.personal.activity.base.PBaseActivity;
import com.rong.mbt.personal.dialog.CustomerServiceDialog;
import com.vise.xsnow.permission.OnPermissionCallback;
import com.vise.xsnow.permission.PermissionManager;

public class FaceRecognitionSettingActivity extends PBaseActivity {


    private Switch mSwitch;
    private Context mContext;
    private String mbtToken;
    private boolean gestureIsOpen; //手势是否打开
    private SharedPreferences.Editor edit;
    private boolean faceIsOpen;

    @Override
    public int getLayoutId() {
        return R.layout.activity_face_recognition_setting;
    }

    @Override
    public void initView() {
        super.initView();
        this.mContext = this;

        SharedPreferences sp = getSharedPreferences("config", MODE_PRIVATE);
        edit = sp.edit();
        gestureIsOpen = sp.getBoolean("GestureIsOpen", false);

        faceIsOpen = sp.getBoolean("faceIsOpen", false);

        mbtToken = getIntent().getStringExtra("mbtToken");
        find();
        listener();
        initPermission();
    }

    private void initPermission() {
        findViewById(R.id.p_frs_line).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    PermissionManager.instance().with((Activity) mContext).request(new OnPermissionCallback() {
                        @Override
                        public void onRequestAllow(String permissionName) {
                            Intent intent = new Intent(mContext, FaceRecognitionActivity.class);
                            intent.putExtra("mbtToken", mbtToken);
                            startActivity(intent);
                        }

                        @Override
                        public void onRequestRefuse(String permissionName) {
                            Toast.makeText(mContext, "权限申请被拒绝，请重试！", Toast.LENGTH_SHORT).show();
                        }

                        @Override
                        public void onRequestNoAsk(String permissionName) {
                            Toast.makeText(mContext, "权限申请被拒绝且不再询问，请进入设置打开权限再试！", Toast.LENGTH_SHORT).show();
                        }
                    }, Manifest.permission.CAMERA);
                } else {
                    Intent intent = new Intent(mContext, FaceRecognitionActivity.class);
                    intent.putExtra("mbtToken", mbtToken);
                    startActivity(intent);
                }
            }
        });

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            PermissionManager.instance().with((Activity) mContext).request(new OnPermissionCallback() {
                @Override
                public void onRequestAllow(String permissionName) {
                }

                @Override
                public void onRequestRefuse(String permissionName) {
                    Toast.makeText(mContext, "权限申请被拒绝，请重试！", Toast.LENGTH_SHORT).show();
                    finish();
                }

                @Override
                public void onRequestNoAsk(String permissionName) {
                    Toast.makeText(mContext, "权限申请被拒绝且不再询问，请进入设置打开权限再试！", Toast.LENGTH_SHORT).show();
                    finish();
                }
            }, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
    }

    private void listener() {


        findViewById(R.id.head_while_iv_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        findViewById(R.id.head_while_iv_right).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CustomerServiceDialog customerServiceDialog = new CustomerServiceDialog(FaceRecognitionSettingActivity.this);
                customerServiceDialog.show();
            }
        });

        mSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (gestureIsOpen) {
                    Toast.makeText(mContext, "请先关闭手势解锁", Toast.LENGTH_SHORT).show();
                    mSwitch.setChecked(false);
                    return;
                }
                edit.putBoolean("faceIsOpen", b);
                edit.commit();

            }
        });
    }

    private void find() {
        mSwitch = findViewById(R.id.act_frs_switch_isopen);
        mSwitch.setChecked(faceIsOpen);
        TextView tvTitle = findViewById(R.id.head_while_tv_center);
        tvTitle.setText("人脸识别");
    }
}
