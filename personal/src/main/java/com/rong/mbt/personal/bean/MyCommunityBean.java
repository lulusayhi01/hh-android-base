package com.rong.mbt.personal.bean;

import java.util.List;

public class MyCommunityBean {

    /**
     * data : {"parent":{"creattime":"2018-07-12 17:22:25","headimage":"http://oss-aliyun-liululu.oss-cn-shanghai.aliyuncs.com/upload/153389023664010270.jpg","id":25,"machineCount":0,"name":"涓涓","phone":"13501031194","trendsImg":[]},"children":[{"children":[],"childrenCount":0,"creattime":"2018-08-02 14:11:57","headimage":"http://p6nlllxof.bkt.clouddn.com/upload/20180705/3f668359c2354f2298f978c6266b560d.jpg","id":37,"machineCount":1,"name":"13383305056","phone":"13383305056","trendsImg":[]}]}
     * msg : 请求成功
     * resultCode : 0
     */

    private DataBean data;
    private String msg;
    private int resultCode;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public int getResultCode() {
        return resultCode;
    }

    public void setResultCode(int resultCode) {
        this.resultCode = resultCode;
    }

    public static class DataBean {
        /**
         * parent : {"creattime":"2018-07-12 17:22:25","headimage":"http://oss-aliyun-liululu.oss-cn-shanghai.aliyuncs.com/upload/153389023664010270.jpg","id":25,"machineCount":0,"name":"涓涓","phone":"13501031194","trendsImg":[]}
         * children : [{"children":[],"childrenCount":0,"creattime":"2018-08-02 14:11:57","headimage":"http://p6nlllxof.bkt.clouddn.com/upload/20180705/3f668359c2354f2298f978c6266b560d.jpg","id":37,"machineCount":1,"name":"13383305056","phone":"13383305056","trendsImg":[]}]
         */

        private ParentBean parent;
        private List<ChildrenBean> children;

        public ParentBean getParent() {
            return parent;
        }

        public void setParent(ParentBean parent) {
            this.parent = parent;
        }

        public List<ChildrenBean> getChildren() {
            return children;
        }

        public void setChildren(List<ChildrenBean> children) {
            this.children = children;
        }

        public static class ParentBean {
            /**
             * creattime : 2018-07-12 17:22:25
             * headimage : http://oss-aliyun-liululu.oss-cn-shanghai.aliyuncs.com/upload/153389023664010270.jpg
             * id : 25
             * machineCount : 0
             * name : 涓涓
             * phone : 13501031194
             * trendsImg : []
             */

            private String creattime;
            private String headimage;
            private int id;
            private int machineCount;
            private String name;
            private String phone;
            private List<?> trendsImg;

            public String getCreattime() {
                return creattime;
            }

            public void setCreattime(String creattime) {
                this.creattime = creattime;
            }

            public String getHeadimage() {
                return headimage;
            }

            public void setHeadimage(String headimage) {
                this.headimage = headimage;
            }

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public int getMachineCount() {
                return machineCount;
            }

            public void setMachineCount(int machineCount) {
                this.machineCount = machineCount;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getPhone() {
                return phone;
            }

            public void setPhone(String phone) {
                this.phone = phone;
            }

            public List<?> getTrendsImg() {
                return trendsImg;
            }

            public void setTrendsImg(List<?> trendsImg) {
                this.trendsImg = trendsImg;
            }
        }

        public static class ChildrenBean {
            /**
             * children : []
             * childrenCount : 0
             * creattime : 2018-08-02 14:11:57
             * headimage : http://p6nlllxof.bkt.clouddn.com/upload/20180705/3f668359c2354f2298f978c6266b560d.jpg
             * id : 37
             * machineCount : 1
             * name : 13383305056
             * phone : 13383305056
             * trendsImg : []
             */

            private int childrenCount;
            private String creattime;
            private String headimage;
            private int id;
            private int machineCount;
            private String name;
            private String phone;
            private List<?> children;
            private List<?> trendsImg;
            private String friendRemark;


            public String getFriendRemark() {
                return friendRemark;
            }

            public void setFriendRemark(String friendRemark) {
                this.friendRemark = friendRemark;
            }

            public int getChildrenCount() {
                return childrenCount;
            }

            public void setChildrenCount(int childrenCount) {
                this.childrenCount = childrenCount;
            }

            public String getCreattime() {
                return creattime;
            }

            public void setCreattime(String creattime) {
                this.creattime = creattime;
            }

            public String getHeadimage() {
                return headimage;
            }

            public void setHeadimage(String headimage) {
                this.headimage = headimage;
            }

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public int getMachineCount() {
                return machineCount;
            }

            public void setMachineCount(int machineCount) {
                this.machineCount = machineCount;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getPhone() {
                return phone;
            }

            public void setPhone(String phone) {
                this.phone = phone;
            }

            public List<?> getChildren() {
                return children;
            }

            public void setChildren(List<?> children) {
                this.children = children;
            }

            public List<?> getTrendsImg() {
                return trendsImg;
            }

            public void setTrendsImg(List<?> trendsImg) {
                this.trendsImg = trendsImg;
            }
        }
    }
}
