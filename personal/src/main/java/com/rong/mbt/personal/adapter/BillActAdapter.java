package com.rong.mbt.personal.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.rong.mbt.personal.R;
import com.rong.mbt.personal.bean.BillBean;
import com.rong.mbt.personal.utils.ImageUtil;
import com.rong.mbt.personal.utils.StringUtil;

import java.util.ArrayList;
import java.util.List;


public class BillActAdapter extends RecyclerView.Adapter {
    private Context context;
    private List<BillBean.DataBean> data = new ArrayList<>();

    public BillActAdapter(Context context) {
        this.context = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_bill_item, parent, false);
        return new BillActAdapterHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        BillActAdapterHolder h = (BillActAdapterHolder) holder;
        BillBean.DataBean dataBean = data.get(position);
        ImageUtil.loadCircleImage(context, dataBean.getUserHeadImage(), h.iv);
        h.tvPhone.setText(dataBean.getUserName());
        h.tvMoney.setText(StringUtil.formatString(dataBean.getPrice()));
        h.tvDate.setText(dataBean.getCreatetime());

        switch (dataBean.getDealstatus()) {
            case 4201:
                h.tvInfo.setText("交易中");
                break;
            case 4202:
                h.tvInfo.setText("交易成功");
                break;
            case 4203:
                h.tvInfo.setText("交易失败");
                break;
        }

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public void setData(List<BillBean.DataBean> data) {
        this.data.addAll(data);
        notifyDataSetChanged();
    }

    class BillActAdapterHolder extends RecyclerView.ViewHolder {
        private ImageView iv;
        private TextView tvPhone;
        private TextView tvDate;
        private TextView tvMoney;
        private TextView tvInfo;

        public BillActAdapterHolder(View itemView) {
            super(itemView);
            iv = itemView.findViewById(R.id.item_ibi_iv);
            tvPhone = itemView.findViewById(R.id.item_ibi_tv_phone);
            tvMoney = itemView.findViewById(R.id.item_ibi_tv_money);
            tvDate = itemView.findViewById(R.id.item_ibi_tv_date);
            tvInfo = itemView.findViewById(R.id.item_ibi_tv_info);
        }
    }
}
