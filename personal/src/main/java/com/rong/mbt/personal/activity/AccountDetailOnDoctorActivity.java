package com.rong.mbt.personal.activity;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.fastjson.JSONObject;
import com.calanger.hh.entity.ResultView;
import com.calanger.hh.model.Order;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.model.Response;
import com.rong.mbt.personal.R;
import com.rong.mbt.personal.activity.base.PBaseActivity;
import com.rong.mbt.personal.adapter.OrderListAdapter;
import com.rong.mbt.personal.http.UrlApi;
import com.rong.mbt.personal.http.callback.JsonCallback;
import com.rong.mbt.personal.utils.SpUtils;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 账户详情-医生模式
 */
public class AccountDetailOnDoctorActivity extends PBaseActivity {

    private OrderListAdapter orderListAdapter;
    private String TAG = "AccountDetailOnDoctorActivity";
    private int pageNum = 1;
    int TOTAL_COUNTER = 0;
    private boolean freshOrMore = false;
    List<Map<String,Object>> list = new ArrayList<Map<String,Object>>();
    private int doctorId;

    @Override
    public int getLayoutId() {
        return R.layout.activity_recycle_view_order;
    }


    @Override
    public void initView() {
        super.initView();

        doctorId = getIntent().getIntExtra("doctorId",0);

        RefreshLayout mRefreshLayout = findViewById(R.id.refreshLayout);
        mRefreshLayout.setOnRefreshListener(new OnRefreshListener() {

            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {
                pageNum = 1;
                initnet(pageNum);
                mRefreshLayout.finishRefresh();
            }
        });

        mRefreshLayout.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore(@NonNull RefreshLayout refreshLayout) {
                if(list.size()>= TOTAL_COUNTER){
//                数据全部加载完毕
                    mRefreshLayout.finishLoadMore();
                }else{
                    freshOrMore = true;
                    pageNum++;
                    initnet(pageNum);
                    mRefreshLayout.finishLoadMore();
                }
            }
        });



        RecyclerView recyclerView = findViewById(R.id.recycler_view_order);
        TextView textTitle = findViewById(R.id.header_center_text);
        textTitle.setText("健康账户");

        LinearLayout back = findViewById(R.id.ll_back);
        back.setOnClickListener(v -> finish());

        LinearLayout right_ll = findViewById(R.id.right_ll);
        right_ll.setVisibility(View.GONE);

        recyclerView.setLayoutManager(new GridLayoutManager(getApplication(), 1));
        orderListAdapter = new OrderListAdapter();
        orderListAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                Map<String,Object> item = (Map<String,Object>) adapter.getItem(position);
                JSONObject jsonMap = new JSONObject(item);
                Order order = jsonMap.getObject("order",Order.class);
                //            (1:订单 2：咨询 3：文章)
                if(order.getProductType()==1){
                    startActivity(new Intent(AccountDetailOnDoctorActivity.this, PatientDetailActivity.class).putExtra("id",order.getProductid()));
                }else if(order.getProductType()==2){
                    startActivity(new Intent(AccountDetailOnDoctorActivity.this, DoctorDetailsActivity.class).putExtra("id", order.getObjId()));
                }else if(order.getProductType()==3){
                    startActivity(new Intent(AccountDetailOnDoctorActivity.this, ArticleDetailActivity.class).putExtra("id", order.getProductid()));
                }else{
                    Toast.makeText(AccountDetailOnDoctorActivity.this, "类型错误", Toast.LENGTH_SHORT);
                }
            }

        });

        recyclerView.setAdapter(orderListAdapter);




    }

    @Override
    protected void onStart() {
        super.onStart();
        pageNum = 1;
        initnet(pageNum);
    }

    private void initnet(int pageNum) {
        OkGo.<ResultView>get(UrlApi.orderList)
                .tag(this)
                .headers("mbtToken", SpUtils.getToken(getApplication()))
                .params("objId",doctorId)
                .params("objType",1)
                .params("pageNumber",pageNum)
                .execute(new JsonCallback<ResultView>() {
                    @Override
                    public void onSuccess(Response<ResultView> response) {
                        if (response != null && response.body() != null) {
                            ResultView resultView = response.body();
                            if (resultView.getData() != null) {
                                TOTAL_COUNTER = resultView.getCount();
                                List<Map<String,Object>> resultList = (List<Map<String, Object>>) resultView.getData();
                                if(freshOrMore){
                                    list.addAll(resultList);
                                    freshOrMore = false;
                                }else{
                                    list = resultList;
                                }
                                orderListAdapter.setNewData(list);
                            }
                        }
                    }

                    @Override
                    public void onError(Response<ResultView> response) {
                        super.onError(response);
                        Log.i("test", "onError   " + response.body());
                    }
                });
    }

}
