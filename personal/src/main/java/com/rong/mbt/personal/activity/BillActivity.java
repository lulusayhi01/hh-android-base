package com.rong.mbt.personal.activity;


import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.alibaba.fastjson.JSON;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.callback.StringCallback;
import com.lzy.okgo.model.Response;
import com.rong.mbt.personal.R;
import com.rong.mbt.personal.activity.base.PBaseActivity;
import com.rong.mbt.personal.adapter.BillActAdapter;
import com.rong.mbt.personal.bean.BillBean;

public class BillActivity extends PBaseActivity {


    private String mbtToken;
    private RecyclerView mRecycler;
    private LinearLayoutManager manager;
    private BillActAdapter adapter;

    private boolean loading;
    private int PAGENUM = 1;
    private int PAGESIZE = 20;

    @Override
    public int getLayoutId() {
        return R.layout.activity_bill;
    }

    @Override
    public void initView() {
        super.initView();
        mbtToken = getIntent().getStringExtra("mbtToken");

        init();
        net();
        listener();

    }

    private void listener() {
        findViewById(R.id.head_blue_iv_left).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        //加载
        mRecycler.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0) {
                    int visibleItemCount = manager.getChildCount();
                    int totalItemCount = manager.getItemCount();
                    int pastVisiblesItems = manager.findFirstVisibleItemPosition();

                    if (!loading && (visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                        loading = !loading;
                        PAGENUM++;
                        net();
                    }
                }
            }
        });
    }

    //        "dealstatus":4201,
//        "dealtype":4102,
//        "id":95,
//        "price":200,
//        "status":1,
//        "tuserid":36,
//        "typedetailid":4304,
    //typedetailid     4301交易 4302佣金 4303 提现 4304 充值 4305 购买矿机
    private void net() {
        //http://service.jiangaifen.com:38082/userDeal/listByPage 交易记录
        //http://service.jiangaifen.com:38082/user/listDealByPage  账单
        OkGo.<String>post("http://service.jiangaifen.com:38082/userDeal/listByPage")
                .tag(this)
                .headers("mbtToken", mbtToken)
                .params("pageNum", PAGENUM)
                .params("pageSize", PAGESIZE)
                .execute(new StringCallback() {
                    @Override
                    public void onSuccess(Response<String> response) {
                        Log.i("test", "onSuccess  " + response.body());
                        BillBean billBean = JSON.parseObject(response.body(), BillBean.class);
                        if (billBean.getResultCode() == 0 && !billBean.getData().isEmpty()) {
                            adapter.setData(billBean.getData());
                            loading = false;
                        }
                    }

                    @Override
                    public void onError(Response<String> response) {
                        super.onError(response);
                        Log.i("test", "onError  " + response.body());
                    }
                });
    }

    private void init() {
        TextView tvTitle = findViewById(R.id.head_blue_tv_center);
        tvTitle.setText("账单");
        findViewById(R.id.head_blue_iv_right).setVisibility(View.GONE);

        mRecycler = findViewById(R.id.act_b_recycler);
        manager = new LinearLayoutManager(this);
        mRecycler.setLayoutManager(manager);
        adapter = new BillActAdapter(this);
        mRecycler.setAdapter(adapter);
    }
}
