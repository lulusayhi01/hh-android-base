package com.rong.mbt.personal.bean.order;

import java.io.Serializable;
import java.util.List;

public class PushOrderFeedBean implements Serializable {


    /**
     * data : {"list":[{"creattime":1567687058000,"id":2,"parentId":1,"status":1,"value":"2"},{"creattime":1567687059000,"id":3,"parentId":1,"status":1,"value":"4"},{"creattime":1567687062000,"id":4,"parentId":1,"status":1,"value":"6"}],"dictionary":{"creattime":1567687055000,"id":1,"name":"baseorderfeed","status":1}}
     * msg : 请求成功
     * resultCode : 0
     */

    private DataBean data;
    private String msg;
    private int resultCode;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public int getResultCode() {
        return resultCode;
    }

    public void setResultCode(int resultCode) {
        this.resultCode = resultCode;
    }

    public static class DataBean {
        /**
         * list : [{"creattime":1567687058000,"id":2,"parentId":1,"status":1,"value":"2"},{"creattime":1567687059000,"id":3,"parentId":1,"status":1,"value":"4"},{"creattime":1567687062000,"id":4,"parentId":1,"status":1,"value":"6"}]
         * dictionary : {"creattime":1567687055000,"id":1,"name":"baseorderfeed","status":1}
         */

        private DictionaryBean dictionary;
        private List<ListBean> list;

        public DictionaryBean getDictionary() {
            return dictionary;
        }

        public void setDictionary(DictionaryBean dictionary) {
            this.dictionary = dictionary;
        }

        public List<ListBean> getList() {
            return list;
        }

        public void setList(List<ListBean> list) {
            this.list = list;
        }

        public static class DictionaryBean {
            /**
             * creattime : 1567687055000
             * id : 1
             * name : baseorderfeed
             * status : 1
             */

            private long creattime;
            private int id;
            private String name;
            private int status;

            public long getCreattime() {
                return creattime;
            }

            public void setCreattime(long creattime) {
                this.creattime = creattime;
            }

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public int getStatus() {
                return status;
            }

            public void setStatus(int status) {
                this.status = status;
            }
        }

        public static class ListBean {
            /**
             * creattime : 1567687058000
             * id : 2
             * parentId : 1
             * status : 1
             * value : 2
             */

            private long creattime;
            private int id;
            private int parentId;
            private int status;
            private String value;

            public long getCreattime() {
                return creattime;
            }

            public void setCreattime(long creattime) {
                this.creattime = creattime;
            }

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public int getParentId() {
                return parentId;
            }

            public void setParentId(int parentId) {
                this.parentId = parentId;
            }

            public int getStatus() {
                return status;
            }

            public void setStatus(int status) {
                this.status = status;
            }

            public String getValue() {
                return value;
            }

            public void setValue(String value) {
                this.value = value;
            }
        }
    }
}
