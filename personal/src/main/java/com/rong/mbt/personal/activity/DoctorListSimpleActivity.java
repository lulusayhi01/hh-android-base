package com.rong.mbt.personal.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.fastjson.JSONObject;
import com.calanger.hh.entity.ResultView;
import com.calanger.hh.model.Doctor;
import com.calanger.hh.model.Follow;
import com.calanger.hh.model.MedicalRecord;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.model.Response;
import com.rong.mbt.personal.R;
import com.rong.mbt.personal.activity.base.PBaseActivity;
import com.rong.mbt.personal.adapter.DoctorListAdapter;
import com.rong.mbt.personal.adapter.DoctorListSimpleAdapter;
import com.rong.mbt.personal.bean.doctor.DoctorIndexBean;
import com.rong.mbt.personal.bean.doctor.DoctorListBean;
import com.rong.mbt.personal.http.UrlApi;
import com.rong.mbt.personal.http.callback.JsonCallback;
import com.rong.mbt.personal.utils.SpUtils;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 医生管理-列表-Simple模式
 */
public class DoctorListSimpleActivity extends PBaseActivity {

    private DoctorListSimpleAdapter doctorListSimpleAdapter;
    private String TAG = "DoctorListSimpleActivity";
    private int pageNum = 1;
    int TOTAL_COUNTER = 0;
    private boolean freshOrMore = false;
    List<Map<String,Object>> list = new ArrayList<Map<String,Object>>();

    @Override
    public int getLayoutId() {
        return R.layout.activity_recycle_view_doctor;
    }


    @Override
    public void initView() {
        super.initView();


        RefreshLayout mRefreshLayout = findViewById(R.id.refreshLayout);
        mRefreshLayout.setOnRefreshListener(new OnRefreshListener() {

            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {
                pageNum = 1;
                initnet(pageNum);
                mRefreshLayout.finishRefresh();
            }
        });

        mRefreshLayout.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore(@NonNull RefreshLayout refreshLayout) {
                if(list.size()>= TOTAL_COUNTER){
//                数据全部加载完毕
                    mRefreshLayout.finishLoadMore();
                }else{
                    freshOrMore = true;
                    pageNum++;
                    initnet(pageNum);
                    mRefreshLayout.finishLoadMore();
                }
            }
        });

        RecyclerView recyclerView = findViewById(R.id.recycler_view_doctor);
        TextView textTitle = findViewById(R.id.header_center_text);
        textTitle.setText("关注医生");

        LinearLayout back = findViewById(R.id.ll_back);
        back.setOnClickListener(v -> finish());

        LinearLayout right_ll = findViewById(R.id.right_ll);
        right_ll.setVisibility(View.GONE);

        recyclerView.setLayoutManager(new GridLayoutManager(getApplication(), 1));
        doctorListSimpleAdapter = new DoctorListSimpleAdapter();
        doctorListSimpleAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                Map<String,Object> item = (Map<String,Object>) adapter.getItem(position);
                JSONObject jsonMap = new JSONObject(item);
                Doctor doctor = jsonMap.getObject("doctor",Doctor.class);
                startActivity(new Intent(DoctorListSimpleActivity.this,DoctorDetailsActivity.class).putExtra("id",doctor.getId()));
            }
        });

        doctorListSimpleAdapter.setOnItemChildClickListener(new BaseQuickAdapter.OnItemChildClickListener() {
            @Override
            public void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {
                Map<String,Object> dataBean = (Map<String, Object>) adapter.getItem(position);
                JSONObject jsonMap = new JSONObject(dataBean);
                Follow follow =  jsonMap.getObject("follow", Follow.class);
                if(view.getId()==R.id.tv_follow_doctor){
                    OkGo.<ResultView>get(UrlApi.followCancel)
                            .tag(this)
                            .headers("mbtToken", SpUtils.getToken(getApplication()))
                            .params("followId",follow.getFollowId())//被关注的医生的id
                            .params("objType",1)//关注对象的类型 码值1:医生 2：文章
                            .execute(new JsonCallback<ResultView>() {
                                @Override
                                public void onSuccess(Response<ResultView> response) {
                                    if (response != null && response.body() != null) {
                                        ResultView resultView = response.body();
                                        if(resultView.getResultCode()==0){
                                            doctorListSimpleAdapter.remove(position);
                                        }
                                    }
                                }
                            });
                }


            }
        });

        recyclerView.setAdapter(doctorListSimpleAdapter);
    }

    @Override
    protected void onStart() {
        super.onStart();
        pageNum = 1;
        initnet(pageNum);
    }

    private void initnet(int pageNum) {
        OkGo.<ResultView>get(UrlApi.followList)
                .tag(this)
                .headers("mbtToken", SpUtils.getToken(getApplication()))
//                .params("userId",SpUtils.getUserId(this))
                .params("objType",1)
                .params("pageNumber",pageNum)
                .execute(new JsonCallback<ResultView>() {
                    @Override
                    public void onSuccess(Response<ResultView> response) {
                        if (response != null && response.body() != null) {
                            ResultView resultView = response.body();
                            if (resultView.getData() != null) {
                                TOTAL_COUNTER = resultView.getCount();
                                List<Map<String,Object>> resultList = (List<Map<String, Object>>) resultView.getData();
                                if(freshOrMore){
                                    list.addAll(resultList);
                                    freshOrMore = false;
                                }else{
                                    list = resultList;
                                }
                                doctorListSimpleAdapter.setNewData(list);
                            }
                        }
                    }

                    @Override
                    public void onError(Response<ResultView> response) {
                        super.onError(response);
                        Log.i("test", "onError   " + response.body());
                    }
                });
    }

}
