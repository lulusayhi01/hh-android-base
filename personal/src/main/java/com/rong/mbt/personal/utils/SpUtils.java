package com.rong.mbt.personal.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;


public class SpUtils {

    /**
     * 获取用户token
     *
     * @param context
     * @return
     */
    public static String getToken(Context context) {
        SharedPreferences config = getSharePerferences(context);
        return config.getString("mbtToken", "");
    }

    public static int getUserId(Context context) {
        SharedPreferences config = getSharePerferences(context);
        return config.getInt("userId", 0);
    }

    public static int getDoctorId(Context context) {
        SharedPreferences config = getSharePerferences(context);
        return config.getInt("doctorId", 0);
    }

    public static int getUserType(Context context) {
        SharedPreferences config = getSharePerferences(context);
        return Integer.valueOf(config.getString("type","0"));
    }


    public static SharedPreferences getSharePerferences(Context context) {
        return context.getSharedPreferences("config", Context.MODE_PRIVATE);
    }
}
