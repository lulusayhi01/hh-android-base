package com.rong.mbt.personal.utils;

import android.content.Context;
import android.text.TextUtils;

import com.alibaba.fastjson.JSON;

import java.text.DecimalFormat;
import java.util.regex.Pattern;

public class StringUtil {


    /**
     * 判断给定字符串是否空白串。 包括由空格、制表符、回车符、换行符组成的字符串
     */
    public static boolean isEmpty(String text) {
        if (TextUtils.isEmpty(text))
            return true;

        for (int i = 0; i < text.length(); i++) {
            char c = text.charAt(i);
            if (c != ' ' && c != '\t' && c != '\r' && c != '\n') {
                return false;
            }
        }
        return true;
    }

    /**
     * 去除换行制表等空格
     */
    public static String replaceBlank(String text) {
        if (TextUtils.isEmpty(text)) {
            return text;
        }

        return Pattern.compile("\\s*|\t|\r|\n").matcher(text).replaceAll("");
    }

    public static String getString(Context context,int resId) {
        try {
            return context.getString(resId);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public static String getLocalLanguage(Context context) {
        return context.getResources().getConfiguration().locale.getLanguage() + "-"
                + context.getResources().getConfiguration().locale.getCountry();
    }

    public static String toJson(Object object) {
        return JSON.toJSONString(object);
    }

    public static String phone_star(String phone) {
        char[] chars = phone.toCharArray();
        for (int i = 0; i < chars.length; i++) {
            if (i > 2 && i < 7) {
                chars[i] = '*';
            }
        }
        return String.valueOf(chars);
    }

    public static String formatString_no(float data) {
        DecimalFormat df = new DecimalFormat("####.00");
        if (df.format(data).equals(".00")) {
            return "0.00";
        } else {
            if (df.format(data).startsWith(".")) {
                return "0" + df.format(data);
            }
            return df.format(data);
        }
    }

    public static String formatString_no(double data) {
        DecimalFormat df = new DecimalFormat("####.00");
        if (df.format(data).equals(".00")) {
            return "0.00";
        } else {
            if (df.format(data).startsWith(".")) {
                return "0" + df.format(data);
            }
            return df.format(data);
        }
    }

    public static String formatString(float data) {
        DecimalFormat df = new DecimalFormat("#,###.00");
        if (df.format(data).equals(".00")) {
            return "0.00";
        } else {
            if (df.format(data).startsWith(".")) {
                return "0" + df.format(data);
            }
            return df.format(data);
        }
    }

    public static String formatString(double data) {
        DecimalFormat df = new DecimalFormat("####.00");
        if (df.format(data).equals(".00")) {
            return "0.00";
        } else {
            if (df.format(data).startsWith(".")) {
                return "0" + df.format(data);
            }
            return df.format(data);
        }
    }

    public static String formatString(String data) {
        DecimalFormat df = new DecimalFormat("#,###.00");
        if (df.format(data).equals(".00")) {
            return "0.00";
        } else {
            if (df.format(data).startsWith(".")) {
                return "0" + df.format(data);
            }
            return df.format(data);
        }
    }

    public static double removeZero_double(double money) {

        String string;
        String s = String.valueOf(money);
        if (s.endsWith(".00")) {
            string = s.replaceAll("\\.00", "");
        } else if (s.endsWith(".0")) {
            string = s.replaceAll("\\.0", "");
        } else {
            string = s;
        }
        return Double.parseDouble(string);
    }


    public static float removeZero_float(float money) {

        String string;
        String s = String.valueOf(money);
        if (s.endsWith(".00")) {
            string = s.replaceAll("\\.00", "");
        } else if (s.endsWith(".0")) {
            string = s.replaceAll("\\.0", "");
        } else {
            string = s;
        }
        return Float.parseFloat(string);
    }

    public static String removeZero(float money) {

        String string;
        String s = String.valueOf(money);
        if (s.endsWith(".00")) {
            string = s.replaceAll("\\.00", "");
        } else if (s.endsWith(".0")) {
            string = s.replaceAll("\\.0", "");
        } else {
            string = s;
        }
        return string;
    }

    public static String removeZero(double money) {

        String string;
        String s = String.valueOf(money);
        if (s.endsWith(".00")) {
            string = s.replaceAll("\\.00", "");
        } else if (s.endsWith(".0")) {
            string = s.replaceAll("\\.0", "");
        } else {
            string = s;
        }
        return string;
    }

    public static String removeZero(String money) {

        String string;
        if (money.endsWith(".00")) {
            string = money.replaceAll("\\.00", "");
        } else if (money.endsWith(".0")) {
            string = money.replaceAll("\\.0", "");
        } else {
            string = money;
        }
        return string;
    }

    public static String subStringEndFour(String string) {
        if (string == null || string.length() < 4) {
            return "";
        }
        return "(" + string.substring(string.length() - 4, string.length()) + ")";
    }

    public static String number2String(String monkey) {

        if (monkey.length() >= 5) {
            return (Integer.parseInt(monkey) / 10000) + "万";
        } else {
            return monkey;
        }
    }
}
