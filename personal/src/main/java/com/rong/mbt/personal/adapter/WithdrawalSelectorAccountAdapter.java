package com.rong.mbt.personal.adapter;

import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.rong.mbt.personal.R;
import com.rong.mbt.personal.bean.CashAccountListBean;

/**
 * Created by jianfei on 2018/9/26.
 */

public class WithdrawalSelectorAccountAdapter extends BaseQuickAdapter<CashAccountListBean.DataBean, BaseViewHolder> {
    public WithdrawalSelectorAccountAdapter() {
        super(R.layout.item_withdrawal_selector_account);
    }

    @Override
    protected void convert(BaseViewHolder helper, CashAccountListBean.DataBean item) {

        if (item.getType() == 5) {
            if (item.isSelector()) {
                helper.setImageResource(R.id.item_iwsa_iv, R.drawable.check_box_select);
            } else {
                helper.setImageResource(R.id.item_iwsa_iv, R.drawable.check_box_unselect);
            }
        }
        TextView tv = helper.getView(R.id.item_iwsa_tv);
        switch (item.getType()) {
            case 1:
                tv.setText("支付宝账号：" + item.getAccount());
                break;
            case 2:
                tv.setText("新浪账号：" + item.getAccount());
                break;
            case 3:
                tv.setText("微信账号：" + item.getAccount());
                break;
            case 4:
                tv.setText("银行卡账号：" + item.getAccount());
                break;
            case 5:
                tv.setText("比特币账号：" + item.getAccount());
                break;
            case 6:
                tv.setText("莱特币账号：" + item.getAccount());
                break;
            case 7:
                tv.setText("以太坊账号：" + item.getAccount());
                break;
            default:
                tv.setText(item.getAccount());
                break;
        }
    }
}
