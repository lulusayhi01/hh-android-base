package com.rong.mbt.personal.bean.base;

import java.io.Serializable;

public class BaseBeanVo implements Serializable {

    /**
     * count : 2
     * data : [{"orderPre":{"age":"20","content":"如果你已经下定决心, 准备学习和安装 TensorFlow, 你可以略过这些文字, 直接阅读 后面的章节. 不用担心, 你仍然会看到 MNIST -- 在阐述 TensorFlow 的特性时, 我们还会使用 MNIST 作为一个样例.","creattime":1567245699000,"feed":"3","headimg":"http://oss-aliyun-liululu.oss-cn-shanghai.aliyuncs.com/upload/156404160692410487.jpg","id":28,"name":"蓝二哥哥","orderStatus":0,"sex":1}},{"orderPre":{"age":"80","content":"放大了附近的开始了疯狂了多少疯狂了多少家疯狂了多少疯狂了倒计时份健康的零食风口浪尖的时间疯狂了多少份健康的零食飞机离开的时间疯狂了多少积分离开的房间看了多少","creattime":1567246950000,"feed":"3","headimg":"http://oss-aliyun-liululu.oss-cn-shanghai.aliyuncs.com/upload/156404160692410487.jpg","id":29,"name":"蓝二哥哥","orderStatus":0,"sex":1,"userId":82},"user":{"belongcode":"s3u5CJgt","channelid":0,"coinstatus":1,"coldmoney":"0","creattime":"2019-07-24 14:31:54","fetchmoney":"0","headimage":"https://hh-liululu.oss-cn-beijing.aliyuncs.com/default/default-ys001.jpg","id":82,"investmoney":"0","inviterid":0,"level":0,"loginnum":0,"logintime":"1563949914","name":"蓝二哥哥","passwd":"662919d207123aa79147b5fc3d54831f","phone":"13501031194","rapnum":"0","registermoney":"10","remark":"","returnmoney":"0","sex":0,"status":1,"summoney":"10","token":"1T27m3FUOURGljkNtHDiyVGFpQlNjXOfWZ2l03ptw05nufSd7dM0JuLP8awIQJPQHsl1uCyG/7wHloUAU5sRyw==","type":1,"waitactivemoney":"0"}}]
     * msg : 请求成功
     * resultCode : 0
     */

    private int count;
    private String msg;
    private int resultCode;

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public int getResultCode() {
        return resultCode;
    }

    public void setResultCode(int resultCode) {
        this.resultCode = resultCode;
    }
}
