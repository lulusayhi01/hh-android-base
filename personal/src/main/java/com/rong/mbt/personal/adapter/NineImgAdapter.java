package com.rong.mbt.personal.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.alibaba.fastjson.JSONObject;
import com.bumptech.glide.Glide;
import com.calanger.hh.model.ImgUrl;
import com.calanger.hh.model.MedicalRecord;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.rong.mbt.personal.R;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 九宫格
 */
public class NineImgAdapter extends BaseAdapter {

    private ArrayList<ImgUrl> imgUrllList;
    private LayoutInflater layoutInflater;

    public NineImgAdapter(ArrayList<ImgUrl> list, Context context){
        imgUrllList = list;
        layoutInflater = LayoutInflater.from(context);
    }


    public NineImgAdapter(Context context){
        layoutInflater = LayoutInflater.from(context);
    }


    public void setData(ArrayList<ImgUrl> list){
         imgUrllList = list;
    }

    @Override
    public int getCount() {
        return imgUrllList.size();
    }

    @Override
    public Object getItem(int position) {
        return imgUrllList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    private  Det det;
    //  把按钮回调出去
    public void delete(Det det){
        this.det=det;
    }
    public interface  Det{
        public void del(int position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if(convertView == null){
            //加载布局
            convertView =layoutInflater.inflate(R.layout.grid_item_img_show_nine,null);

            holder = new ViewHolder();
            holder.imgChannel = (ImageView)convertView.findViewById(R.id.channel_img);
            holder.decChannel = (TextView)convertView.findViewById(R.id.channel_dec);
            holder.imgClose = (ImageView)convertView.findViewById(R.id.iv_close);

            convertView.setTag(holder);
        }else{
            holder = (ViewHolder)convertView.getTag();
        }

        //设置图标和文字
        ImgUrl imgUrl = imgUrllList.get(position);
        if(imgUrl != null){
            holder.decChannel.setText(imgUrl.getRemarker());
            if("add".equalsIgnoreCase(imgUrl.getRemarker())){
                holder.imgChannel.setImageResource(R.drawable.abc_add_image);
                holder.imgClose.setVisibility(View.GONE);
            }else{
                holder.imgClose.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(det!=null) {
                            det.del(position);
                        }

                    }
                });
                Glide.with(layoutInflater.getContext()).load(imgUrl.getImgUrl()).into(holder.imgChannel);
            }
//            switch (imgUrl.getStatus()){
//                case 0:
////                    holder.imgChannel.setImageResource(R.drawable.head_dafault);
//                    break;
//                case 1:
//                    Glide.with(layoutInflater.getContext()).load(imgUrl.getImgUrl()).into(holder.imgChannel);
////                    holder.imgChannel.setImageResource(R.drawable.test);
//                    break;
//            }

            if("add".equalsIgnoreCase(imgUrl.getRemarker())&&imgUrllList.size()>9){
                convertView.setVisibility(View.GONE);
            }

        }
        return convertView;
    }

    class ViewHolder{
        ImageView imgChannel;
        ImageView imgClose;
        TextView decChannel;
    }
}