package com.rong.mbt.personal.activity;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.callback.StringCallback;
import com.lzy.okgo.model.Response;
import com.rong.mbt.personal.R;
import com.rong.mbt.personal.activity.base.PBaseActivity;
import com.rong.mbt.personal.bean.BiGoMeiYuanBean;
import com.rong.mbt.personal.bean.CopyUserBean;

public class Recharge2CopyUserActivity extends PBaseActivity {


    private String mbtToken;
    private TextView tvType;
    private TextView tvUser;
    private TextView tvMeiYuan;
    private String account;

    @Override
    public int getLayoutId() {
        return R.layout.activity_recharge2_copy_user;
    }

    @Override
    public void initView() {
        super.initView();
        mbtToken = getIntent().getStringExtra("mbtToken");
        find();

    }

    @Override
    protected void onStart() {
        super.onStart();
        net();
    }

    private void net() {
        OkGo.<String>get("http://service.jiangaifen.com:38082/userAccount/official")
                .tag(this)
                .headers("mbtToken", mbtToken)
                .params("coinid", "1")
                .execute(new StringCallback() {
                    @Override
                    public void onSuccess(Response<String> response) {
                        //{"data":{"account":"15888561823","type":6},"msg":"请求成功","resultCode":0}
                        Log.i("test", "official " + response.body());

                        CopyUserBean copyUserBean = JSON.parseObject(response.body(), CopyUserBean.class);
                        if (copyUserBean.getResultCode() == 0) {
                            switch (copyUserBean.getData().getType()) {
                                case 5:
                                    tvType.setText("比特币");
                                    break;
                                case 6:
                                    tvType.setText("莱特币");
                                    break;
                                case 7:
                                    tvType.setText("以太坊");
                                    break;
                            }
                            account = copyUserBean.getData().getAccount();
                            tvUser.setText("账号：" + copyUserBean.getData().getAccount());
                        }
                    }

                    @Override
                    public void onError(Response<String> response) {
                        super.onError(response);
                        Log.i("test", "official  onError " + response.message());
                    }
                });


        OkGo.<String>get("http://service.jiangaifen.com:38082/coinPrice/ctusdExchange")
                .tag(this)
                .headers("mbtToken", mbtToken)
                .execute(new StringCallback() {
                    @Override
                    public void onSuccess(Response<String> response) {
                        BiGoMeiYuanBean biGoMeiYuanBean = JSON.parseObject(response.body(), BiGoMeiYuanBean.class);
                        if (biGoMeiYuanBean.getResultCode() == 0) {
                            tvMeiYuan.setText("1个= " + biGoMeiYuanBean.getData().getPricenow() + " 美元");
                        }
                    }
                    @Override
                    public void onError(Response<String> response) {
                        super.onError(response);
                        Log.i("test", "ctusdExchange  onError " + response.message());
                    }
                });
    }

    private void find() {
        findViewById(R.id.head_blue_iv_left).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        TextView tvTitle = findViewById(R.id.head_blue_tv_center);
        tvTitle.setText("充值");
        findViewById(R.id.head_blue_iv_right).setVisibility(View.GONE);

        tvType = findViewById(R.id.act_rcu_tv_type);
        tvUser = findViewById(R.id.act_rcu_tv_access);
        tvMeiYuan = findViewById(R.id.act_rcu_tv_meiy);
    }

    public void copyUserComit(View view) {

        Intent intent = new Intent(Recharge2CopyUserActivity.this, Recharge2LoadImgActivity.class);
        intent.putExtra("mbtToken", mbtToken);
        startActivity(intent);
        finish();
    }

    public void copyAccount(View view) {
        if (account != null && !account.isEmpty()) {
            ClipboardManager cm = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
            ClipData label = ClipData.newPlainText("Label", account);
            cm.setPrimaryClip(label);
            Toast.makeText(Recharge2CopyUserActivity.this, "复制成功", Toast.LENGTH_SHORT).show();
        }
    }
}
