package com.rong.mbt.personal.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import butterknife.ButterKnife;


public class PBaseFragment extends Fragment {

    protected View rootView;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        if (rootView == null) {
            try {
                rootView = inflater.inflate(getLayoutId(), container, false);
            } catch (Exception e) {
                return null;
            }
        }

        // 避免重复加载
        ViewGroup parent = (ViewGroup) rootView.getParent();
        if (parent != null) {
            parent.removeAllViews();
        }
        ButterKnife.bind(this, rootView);


        initView();

        return rootView;
    }

    public void startAty(Class<?> cls) {
        getActivity().startActivity(new Intent(getActivity(), cls));
    }

    public void initView() {
    }

    public int getLayoutId() {
        return 0;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
