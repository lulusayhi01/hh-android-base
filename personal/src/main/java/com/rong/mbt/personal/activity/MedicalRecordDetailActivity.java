package com.rong.mbt.personal.activity;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.bumptech.glide.Glide;
import com.calanger.hh.entity.ResultView;
import com.calanger.hh.model.Doctor;
import com.calanger.hh.model.Hospital;
import com.calanger.hh.model.MedicalRecord;
import com.calanger.hh.model.User;
import com.calanger.hh.util.DateUtils;
import com.google.gson.JsonObject;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.model.Response;
import com.rong.mbt.personal.R;
import com.rong.mbt.personal.activity.base.PBaseActivity;
import com.rong.mbt.personal.adapter.ImageListAdapter;
import com.rong.mbt.personal.bean.ImgUrl;
import com.rong.mbt.personal.bean.article.ArticleItemBean;
import com.rong.mbt.personal.bean.image.ImageBean;
import com.rong.mbt.personal.http.UrlApi;
import com.rong.mbt.personal.http.callback.JsonCallback;
import com.rong.mbt.personal.utils.SerializableMap;
import com.rong.mbt.personal.utils.SpUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import io.rong.imkit.RongIM;

/**
 * 病历详情
 */
public class MedicalRecordDetailActivity extends PBaseActivity {

//头部申明
    private LinearLayout mLlBack;
    private TextView headerTxtTitle;
    private LinearLayout mLlRight;
//内容申明
    private TextView mTxtTitle;
    private TextView mTxtInfo;
    private TextView mTxtCreattime;
    private RecyclerView mRecyclerImageView;
    private ImageListAdapter imageListAdapter;
//数据申明
    private JSONObject item;

    @Override
    public int getLayoutId() {
        return R.layout.activity_detail_medical_record;
    }

    @Override
    public void initView() {
        super.initView();

        Map<String,Object> singleBean  = (Map<String,Object>) getIntent().getSerializableExtra("singleBean");
        item = new JSONObject(singleBean);

        mLlBack = findViewById(R.id.ll_back);
        mLlRight = findViewById(R.id.right_ll);
        mLlRight.setVisibility(View.GONE);
        headerTxtTitle = findViewById(R.id.header_center_text);
        mTxtTitle = findViewById(R.id.tv_title);
        mTxtInfo = findViewById(R.id.tv_content);
        mTxtCreattime  = findViewById(R.id.tv_creattime);
        mRecyclerImageView = findViewById(R.id.recycler_image_view);
        mLlBack.setOnClickListener(v -> finish());
        initData();
    }

    private void initData() {
        if(item!=null){
            setData();
        }else{
            Integer id = (Integer) getIntent().getIntExtra("id",0);
            if(id==0){
                Toast.makeText(this, "对象ID值错误", Toast.LENGTH_SHORT);
            }else{
                getSingle(id);
            }
        }
    }

    private void setData(){
        MedicalRecord medicalRecord = item.getObject("medicalRecord",MedicalRecord.class);
        JSONArray imgUrlListJSONArray = item.getJSONArray("imgUrlList");
        headerTxtTitle.setText("病历详情");
        mTxtTitle.setText(medicalRecord.getTitle());
        mTxtInfo.setText(medicalRecord.getInfo());
        mTxtCreattime.setText(DateUtils.dateToString(medicalRecord.getCreattime()));
        mRecyclerImageView.setLayoutManager(new LinearLayoutManager(this));
        imageListAdapter = new ImageListAdapter();
        mRecyclerImageView.setAdapter(imageListAdapter);

        for(int i=0;i<imgUrlListJSONArray.size();i++){
            JSONObject single = imgUrlListJSONArray.getJSONObject(i);
            ImgUrl iu = single.toJavaObject(ImgUrl.class);
            Log.i("iu",iu.getImgUrl());
            List<ImageBean> imageBeans = new ArrayList<>();
            ImageBean imageBean = new ImageBean();
            imageBean.setUrl(iu.getImgUrl());
            imageBeans.add(imageBean);
            imageListAdapter.setNewData(imageBeans);
        }
    }

    public void getSingle(int id){
        OkGo.<ResultView>get(UrlApi.doctorGetSingle)
                .tag(this)
                .headers("mbtToken", SpUtils.getToken(this))
                .params("id", id)
                .execute(new JsonCallback<ResultView>() {
                    @Override
                    public void onSuccess(Response<ResultView> response) {
                        if (response != null && response.body() != null) {
                            ResultView resultView = response.body();
                            if (resultView.getResultCode() == 0) {
                                Map<String,Object> singleBean = (Map<String, Object>) resultView.getData();
                                item = new JSONObject(singleBean);
                                setData();
                                finish();
                            } else {
                                Toast.makeText(getApplicationContext(), resultView.getMsg(), Toast.LENGTH_SHORT);
                            }
                        }
                    }
                });
    }

}
