package com.rong.mbt.personal.bean;

import java.util.List;

public class FaceBean {



    private double confidence;
    private int errno;
    private String request_id;
    private List<Double> thresholds;
    private List<Integer> rectA;
    private List<Integer> rectB;

    public double getConfidence() {
        return confidence;
    }

    public void setConfidence(double confidence) {
        this.confidence = confidence;
    }

    public int getErrno() {
        return errno;
    }

    public void setErrno(int errno) {
        this.errno = errno;
    }

    public String getRequest_id() {
        return request_id;
    }

    public void setRequest_id(String request_id) {
        this.request_id = request_id;
    }

    public List<Double> getThresholds() {
        return thresholds;
    }

    public void setThresholds(List<Double> thresholds) {
        this.thresholds = thresholds;
    }

    public List<Integer> getRectA() {
        return rectA;
    }

    public void setRectA(List<Integer> rectA) {
        this.rectA = rectA;
    }

    public List<Integer> getRectB() {
        return rectB;
    }

    public void setRectB(List<Integer> rectB) {
        this.rectB = rectB;
    }
}
