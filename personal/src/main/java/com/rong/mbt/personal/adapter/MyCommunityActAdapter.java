package com.rong.mbt.personal.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.rong.mbt.personal.R;
import com.rong.mbt.personal.bean.ChilrenBean;
import com.rong.mbt.personal.bean.MyCommunityBean;
import com.rong.mbt.personal.utils.ImageUtil;

import java.util.List;


public class MyCommunityActAdapter extends RecyclerView.Adapter {

    private Context context;
    private MyCommunityBean.DataBean.ParentBean parent;
    private List<ChilrenBean> child;
    private int count = 0;

    public MyCommunityActAdapter(Context context) {
        this.context = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_my_community, viewGroup, false);
        return new MyCommunityActAdapterHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
        MyCommunityActAdapterHolder h = (MyCommunityActAdapterHolder) viewHolder;

        if (parent == null && i == 0) {
            ChilrenBean childrenBean = child.get(i);
            ImageUtil.loadCircleImage(context, childrenBean.getHeadimage(), h.img);

            String phone = "";
            if (childrenBean.getFriendRemark() != null && !childrenBean.getFriendRemark().isEmpty()) {
                phone = childrenBean.getFriendRemark();
            } else if (childrenBean.getName() != null && !childrenBean.getName().isEmpty()) {
                phone = childrenBean.getName();
            } else if (childrenBean.getPhone() != null && !childrenBean.getPhone().isEmpty()) {
                phone = childrenBean.getPhone();
            }
            int machineCount = childrenBean.getMachineCount();
            String creattime = childrenBean.getCreattime();
            h.tv.setText(phone + "\n" + "已买矿机：" + machineCount + "\n" + creattime);
            h.tv.setTextColor(Color.parseColor("#FFB5B5B5"));
        } else if (parent != null && i == 0) {
            h.tv.setText("我的社长：" + parent.getName());
            ImageUtil.loadCircleImage(context, parent.getHeadimage(), h.img);
        } else if (child != null && !child.isEmpty() && i != 0) {
            ChilrenBean childrenBean = child.get(i - 1);
            ImageUtil.loadCircleImage(context, childrenBean.getHeadimage(), h.img);
            String phone = "";
            if (childrenBean.getFriendRemark() != null && !childrenBean.getFriendRemark().isEmpty()) {
                phone = childrenBean.getFriendRemark();
            } else if (childrenBean.getName() != null && !childrenBean.getName().isEmpty()) {
                phone = childrenBean.getName();
            } else if (childrenBean.getPhone() != null && !childrenBean.getPhone().isEmpty()) {
                phone = childrenBean.getPhone();
            }
            int machineCount = childrenBean.getMachineCount();
            String creattime = childrenBean.getCreattime();
            h.tv.setText(phone + "\n" + "已买矿机：" + machineCount + "\n" + creattime);
            h.tv.setTextColor(Color.parseColor("#FFB5B5B5"));
        }
    }

    @Override
    public int getItemCount() {
        return count;
    }

    public void setParent(MyCommunityBean.DataBean.ParentBean parent) {
        this.parent = parent;
        if (parent != null) {
            count++;
        }
        notifyDataSetChanged();
    }

    public void setChild(List<ChilrenBean> child) {
        this.child = child;
        if (child != null && !child.isEmpty()) {
            count = child.size();
        }
    }

    class MyCommunityActAdapterHolder extends RecyclerView.ViewHolder {


        private ImageView img;
        private final TextView tv;

        public MyCommunityActAdapterHolder(View itemView) {
            super(itemView);
            img = itemView.findViewById(R.id.act_imc_img);
            tv = itemView.findViewById(R.id.act_imc_tv);
        }
    }
}
