package com.rong.mbt.personal.activity;

import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.callback.StringCallback;
import com.lzy.okgo.model.Response;
import com.rong.mbt.personal.R;
import com.rong.mbt.personal.activity.base.PBaseActivity;
import com.rong.mbt.personal.adapter.CashAccountListAdapter;
import com.rong.mbt.personal.bean.CashAccountListBean;
import com.rong.mbt.personal.http.UrlApi;

import java.util.List;

public class CashAccountListActivity extends PBaseActivity {

    private String mbtToken;
    private String phone;
    private String name;
    private CashAccountListAdapter adapter;

    @Override
    public int getLayoutId() {
        return R.layout.activity_cash_account_list;
    }

    @Override
    public void initView() {
        super.initView();

        mbtToken = getIntent().getStringExtra("mbtToken");
        phone = getIntent().getStringExtra("phone");
        name = getIntent().getStringExtra("name");

        find();
        listener();

    }

    private void listener() {
        adapter.setOnItemChildClickListener(new BaseQuickAdapter.OnItemChildClickListener() {
            @Override
            public void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {
                CashAccountListBean.DataBean dataBean = (CashAccountListBean.DataBean) adapter.getItem(position);
                OkGo.<String>get(UrlApi.userAccountDelete)
                        .tag(this)
                        .headers("mbtToken", mbtToken)
                        .params("type", dataBean.getType())
                        .execute(new StringCallback() {
                            @Override
                            public void onSuccess(Response<String> response) {

                                Log.i("test", "delete   " + response.body());
                                JSONObject jsonObject = JSON.parseObject(response.body());
                                if (jsonObject.getIntValue("resultCode") == 0) {
                                    net();
                                } else {
                                    Toast.makeText(CashAccountListActivity.this, jsonObject.getString("msg"), Toast.LENGTH_SHORT).show();
                                }
                            }

                            @Override
                            public void onError(Response<String> response) {
                                super.onError(response);
                                Log.i("test", " delete onError   " + response.body());
                            }
                        });
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        net();
    }

    private void find() {
        findViewById(R.id.head_blue_iv_left).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        TextView tvTitle = findViewById(R.id.head_blue_tv_center);
        tvTitle.setText("提现账户列表");
        findViewById(R.id.head_blue_iv_right).setVisibility(View.GONE);
        TextView tvRight = findViewById(R.id.head_blue_tv_right);
        tvRight.setText("添加");
        tvRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(CashAccountListActivity.this, AddThirdPartyAccountActivity.class);
                intent.putExtra("mbtToken", mbtToken);
                intent.putExtra("phone", phone);
                intent.putExtra("name", name);
                startActivity(intent);
            }
        });

        RecyclerView mRecycler = findViewById(R.id.act_cash_account_recycler);
        mRecycler.setLayoutManager(new LinearLayoutManager(CashAccountListActivity.this));


        adapter = new CashAccountListAdapter();
        mRecycler.setAdapter(adapter);
    }

    private void net() {
        OkGo.<String>get(UrlApi.userAccountListByPage)
                .tag(this)
                .headers("mbtToken", mbtToken)
                .params("pageNumber", 1)
                .params("pageSize", 100)
                .params("dealtype", "4202")
                .execute(new StringCallback() {
                    @Override
                    public void onSuccess(Response<String> response) {
                        //{"count":0,"data":[],"msg":"请求成功","resultCode":0}
                        Log.i("test", "listBypage " + response.body());
                        CashAccountListBean bean = JSON.parseObject(response.body(), CashAccountListBean.class);
                        if (bean.getResultCode() == 0) {

                            List<CashAccountListBean.DataBean> data = bean.getData();

                            adapter.setNewData(data);
                        }
                    }
                });
    }
}
