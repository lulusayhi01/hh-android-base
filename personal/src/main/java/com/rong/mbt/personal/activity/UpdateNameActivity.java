package com.rong.mbt.personal.activity;


import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.lzy.okgo.OkGo;
import com.lzy.okgo.callback.StringCallback;
import com.lzy.okgo.model.Response;
import com.rong.mbt.personal.R;
import com.rong.mbt.personal.activity.base.PBaseActivity;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UpdateNameActivity extends PBaseActivity {


    private String mbtToken;

    @Override
    public int getLayoutId() {
        return R.layout.activity_p_update_name;
    }

    @Override
    public void initView() {
        super.initView();
        mbtToken = getIntent().getStringExtra("mbtToken");
        String name = getIntent().getStringExtra("name");

        findViewById(R.id.head_while_iv_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        findViewById(R.id.head_while_iv_right).setVisibility(View.GONE);
        TextView tvTitle = findViewById(R.id.head_while_tv_center);
        tvTitle.setText("修改昵称");

        final EditText editText = findViewById(R.id.act_update_name_et);
        editText.setText(name);

        findViewById(R.id.act_update_name_bt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String editStr = editText.getText().toString();
                if (!editStr.isEmpty() && editStr.length() >= 2 && editStr.length() <= 12 && !isSpecialChar(editStr)) {
                    OkGo.<String>post("http://service.jiangaifen.com:38082/user/editUserName")
                            .tag(this)
                            .isMultipart(true)
                            .headers("mbtToken", mbtToken)
                            .params("name", editStr)
                            .execute(new StringCallback() {
                                @Override
                                public void onSuccess(Response<String> response) {
                                    finish();
                                }

                                @Override
                                public void onError(Response<String> response) {
                                    super.onError(response);
                                }
                            });
                } else {
                    Toast.makeText(UpdateNameActivity.this, "昵称不能为空且不能包含特殊字符", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }


    public static boolean isSpecialChar(String str) {
        String regEx = "[ _`~!@#$%^&*()+=|{}':;',\\[\\].<>/?~！@#￥%……&*（）——+|{}【】‘；：”“’。，、？]|\n|\r|\t";
        Pattern p = Pattern.compile(regEx);
        Matcher m = p.matcher(str);
        return m.find();
    }
}
