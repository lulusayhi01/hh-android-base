package com.rong.mbt.personal.activity;

import android.content.Intent;
import android.view.View;
import android.widget.TextView;

import com.rong.mbt.personal.R;
import com.rong.mbt.personal.activity.base.PBaseActivity;

public class Recharge2Activity extends PBaseActivity {


    private String mbtToken;

    @Override
    public int getLayoutId() {
        return R.layout.activity_recharge2;
    }

    @Override
    public void initView() {
        super.initView();
        mbtToken = getIntent().getStringExtra("mbtToken");
        listener();
        setText();
    }


    private void setText() {
        TextView tvTitle = findViewById(R.id.head_blue_tv_center);
        tvTitle.setText("充值");
        findViewById(R.id.head_blue_iv_right).setVisibility(View.GONE);
    }

    private void listener() {

        findViewById(R.id.head_blue_iv_left).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        findViewById(R.id.act_recharge2_copy_user).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Recharge2Activity.this, Recharge2CopyUserActivity.class);
                intent.putExtra("mbtToken", mbtToken);
                startActivity(intent);
            }
        });
        findViewById(R.id.act_recharge2_loadimg).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Recharge2Activity.this, Recharge2LoadImgActivity.class);
                intent.putExtra("mbtToken", mbtToken);
                startActivity(intent);
            }
        });
    }
}
