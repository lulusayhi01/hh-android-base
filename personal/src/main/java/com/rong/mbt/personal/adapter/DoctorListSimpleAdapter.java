package com.rong.mbt.personal.adapter;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.alibaba.fastjson.JSONObject;
import com.bumptech.glide.Glide;
import com.calanger.hh.model.Doctor;
import com.calanger.hh.model.Hospital;
import com.calanger.hh.model.User;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.rong.mbt.personal.R;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 医生列表-适配器-Simple模式
 */
public class DoctorListSimpleAdapter extends BaseQuickAdapter<Map<String,Object>, BaseViewHolder> {

    public DoctorListSimpleAdapter() {
        super(R.layout.item_ot_doctor);
    }

    @Override
    protected void convert(BaseViewHolder helper,  Map<String,Object> item) {

        try {
        	JSONObject jsonMap = new JSONObject(item);
            Doctor doctor = jsonMap.getObject("doctor",Doctor.class);
            User user = jsonMap.getObject("user",User.class);
            Hospital hospital = jsonMap.getObject("hospital",Hospital.class);
        
            ImageView ivHead = helper.getView(R.id.iv_head);
            Glide.with(helper.itemView.getContext()).load(doctor.getHeaderimg()).into(ivHead);

            TextView tvName = helper.getView(R.id.tv_name);
            tvName.setText(user.getName());

            String doctorTypeName = "";
            int doctorTitle = doctor.getDoctorTitle();
            if (doctorTitle == 1) {
                doctorTypeName = "助理医生";
            } else {
                doctorTypeName = "执业医生";
            }
            TextView tvDoctorType = helper.getView(R.id.tv_doctor_type);
            tvDoctorType.setText(doctorTypeName);//医生头衔(1.助理医生 2.执业医生)


            TextView tvDoctorSummary = helper.getView(R.id.tv_doctor_summary);
            tvDoctorSummary.setText(doctor.getSummary());


            TextView tvFollowDoctor = helper.getView(R.id.tv_follow_doctor);
            helper.addOnClickListener(R.id.tv_follow_doctor);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
