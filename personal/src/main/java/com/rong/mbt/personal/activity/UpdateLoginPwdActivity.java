package com.rong.mbt.personal.activity;


import android.content.Intent;
import android.graphics.Color;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.lzy.okgo.OkGo;
import com.lzy.okgo.callback.StringCallback;
import com.lzy.okgo.model.Response;
import com.rong.mbt.personal.R;
import com.rong.mbt.personal.activity.base.PBaseActivity;
import com.rong.mbt.personal.dialog.CustomerServiceDialog;
import com.rong.mbt.personal.utils.PhoneUtil;

//修改登录密码
public class UpdateLoginPwdActivity extends PBaseActivity {

    private ImageView ivRight;
    private String phone;
    private CountDownTimer timer;
    private TextView tvVcode;
    private String mbtToken;
    private EditText etVcode;
    private String startAct;

    @Override
    public int getLayoutId() {
        return R.layout.activity_update_login_pwd;
    }

    @Override
    public void initView() {
        super.initView();
        mbtToken = getIntent().getStringExtra("mbtToken");
        phone = getIntent().getStringExtra("phone");
        startAct = getIntent().getStringExtra("startAct");

        downTimer();
        find();
        listener();
    }

    private void listener() {
        findViewById(R.id.head_while_iv_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        ivRight.setVisibility(View.VISIBLE);
        ivRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CustomerServiceDialog csDialog = new CustomerServiceDialog(UpdateLoginPwdActivity.this);
                csDialog.show();
            }
        });
        //验证码
        tvVcode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                timer.start();

                if (!phone.isEmpty()) {
                    OkGo.<String>post("http://service.jiangaifen.com:38082/register/get-check-code")
                            .tag(this)
//                            .isMultipart(true)
                            .headers("mbtToken", mbtToken)
                            .params("mobile", phone)
                            .params("action", "2")
                            .params("isChannel", true)
                            .execute(new StringCallback() {
                                @Override
                                public void onSuccess(Response<String> response) {
                                    Log.i("test", "onSuccess  " + response.body());
                                }

                                @Override
                                public void onError(Response<String> response) {
                                    super.onError(response);
                                    Log.i("test", "onError  " + response.message());
                                }
                            });
                }
            }
        });

        //下一步
        findViewById(R.id.act_upa_button_next).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (etVcode.getText().toString().isEmpty()) {
                    return;
                }
                OkGo.<String>post("http://service.jiangaifen.com:38082/user/checkPhone")
                        .tag(this)
//                            .isMultipart(true)
                        .headers("mbtToken", mbtToken)
                        .params("phoneNum", phone)
                        .params("vcode", etVcode.getText().toString())
                        .execute(new StringCallback() {
                            @Override
                            public void onSuccess(Response<String> response) {
                                if (startAct.equals("Security")) {
                                    Intent intent = new Intent(UpdateLoginPwdActivity.this, ValidationPwdActivity.class);
                                    intent.putExtra("mbtToken", mbtToken);
                                    intent.putExtra("phone", phone);
                                    intent.putExtra("etVcode", etVcode.getText().toString());
                                    startActivity(intent);
                                    finish();
                                } else {
                                    Intent intent = new Intent(UpdateLoginPwdActivity.this, TradingPwdSettingActivity.class);
                                    intent.putExtra("mbtToken", mbtToken);
                                    intent.putExtra("phone", phone);
                                    intent.putExtra("etVcode", etVcode.getText().toString());
                                    intent.putExtra("isNoShow",true);
                                    startActivity(intent);
                                    finish();
                                }
                            }

                            @Override
                            public void onError(Response<String> response) {
                                super.onError(response);
                                Log.i("test", "onError  " + response.message());
                            }
                        });
            }

        });
    }

    private void find() {
        TextView tvTitle = findViewById(R.id.head_while_tv_center);
        tvTitle.setText("修改登录密码");
        ivRight = findViewById(R.id.head_while_iv_right);
        TextView tvPhone = findViewById(R.id.act_ulp_tv_phone);
        tvPhone.setText("手机号码：" + PhoneUtil.phoneShowStar(phone));

        tvVcode = findViewById(R.id.tv_get_vcode);
        etVcode = findViewById(R.id.et_vcode);
    }


    private void downTimer() {
        timer = new CountDownTimer(60000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                tvVcode.setText(((millisUntilFinished + 1000) / 1000) + "s");
                tvVcode.setEnabled(false);
                tvVcode.setTextColor(Color.parseColor("#999999"));
            }

            @Override
            public void onFinish() {
                tvVcode.setEnabled(true);
                tvVcode.setText("重新发送");
                tvVcode.setTextColor(Color.parseColor("#7282FB"));
            }
        };

    }

    @Override
    protected void onStop() {
        super.onStop();
        if (timer != null) {
            timer.cancel();
        }
    }
}
