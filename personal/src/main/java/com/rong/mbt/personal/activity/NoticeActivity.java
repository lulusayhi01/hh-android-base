package com.rong.mbt.personal.activity;

import android.annotation.SuppressLint;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.flyco.tablayout.SlidingTabLayout;
import com.rong.mbt.personal.R;
import com.rong.mbt.personal.activity.base.PBaseActivity;
import com.rong.mbt.personal.adapter.TabPagerAdapter;
import com.rong.mbt.personal.fragment.NoticeItemFragment;
import com.rong.mbt.personal.fragment.SearchItemFragment;
import com.sj.friendcircle.common.router.RouterList;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * 更多文章列表
 */
@Route(path = RouterList.NoticeActivity.path)
@SuppressLint("NewApi")
public class NoticeActivity extends PBaseActivity {


    private LinearLayout mLlBack;
    private TextView mTxtTitle;
    private View mIvRight;
    private LinearLayout mLlRightMenu;
    private TextView mTxtRight;
    private LinearLayout mLlRightText;
    private SlidingTabLayout mSlidingTabLayout;
    private ViewPager mViewPager;


    private String[] mTitles = {"问答","通知"};
    private Integer[] codes = {1,0};
    private ArrayList<Fragment> mFragments = new ArrayList<>();

    @Override
    public int getLayoutId() {
        return R.layout.activity_notice;
    }


    @Override
    public void initView() {
        super.initView();
        mLlBack = findViewById(R.id.ll_back);
        mTxtTitle = findViewById(R.id.txt_title);
        mIvRight = findViewById(R.id.iv_right);
        mLlRightMenu = findViewById(R.id.ll_right_menu);
        mTxtRight = findViewById(R.id.txt_right);
        mLlRightText = findViewById(R.id.ll_right_text);
        mSlidingTabLayout = findViewById(R.id.sliding_tab_layout);
        mViewPager = findViewById(R.id.view_pager);

        mTxtTitle.setText("我的消息");

        initTabLayout();
        initListener();
    }

    private void initTabLayout() {
        int userType = getIntent().getIntExtra("userType",0);
            for(int i=0;i<codes.length;i++){
                mFragments.add(NoticeItemFragment.getInstance(mTitles[i],codes[i]));
                TabPagerAdapter tabPagerAdapter = new TabPagerAdapter(getSupportFragmentManager(), Arrays.asList(mTitles), mFragments);
                mViewPager.setAdapter(tabPagerAdapter);
            }

        mSlidingTabLayout.setViewPager(mViewPager);
    }

    private void initListener() {
        mLlBack.setOnClickListener(v -> finish());
    }

}
