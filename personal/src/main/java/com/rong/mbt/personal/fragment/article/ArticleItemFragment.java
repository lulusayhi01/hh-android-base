package com.rong.mbt.personal.fragment.article;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.model.HttpParams;
import com.lzy.okgo.model.Response;
import com.rong.mbt.personal.R;
import com.rong.mbt.personal.activity.ArticleDetailsActivity;
import com.rong.mbt.personal.adapter.IndexArticleListAdapter;
import com.rong.mbt.personal.bean.article.ArticleItemBean;
import com.rong.mbt.personal.bean.article.ArticleListPageBean;
import com.rong.mbt.personal.http.UrlApi;
import com.rong.mbt.personal.http.callback.JsonCallback;
import com.rong.mbt.personal.utils.SpUtils;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshLoadMoreListener;

import java.util.ArrayList;
import java.util.List;

/**
 * 文章列表
 */
public class ArticleItemFragment extends Fragment implements OnRefreshLoadMoreListener {

    private String mTitle;
    private Integer code = 0;
    private Integer mode = 0;
    private RecyclerView mRecyclerView;
    private SmartRefreshLayout mRefreshLayout;
    private IndexArticleListAdapter articleListAdapter;
    private int pageNumber = 1;
    private List<ArticleItemBean> initData = new ArrayList<ArticleItemBean>();
    private boolean freshOrMore = false;
    private int total_count = 0;

    public static ArticleItemFragment getInstance(String title, Integer code) {
        ArticleItemFragment sf = new ArticleItemFragment();
        sf.mTitle = title;
        sf.code = code;
        return sf;
    }

    public static ArticleItemFragment getInstance(String title, Integer code,Integer mode) {
        ArticleItemFragment sf = new ArticleItemFragment();
        sf.mTitle = title;
        sf.code = code;
        sf.mode = mode;
        return sf;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        return super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_smart_refresh_layout, null);
        initView(view);
        return view;
    }

    private void initView(View view) {
        mRecyclerView = view.findViewById(R.id.recyclerView);
        mRefreshLayout = view.findViewById(R.id.refreshLayout);

        mRefreshLayout.setOnRefreshLoadMoreListener(this);
        articleListAdapter = new IndexArticleListAdapter();
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerView.setAdapter(articleListAdapter);
        articleListAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                ArticleItemBean item = (ArticleItemBean) adapter.getItem(position);
                startActivity(new Intent(getActivity(), ArticleDetailsActivity.class).putExtra("ArticleItemBean", item));
            }
        });
        requestArticle();

    }

    private void requestArticle() {
        int categoryId = code;
        OkGo okGo = OkGo.getInstance();
        HttpParams commonParams = new HttpParams();
        if (code != 0) {
            //文章列表
            //范畴：1  快速回答2  精选讲堂3  育婴护理4  精神健康
            commonParams.put("categoryId", categoryId);
            okGo.addCommonParams(commonParams);
        }
        if(mode==1){
            commonParams.put("userid", SpUtils.getDoctorId(getActivity()));
        }
            //文章列表
        okGo.<ArticleListPageBean>get(UrlApi.articleListByPage)
                    .tag(this)
                    .headers("mbtToken", SpUtils.getToken(getActivity()))
                    .params("pageNumber", pageNumber)
                    .execute(new JsonCallback<ArticleListPageBean>() {
                        @Override
                        public void onSuccess(Response<ArticleListPageBean> response) {
                            if (response != null && response.body() != null) {
                                ArticleListPageBean body = response.body();
                                if (body.getData() != null) {
                                    total_count = body.getCount();
                                    List<ArticleItemBean> data = body.getData();

                                    if (freshOrMore) {
                                        if(initData.size()>= total_count){
                                            mRefreshLayout.finishLoadMore();
                                        }else{
                                            initData.addAll(data);
                                        }
                                        freshOrMore = false;
                                        articleListAdapter.setNewData(initData);
                                        mRefreshLayout.finishLoadMore();
                                    } else {
                                        initData = data;
                                        articleListAdapter.setNewData(initData);
                                        mRefreshLayout.finishRefresh();
                                    }
                                }
                                mRefreshLayout.finishRefresh();
                                mRefreshLayout.finishLoadMore();
                            }
                        }
                    });


    }

    @Override
    public void onLoadMore(@NonNull RefreshLayout refreshLayout) {
        pageNumber++;
        freshOrMore = true;
        requestArticle();
    }

    @Override
    public void onRefresh(@NonNull RefreshLayout refreshLayout) {
        pageNumber = 1;
        requestArticle();
    }
}
