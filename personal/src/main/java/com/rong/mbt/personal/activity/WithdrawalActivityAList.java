package com.rong.mbt.personal.activity;

import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.callback.StringCallback;
import com.lzy.okgo.model.Response;
import com.rong.mbt.personal.R;
import com.rong.mbt.personal.activity.base.PBaseActivity;
import com.rong.mbt.personal.adapter.WithdrawalSelectorAccountAdapter;
import com.rong.mbt.personal.bean.CashAccountListBean;

import java.util.ArrayList;
import java.util.List;

/**
 * 提现账户列表
 */
public class WithdrawalActivityAList extends PBaseActivity {


    private String mbtToken;
    private WithdrawalSelectorAccountAdapter adapter;
    private List<CashAccountListBean.DataBean> mData = new ArrayList<>();
    private int dealtypeId = -1;
    private int type;

    @Override
    public int getLayoutId() {
        return R.layout.activity_withdrawal_alist;
    }

    @Override
    public void initView() {
        super.initView();

        mbtToken = getIntent().getStringExtra("mbtToken");
        find();
        initrecycler();
        net();
    }

    private void initrecycler() {
        RecyclerView mRecycler = findViewById(R.id.act_selector_account_recycler);
        mRecycler.setLayoutManager(new LinearLayoutManager(this));

        adapter = new WithdrawalSelectorAccountAdapter();
        mRecycler.setAdapter(adapter);

        adapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                CashAccountListBean.DataBean dataBean = (CashAccountListBean.DataBean) adapter.getItem(position);
                type = dataBean.getType();
                dealtypeId = dataBean.getId();
                Log.i("test", "type " + type);
                for (int i = 0; i < mData.size(); i++) {
                    if (dataBean.getId() == mData.get(i).getId()) {
                        mData.get(i).setSelector(true);
                    } else {
                        mData.get(i).setSelector(false);
                    }
                }
                adapter.setNewData(mData);
            }
        });
    }

    private void find() {
        findViewById(R.id.head_blue_iv_left).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        TextView tvTitle = findViewById(R.id.head_blue_tv_center);
        tvTitle.setText("选择提现账户");
        findViewById(R.id.head_blue_iv_right).setVisibility(View.GONE);

        TextView tvRight = findViewById(R.id.head_blue_tv_right);
        tvRight.setText("确定");

        tvRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (dealtypeId == -1 || type != 5) {
                    Toast.makeText(WithdrawalActivityAList.this, "请选择提现账户", Toast.LENGTH_SHORT).show();
                    return;
                }
                Intent intent = new Intent(WithdrawalActivityAList.this, WithdrawalActivity.class);
                intent.putExtra("mbtToken", mbtToken);
                intent.putExtra("dealtypeId", dealtypeId);
                startActivity(intent);
                finish();
            }
        });
    }

    private void net() {
        OkGo.<String>get("http://service.jiangaifen.com:38082/userAccount/listByPage")
                .tag(this)
                .headers("mbtToken", mbtToken)
                .params("pageNumber", 1)
                .params("pageSize", 100)
                .params("dealtype", "4202")
                .execute(new StringCallback() {
                    @Override
                    public void onSuccess(Response<String> response) {
                        //{"count":0,"data":[],"msg":"请求成功","resultCode":0}
                        Log.i("test", "listBypage " + response.body());
                        CashAccountListBean bean = JSON.parseObject(response.body(), CashAccountListBean.class);
                        if (bean.getResultCode() == 0) {
                            mData.addAll(bean.getData());
                            adapter.setNewData(mData);
                        }
                    }
                });
    }
}
