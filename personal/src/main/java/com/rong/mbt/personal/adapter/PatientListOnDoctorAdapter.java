package com.rong.mbt.personal.adapter;

import android.widget.ImageView;
import android.widget.TextView;

import com.alibaba.fastjson.JSONObject;
import com.bumptech.glide.Glide;
import com.calanger.hh.model.Doctor;
import com.calanger.hh.model.Hospital;
import com.calanger.hh.model.Order;
import com.calanger.hh.model.User;
import com.calanger.hh.util.DateUtils;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.rong.mbt.personal.R;

import java.util.Map;

/**
 * 医生列表-适配器-Simple模式
 */
public class PatientListOnDoctorAdapter extends BaseQuickAdapter<Map<String,Object>, BaseViewHolder> {

    public PatientListOnDoctorAdapter() {
        super(R.layout.item_patient_on_doctor);
    }

    @Override
    protected void convert(BaseViewHolder helper,  Map<String,Object> item) {

        try {
        	JSONObject jsonMap = new JSONObject(item);
            Order order = jsonMap.getObject("order", Order.class);
            User user = jsonMap.getObject("user",User.class);

            ImageView ivHead = helper.getView(R.id.iv_head);
            Glide.with(helper.itemView.getContext()).load(user.getHeadimage()).into(ivHead);

            TextView tvName = helper.getView(R.id.tv_name);
            tvName.setText(user.getName());

            TextView tvDoctorSummary = helper.getView(R.id.tv_contect_time);
            tvDoctorSummary.setText(DateUtils.dateToString(order.getCreattime()));

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
