package com.rong.mbt.personal.activity;

import android.widget.LinearLayout;
import android.widget.TextView;

import com.just.agentweb.AgentWeb;
import com.rong.mbt.personal.R;
import com.rong.mbt.personal.activity.base.PBaseActivity;

public class WebViewAcitvity extends PBaseActivity {


    private LinearLayout webView;
    private TextView title;

    @Override
    public void initView() {
        super.initView();

        webView = findViewById(R.id.web_view);
        title = findViewById(R.id.txt_title);
        LinearLayout back = findViewById(R.id.ll_back);
        back.setOnClickListener(v -> finish());

        String url = getIntent().getStringExtra("url");

        AgentWeb mAgentWeb = AgentWeb.with(this)
                .setAgentWebParent(webView, new LinearLayout.LayoutParams(-1, -1))
                .useDefaultIndicator()
                .createAgentWeb()
                .ready()
                .go(url);

        title.setText("详情");
    }

    @Override
    public int getLayoutId() {
        return R.layout.acitivity_web_view;
    }
}
