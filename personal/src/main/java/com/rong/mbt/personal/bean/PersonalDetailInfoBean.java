package com.rong.mbt.personal.bean;

import java.io.Serializable;

public class PersonalDetailInfoBean implements Serializable{


    private DataBean data;
    private String msg;
    private int resultCode;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public int getResultCode() {
        return resultCode;
    }

    public void setResultCode(int resultCode) {
        this.resultCode = resultCode;
    }

    public static class DataBean implements Serializable{
        /**
         * academy : sdasdsa
         * address : asdsadasd
         * business : 互联网
         * createtime : 2018-08-15 17:11:26
         * education : 本科
         * id : 5
         * income : 3000以下
         * marriage : 已婚
         * position : asdas
         * purchasecar : 是
         * purchasehouse : 是
         * scale : 0-20
         * status : 1
         * urgencylinkman : sdfsdf
         * urgencyphone : sdfasdf
         * urgencyrelation : 家人
         * userid : 36
         */

        private String academy;
        private String address;
        private String business;
        private String createtime;
        private String education;
        private int id;
        private String income;
        private String marriage;
        private String position;
        private String purchasecar;
        private String purchasehouse;
        private String scale;
        private int status;
        private String urgencylinkman;
        private String urgencyphone;
        private String urgencyrelation;
        private int userid;

        public String getAcademy() {
            return academy;
        }

        public void setAcademy(String academy) {
            this.academy = academy;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getBusiness() {
            return business;
        }

        public void setBusiness(String business) {
            this.business = business;
        }

        public String getCreatetime() {
            return createtime;
        }

        public void setCreatetime(String createtime) {
            this.createtime = createtime;
        }

        public String getEducation() {
            return education;
        }

        public void setEducation(String education) {
            this.education = education;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getIncome() {
            return income;
        }

        public void setIncome(String income) {
            this.income = income;
        }

        public String getMarriage() {
            return marriage;
        }

        public void setMarriage(String marriage) {
            this.marriage = marriage;
        }

        public String getPosition() {
            return position;
        }

        public void setPosition(String position) {
            this.position = position;
        }

        public String getPurchasecar() {
            return purchasecar;
        }

        public void setPurchasecar(String purchasecar) {
            this.purchasecar = purchasecar;
        }

        public String getPurchasehouse() {
            return purchasehouse;
        }

        public void setPurchasehouse(String purchasehouse) {
            this.purchasehouse = purchasehouse;
        }

        public String getScale() {
            return scale;
        }

        public void setScale(String scale) {
            this.scale = scale;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public String getUrgencylinkman() {
            return urgencylinkman;
        }

        public void setUrgencylinkman(String urgencylinkman) {
            this.urgencylinkman = urgencylinkman;
        }

        public String getUrgencyphone() {
            return urgencyphone;
        }

        public void setUrgencyphone(String urgencyphone) {
            this.urgencyphone = urgencyphone;
        }

        public String getUrgencyrelation() {
            return urgencyrelation;
        }

        public void setUrgencyrelation(String urgencyrelation) {
            this.urgencyrelation = urgencyrelation;
        }

        public int getUserid() {
            return userid;
        }

        public void setUserid(int userid) {
            this.userid = userid;
        }
    }
}
