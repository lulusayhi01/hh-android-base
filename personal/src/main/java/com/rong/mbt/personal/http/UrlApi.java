package com.rong.mbt.personal.http;

public interface UrlApi {

    String baseUrl = "http://service.jiangaifen.com:38082/";


    String userAccountDelete = baseUrl + "userAccount/delete";

    String userAccountListByPage = baseUrl + "userAccount/listByPage";

    String userGetUser = baseUrl + "user/getUser";

    String coinGetCoinCountByUser = baseUrl + "coin/getCoinCountByUser";

    //轮播图
    String bannerListByPage = baseUrl + "banner/listByPage";

    //订单列表
    String orderPreListByPage = baseUrl + "orderPre/listByPage";

    //订单详情
    String orderPreGetSingle = baseUrl + "orderPre/getSingle";

    //订单取消
    String orderPreRemove = baseUrl + "orderPre/remove";

    //订单完成
    String orderPreUpdate = baseUrl + "orderPre/update";

    //    订单接单接口
//    orderPre/receipt
//    参数：
//    id   (值来源于订单id)
    String orderPreReceipt = baseUrl + "orderPre/receipt";


    //    文章列表
//    接口：article/listByPage
//    参数：categoryId=1
    String articleListByPage = baseUrl + "article/listByPage";

    //
    String friendSelectFriendRemark = baseUrl +"friend/selectFriendRemark";

    //订单发送（不带图片）
    String orderPreInsert = baseUrl + "orderPre/insert";

    //订单发送（带图片）
    String orderPreUploadOrderImg = baseUrl + "orderPre/uploadOrderImg";

    //医生列表
    String userIndexDoctorList = baseUrl + "userIndex/doctorList";

    //科室
    String officesFind = baseUrl + "offices/find";

    /**
     * 普通订单---费用选择项的接口：
     *     dictionary/find
     *     参数：name=baseorderfeed
     *     健康订单---费用选择项的接口：
     *     dictionary/find
     *     参数：name=healthorderfeed
     *     健康订单---健康时间选择项的接口：
     *     dictionary/find
     *     参数：name=healthtime
     */
    String dictionaryFind = "dictionary/find";

    /**
     * 文章范畴
     * 接口：tagCategory/find
     * 参数：objTpye=1         type=2
     */
    String tagCategoryFind = "tagCategory/find";

    /**
     * 接口：如果是没有图片的接口是
     * article/insert
     * 参数：
     * title
     * content
     */
    String articleInsert = "article/insert";

    /**
     * 有图片的一起上车的接口是（最好控制图片是必输的）
     *article/addArticleImg
     * 参数：
     * feed (文章费用)
     * title
     * categoryId
     * content
     * file  (文件数组)
     */
    String articleAddArticleImg = "article/addArticleImg";

    //病例列表
    String medicalRecordList = baseUrl + "medicalRecord/listByPage";

    //病例新增
    String medicalRecordAdd = baseUrl + "medicalRecord/addObjectAndImg";

    //病例删除
    String medicalRecordRemove = baseUrl + "medicalRecord/remove";

    //费用列表
    String orderList = baseUrl + "order/listByPage";

    //费用新增
    String orderAdd = baseUrl + "order/addObjectAndImg";

    //费用更新
    String orderEdit = baseUrl + "order/edit";

    //费用删除
    String orderRemove = baseUrl + "order/remove";

    //医生列表
    String doctorList = baseUrl + "doctor/listByPage";

    //医生详情
    String doctorGetSingle = baseUrl + "doctor/getSingle";

    //医生新增
    String doctorAdd = baseUrl + "doctor/addObjectAndImg";

    //医生更新
    String doctorEdit = baseUrl + "doctor/update";

    //医生删除
    String doctorRemove = baseUrl + "doctor/remove";

    //文章管理-详情
    String articleGetSingle = baseUrl + "article/getSingle";

    //关注列表
    String followList = baseUrl + "follow/listByPage";
    //关注撤销
    String followCancel = baseUrl + "follow/cancel";
    //关注插入
    String followInsert = baseUrl + "follow/insert";

    //用户账户
    String accountGetSingle = baseUrl + "account/getSingleForUser";

    //搜索列表
    String searchList = baseUrl + "search/listByPage";

    //通知账户
    String noticeList = baseUrl + "notice/listByPage";

    //通知账户
    String customerServiceGetSingle = baseUrl + "customerService/getSingle";

    //咨询患者
    String patientListOnDoctor = baseUrl + "order/getPatient";

    //用户查看点赞评价列表
    String userViewLikeCommentList = baseUrl + "userViewLikeComment/listByPage";

}
