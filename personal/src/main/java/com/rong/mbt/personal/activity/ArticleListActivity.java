package com.rong.mbt.personal.activity;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.flyco.tablayout.SlidingTabLayout;
import com.rong.mbt.personal.R;
import com.rong.mbt.personal.activity.base.PBaseActivity;
import com.rong.mbt.personal.adapter.TabPagerAdapter;
import com.rong.mbt.personal.fragment.article.ArticleItemFragment;
import com.rong.mbt.personal.utils.SpUtils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * 更多文章列表
 */
public class ArticleListActivity extends PBaseActivity {


    private LinearLayout mLlBack;
    private TextView mTxtTitle;
    private View mIvRight;
    private LinearLayout mLlRightMenu;
    private TextView mTxtRight;
    private LinearLayout mLlRightText;
    private SlidingTabLayout mSlidingTabLayout;
    private ViewPager mViewPager;

    private String[] mTitles = {"全部","快速回答", "精选讲堂", "育婴护理", "精神健康"};//1  快速回答2  精选讲堂3  育婴护理4  精神健康
    private int categoryId;
    private Integer[] codes = {0,1,2, 3, 4};
    private ArrayList<Fragment> mFragments = new ArrayList<>();
    private ImageView addArticle;

    @Override
    public void initView() {
        super.initView();
        mLlBack = findViewById(R.id.ll_back);
        mTxtTitle = findViewById(R.id.txt_title);
        mIvRight = findViewById(R.id.iv_right);
        mLlRightMenu = findViewById(R.id.ll_right_menu);
        mTxtRight = findViewById(R.id.txt_right);
        mLlRightText = findViewById(R.id.ll_right_text);
        addArticle = findViewById(R.id.add_article);
        if(SpUtils.getUserType(this)==1){
            addArticle.setVisibility(View.VISIBLE);
//            mLlRightText.setVisibility(View.VISIBLE);
//            mTxtRight.setText("发表文章");
        }
        mSlidingTabLayout = findViewById(R.id.sliding_tab_layout);
        mViewPager = findViewById(R.id.view_pager);
        mTxtTitle.setText("优质内容");
        mLlBack.setOnClickListener(v -> finish());

        categoryId = getIntent().getIntExtra("categoryId", 1);

        initTabLayout();

        mLlRightText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ArticleListActivity.this,ArticleAddActivity.class));
            }
        });

        //添加文章
        addArticle.setOnClickListener(v -> {
            startActivity(new Intent(ArticleListActivity.this,ArticleAddActivity.class));
        });

    }

    private void initTabLayout() {
        for(int i=0;i<mTitles.length;i++){
            mFragments.add(ArticleItemFragment.getInstance(mTitles[i],codes[i]));
        }
        TabPagerAdapter tabPagerAdapter = new TabPagerAdapter(getSupportFragmentManager(), Arrays.asList(mTitles), mFragments);
        mViewPager.setAdapter(tabPagerAdapter);
        mViewPager.setCurrentItem(categoryId);
        mSlidingTabLayout.setViewPager(mViewPager);
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_flyco_tablayout_acticle;
    }
}
