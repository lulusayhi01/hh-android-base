package com.rong.mbt.personal.activity;

import android.graphics.Color;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.callback.StringCallback;
import com.lzy.okgo.model.Response;
import com.rong.mbt.personal.R;
import com.rong.mbt.personal.activity.base.PBaseActivity;
import com.rong.mbt.personal.utils.PhoneUtil;

public class AddThirdPartyAccountActivity extends PBaseActivity {


    private String mbtToken;
    private String phone;
    private TextView tvYzm;
    private CountDownTimer timer;
    private int type = 5;
    private EditText phoneCode;
    private EditText edAccount;
    private ImageView ivBtb;
    private ImageView ivLtb;
    private ImageView ivYtf;
    private String name;

    @Override
    public int getLayoutId() {
        return R.layout.activity_add_third_party_account;
    }

    @Override
    public void initView() {
        super.initView();

        mbtToken = getIntent().getStringExtra("mbtToken");
        phone = getIntent().getStringExtra("phone");
        name = getIntent().getStringExtra("name");

        find();
        setText();
        downTimer();

        visiGone();
    }

    private void visiGone() {
        //比特币
        findViewById(R.id.act_apa_tv_zfb).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ivBtb.setBackgroundResource(R.drawable.check_box_select);
                ivLtb.setBackgroundResource(R.drawable.check_box_unselect);
                ivYtf.setBackgroundResource(R.drawable.check_box_unselect);
                type = 5;
            }
        });
        //莱特币
        findViewById(R.id.act_apa_tv_ltb).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ivBtb.setBackgroundResource(R.drawable.check_box_unselect);
                ivLtb.setBackgroundResource(R.drawable.check_box_select);
                ivYtf.setBackgroundResource(R.drawable.check_box_unselect);
                type = 6;
            }
        });
        //以太坊
        findViewById(R.id.act_apa_tv_ytf).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ivBtb.setBackgroundResource(R.drawable.check_box_unselect);
                ivLtb.setBackgroundResource(R.drawable.check_box_unselect);
                ivYtf.setBackgroundResource(R.drawable.check_box_select);
                type = 7;
            }
        });
    }

    public void addThirdPartyAccount(View view) {
        String code = phoneCode.getText().toString();
        String account = edAccount.getText().toString();
        OkGo.<String>post("http://service.jiangaifen.com:38082/userAccount/edit")
                .tag(this)
                .headers("mbtToken", mbtToken)
                .params("type", type)
                .params("phoneNum", phone)
                .params("vcode", code)
                .params("account", account)
                .params("name", name)
                .params("dealtype","4202")
                .execute(new StringCallback() {
                    @Override
                    public void onSuccess(Response<String> response) {
                        Log.i("test", "onSuccess  " + response.body());

                        JSONObject jsonObject = JSON.parseObject(response.body());
                        if(jsonObject.getIntValue("resultCode") == 0){
                            finish();
                        }else{
                            Toast.makeText(AddThirdPartyAccountActivity.this, jsonObject.getString("msg"), Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onError(Response<String> response) {
                        super.onError(response);
                        Toast.makeText(AddThirdPartyAccountActivity.this, response.message(), Toast.LENGTH_SHORT).show();
                    }

                });

    }

    private void setText() {
        TextView tvPone = findViewById(R.id.act_atpa_phone_info);
        tvPone.setText("手机验证码将发送到" + PhoneUtil.phoneShowStar(phone) + "手机上，请注意查收");
    }

    private void find() {
        ivBtb = findViewById(R.id.act_apa_check_btb);
        ivLtb = findViewById(R.id.act_apa_check_ltb);
        ivYtf = findViewById(R.id.act_apa_check_ytf);

        findViewById(R.id.head_blue_iv_left).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        TextView tvTitle = findViewById(R.id.head_blue_tv_center);
        tvTitle.setText("添加账户");
        findViewById(R.id.head_blue_iv_right).setVisibility(View.GONE);

        phoneCode = findViewById(R.id.act_atpa_yzm);
        edAccount = findViewById(R.id.act_et3_account);


        tvYzm = findViewById(R.id.act_atpa_tv_yzm);
        tvYzm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                timer.start();
                if (!phone.isEmpty()) {
                    OkGo.<String>post("http://service.jiangaifen.com:38082/register/get-check-code")
                            .tag(this)
//                            .isMultipart(true)
                            .headers("mbtToken", mbtToken)
                            .params("mobile", phone)
                            .params("action", "14")
                            .params("isChannel", true)
                            .execute(new StringCallback() {
                                @Override
                                public void onSuccess(Response<String> response) {
                                    Log.i("test", "onSuccess  " + response.body());
                                }

                                @Override
                                public void onError(Response<String> response) {
                                    super.onError(response);
                                    Log.i("test", "onError  " + response.message());
                                }

                            });
                }
            }
        });
    }

    private void downTimer() {
        timer = new CountDownTimer(60000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                tvYzm.setText(((millisUntilFinished + 1000) / 1000) + "s");
                tvYzm.setEnabled(false);
                tvYzm.setTextColor(Color.parseColor("#999999"));
            }

            @Override
            public void onFinish() {
                tvYzm.setEnabled(true);
                tvYzm.setText("重新发送");
                tvYzm.setTextColor(Color.parseColor("#7282FB"));
            }
        };
    }

}
