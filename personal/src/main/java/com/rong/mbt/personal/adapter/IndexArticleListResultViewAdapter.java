package com.rong.mbt.personal.adapter;

import android.widget.ImageView;
import android.widget.TextView;

import com.alibaba.fastjson.JSONObject;
import com.bumptech.glide.Glide;
import com.calanger.hh.model.Article;
import com.calanger.hh.model.Doctor;
import com.calanger.hh.model.User;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.rong.mbt.personal.R;
import com.rong.mbt.personal.bean.article.ArticleItemBean;

import java.util.Map;

/**
 * 文章列表
 */
public class IndexArticleListResultViewAdapter extends BaseQuickAdapter<Map<String,Object>, BaseViewHolder> {


    public IndexArticleListResultViewAdapter() {
        super(R.layout.item_patient_index_article);
    }

    @Override
    protected void convert(BaseViewHolder helper, Map<String,Object> item) {

        try {

            JSONObject jo = new JSONObject(item);
            User user = jo.getObject("user", User.class);
            Article article = jo.getObject("article", Article.class);
            Integer userViewLikeCommentListSize = jo.getObject("userViewLikeCommentListSize", Integer.class);

            ImageView ivHead = helper.getView(R.id.iv_head);
            Glide.with(helper.itemView.getContext()).load(article.getPageImg()).into(ivHead);

            TextView tvArticleTitle = helper.getView(R.id.tv_article_title);
            tvArticleTitle.setText(article.getTitle());

            TextView tvDoctorName = helper.getView(R.id.tv_doctor_name);
            tvDoctorName.setText(user.getName());

            TextView tvReadingNumber = helper.getView(R.id.tv_reading_number);
            tvReadingNumber.setText(userViewLikeCommentListSize + "");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
