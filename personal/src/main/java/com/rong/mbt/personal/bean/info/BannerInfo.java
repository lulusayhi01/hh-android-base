package com.rong.mbt.personal.bean.info;

import com.stx.xhb.xbanner.entity.SimpleBannerInfo;

public class BannerInfo extends SimpleBannerInfo {

    private String imageUrl;
    private String url;

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    @Override
    public String getXBannerUrl() {
        return imageUrl;
    }


    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
