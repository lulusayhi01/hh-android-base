package com.rong.mbt.personal.activity;

import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.calanger.hh.entity.ResultView;
import com.calanger.hh.model.Doctor;
import com.calanger.hh.model.Hospital;
import com.calanger.hh.model.OrderPre;
import com.calanger.hh.model.User;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.model.Response;
import com.rong.mbt.personal.R;
import com.rong.mbt.personal.activity.base.PBaseActivity;
import com.rong.mbt.personal.bean.doctor.DoctorListBean;
import com.rong.mbt.personal.http.UrlApi;
import com.rong.mbt.personal.http.callback.JsonCallback;
import com.rong.mbt.personal.utils.SpUtils;
import com.shehuan.niv.NiceImageView;

import java.util.Map;

import io.rong.imkit.RongIM;

/**
 * 医生详情（咨询医生页面）
 */
public class DoctorDetailActivity extends PBaseActivity {

    @Override
    public void initView() {
        super.initView();
        DoctorListBean orderPreBean = (DoctorListBean) getIntent().getSerializableExtra("DoctorListBean");

        NiceImageView ivHead = findViewById(R.id.iv_head);
        TextView tvName = findViewById(R.id.tv_name);
        TextView tvDoctorType = findViewById(R.id.tv_doctor_type);
        TextView tvHospitalName = findViewById(R.id.tv_hospital_name);
        TextView tvAddress = findViewById(R.id.tv_address);
        ImageView ivCard1 = findViewById(R.id.iv_Card1);
        ImageView ivCard2 = findViewById(R.id.iv_Card2);
        TextView tvConsultation = findViewById(R.id.tv_Consultation);
        TextView tvFollow = findViewById(R.id.tv_follow);
        TextView txtTitle = findViewById(R.id.txt_title);
        txtTitle.setText("医师信息");
        tvFollow.setOnClickListener(v -> Toast.makeText(getApplicationContext(),"已关注",Toast.LENGTH_LONG));
        tvFollow.setText("关注他");

        LinearLayout back = findViewById(R.id.ll_back);
        back.setOnClickListener(v -> finish());

        if (orderPreBean != null) {

            Glide.with(this).load(orderPreBean.getDoctor().getHeaderimg()).into(ivHead);
            Glide.with(this).load(orderPreBean.getDoctor().getIdCardBackImg()).into(ivCard1);
            Glide.with(this).load(orderPreBean.getDoctor().getIdCardFrontImg()).into(ivCard2);

            tvName.setText(orderPreBean.getUser().getName());

            String doctorTypeName = "";
            int doctorTitle = orderPreBean.getDoctor().getDoctorTitle();
            if (doctorTitle == 1) {
                doctorTypeName = "助理医生";
            } else {
                doctorTypeName = "执业医生";
            }
            tvDoctorType.setText(doctorTypeName);//医生头衔(1.助理医生 2.执业医生)
            tvHospitalName.setText(orderPreBean.getHospital().getName());

            tvAddress.setText(orderPreBean.getHospital().getAddress());

            tvConsultation.setOnClickListener(v -> {
                RongIM.getInstance().startPrivateChat(this, orderPreBean.getDoctor().getUserId() + "", orderPreBean.getUser().getName());

            });
        }else{
            Integer id = (Integer) getIntent().getIntExtra("id",0);

            OkGo.<ResultView>get(UrlApi.doctorGetSingle)
                    .tag(this)
                    .headers("mbtToken", SpUtils.getToken(this))
                    .params("id", id)
                    .execute(new JsonCallback<ResultView>() {
                        @Override
                        public void onSuccess(Response<ResultView> response) {
                            if (response != null && response.body() != null) {
                                ResultView resultView = response.body();
                                if (resultView.getResultCode() == 0) {
//                                        requestSelectFriendRemark(orderPreBean.getUserId()+"",orderPreBean.getName());
                                    Map<String,Object> map = (Map<String, Object>) resultView.getData();
                                    Doctor doctor = (Doctor) map.get("doctor");
                                    User user = (User) map.get("user");
                                    Hospital hospital = (Hospital) map.get("hospital");

                                    Glide.with(DoctorDetailActivity.this).load(doctor.getHeaderimg()).into(ivHead);
                                    Glide.with(DoctorDetailActivity.this).load(doctor.getIdCardBackImg()).into(ivCard1);
                                    Glide.with(DoctorDetailActivity.this).load(doctor.getIdCardFrontImg()).into(ivCard2);

                                    tvName.setText(user.getName());

                                    String doctorTypeName = "";
                                    int doctorTitle = doctor.getDoctorTitle();
                                    if (doctorTitle == 1) {
                                        doctorTypeName = "助理医生";
                                    } else {
                                        doctorTypeName = "执业医生";
                                    }
                                    tvDoctorType.setText(doctorTypeName);//医生头衔(1.助理医生 2.执业医生)
                                    tvHospitalName.setText(hospital.getName());

                                    tvAddress.setText(hospital.getAddress());

                                    tvConsultation.setOnClickListener(v -> {
                                        RongIM.getInstance().startPrivateChat(DoctorDetailActivity.this, doctor.getUserId() + "", user.getName());

                                    });


                                    finish();
                                } else {
                                    Toast.makeText(getApplicationContext(), resultView.getMsg(), Toast.LENGTH_SHORT);
                                }
                            }
                        }
                    });



        }
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_doctor_details;
    }

}
