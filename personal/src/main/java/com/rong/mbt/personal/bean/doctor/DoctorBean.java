package com.rong.mbt.personal.bean.doctor;

import java.io.Serializable;

public class DoctorBean implements Serializable {

    /**
     * certifiedImg : https://hh-liululu.oss-cn-beijing.aliyuncs.com/zy2.jpg
     * creattime : 1566885162000
     * doctorTitle : 1
     * headerimg : https://hh-liululu.oss-cn-beijing.aliyuncs.com/default/default-ys001.jpg
     * hospitalId : 1
     * id : 83
     * idCardBackImg : https://hh-liululu.oss-cn-beijing.aliyuncs.com/sfz/sfzb0.jpeg
     * idCardFrontImg : https://hh-liululu.oss-cn-beijing.aliyuncs.com/sfz/sfz2.jpg
     * idCardNo : 362323199011070066
     * level : 5
     * name : 刘主任
     * officesId : 2
     * remark :
     * status : 1
     * summary : 擅长外壳各种突发症状1
     * userId : 82
     */

    private String certifiedImg;
    private long creattime;
    private int doctorTitle;
    private String headerimg;
    private int hospitalId;
    private int id;
    private String idCardBackImg;
    private String idCardFrontImg;
    private String idCardNo;
    private int level;
    private String name;
    private int officesId;
    private String remark;
    private int status;
    private String summary;
    private int userId;

    public String getCertifiedImg() {
        return certifiedImg;
    }

    public void setCertifiedImg(String certifiedImg) {
        this.certifiedImg = certifiedImg;
    }

    public long getCreattime() {
        return creattime;
    }

    public void setCreattime(long creattime) {
        this.creattime = creattime;
    }

    public int getDoctorTitle() {
        return doctorTitle;
    }

    public void setDoctorTitle(int doctorTitle) {
        this.doctorTitle = doctorTitle;
    }

    public String getHeaderimg() {
        return headerimg;
    }

    public void setHeaderimg(String headerimg) {
        this.headerimg = headerimg;
    }

    public int getHospitalId() {
        return hospitalId;
    }

    public void setHospitalId(int hospitalId) {
        this.hospitalId = hospitalId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getIdCardBackImg() {
        return idCardBackImg;
    }

    public void setIdCardBackImg(String idCardBackImg) {
        this.idCardBackImg = idCardBackImg;
    }

    public String getIdCardFrontImg() {
        return idCardFrontImg;
    }

    public void setIdCardFrontImg(String idCardFrontImg) {
        this.idCardFrontImg = idCardFrontImg;
    }

    public String getIdCardNo() {
        return idCardNo;
    }

    public void setIdCardNo(String idCardNo) {
        this.idCardNo = idCardNo;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getOfficesId() {
        return officesId;
    }

    public void setOfficesId(int officesId) {
        this.officesId = officesId;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

}
