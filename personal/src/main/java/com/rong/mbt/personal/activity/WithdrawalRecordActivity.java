package com.rong.mbt.personal.activity;


import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.alibaba.fastjson.JSON;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.callback.StringCallback;
import com.lzy.okgo.model.Response;
import com.rong.mbt.personal.R;
import com.rong.mbt.personal.activity.base.PBaseActivity;
import com.rong.mbt.personal.adapter.WithdrawalRecordAdapter;
import com.rong.mbt.personal.bean.RechargeRecordInfoBean;

//提现记录
public class WithdrawalRecordActivity extends PBaseActivity {


    private RecyclerView mRecycler;
    private LinearLayoutManager manager;
    private WithdrawalRecordAdapter adapter;
    private String mbtToken;

    @Override
    public int getLayoutId() {
        return R.layout.activity_withdrawal_record;
    }

    private int PAGENUM = 1;
    private int PAGESIZE = 20;
    private boolean loading;

    @Override
    public void initView() {
        super.initView();

        find();
        listener();

        mbtToken = getIntent().getStringExtra("mbtToken");
        net();
    }

    private void net() {
        //typedetailid     4301交易 4302佣金 4303 提现 4304 充值 4305 购买矿机
        OkGo.<String>post("http://service.jiangaifen.com:38082/user/listDealByPage")
                .tag(this)
                .headers("mbtToken", mbtToken)
//                .params("typedetailid", "4303")
                .params("pageNum", PAGENUM)
                .params("pageSize", PAGESIZE)
                .execute(new StringCallback() {
                    @Override
                    public void onSuccess(Response<String> response) {
                        Log.i("test", "onSuccess  " + response.body());
                        RechargeRecordInfoBean bean = JSON.parseObject(response.body(), RechargeRecordInfoBean.class);
                        if (bean.getResultCode() == 0 && !bean.getData().isEmpty()) {
                            adapter.setData(bean.getData());
                            loading = false;
                        }
                    }

                    @Override
                    public void onError(Response<String> response) {
                        super.onError(response);
                        Log.i("test", "onError  " + response.body());
                    }
                });
    }

    private void listener() {
        findViewById(R.id.head_while_iv_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        //加载
        mRecycler.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0) {
                    int visibleItemCount = manager.getChildCount();
                    int totalItemCount = manager.getItemCount();
                    int pastVisiblesItems = manager.findFirstVisibleItemPosition();

                    if (!loading && (visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                        loading = !loading;
                        PAGENUM++;
                        net();
                    }
                }
            }
        });
    }

    private void find() {
        TextView tvTitle = findViewById(R.id.head_while_tv_center);
        tvTitle.setText("提现记录");
        findViewById(R.id.head_while_iv_right).setVisibility(View.GONE);


        mRecycler = findViewById(R.id.act_wr_recycler);
        manager = new LinearLayoutManager(this);
        mRecycler.setLayoutManager(manager);
        adapter = new WithdrawalRecordAdapter(this);
        mRecycler.setAdapter(adapter);

    }
}
