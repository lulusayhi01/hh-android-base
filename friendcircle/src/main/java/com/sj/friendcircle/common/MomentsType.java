package com.sj.friendcircle.common;

/**
 * 朋友圈类型
 */

public interface MomentsType {
    //空内容，容错用
    int EMPTY_CONTENT = 0;
    //纯文字
    int TEXT_ONLY = 1;
    //多图
    int MULTI_IMAGES = 2;
    //网页
    int WEB = 4;
    // TODO: 2018/7/29  增加视频类型，广告类型等
}
