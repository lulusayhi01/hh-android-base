package com.sj.friendcircle.common.router;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * 跳转路由管理
 */

public interface RouterList {

    @Retention(RetentionPolicy.SOURCE)
    @interface PhotoMultiBrowserActivity {
        String path = "/photo/browser";
        String key_browserinfo = "browserinfo";
        String key_maxSelectCount = "maxSelectCount";
        String key_result = "selectedphoto";
        int requestCode = 0x20;
    }

    @Retention(RetentionPolicy.SOURCE)
    @interface PublishActivity {
        String path = "/publish/edit";
        String key_mode = "mode";
        String key_photoList = "photoList";
        int MODE_TEXT = 0x10;
        int MODE_MULTI = 0x11;
        int requestCode = 0x21;
    }

    @Retention(RetentionPolicy.SOURCE)
    @interface PhotoSelectActivity {
        String path = "/photo/select";
        String key_maxSelectCount = "maxSelectCount";
        String key_result = "selectedphoto";
        int requestCode = 0x22;
    }

    @Retention(RetentionPolicy.SOURCE)
    @interface FriendCircleBg {
        int requestCode = 0x23;
        int REQUEST_FROM_CAMERA_BG = 0x15;
        int REQUEST_FROM_CAMERA_COMMUNITY_BG = 0x16;
    }

    /**
     * 朋友圈
     */
    @Retention(RetentionPolicy.SOURCE)
    @interface FriendCircleActivity{
        String path = "/friend/circle";
    }

    /***
     * 社区
     */
    @Retention(RetentionPolicy.SOURCE)
    @interface CommunityActivity{
        String path = "/friend/community";
    }

    @Retention(RetentionPolicy.SOURCE)
    @interface LoginActivity{
        String path = "/acitvity/login";
    }

    /**矿机*/
    @Retention(RetentionPolicy.SOURCE)
    @interface MachineListActivity{
        String path = "/acitvity/MachineListActivity";
    }

    /**手机联系人*/
    @Retention(RetentionPolicy.SOURCE)
    @interface PhoneContactActivity{
        String path = "/acitvity/PhoneContactActivity";
    }

    /**附近的人*/
    @Retention(RetentionPolicy.SOURCE)
    @interface PeopleNearbyActivity{
        String path = "/acitvity/PeopleNearbyActivity";
    }

    /**群聊列表*/
    @Retention(RetentionPolicy.SOURCE)
    @interface GroupListActivity {
        String path = "/acitvity/GroupListActivity";
    }

    /**关于*/
    @Retention(RetentionPolicy.SOURCE)
    @interface AboutActivity {
        String path = "/acitvity/AboutActivity";
    }

    /**黑匣子*/
    @Retention(RetentionPolicy.SOURCE)
    @interface BlackBoxActivity {
        String path = "/acitvity/BlackBoxActivity";
    }


    /**发布订单*/
    @Retention(RetentionPolicy.SOURCE)
    @interface OrderPreActivity {
        String path = "/order/OrderPreActivity";
    }

    /**发布文章*/
    @Retention(RetentionPolicy.SOURCE)
    @interface PublishArticlesActivity {
        String path = "/order/PublishArticlesActivity";
    }

    /**搜索*/
    @Retention(RetentionPolicy.SOURCE)
    @interface SearchActivity {
        String path = "/search/SearchActivity";
    }

    /**通知*/
    @Retention(RetentionPolicy.SOURCE)
    @interface NoticeActivity {
        String path = "/notice/NoticeActivity";
    }

}
