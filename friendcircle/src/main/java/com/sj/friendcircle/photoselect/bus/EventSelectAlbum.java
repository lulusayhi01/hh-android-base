package com.sj.friendcircle.photoselect.bus;

/**
 * Created by huluhong on 2018/7/29.
 * <p>
 * 选择相册的event
 */

public class EventSelectAlbum {
    private String albumName;

    public EventSelectAlbum(String albumName) {
        this.albumName = albumName;
    }

    public String getAlbumName() {
        return albumName;
    }

    public void setAlbumName(String albumName) {
        this.albumName = albumName;
    }
}
