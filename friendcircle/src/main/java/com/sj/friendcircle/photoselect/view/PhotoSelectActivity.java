package com.sj.friendcircle.photoselect.view;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;

import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.sj.friendcircle.R;
import com.sj.friendcircle.common.router.RouterList;
import com.sj.friendcircle.lib.base.BaseFragment;
import com.sj.friendcircle.lib.entity.ImageInfo;
import com.sj.friendcircle.lib.manager.localphoto.LocalPhotoManager;
import com.sj.friendcircle.lib.utils.ToolUtil;
import com.sj.friendcircle.photoselect.bus.EventSelectAlbum;
import com.sj.friendcircle.photoselect.view.fragment.PhotoAlbumFragement;
import com.sj.friendcircle.photoselect.view.fragment.PhotoGridFragement;
import com.sj.friendcircle.ui.base.BaseTitleBarActivity;
import com.sj.friendcircle.ui.util.SwitchActivityTransitionUtil;
import com.sj.friendcircle.ui.widget.common.TitleBar;
import com.socks.library.KLog;


import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by huluhong on 2018/7/27.
 * <p>
 * 图片选择器
 */

@Route(path = RouterList.PhotoSelectActivity.path)
public class PhotoSelectActivity extends BaseTitleBarActivity {
    private static final String TAG = "PhotoSelectActivity";


    @Autowired(name = RouterList.PhotoSelectActivity.key_maxSelectCount)
    int maxCount;

    private PhotoGridFragement gridFragement;
    private PhotoAlbumFragement albumFragement;

    private BaseFragment currentFragment;


    @Subscribe
    public void onEventMainThread(EventSelectAlbum event) {
        if (event == null || TextUtils.isEmpty(event.getAlbumName())) return;
        if (!TextUtils.equals(getBarTitle(), event.getAlbumName())) {
            gridFragement.changeAlbum(event.getAlbumName());
        }
        setTitleMode(TitleBar.MODE_BOTH);
        setTitle(event.getAlbumName());
        changeFragment(currentFragment, gridFragement, true);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photoselect);
        init();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onHandleIntent(Intent intent) {

    }

    private void init() {
        initTitle();
        initFrag();
    }

    private void initTitle() {
        setTitle(LocalPhotoManager.INSTANCE.getAllPhotoTitle());
        setTitleMode(TitleBar.MODE_BOTH);
        setTitleBarBackground(getResources().getColor(R.color.color_c5181818));
        setTitleLeftText("相册");
        setTitleRightText("取消");
        setTitleRightIcon(0);
    }

    private void initFrag() {
        if (gridFragement == null) {
            KLog.i(TAG, "maxCount = " + maxCount);
            gridFragement = PhotoGridFragement.newInstance(maxCount);
        }
        if (albumFragement == null) {
            albumFragement = new PhotoAlbumFragement();
        }
        changeFragment(currentFragment, gridFragement, false);
    }

    private void changeFragment(BaseFragment from, BaseFragment to, boolean needAnima) {
        if (to == null) return;
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        if (needAnima) {
            setUpAnima(transaction, from, to);
        }
        if (!to.isAdded()) {
            transaction.add(R.id.photo_select_content, to);
        } else {
            if (from != null) {
                transaction.hide(from).show(to);
            }
        }
        currentFragment = to;
        transaction.commitAllowingStateLoss();
    }

    private void setUpAnima(FragmentTransaction transaction, BaseFragment from, BaseFragment to) {
        if (to == albumFragement) {
            transaction.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right);
        } else {
            transaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RouterList.PhotoMultiBrowserActivity.requestCode) {
            if (resultCode == RESULT_OK) {
                if (gridFragement != null && data != null && data.hasExtra(RouterList.PhotoMultiBrowserActivity.key_result)) {
                    List<ImageInfo> newDatas = data.getParcelableArrayListExtra(RouterList.PhotoMultiBrowserActivity.key_result);
                    gridFragement.updateSelectList(newDatas);
                }
            }
        }
    }

    @Override
    public void onTitleLeftClick() {
        setTitleMode(TitleBar.MODE_RIGHT);
        setTitle("相册");
        changeFragment(gridFragement, albumFragement, true);
    }

    @Override
    public void onTitleRightClick() {
        finish();
    }

    public void finish(List<ImageInfo> selectedPhoto) {
        if (!ToolUtil.isListEmpty(selectedPhoto)) {
            Intent intent = new Intent();
            intent.putParcelableArrayListExtra(RouterList.PhotoSelectActivity.key_result, (ArrayList<? extends Parcelable>) selectedPhoto);
            setResult(RESULT_OK, intent);
            finish();
        } else {
            Intent intent = new Intent();
            setResult(RESULT_CANCELED, intent);
            finish();
        }
    }

    @Override
    public void finish() {
        super.finish();
        SwitchActivityTransitionUtil.transitionVerticalOnFinish(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        LocalPhotoManager.INSTANCE.writeToLocal();
        EventBus.getDefault().unregister(this);
    }
}
