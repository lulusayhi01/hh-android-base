package com.sj.friendcircle.lib.manager.localphoto;

/**
 * Created by huluhong on 2018/7/27.
 */

public class LPException extends Exception {

    public LPException(String r) {
        this(r, null);
    }

    public LPException(String message, Exception cause) {
        super(message, cause);
    }
}
