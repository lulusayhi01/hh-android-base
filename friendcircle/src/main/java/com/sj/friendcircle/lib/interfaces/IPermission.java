package com.sj.friendcircle.lib.interfaces;


import com.sj.friendcircle.lib.helper.PermissionHelper;

/**
 *
 */
public interface IPermission {
    PermissionHelper getPermissionHelper();
}
