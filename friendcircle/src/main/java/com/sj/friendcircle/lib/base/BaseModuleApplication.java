package com.sj.friendcircle.lib.base;

import android.app.Application;

import com.sj.friendcircle.lib.api.AppContext;
import com.sj.friendcircle.lib.manager.localphoto.LocalPhotoManager;

/**
 * Created by huluhong on 2018/7/27.
 * <p>
 * module的application父类..主要用来初始ARouter等
 */

public class BaseModuleApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        AppContext.initARouter();
        LocalPhotoManager.INSTANCE.registerContentObserver(null);
    }

}
