package com.sj.friendcircle.lib.mvp;

/**
 * Created by huluhong on 2018/7/7.
 * <p>
 * presenter基类接口
 */

public interface IBasePresenter<V extends IBaseView> {

    /**
     * 绑定view
     */
    IBasePresenter<V> bindView(V view);

    /**
     * 取消绑定
     */
    IBasePresenter<V> unbindView();

}
