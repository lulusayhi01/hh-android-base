package com.sj.friendcircle.ui.base.adapter;

import android.view.View;

/**
 * Created by huluhong on 2018/7/1.
 */

public interface OnRecyclerViewItemClickListener<T> {
    void onItemClick(View v, int position, T data);
}
