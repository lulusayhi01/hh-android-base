package com.sj.friendcircle.ui.widget.commentwidget;

import android.support.annotation.NonNull;

/**
 * Created by huluhong on 2018/7/27.
 * <p>
 * 评论控件点击
 */

public interface OnCommentUserClickListener {
    void onCommentClicked(@NonNull IComment comment, CharSequence text);
}
