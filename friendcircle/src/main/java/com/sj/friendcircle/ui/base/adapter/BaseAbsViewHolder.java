package com.sj.friendcircle.ui.base.adapter;

import android.view.View;

/**
 * Created by huluhong on 2018/7/9.
 */

public interface BaseAbsViewHolder {
    void onInFlate(View v);
}
