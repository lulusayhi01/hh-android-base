package com.sj.friendcircle.ui.base.adapter;

/**
 * Created by huluhong on 2018/4/10.
 * <p>
 * multitype
 */
public interface MultiType {

    int getItemType();

}
