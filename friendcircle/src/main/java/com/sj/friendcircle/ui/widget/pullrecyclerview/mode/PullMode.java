package com.sj.friendcircle.ui.widget.pullrecyclerview.mode;

/**
 * Created by huluhong on 2018/7/27.
 */

public enum PullMode {
    NONE, FROM_START, FROM_BOTTOM
}
