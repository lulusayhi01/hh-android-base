package com.sj.friendcircle.ui.widget.pullrecyclerview.wrapperadapter;

import android.support.v7.widget.RecyclerView;

/**
 * Created by huluhong on 2018/7/27.
 */

public interface WrapperRecyclerAdapter {

    RecyclerView.Adapter getWrappedAdapter();
}
