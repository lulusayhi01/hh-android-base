package com.sj.friendcircle.ui.widget.pullrecyclerview.interfaces;

/**
 * Created by huluhong on 2018/7/29.
 *
 * 下拉和上拉的回调
 */

public interface OnRefreshListener2 {
    void onRefresh();
    void onLoadMore();
}
