# hh-android-base

#### 介绍
医疗安卓版

#### 软件架构
软件架构说明

Brvah——一个强大的Adapter框架
https://www.jianshu.com/p/cd9058ce74f6

【FasterDialog】可能是Android上最好用的Dialog框架
https://www.jianshu.com/p/c6143fe91c2c

腾讯开源的Android UI框架——QMUI Android
https://blog.csdn.net/urDFmQCUL2/article/details/78738609

Android酷炫实用的开源框架（UI框架）
https://www.runoob.com/w3cnote/android-ui-framework.html

XUI 一个简洁而优雅的Android原生UI框架，解放你的双手
https://www.jianshu.com/p/c39b2f0f043a

Android 开源UI框架汇总
https://blog.csdn.net/qq_42250299/article/details/91490851

#### 安装教程

1. xxxx
2. xxxx
3. xxxx

#### 使用说明

1. xxxx
2. xxxx
3. xxxx

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)