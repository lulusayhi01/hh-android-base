package com.mbt.deal.bean;

/**
 * 创建日期：2018/8/16 on 16:33
 * 描述:
 * 作者:王甜
 */
public class TransActionRecordBean {

    /**
     * createtime : 2018-08-16 19:46:13
     * dealstatus : 4202
     * dealtype : 4101
     * dealtypeid : 122
     * id : 96
     * price : 28.0
     * status : 1
     * tuserid : 25
     * typedetailid : 4301
     * userHeadImage : http://oss-aliyun-liululu.oss-cn-shanghai.aliyuncs.com/upload/153389023664010270.jpg
     * userId : 25
     * userName : 涓涓
     * userPhone : 13501031194
     */

    private String createtime;
    private int dealstatus;
    private int dealtype;
    private int dealtypeid;
    private int id;
    private double price;
    private int status;
    private int tuserid;
    private int typedetailid;
    private String userHeadImage;
    private int userId;
    private String userName;
    private String userPhone;
    private double pricefront;

    public double getPricefront() {
        return pricefront;
    }

    public void setPricefront(double pricefront) {
        this.pricefront = pricefront;
    }

    public String getCreatetime() {
        return createtime;
    }

    public void setCreatetime(String createtime) {
        this.createtime = createtime;
    }

    public int getDealstatus() {
        return dealstatus;
    }

    public void setDealstatus(int dealstatus) {
        this.dealstatus = dealstatus;
    }

    public int getDealtype() {
        return dealtype;
    }

    public void setDealtype(int dealtype) {
        this.dealtype = dealtype;
    }

    public int getDealtypeid() {
        return dealtypeid;
    }

    public void setDealtypeid(int dealtypeid) {
        this.dealtypeid = dealtypeid;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getTuserid() {
        return tuserid;
    }

    public void setTuserid(int tuserid) {
        this.tuserid = tuserid;
    }

    public int getTypedetailid() {
        return typedetailid;
    }

    public void setTypedetailid(int typedetailid) {
        this.typedetailid = typedetailid;
    }

    public String getUserHeadImage() {
        return userHeadImage;
    }

    public void setUserHeadImage(String userHeadImage) {
        this.userHeadImage = userHeadImage;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserPhone() {
        return userPhone;
    }

    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone;
    }
}
