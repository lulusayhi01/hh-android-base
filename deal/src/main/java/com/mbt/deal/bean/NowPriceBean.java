package com.mbt.deal.bean;

/**
 * @author "赵盼龙"
 * @createtime：2018/9/11 22:50
 */
public class NowPriceBean {
    private double pricenow;

    public double getPricenow() {
        return pricenow;
    }

    public void setPricenow(double pricenow) {
        this.pricenow = pricenow;
    }
}
