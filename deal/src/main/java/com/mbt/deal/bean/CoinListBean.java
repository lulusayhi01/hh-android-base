package com.mbt.deal.bean;

import java.io.Serializable;

/**
 * @author Administrator
 * @createtime：2018/8/16 1:06
 */
public class CoinListBean implements Serializable{


    /**
     * content : 脉宝币
     * id : 1
     * name : 脉宝币
     * remarker : 测试脉宝币
     * status : 1
     */

    private String content;
    private int id;
    private String name;
    private String remarker;
    private int status;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRemarker() {
        return remarker;
    }

    public void setRemarker(String remarker) {
        this.remarker = remarker;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
