package com.mbt.deal.bean;

/**
 * @author Administrator
 * @createtime：2018/8/16 11:40
 */
public class OrderBean {


    /**
     * coinId : 1
     * coinName : 脉宝币
     * id : 70
     * number : 1
     * price : 103
     * service : 1
     * total : 103
     * userHeadImage : http://oss-aliyun-liululu.oss-cn-shanghai.aliyuncs.com/upload/153311359507510081.jpg
     * userId : 27
     * userName : 123456
     * userPhone : 13500000001
     */

    private int coinId;
    private String coinName;
    private int id;
    private String number;
    private String price;
    private String service;
    private String total;
    private String userHeadImage;
    private int userId;
    private String userName;
    private String userPhone;
    private String starttime;

    public String getStarttime() {
        return starttime;
    }

    public void setStarttime(String starttime) {
        this.starttime = starttime;
    }

    public int getCoinId() {
        return coinId;
    }

    public void setCoinId(int coinId) {
        this.coinId = coinId;
    }

    public String getCoinName() {
        return coinName;
    }

    public void setCoinName(String coinName) {
        this.coinName = coinName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getUserHeadImage() {
        return userHeadImage;
    }

    public void setUserHeadImage(String userHeadImage) {
        this.userHeadImage = userHeadImage;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserPhone() {
        return userPhone;
    }

    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone;
    }
}
