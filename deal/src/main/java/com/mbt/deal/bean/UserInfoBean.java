package com.mbt.deal.bean;

import java.util.List;

/**
 * @author Administrator
 * @createtime：2018/8/17 11:11
 */
public class UserInfoBean {


    /**
     * belongcode : cOYLkaNx
     * channelid : 0
     * coldmoney : 0
     * creattime : 2018-08-16 17:14:34
     * fetchmoney : 0
     * friendStatus : 6001
     * headimage : http://p6nlllxof.bkt.clouddn.com/upload/20180705/3f668359c2354f2298f978c6266b560d.jpg
     * id : 46
     * investmoney : 0
     * inviterid : 0
     * isf : 63
     * level : 0
     * loginnum : 0
     * logintime : 1534410874
     * name : 15670935621
     * passwd : 4f0d3dafef4812269e0adaf5cc71f2c3
     * phone : 15670935621
     * rapnum : 0
     * registermoney : 10
     * remark : 无
     * returnmoney : 0
     * status : 1
     * summoney : 10
     * token : Y9uLM85gPiyT83OnMjRi7izeU8UbdXP4rZJRD+mIPcz6Kzsb8ApfLNCT2n1rRFL2hPqoDus35j5z55a5lR7i3Q==
     * trendsImg : []
     * usablemoney : 38.5
     * userInfostatus : 3302
     * waitactivemoney : 0
     */

    private String belongcode;
    private int channelid;
    private String coldmoney;
    private String creattime;
    private String fetchmoney;
    private int friendStatus;
    private String headimage;
    private int id;
    private String investmoney;
    private int inviterid;
    private int isf;
    private int level;
    private int loginnum;
    private String logintime;
    private String name;
    private String passwd;
    private String phone;
    private String rapnum;
    private String registermoney;
    private String remark;
    private String returnmoney;
    private int status;
    private String summoney;
    private String token;
    private String usablemoney;
    private int userInfostatus;
    private String waitactivemoney;
    private List<?> trendsImg;

    public String getBelongcode() {
        return belongcode;
    }

    public void setBelongcode(String belongcode) {
        this.belongcode = belongcode;
    }

    public int getChannelid() {
        return channelid;
    }

    public void setChannelid(int channelid) {
        this.channelid = channelid;
    }

    public String getColdmoney() {
        return coldmoney;
    }

    public void setColdmoney(String coldmoney) {
        this.coldmoney = coldmoney;
    }

    public String getCreattime() {
        return creattime;
    }

    public void setCreattime(String creattime) {
        this.creattime = creattime;
    }

    public String getFetchmoney() {
        return fetchmoney;
    }

    public void setFetchmoney(String fetchmoney) {
        this.fetchmoney = fetchmoney;
    }

    public int getFriendStatus() {
        return friendStatus;
    }

    public void setFriendStatus(int friendStatus) {
        this.friendStatus = friendStatus;
    }

    public String getHeadimage() {
        return headimage;
    }

    public void setHeadimage(String headimage) {
        this.headimage = headimage;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getInvestmoney() {
        return investmoney;
    }

    public void setInvestmoney(String investmoney) {
        this.investmoney = investmoney;
    }

    public int getInviterid() {
        return inviterid;
    }

    public void setInviterid(int inviterid) {
        this.inviterid = inviterid;
    }

    public int getIsf() {
        return isf;
    }

    public void setIsf(int isf) {
        this.isf = isf;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getLoginnum() {
        return loginnum;
    }

    public void setLoginnum(int loginnum) {
        this.loginnum = loginnum;
    }

    public String getLogintime() {
        return logintime;
    }

    public void setLogintime(String logintime) {
        this.logintime = logintime;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPasswd() {
        return passwd;
    }

    public void setPasswd(String passwd) {
        this.passwd = passwd;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getRapnum() {
        return rapnum;
    }

    public void setRapnum(String rapnum) {
        this.rapnum = rapnum;
    }

    public String getRegistermoney() {
        return registermoney;
    }

    public void setRegistermoney(String registermoney) {
        this.registermoney = registermoney;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getReturnmoney() {
        return returnmoney;
    }

    public void setReturnmoney(String returnmoney) {
        this.returnmoney = returnmoney;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getSummoney() {
        return summoney;
    }

    public void setSummoney(String summoney) {
        this.summoney = summoney;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getUsablemoney() {
        return usablemoney;
    }

    public void setUsablemoney(String usablemoney) {
        this.usablemoney = usablemoney;
    }

    public int getUserInfostatus() {
        return userInfostatus;
    }

    public void setUserInfostatus(int userInfostatus) {
        this.userInfostatus = userInfostatus;
    }

    public String getWaitactivemoney() {
        return waitactivemoney;
    }

    public void setWaitactivemoney(String waitactivemoney) {
        this.waitactivemoney = waitactivemoney;
    }

    public List<?> getTrendsImg() {
        return trendsImg;
    }

    public void setTrendsImg(List<?> trendsImg) {
        this.trendsImg = trendsImg;
    }
}
