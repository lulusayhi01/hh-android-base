package com.mbt.deal.bean;

import java.io.Serializable;

/**
 * @author Administrator
 * @createtime：2018/8/21 18:07
 */
public class MessageBean implements Serializable{

    /**
     * id : 1
     * title : 天气预报
     */

    private int id;
    private String title;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
