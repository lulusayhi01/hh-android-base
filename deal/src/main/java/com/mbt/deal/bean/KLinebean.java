package com.mbt.deal.bean;

/**
 * @author Administrator
 * @createtime：2018/8/23 17:53
 */
public class KLinebean {

    /**
     * createtime : 2018-08-20 17:38:00
     * pricelow : 1.1
     * pricehigh : 1.6
     */

    private String createtime;
    private String pricelow;
    private String pricehigh;

    public String getCreatetime() {
        return createtime;
    }

    public String getshowTime(int timeType) {
        if (timeType == 1) {
            return createtime.split(" ")[1].substring(0, 5);
        }
        return createtime.split(" ")[0].substring(5);
    }

    public void setCreatetime(String createtime) {
        this.createtime = createtime;
    }


    public String getPricelow() {
        return pricelow;
    }

    public void setPricelow(String pricelow) {
        this.pricelow = pricelow;
    }

    public String getPricehigh() {
        return pricehigh;
    }

    public void setPricehigh(String pricehigh) {
        this.pricehigh = pricehigh;
    }
}
