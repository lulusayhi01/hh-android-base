package com.mbt.deal.bean;

/**
 * @author Administrator
 * @createtime：2018/8/16 1:12
 */
public class CoinPriceBean {

    /**
     * pricehigh : 99
     * pricelow : 1
     * pricenow : 6
     */

    private String pricehigh;
    private String pricelow;
    private String pricenow;

    public String getPricehigh() {
        return pricehigh;
    }

    public void setPricehigh(String pricehigh) {
        this.pricehigh = pricehigh;
    }

    public String getPricelow() {
        return pricelow;
    }

    public void setPricelow(String pricelow) {
        this.pricelow = pricelow;
    }

    public String getPricenow() {
        return pricenow;
    }

    public void setPricenow(String pricenow) {
        this.pricenow = pricenow;
    }
}
