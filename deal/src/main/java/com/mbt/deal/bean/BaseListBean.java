package com.mbt.deal.bean;

import java.util.List;

/**
 * @author Administrator
 * @createtime：2018/8/16 1:03
 */
public class BaseListBean<T> extends BaseBean {
    public List<T> data;
    public int count;
}
