package com.mbt.deal.bean;

/**
 * 创建日期：2018/8/16 on 20:32
 * 描述:
 * 作者:王甜
 */
public class OrderListBean {

    /**
     * coinId : 1
     * coinName : 脉宝币
     * id : 122
     * number : 10
     * price : 2.8
     * service : 2
     * total : 28.0
     * userHeadImage : http://oss-aliyun-liululu.oss-cn-shanghai.aliyuncs.com/upload/153389023664010270.jpg
     * userId : 25
     * userName : 涓涓
     * userPhone : 13501031194
     * userRemark : 手动添加好友（重新注册后自动添加）
     */

    private int coinId;
    private String coinName;
    private int id;
    private String number;
    private String price;
    private String service;
    private String total;
    private String userHeadImage;
    private int userId;
    private String userName;
    private String userPhone;
    private String userRemark;
    private String starttime;
    private int type;
    private int orderstatus;
    private String numberz;

    public String getNumberz() {
        return numberz;
    }

    public void setNumberz(String numberz) {
        this.numberz = numberz;
    }

    public int getOrderstatus() {
        return orderstatus;
    }

    public void setOrderstatus(int orderstatus) {
        this.orderstatus = orderstatus;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getType() {
        return type;
    }

    public void setStarttime(String starttime) {
        this.starttime = starttime;
    }

    public String getStarttime() {
        return starttime;
    }

    public int getCoinId() {
        return coinId;
    }

    public void setCoinId(int coinId) {
        this.coinId = coinId;
    }

    public String getCoinName() {
        return coinName;
    }

    public void setCoinName(String coinName) {
        this.coinName = coinName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getUserHeadImage() {
        return userHeadImage;
    }

    public void setUserHeadImage(String userHeadImage) {
        this.userHeadImage = userHeadImage;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserPhone() {
        return userPhone;
    }

    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone;
    }

    public String getUserRemark() {
        return userRemark;
    }

    public void setUserRemark(String userRemark) {
        this.userRemark = userRemark;
    }
}
