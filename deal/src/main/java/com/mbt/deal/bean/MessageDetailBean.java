package com.mbt.deal.bean;

/**
 * @author Administrator
 * @createtime：2018/8/21 20:41
 */
public class MessageDetailBean {

    /**
     * content : <a href='http://www.taobao.com'>测试连接</a> ,  今天天气还不错
     * createtime : 2018-08-01
     * id : 1
     * readnum : 0
     * status : 1
     * title : 天气预报
     */

    private String content;
    private String createtime;
    private int id;
    private int readnum;
    private int status;
    private String title;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getCreatetime() {
        return createtime;
    }

    public void setCreatetime(String createtime) {
        this.createtime = createtime;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getReadnum() {
        return readnum;
    }

    public void setReadnum(int readnum) {
        this.readnum = readnum;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
