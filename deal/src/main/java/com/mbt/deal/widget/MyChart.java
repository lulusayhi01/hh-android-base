package com.mbt.deal.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;

import lecho.lib.hellocharts.view.LineChartView;

/**
 * @author Administrator
 * @createtime：2018/8/14 15:51
 */
public class MyChart extends LineChartView {
    public MyChart(Context context) {
        super(context);
    }

    public MyChart(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MyChart(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return super.onTouchEvent(event);
    }
}
