package com.mbt.deal.util;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.AbsoluteSizeSpan;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import java.math.RoundingMode;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author Administrator
 * @createtime：2018/8/16 0:55
 */
public class MyUtils {
    public static String getToken(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("config", 0);
        return sharedPreferences.getString("mbtToken", (String) null);
    }

    /**
     * 字符串start和end之间字符串高亮显示
     *
     * @param tv
     * @param str
     * @param start
     * @param end
     * @param size
     * @param color
     */
    public static void setText(TextView tv, String str, int start, int end, int size, int color) {
        if (start < 0 || end < 0) {
            tv.setText(str);
            return;
        }
        SpannableString spanString = new SpannableString(str);
        AbsoluteSizeSpan span_size = new AbsoluteSizeSpan(size);
        ForegroundColorSpan span_color = new ForegroundColorSpan(color);
        spanString.setSpan(span_size, start, end, Spannable.SPAN_EXCLUSIVE_INCLUSIVE);
        spanString.setSpan(span_color, start, end, Spannable.SPAN_EXCLUSIVE_INCLUSIVE);
        tv.setText(spanString);
    }
    public static void hideKeyBoard(View view, Activity act) {
        //隐藏键盘
        InputMethodManager imm = (InputMethodManager) act.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0); //强制隐藏键盘
    }
    public static String formatDouble(double d){
        NumberFormat nf = NumberFormat.getNumberInstance();


        // 保留两位小数
        nf.setMaximumFractionDigits(2);


        // 如果不需要四舍五入，可以使用RoundingMode.DOWN
        nf.setRoundingMode(RoundingMode.UP);


        return nf.format(d);
    }
    public static String NowDate(){
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd HH:mm:ss");
        return  dateFormat.format(new Date());
    }
}
