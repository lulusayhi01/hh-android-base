package com.mbt.deal.util;

import android.content.Context;
import android.widget.Toast;

/**
 * @author Administrator
 * @createtime：2018/8/21 19:19
 */
public class ToastUtil {
    public static void makeToast(Context context, String msg) {
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }
}
