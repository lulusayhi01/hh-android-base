package com.mbt.deal.util;

import android.widget.ImageView;

import com.mbt.deal.R;

import io.rong.imageloader.core.DisplayImageOptions;
import io.rong.imageloader.core.ImageLoader;
import io.rong.imageloader.core.display.FadeInBitmapDisplayer;

/**
 * @author Administrator
 * @createtime：2018/8/16 9:50
 */
public class ImageLoadUtil {
    private static DisplayImageOptions options;

    public static void loadImage(String url, ImageView imageView) {
        if (options == null) {
            options = new DisplayImageOptions.Builder()
                    .showImageForEmptyUri(R.mipmap.de_default_portrait)
                    .showImageOnFail(R.mipmap.de_default_portrait)
                    .showImageOnLoading(R.mipmap.de_default_portrait)
                    .displayer(new FadeInBitmapDisplayer(300))
                    .cacheInMemory(true)
                    .cacheOnDisk(true)
                    .build();
        }
        ImageLoader.getInstance().displayImage(url, imageView, options);
    }
}
