package com.mbt.deal.util;

import android.util.SparseArray;
import android.view.View;

/**
 * 创建日期：2018/8/14 on 19:35
 * 描述:
 * 作者:王甜
 */
public class HoloderUtil {
    /**
     * ListView，GridView中使用到的holder
     *
     * @param view
     * @param id
     * @param <T>
     * @return
     */
    public static <T extends View> T getHolderView(View view, int id) {
        SparseArray<View> viewHolder = (SparseArray<View>) view.getTag();
        View childView;
        if (viewHolder == null) {
            viewHolder = new SparseArray<View>();
            view.setTag(viewHolder);
            // 先创建的ViewHolder自然没有缓存View所以不用判断
            childView = view.findViewById(id);
            viewHolder.put(id, childView);
        } else {
            childView = viewHolder.get(id);
            if (childView == null) {
                childView = view.findViewById(id);
                viewHolder.put(id, childView);
            }
        }
        return (T) childView;
    }
}
