package com.mbt.deal.ui.widgets;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ListView;

/**
 * 创建日期：2018/8/16 on 19:59
 * 描述:
 * 作者:王甜
 */
public class OrderListView extends ListView {

    Context context;

    public OrderListView(Context context) {
        super(context);
        initData(context);
    }

    public OrderListView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initData(context);

    }

    public OrderListView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initData(context);
    }

    private void initData(Context context) {
        this.context = context;
    }
}
