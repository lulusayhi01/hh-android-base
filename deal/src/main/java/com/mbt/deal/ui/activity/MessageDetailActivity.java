package com.mbt.deal.ui.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mbt.deal.R;
import com.mbt.deal.api.Api;
import com.mbt.deal.api.ApiServer;
import com.mbt.deal.api.RxResultHelper;
import com.mbt.deal.api.RxSchedulers;
import com.mbt.deal.api.RxSubscriber;
import com.mbt.deal.bean.BaseDataBean;
import com.mbt.deal.bean.MessageDetailBean;
import com.mbt.deal.ui.widgets.LoadDialog;
import com.mbt.deal.util.MyUtils;
import com.mbt.deal.widget.MyChart;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Administrator
 * @createtime：2018/8/21 19:44
 */
public class MessageDetailActivity extends BaseActivity {
    private TextView tvTitle;
    private LinearLayout ll_back;
    private WebView webView;
    private int id;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message_detail);
        tvTitle = findViewById(R.id.txt_title);
        ll_back = findViewById(R.id.ll_back);
        tvTitle.setText("公告详细");
        ll_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        webView = findViewById(R.id.webview);
        id = getIntent().getIntExtra("id", 0);
        reciveData();
    }

    private void reciveData() {
        LoadDialog.show(act);
        Map map = new HashMap();
        map.put("id", id);
        Api.getServer(ApiServer.class).getMessageDetail(MyUtils.getToken(act), map).compose(RxResultHelper.<BaseDataBean<MessageDetailBean>>handleResult())
                .compose(RxSchedulers.<BaseDataBean<MessageDetailBean>>io_main()).subscribe(new RxSubscriber<BaseDataBean<MessageDetailBean>>(act) {

            @Override
            public void _onNext(BaseDataBean<MessageDetailBean> stringBaseDataBean) {
                webView.loadData(stringBaseDataBean.data.getContent(),"text/html", "UTF -8");
            }

            @Override
            public void _onError(String message) {

            }

            @Override
            public void onCompleted() {
                LoadDialog.dismiss(act);
            }
        });
    }
}
