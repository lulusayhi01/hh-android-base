package com.mbt.deal.ui.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mbt.deal.R;
import com.mbt.deal.R2;
import com.mbt.deal.api.ApiServer;
import com.mbt.deal.ui.contract.RechargeContract;
import com.mbt.deal.ui.model.RechargeModel;
import com.mbt.deal.ui.presenter.RechargePresenter;
import com.mbt.deal.ui.widgets.LoadDialog;
import com.mbt.deal.util.NToast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * 创建日期：2018/8/17 on 16:32
 * 描述:
 * 作者:王甜
 */
public class RechargeActivity extends BaseActivity<RechargePresenter, RechargeModel> implements RechargeContract.View {

    @BindView(R2.id.ll_back)
    LinearLayout llBack;
    @BindView(R2.id.txt_title)
    TextView txtTitle;
    @BindView(R2.id.iv_right)
    View ivRight;
    @BindView(R2.id.ll_right_menu)
    LinearLayout llRightMenu;
    @BindView(R2.id.txt_right)
    TextView txtRight;
    @BindView(R2.id.ll_right_text)
    LinearLayout llRightText;
    @BindView(R2.id.txt_money_desc)
    TextView txtMoneyDesc;
    @BindView(R2.id.edit_charge_money)
    EditText editChargeMoney;
    @BindView(R2.id.txt_charge)
    TextView txtCharge;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recharge);
        ButterKnife.bind(this);
        mPresenter.setVM(this, mModel);

        initData();
    }

    private void initData() {
        txtTitle.setText("充值");
        llRightText.setVisibility(View.VISIBLE);

        editChargeMoney.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.toString().contains(".")) {
                    if (charSequence.length() - 1 - charSequence.toString().indexOf(".") > 2) {
                        charSequence = charSequence.toString().subSequence(0, charSequence.toString().indexOf(".") + 3);
                        editChargeMoney.setText(charSequence);
                        editChargeMoney.setSelection(charSequence.length());
                    }
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    @OnClick({R2.id.ll_back, R2.id.ll_right_text, R2.id.txt_charge})
    public void onViewClicked(View view) {
        int i = view.getId();
        if (i == R.id.ll_back) {
            finish();
        } else if (i == R.id.ll_right_text) {

        } else if (i == R.id.txt_charge) {
            if (TextUtils.isEmpty(editChargeMoney.getText().toString())) {
                NToast.longToast(act, "请输入充值金额");
                return;
            }
            LoadDialog.show(act);
            mPresenter.recharge(editChargeMoney.getText().toString());
        }
    }

    @Override
    public void chargeSuccess(String jsonValues) {
        LoadDialog.dismiss(act);
        finish();
    }

    @Override
    public void onError(String errorMsg) {
        LoadDialog.dismiss(act);
        NToast.longToast(act, errorMsg);
    }
}
