package com.mbt.deal.ui.widgets;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

/**
 * Created by 王甜 on 2017/5/26.
 */

public class BottomLineRelativeLayout extends RelativeLayout {

    Paint mPaint;

    public BottomLineRelativeLayout(Context context) {
        super(context);
        initPaint();
    }

    public BottomLineRelativeLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        initPaint();
    }

    public BottomLineRelativeLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initPaint();
    }

    private void initPaint() {
        setWillNotDraw(false);
        mPaint = new Paint();
        mPaint.setColor(0xFFEDEDED);
        mPaint.setStyle(Paint.Style.FILL);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        int height = getHeight() - 1;
        canvas.drawLine(0, height, getWidth(), height, mPaint);
    }
}
