package com.mbt.deal.ui.presenter;

import com.mbt.deal.api.RxSubscriber;
import com.mbt.deal.bean.BaseDataBean;
import com.mbt.deal.ui.contract.RechargeContract;
import com.mbt.deal.util.MyUtils;

import java.util.HashMap;
import java.util.Map;

import rx.Subscription;

/**
 * @author Administrator
 * @createtime：2018/8/16 0:49
 */
public class RechargePresenter extends RechargeContract.Presenter {

    @Override
    public void recharge(String price) {
        Map<String, String> map = new HashMap<>();
        map.put("price", price);

        mModel.recharge(MyUtils.getToken(mContext), map).subscribe(new RxSubscriber<BaseDataBean<String>>(mContext) {
            @Override
            public void _onNext(BaseDataBean<String> stringBaseDataBean) {
                mView.chargeSuccess(stringBaseDataBean.data);
            }

            @Override
            public void _onError(String message) {
                mView.onError(message);
            }

            @Override
            public void onCompleted() {

            }
        });

    }
}
