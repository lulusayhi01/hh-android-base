package com.mbt.deal.ui.fragment;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.RadioButton;

import com.github.jdsjlzx.ItemDecoration.DividerDecoration;
import com.github.jdsjlzx.recyclerview.LRecyclerView;
import com.github.jdsjlzx.recyclerview.LRecyclerViewAdapter;
import com.mbt.deal.R;
import com.mbt.deal.ui.adapter.DealAdapter;
import com.mbt.deal.ui.adapter.RecycleViewDivider;

/**
 * @author Administrator
 * @createtime：2018/8/13 10:53
 */
public class DealbuinessFragmet extends BaseFragment {
    private LRecyclerView recyclerView;
    private View type_layout;
    private RadioButton rb_money_buy;
    private RadioButton rb_money_sale;
    DividerDecoration divider;
    private DealAdapter dealAdapter;
    private boolean isshowType = false;

    @Override
    protected int getLayoutResource() {
        return R.layout.deal_business_frgment;
    }

    @Override
    public void initPresenter() {

    }

    @Override
    protected void initView(View view) {
        recyclerView = view.findViewById(R.id.recycle_view);
        type_layout = view.findViewById(R.id.type_layout);
        rb_money_buy = view.findViewById(R.id.rb_money_buy);
        rb_money_sale = view.findViewById(R.id.rb_money_sale);
        rb_money_buy.setChecked(true);
        //设置头部加载颜色
        recyclerView.setHeaderViewColor(R.color.white, R.color.white, R.color.trance);
        //设置底部加载颜色
        recyclerView.setFooterViewColor(R.color.refresh_bottom, R.color.refresh_bottom, R.color.white);
        recyclerView.setFooterViewHint("正在加载更多", "没有更多数据", "网络不给力啊，点击再试一次吧");
        divider = new DividerDecoration.Builder(getActivity())
                .setHeight(R.dimen.default_divider_height)
                .setColorResource(R.color.iteam_line_color)
                .build();
        recyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        recyclerView.addItemDecoration(divider);
        dealAdapter = new DealAdapter(getActivity());
        LRecyclerViewAdapter lRecyclerViewAdapter = new LRecyclerViewAdapter(dealAdapter);
        recyclerView.setAdapter(lRecyclerViewAdapter);
        recyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                LinearLayoutManager l = (LinearLayoutManager) recyclerView.getLayoutManager();
                int adapterNowPos = l.findFirstVisibleItemPosition();
                if (adapterNowPos < 4) {
                    if (type_layout.getVisibility() == View.VISIBLE) {
                        type_layout.setVisibility(View.GONE);
                    }
                } else if (adapterNowPos > 4) {
                    if (type_layout.getVisibility() == View.GONE) {
                        type_layout.setVisibility(View.VISIBLE);
                    }
                } else if (adapterNowPos == 4) {
                    if (type_layout.getVisibility() == View.GONE) {
                        type_layout.setVisibility(View.VISIBLE);
                    }
                }
            }
        });
    }
}
