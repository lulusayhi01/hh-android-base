package com.mbt.deal.ui.presenter;

import com.mbt.deal.api.RxSubscriber;
import com.mbt.deal.bean.BaseDataBean;
import com.mbt.deal.ui.contract.OrderSaleContract;
import com.mbt.deal.util.MyUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Administrator
 * @createtime：2018/8/16 0:49
 */
public class OrderBuySalePresenter extends OrderSaleContract.Presenter {

    @Override
    public void sale(String coinid, String number, String price) {
        Map<String, String> saleMap = new HashMap<>();
        saleMap.put("coinid", coinid);
        saleMap.put("number", number);
        saleMap.put("price", price);
        mModel.sale(MyUtils.getToken(mContext), saleMap).subscribe(new RxSubscriber<BaseDataBean<String>>(mContext) {
            @Override
            public void _onNext(BaseDataBean<String> stringBaseDataBean) {
                mView.sale(stringBaseDataBean.data);
            }

            @Override
            public void _onError(String message) {
                mView.onError(message);
            }

            @Override
            public void onCompleted() {

            }
        });

    }

    @Override
    public void buy(String coinid, String number, String price) {
        Map<String, String> buyMap = new HashMap<>();
        buyMap.put("coinid", coinid);
        buyMap.put("number", number);
        buyMap.put("price", price);
        mModel.buy(MyUtils.getToken(mContext), buyMap).subscribe(new RxSubscriber<BaseDataBean<String>>(mContext) {
            @Override
            public void _onNext(BaseDataBean<String> stringBaseDataBean) {
                mView.sale(stringBaseDataBean.data);
            }

            @Override
            public void _onError(String message) {
                mView.onError(message);
            }

            @Override
            public void onCompleted() {

            }
        });
    }
}
