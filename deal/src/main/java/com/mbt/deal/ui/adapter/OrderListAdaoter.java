package com.mbt.deal.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.alibaba.android.arouter.launcher.ARouter;
import com.mbt.deal.R;
import com.mbt.deal.api.Api;
import com.mbt.deal.api.ApiServer;
import com.mbt.deal.api.RxResultHelper;
import com.mbt.deal.api.RxSchedulers;
import com.mbt.deal.api.RxSubscriber;
import com.mbt.deal.bean.BaseBean;
import com.mbt.deal.bean.BaseDataBean;
import com.mbt.deal.bean.OrderListBean;
import com.mbt.deal.ui.activity.OrderListActivity;
import com.mbt.deal.ui.widgets.LoadDialog;
import com.mbt.deal.ui.widgets.PhotoAlbum;
import com.mbt.deal.util.HoloderUtil;
import com.mbt.deal.util.MyUtils;
import com.mbt.deal.widget.MyChart;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 创建日期：2018/8/16 on 20:05
 * 描述:
 * 作者:王甜
 */
public class OrderListAdaoter extends BaseAdapter {

    Context context;

    List<OrderListBean> orderListBeans = new ArrayList<>();
    private View.OnClickListener sureZhuangzhang;

    public OrderListAdaoter(Context context) {
        this.context = context;
    }

    @Override
    public int getCount() {
        if (orderListBeans == null) {
            return 0;
        }
        return orderListBeans.size();
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        if (view == null || view.getTag() == null) {
            view = LayoutInflater.from(context).inflate(R.layout.adapter_order_transaction, null);
        }

        TextView txt_type = HoloderUtil.getHolderView(view, R.id.txt_type);

        TextView txt_one_price = HoloderUtil.getHolderView(view, R.id.txt_one_price);

        TextView txt_total_money = HoloderUtil.getHolderView(view, R.id.txt_total_money);
        TextView txt_maibao_count = HoloderUtil.getHolderView(view, R.id.txt_maibao_count);
        TextView txt_create_time = HoloderUtil.getHolderView(view, R.id.txt_create_time);
        TextView tv_jiaoyi_type = HoloderUtil.getHolderView(view, R.id.tv_jiaoyi_type);
        TextView upload_view = HoloderUtil.getHolderView(view, R.id.upload_view);
        TextView sure_zhuan_zhang = HoloderUtil.getHolderView(view, R.id.sure_zhuan_zhang);
        TextView zhanghu = HoloderUtil.getHolderView(view, R.id.zhanghu);
        LinearLayout ll_gongneng = HoloderUtil.getHolderView(view, R.id.ll_gongneng);
        LinearLayout ll_gongneng2 = HoloderUtil.getHolderView(view, R.id.ll_gongneng2);
        TextView sure_zhuan_zhang_daozhang = HoloderUtil.getHolderView(view,R.id.sure_zhuan_zhang_daozhang);
        final OrderListBean curBean = orderListBeans.get(position);
        if (curBean != null) {
            if (curBean.getType() == 2) {
                txt_type.setText("出售");
                tv_jiaoyi_type.setText("出售总价:");
            } else if (curBean.getType() == 1) {
                txt_type.setText("购买");
                tv_jiaoyi_type.setText("购买总价:");
            }
            txt_one_price.setText("单价：" + curBean.getPrice() + "$");

            txt_total_money.setText(curBean.getTotal());
            txt_maibao_count.setText(curBean.getCoinName() + ":" + curBean.getNumberz() + "个");
            txt_create_time.setText(curBean.getStarttime());
        }
        if (curBean.getOrderstatus()== 3107 && curBean.getType() == 1) {
            ll_gongneng.setVisibility(View.VISIBLE);
        } else {
            ll_gongneng.setVisibility(View.GONE);
        }

        if (curBean.getOrderstatus() == 3108 && curBean.getType() == 2) {
            ll_gongneng2.setVisibility(View.VISIBLE);
        } else {
            ll_gongneng2.setVisibility(View.GONE);
        }
        zhanghu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ARouter.getInstance().build("/personal/PersonAssountListActivity").withString("mbtToken",MyUtils.getToken(context)).withString("phone",curBean.getUserPhone()).navigation();
            }
        });
        upload_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PhotoAlbum photoAlbum = new PhotoAlbum(context);
                photoAlbum.open();
                OrderListActivity.orderId = curBean.getId();
            }
        });
        sure_zhuan_zhang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Map map = new HashMap();
                map.put("id", curBean.getId());
                LoadDialog.show(context);
                Api.getServer(ApiServer.class).surezhuangzhang(MyUtils.getToken(context), map).compose(RxSchedulers.io_main()).compose(RxResultHelper.handleResult())
                        .subscribe(new RxSubscriber<BaseDataBean>(context) {


                            @Override
                            public void _onNext(BaseDataBean baseDataBean) {
                                if (refershListen != null) {
                                    refershListen.refresh();
                                }
                            }

                            @Override
                            public void _onError(String message) {

                            }

                            @Override
                            public void onCompleted() {
                                LoadDialog.dismiss(context);
                            }
                        });
            }
        });
        sure_zhuan_zhang_daozhang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Map map = new HashMap();
                map.put("id", curBean.getId());
                LoadDialog.show(context);
                Api.getServer(ApiServer.class).daozhang(MyUtils.getToken(context), map).compose(RxSchedulers.io_main()).compose(RxResultHelper.handleResult())
                        .subscribe(new RxSubscriber<BaseDataBean>(context) {


                            @Override
                            public void _onNext(BaseDataBean baseDataBean) {
                                if (refershListen != null) {
                                    refershListen.refresh();
                                }
                            }

                            @Override
                            public void _onError(String message) {

                            }

                            @Override
                            public void onCompleted() {
                                LoadDialog.dismiss(context);
                            }
                        });
            }
        });
        return view;
    }


    public void notifyData(List<OrderListBean> orderListBeans) {
        this.orderListBeans = orderListBeans;
        notifyDataSetChanged();
    }

    public void addAllData(List<OrderListBean> list) {
        orderListBeans.addAll(list);
        notifyDataSetChanged();
    }

    public void setRefreshListen(refershListen listen) {
        this.refershListen = listen;
    }

    private refershListen refershListen;

    public interface refershListen {
        void refresh();
    }

    public void clearData(){
        orderListBeans.clear();
        notifyDataSetChanged();
    }
}
