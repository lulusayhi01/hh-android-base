package com.mbt.deal.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.mbt.deal.R;
import com.mbt.deal.bean.TransActionRecordBean;
import com.mbt.deal.util.Arith;

import java.util.ArrayList;
import java.util.List;

/**
 * 创建日期：2018/8/14 on 19:46
 * 描述:
 * 作者:王甜
 */
public class TransActionRecordAdapter extends BaseAdapter {

    Context mContext;

    List<TransActionRecordBean> transActionList = new ArrayList<>();

    public TransActionRecordAdapter(Context context) {
        mContext = context;
    }

    @Override
    public int getCount() {
        if (transActionList == null) {
            return 0;
        }
        return transActionList.size();
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        if (view == null || view.getTag() == null) {
            view = LayoutInflater.from(mContext).inflate(R.layout.adapter_transaction_record, null);
        }

        /**
         * 月份交易汇总
         */
        TextView txt_month_total = view.findViewById(R.id.txt_month_total);

        /**
         * 姓名
         */
        TextView txt_money_kind = view.findViewById(R.id.txt_money_kind);

        /**
         * 日期
         */
        TextView txt_transaction_date = view.findViewById(R.id.txt_transaction_date);

        /**
         * 交易金额
         */
        TextView txt_transaction_money = view.findViewById(R.id.txt_transaction_money);

        /**
         * 交易状态
         */
        TextView txt_transaction_state = view.findViewById(R.id.txt_transaction_state);

        TransActionRecordBean curBean = transActionList.get(position);

        if (curBean != null){
           // txt_money_kind.setText(curBean.getUserName());

            txt_transaction_date.setText(curBean.getCreatetime());
            if(curBean.getDealtype() == 4101){
                //支出
                txt_transaction_money.setText("-$"+curBean.getPrice());
                txt_transaction_state.setText("$"+ Arith.sub(curBean.getPricefront(),curBean.getPrice()));
            }else if (curBean.getDealtype() == 4102){
            /*    if (curBean.getTypedetailid()==4307){
                    txt_transaction_money.setText("+"+curBean.getPrice());
                    txt_transaction_state.setText((curBean.getPricefront()+curBean.getPrice()));
                }else {
                    txt_transaction_money.setText("+$"+curBean.getPrice());
                }*/

                txt_transaction_money.setText("+$"+curBean.getPrice());
                txt_transaction_state.setText("$"+Arith.add(curBean.getPricefront(),curBean.getPrice()));

            }



            String status = "";
            if (curBean.getDealstatus() == 4201){
                status = "交易中";
            } else if (curBean.getDealstatus() == 4202){
                status = "成功";
            } else if (curBean.getDealstatus() == 4203){
                status = "失败";
            } else {
                status = "";
            }
            txt_money_kind.setText(getstatues(curBean.getTypedetailid())+status);
        }

        return view;
    }

    public void addAllList(List<TransActionRecordBean> transActionList) {
        if (this.transActionList != null) {
            this.transActionList.addAll(transActionList);
        }
        notifyDataSetChanged();
    }

    public void notifyData(List<TransActionRecordBean> list) {
        this.transActionList = list;
        notifyDataSetChanged();
    }
    public String getstatues(int state){

       if (state == 4301)
           return "余额交易";
       if (state == 4302)
           return "佣金";
       if (state == 4303)
           return "提现";
       if (state == 4304)
           return "充值";
       if (state == 4305){
           return "购买矿机";
       }
       if (state ==4306)
           return "聊天红包";
       if (state ==4307)
           return "币交易";
       if (state == 4309){
           return "币佣金";
       }
        return "";

    }

}
