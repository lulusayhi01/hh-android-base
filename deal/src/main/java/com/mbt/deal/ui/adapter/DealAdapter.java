package com.mbt.deal.ui.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;

import com.mbt.deal.R;
import com.mbt.deal.ui.activity.OrderListActivity;
import com.mbt.deal.ui.activity.OrderSaleorBuyActivity;
import com.mbt.deal.ui.activity.TansActionRecordActivity;
import com.mbt.deal.ui.viewbinder.KLineViewBinder;

import lecho.lib.hellocharts.view.LineChartView;

/**
 * @author Administrator
 * @createtime：2018/8/13 12:53
 */
public class DealAdapter extends RecyclerView.Adapter {
    private KLineViewBinder kLineViewBinder;
    private View.OnTouchListener touchListener;

    Context context;

    public DealAdapter(Context context) {
        this.context = context;
    }

    @Override
    public int getItemViewType(int position) {
        if (position < 4) {
            return position;
        }
        return 4;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case 0:
                return new HeardViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.head_layout, parent, false));
            case 1:
                return new KLineViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.k_line_layout, parent, false));
            case 2:
                return new MenuViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.menu_layout, parent, false));
            case 3:
                return new DealHeardViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.type_layout, parent, false));
            case 4:
                return new DealIteamViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.deal_money_iteam, parent, false));
            default:
                return null;
        }

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        int itemViewType = getItemViewType(position);
        if (itemViewType == 1) {
            kLineViewBinder = new KLineViewBinder((KLineViewHolder) holder);
        } else if (itemViewType == 3) {
            bindTypeBind((DealHeardViewHolder) holder);
        }
    }

    @Override
    public int getItemCount() {
        return 20;
    }


    private void bindTypeBind(DealHeardViewHolder viewHolder) {
        viewHolder.rb_money_buy.setChecked(true);
    }


    public class HeardViewHolder extends RecyclerView.ViewHolder {

        public HeardViewHolder(View itemView) {
            super(itemView);
        }
    }

    public class KLineViewHolder extends RecyclerView.ViewHolder {
        public LineChartView chart;

        public KLineViewHolder(View itemView) {
            super(itemView);
            chart = itemView.findViewById(R.id.chart);
        }
    }

    public class MenuViewHolder extends RecyclerView.ViewHolder {

        public View ll_sale_order;

        public View ll_buy_order;

        public View ll_order_list;

        public View ll_transaction_record;

        public MenuViewHolder(View itemView) {
            super(itemView);
            ll_sale_order = itemView.findViewById(R.id.ll_sale_order);
            ll_sale_order.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, OrderSaleorBuyActivity.class);
                    intent.putExtra("orderType", 2);
                    context.startActivity(intent);

                }
            });

            ll_buy_order = itemView.findViewById(R.id.ll_buy_order);
            ll_buy_order.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, OrderSaleorBuyActivity.class);
                    intent.putExtra("orderType", 1);
                    context.startActivity(intent);
                }
            });

            ll_order_list = itemView.findViewById(R.id.ll_order_list);
            ll_order_list.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    context.startActivity(new Intent(context, OrderListActivity.class));
                }
            });

            ll_transaction_record = itemView.findViewById(R.id.ll_transaction_record);
            ll_transaction_record.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    context.startActivity(new Intent(context, TansActionRecordActivity.class));
                }
            });
        }
    }

    public class DealHeardViewHolder extends RecyclerView.ViewHolder {
        public RadioButton rb_money_buy;
        public RadioButton rb_money_sale;

        public DealHeardViewHolder(View itemView) {
            super(itemView);
            rb_money_buy = itemView.findViewById(R.id.rb_money_buy);
            rb_money_sale = itemView.findViewById(R.id.rb_money_sale);
        }
    }

    public class DealIteamViewHolder extends RecyclerView.ViewHolder {

        public DealIteamViewHolder(View itemView) {
            super(itemView);
        }
    }

    public void setTouchListener(View.OnTouchListener touchListener) {
        this.touchListener = touchListener;
        kLineViewBinder.getVh().chart.setOnTouchListener(touchListener);
    }
}
