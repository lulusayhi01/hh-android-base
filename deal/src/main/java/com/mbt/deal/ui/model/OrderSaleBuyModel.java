package com.mbt.deal.ui.model;

import com.mbt.deal.api.Api;
import com.mbt.deal.api.ApiServer;
import com.mbt.deal.api.RxResultHelper;
import com.mbt.deal.api.RxSchedulers;
import com.mbt.deal.bean.BaseDataBean;
import com.mbt.deal.bean.BaseListBean;
import com.mbt.deal.bean.OrderListBean;
import com.mbt.deal.ui.contract.OrderListContract;
import com.mbt.deal.ui.contract.OrderSaleContract;

import java.util.Map;

import rx.Observable;

/**
 * @author Administrator
 * @createtime：2018/8/16 0:48
 */
public class OrderSaleBuyModel implements OrderSaleContract.Model {

    @Override
    public Observable<BaseDataBean<String>> sale(String token, Map map) {
        return Api.getServer(ApiServer.class).goSaleOrder(token, map).compose(RxSchedulers.io_main()).compose(RxResultHelper.handleResult());
    }

    @Override
    public Observable<BaseDataBean<String>> buy(String token, Map map) {
        return Api.getServer(ApiServer.class).goBuyOrder(token, map).compose(RxSchedulers.io_main()).compose(RxResultHelper.handleResult());
    }
}
