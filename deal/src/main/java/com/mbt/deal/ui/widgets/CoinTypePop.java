package com.mbt.deal.ui.widgets;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.mbt.deal.R;
import com.mbt.deal.bean.CoinListBean;
import com.mbt.deal.ui.adapter.PopMoneyAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * 创建日期：2018/8/14 on 19:42
 * 描述:
 * 作者:王甜
 */
public class CoinTypePop extends PopupWindow {

    private Context mContext;

    private View popView;

    private ListView mListView;
    private List<CoinListBean> beans = new ArrayList<>();
    private CoinTypeAdapter adapter;

    public CoinTypePop(Context context) {
        super(context);
        mContext = context;
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        popView = inflater.inflate(R.layout.layout_right_menu_pop, null);
        mListView = popView.findViewById(R.id.list_pop);
        adapter = new CoinTypeAdapter();
        mListView.setAdapter(adapter);
        this.setContentView(popView);
        this.setOutsideTouchable(true);
        ColorDrawable dw = new ColorDrawable(0x00ffffff);
        //设置SelectPicPopupWindow弹出窗体的背景
        this.setBackgroundDrawable(dw);
    }

    public void setData(List<CoinListBean> ls) {
        this.beans = ls;
        adapter.notifyDataSetChanged();
    }

    public class CoinTypeAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return beans.size();
        }

        @Override
        public Object getItem(int position) {
            return beans.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = LayoutInflater.from(mContext).inflate(R.layout.cointype_iteam, parent, false);
            CoinListBean coinListBean = beans.get(position);
            TextView textView = view.findViewById(R.id.tv_content);
            textView.setText(coinListBean.getName());
            return view;
        }
    }

    public ListView getmListView() {
        return mListView;
    }
}
