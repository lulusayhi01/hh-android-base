package com.mbt.deal.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.mbt.deal.R;
import com.mbt.deal.util.HoloderUtil;

/**
 * 创建日期：2018/8/20 on 10:23
 * 描述:
 * 作者:王甜
 */
public class RechargeAdapter extends BaseAdapter {

    Context context;

    public RechargeAdapter(Context context) {
        this.context = context;
    }

    @Override
    public int getCount() {
        return 0;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        if (view == null || view.getTag() == null) {
            view = LayoutInflater.from(context).inflate(R.layout.adapter_recharge_record, null);
        }

        TextView txt_money_recharge = HoloderUtil.getHolderView(view, R.id.txt_money_recharge);

        TextView txt_date_recharge = HoloderUtil.getHolderView(view, R.id.txt_date_recharge);

        TextView txt_state_recharge = HoloderUtil.getHolderView(view, R.id.txt_state_recharge);

        return view;
    }
}
