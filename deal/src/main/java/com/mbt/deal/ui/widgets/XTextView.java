package com.mbt.deal.ui.widgets;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.widget.TextView;

import com.mbt.deal.R;

@SuppressLint("AppCompatCustomView")
public class XTextView extends TextView {

    Paint mPaint;
    Paint txtPaint;

    public XTextView(Context context) {
        super(context);
        InitPaint();
    }

    public XTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        InitPaint();
    }

    private void InitPaint() {
        mPaint = new Paint();
        mPaint.setStyle(Style.FILL);

        txtPaint = new Paint();
        txtPaint.setStyle(Style.FILL);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        if (!isSelected()) {
            mPaint.setColor(getResources().getColor(R.color.common_bg));
            txtPaint.setColor(getResources().getColor(R.color.common_bg));
            setTextColor(getResources().getColor(R.color.common_bg));
            float height = getResources().getDimension(R.dimen.line_height);
            canvas.drawRect(0, getHeight() - height, getWidth(), getHeight(), mPaint);
            canvas.drawRect(0, 0, getWidth(), height, mPaint);
        } else {
            mPaint.setColor(getResources().getColor(R.color.common_bg));
            txtPaint.setColor(getResources().getColor(R.color.white));
            setTextColor(getResources().getColor(R.color.white));
            canvas.drawRect(new Rect(0, 0, getMeasuredWidth(), getMeasuredHeight()), mPaint);
        }
        super.onDraw(canvas);

    }
}
