package com.mbt.deal.ui.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.mbt.deal.R;
import com.mbt.deal.R2;
import com.mbt.deal.ui.widgets.TestPtrFrameLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * 创建日期：2018/8/20 on 9:54
 * 描述:
 * 作者:王甜
 */
public class RechargeRecordActivity extends BaseActivity {

    @BindView(R2.id.ll_back)
    LinearLayout llBack;
    @BindView(R2.id.txt_title)
    TextView txtTitle;
    @BindView(R2.id.iv_right)
    View ivRight;
    @BindView(R2.id.ll_right_menu)
    LinearLayout llRightMenu;
    @BindView(R2.id.txt_right)
    TextView txtRight;
    @BindView(R2.id.ll_right_text)
    LinearLayout llRightText;
    @BindView(R2.id.tv_no_data)
    TextView tvNoData;
    @BindView(R2.id.list_transaction_record)
    ListView listTransactionRecord;
    @BindView(R2.id.rotate_header_list_view_frame)
    TestPtrFrameLayout rotateHeaderListViewFrame;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rcharge_record);
        ButterKnife.bind(this);
    }

    @OnClick(R2.id.ll_back)
    public void onViewClicked() {
        finish();
    }
}
