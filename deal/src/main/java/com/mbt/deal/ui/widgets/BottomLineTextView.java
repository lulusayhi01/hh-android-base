package com.mbt.deal.ui.widgets;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.widget.TextView;

import com.mbt.deal.R;

@SuppressLint("AppCompatCustomView")
public class BottomLineTextView extends TextView {

    Paint mPaint;

    public BottomLineTextView(Context context) {
        super(context);
        InitPaint();
    }

    public BottomLineTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        InitPaint();
    }

    private void InitPaint() {
        mPaint = new Paint();
        mPaint.setStyle(Style.FILL);

    }

    @Override
    protected void onDraw(Canvas canvas) {
        mPaint.setColor(getResources().getColor(R.color.common_bg));
        float height = getResources().getDimension(R.dimen.line_height);
        canvas.drawRect(0, getHeight() - height, getWidth(), getHeight(), mPaint);
        super.onDraw(canvas);

    }
}
