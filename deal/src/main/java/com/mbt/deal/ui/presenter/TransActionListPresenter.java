package com.mbt.deal.ui.presenter;

import com.mbt.deal.api.RxSubscriber;
import com.mbt.deal.bean.BaseDataBean;
import com.mbt.deal.bean.BaseListBean;
import com.mbt.deal.bean.CoinListBean;
import com.mbt.deal.bean.CoinPriceBean;
import com.mbt.deal.bean.OrderBean;
import com.mbt.deal.bean.TransActionRecordBean;
import com.mbt.deal.ui.contract.DealMainContract;
import com.mbt.deal.ui.contract.TransActionContract;
import com.mbt.deal.util.MyUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Administrator
 * @createtime：2018/8/16 0:49
 */
public class TransActionListPresenter extends TransActionContract.Presenter {


    @Override
    public void getTraansActionOrderList(int pageNum, int pageSize) {
        Map map = new HashMap();
        map.put("pageNum", pageNum + "");
        map.put("pageSize", pageSize + "");
        mModel.showTransActionList(MyUtils.getToken(mContext), map).subscribe(new RxSubscriber<BaseListBean<TransActionRecordBean>>(mContext) {
            @Override
            public void _onNext(BaseListBean<TransActionRecordBean> stringBaseListBean) {
                mView.showTransActionList(stringBaseListBean.data, stringBaseListBean.count);
            }

            @Override
            public void _onError(String message) {
                mView.onError(message);
            }

            @Override
            public void onCompleted() {

            }
        });
    }
}
