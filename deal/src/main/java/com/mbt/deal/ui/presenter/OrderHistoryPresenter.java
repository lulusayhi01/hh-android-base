package com.mbt.deal.ui.presenter;

import android.text.TextUtils;

import com.mbt.deal.api.RxSubscriber;
import com.mbt.deal.bean.BaseListBean;
import com.mbt.deal.bean.OrderListBean;
import com.mbt.deal.bean.TransActionRecordBean;
import com.mbt.deal.ui.contract.OrderListContract;
import com.mbt.deal.ui.contract.TransActionContract;
import com.mbt.deal.util.MyUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Administrator
 * @createtime：2018/8/16 0:49
 */
public class OrderHistoryPresenter extends OrderListContract.Presenter {

    @Override
    public void showOrderList(int pageNum, int pageSize, String orderStatus, String type) {
        Map map = new HashMap();
        map.put("pageNum", pageNum + "");
        map.put("pageSize", pageSize + "");
        if (!TextUtils.isEmpty(orderStatus)) {
            map.put("orderstatus", orderStatus + "");
        }
        if (!TextUtils.isEmpty(type)) {
            map.put("type", type);
        }

        mModel.showOrderList(MyUtils.getToken(mContext), map).subscribe(new RxSubscriber<BaseListBean<OrderListBean>>(mContext) {
            @Override
            public void _onNext(BaseListBean<OrderListBean> stringBaseListBean) {
                mView.showOrderList(stringBaseListBean.data, stringBaseListBean.count);
            }

            @Override
            public void _onError(String message) {
                mView.onError(message);
            }

            @Override
            public void onCompleted() {

            }
        });
    }
}
