package com.mbt.deal.ui.presenter;

import com.mbt.deal.api.RxSubscriber;
import com.mbt.deal.bean.BaseBean;
import com.mbt.deal.bean.BaseDataBean;
import com.mbt.deal.bean.BaseListBean;
import com.mbt.deal.bean.CoinListBean;
import com.mbt.deal.bean.CoinPriceBean;
import com.mbt.deal.bean.OrderBean;
import com.mbt.deal.bean.UserCoinCount;
import com.mbt.deal.ui.contract.DealMainContract;
import com.mbt.deal.util.LogUtils;
import com.mbt.deal.util.MyUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Administrator
 * @createtime：2018/8/16 0:49
 */
public class DealMainPresenter extends DealMainContract.Presenter {

    @Override
    public void getCoinList() {
        mModel.loadCoinList(MyUtils.getToken(mContext), new HashMap()).subscribe(new RxSubscriber<BaseListBean<CoinListBean>>(mContext) {
            @Override
            public void _onNext(BaseListBean<CoinListBean> coinListBeanBaseListBean) {
                String mCoinid = coinListBeanBaseListBean.data.get(0).getId() + "";
                getConinPrice(coinListBeanBaseListBean.data.get(0).getId() + "");
                getCoinCount(mCoinid);
                mView.showcurrency(coinListBeanBaseListBean.data);
            }

            @Override
            public void _onError(String message) {

            }

            @Override
            public void onCompleted() {

            }
        });
    }

    @Override
    public void getConinPrice(String id) {
        Map map = new HashMap();
        map.put("id", id);
        mModel.loadprice(MyUtils.getToken(mContext), map).subscribe(new RxSubscriber<BaseDataBean<CoinPriceBean>>(mContext) {
            @Override
            public void _onNext(BaseDataBean<CoinPriceBean> coinPriceBeanBaseDataBean) {
                mView.showPric(coinPriceBeanBaseDataBean.data);
            }

            @Override
            public void _onError(String message) {

            }

            @Override
            public void onCompleted() {

            }
        });
    }

    @Override
    public void getCoinCount(String coinid) {
        Map map = new HashMap();
        map.put("coinid", coinid);
        mModel.loaduserCoinCount(MyUtils.getToken(mContext), map).subscribe(new RxSubscriber<BaseDataBean<String>>(mContext) {
            @Override
            public void _onNext(BaseDataBean<String> userCoinCountBaseDataBean) {
                mView.showCoinCount(userCoinCountBaseDataBean.data);
            }

            @Override
            public void _onError(String message) {

            }

            @Override
            public void onCompleted() {

            }
        });
    }

    @Override
    public void getOrderList(int type, int pageNum, int pageSize) {
        Map map = new HashMap();
        map.put("pageNum", pageNum + "");
        map.put("pageSize", pageSize + "");
        map.put("type", type + "");
        LogUtils.i("请求列表的参数:" + map.toString());
        mModel.loadOrderList(MyUtils.getToken(mContext), map).subscribe(new RxSubscriber<BaseListBean<OrderBean>>(mContext) {
            @Override
            public void _onNext(BaseListBean<OrderBean> stringBaseListBean) {
                mView.showOrderBean(stringBaseListBean.data, stringBaseListBean.count);

            }

            @Override
            public void _onError(String message) {

            }

            @Override
            public void onCompleted() {

            }
        });
    }

}
