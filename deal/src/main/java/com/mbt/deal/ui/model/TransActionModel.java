package com.mbt.deal.ui.model;

import com.mbt.deal.api.Api;
import com.mbt.deal.api.ApiServer;
import com.mbt.deal.api.RxResultHelper;
import com.mbt.deal.api.RxSchedulers;
import com.mbt.deal.bean.BaseDataBean;
import com.mbt.deal.bean.BaseListBean;
import com.mbt.deal.bean.CoinListBean;
import com.mbt.deal.bean.CoinPriceBean;
import com.mbt.deal.bean.OrderBean;
import com.mbt.deal.bean.TransActionRecordBean;
import com.mbt.deal.ui.contract.DealMainContract;
import com.mbt.deal.ui.contract.TransActionContract;

import java.util.Map;

import rx.Observable;

/**
 * @author Administrator
 * @createtime：2018/8/16 0:48
 */
public class TransActionModel implements TransActionContract.Model {

    @Override
    public Observable<BaseListBean<TransActionRecordBean>> showTransActionList(String token, Map map) {
        return Api.getServer(ApiServer.class).getTransactionList(token, map).compose(RxSchedulers.io_main()).compose(RxResultHelper.handleResult());
    }
}
