package com.mbt.deal.ui.contract;

import com.mbt.deal.bean.BaseListBean;
import com.mbt.deal.bean.OrderListBean;
import com.mbt.deal.bean.TransActionRecordBean;
import com.mbt.deal.ui.BaseModel;
import com.mbt.deal.ui.BasePresenter;
import com.mbt.deal.ui.BaseView;

import java.util.List;
import java.util.Map;

import rx.Observable;

/**
 * @author Administrator
 * @createtime：2018/8/16 0:46
 */
public interface OrderListContract {
    interface View extends BaseView {
        void showOrderList(List<OrderListBean> list, int totalcount);

        void onError(String errorMsg);
    }

    interface Model extends BaseModel {
        Observable<BaseListBean<OrderListBean>> showOrderList(String token, Map map);
    }

    abstract class Presenter extends BasePresenter<View, Model> {
        public abstract void showOrderList(int pageNum, int pageSize, String orderStatus,String type);
    }
}
