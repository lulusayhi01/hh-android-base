package com.mbt.deal.ui.activity;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.mbt.deal.ui.BaseModel;
import com.mbt.deal.ui.BasePresenter;
import com.mbt.deal.util.TUtil;

/**
 * 创建日期：2018/8/14 on 18:00
 * 描述:
 * 作者:王甜
 */
public class BaseActivity<T extends BasePresenter, E extends BaseModel> extends Activity {

    Activity act;
    public T mPresenter;
    public E mModel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        act = this;
        mPresenter = TUtil.getT(this, 0);
        mModel = TUtil.getT(this, 1);
        if (mPresenter != null) {
            mPresenter.mContext = act;
        }
    }
}
