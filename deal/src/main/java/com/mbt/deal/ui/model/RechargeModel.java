package com.mbt.deal.ui.model;

import com.mbt.deal.api.Api;
import com.mbt.deal.api.ApiServer;
import com.mbt.deal.api.RxResultHelper;
import com.mbt.deal.api.RxSchedulers;
import com.mbt.deal.bean.BaseDataBean;
import com.mbt.deal.bean.BaseListBean;
import com.mbt.deal.bean.OrderListBean;
import com.mbt.deal.ui.contract.OrderListContract;
import com.mbt.deal.ui.contract.RechargeContract;

import java.util.Map;

import rx.Observable;

/**
 * @author Administrator
 * @createtime：2018/8/16 0:48
 */
public class RechargeModel implements RechargeContract.Model {

    @Override
    public Observable<BaseDataBean<String>> recharge(String token, Map map) {
        return Api.getServer(ApiServer.class).recharge(token, map).compose(RxSchedulers.io_main()).compose(RxResultHelper.handleResult());

    }
}
