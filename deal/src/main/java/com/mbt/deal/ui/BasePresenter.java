package com.mbt.deal.ui;

import android.content.Context;

/**
 * @author 赵盼龙
 * @createtime：2018/4/16 9:53
 */
public class BasePresenter<T extends BaseView, E extends BaseModel> {
    public Context mContext;
    public E mModel;
    public T mView;

    public void setVM(T v, E m) {
        this.mView = v;
        this.mModel = m;
        this.onStart();
    }

    public void onStart() {
    }

    ;

    public void onDestroy() {

    }
}
