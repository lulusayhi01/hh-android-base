package com.mbt.deal.ui.widgets;

import android.content.Intent;

/**
 * @author Administrator
 * @createtime：2018/9/7 14:25
 */
public interface PicHandle {
    public static final int OPEN_PHOTO = 40001;

    /**
     * 打开资源 相机或者图片资源
     */
    public void open();

    /**
     * 获取图片的资源
     *
     * @param data
     */
    public String getPicImagePath(Intent data);
}
