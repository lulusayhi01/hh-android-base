package com.mbt.deal.ui.model;

import com.mbt.deal.api.Api;
import com.mbt.deal.api.ApiServer;
import com.mbt.deal.api.RxResultHelper;
import com.mbt.deal.api.RxSchedulers;
import com.mbt.deal.bean.BaseBean;
import com.mbt.deal.bean.BaseDataBean;
import com.mbt.deal.bean.BaseListBean;
import com.mbt.deal.bean.CoinListBean;
import com.mbt.deal.bean.CoinPriceBean;
import com.mbt.deal.bean.OrderBean;
import com.mbt.deal.bean.UserCoinCount;
import com.mbt.deal.ui.contract.DealMainContract;
import com.mbt.deal.util.MyUtils;
import com.mbt.deal.widget.MyChart;

import java.util.Map;

import rx.Observable;

/**
 * @author Administrator
 * @createtime：2018/8/16 0:48
 */
public class DealMainModel implements DealMainContract.Model {
    @Override
    public Observable<BaseListBean<CoinListBean>> loadCoinList(String token, Map map) {
        return Api.getServer(ApiServer.class).getCurrencyList(token, map).compose(RxSchedulers.io_main()).compose(RxResultHelper.handleResult());
    }

    @Override
    public Observable<BaseDataBean<CoinPriceBean>> loadprice(String token, Map map) {
        return Api.getServer(ApiServer.class).getCoinPrice(token, map).compose(RxSchedulers.io_main()).compose(RxResultHelper.handleResult());
    }

    @Override
    public Observable<BaseDataBean<String>> loaduserCoinCount(String token, Map map) {
        return Api.getServer(ApiServer.class).getCoinCount(token, map).compose(RxSchedulers.io_main()).compose(RxResultHelper.handleResult());
    }

    @Override
    public Observable<BaseListBean<OrderBean>> loadOrderList(String token, Map map) {
        return Api.getServer(ApiServer.class).getorderList(token, map).compose(RxSchedulers.io_main()).compose(RxResultHelper.handleResult());
    }
}
