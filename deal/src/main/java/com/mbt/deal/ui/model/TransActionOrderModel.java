package com.mbt.deal.ui.model;

import com.mbt.deal.api.Api;
import com.mbt.deal.api.ApiServer;
import com.mbt.deal.api.RxResultHelper;
import com.mbt.deal.api.RxSchedulers;
import com.mbt.deal.bean.BaseListBean;
import com.mbt.deal.bean.OrderListBean;
import com.mbt.deal.bean.TransActionRecordBean;
import com.mbt.deal.ui.contract.OrderListContract;
import com.mbt.deal.ui.contract.TransActionContract;

import java.util.Map;

import rx.Observable;

/**
 * @author Administrator
 * @createtime：2018/8/16 0:48
 */
public class TransActionOrderModel implements OrderListContract.Model {

    @Override
    public Observable<BaseListBean<OrderListBean>> showOrderList(String token, Map map) {
        return Api.getServer(ApiServer.class).getOrderList(token, map).compose(RxSchedulers.io_main()).compose(RxResultHelper.handleResult());
    }
}
