package com.mbt.deal.ui.adapter;

import android.app.Activity;
import android.support.v4.view.PagerAdapter;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mbt.deal.R;
import com.mbt.deal.bean.OrderListBean;
import com.mbt.deal.ui.widgets.OrderListView;
import com.mbt.deal.ui.widgets.TransActionOrderView;

import java.util.List;


public class OrderViewPagerAdapter extends PagerAdapter {

    Activity context;

    SparseArray<TransActionOrderView> lists;

    int totalItem = 4;

    String orderStatus;
    String type;
    public OrderViewPagerAdapter(Activity context) {
        this.context = context;
        this.lists = new SparseArray<TransActionOrderView>();
        for (int i = 0; i < totalItem; i++) {
            lists.put(i, null);
        }
    }

    @Override
    public int getCount() {
        return lists.size();
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        TransActionOrderView root;
        if (lists.get(position) != null) {
            root = lists.get(position);
        } else {
            root = new TransActionOrderView(context);
            lists.put(position, root);
            if (position == 0) {
                orderStatus = null;
                type = "2";
            } else if (position == 1) {
                orderStatus = null;
                type = "1";
            } else if (position == 2) {
                orderStatus = "3108";
                type= null;
            }else if (position == 3){
                orderStatus = "3102";
                type = null;
            }
        }
        root.setOrderStatus(orderStatus,type);
        container.addView(root, 0);
        return root;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        if (lists.get(position) != null) {
            container.removeView(lists.get(position));
        }
    }

    @Override
    public boolean isViewFromObject(View view, Object obj) {
        return view == obj;
    }

    public TransActionOrderView getViewByPoint(int point){
        return  lists.get(point);
    }
}
