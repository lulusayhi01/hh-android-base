package com.mbt.deal.ui.contract;

import android.view.View;

import com.mbt.deal.bean.BaseBean;
import com.mbt.deal.bean.BaseDataBean;
import com.mbt.deal.bean.BaseListBean;
import com.mbt.deal.bean.CoinListBean;
import com.mbt.deal.bean.CoinPriceBean;
import com.mbt.deal.bean.OrderBean;
import com.mbt.deal.bean.UserCoinCount;
import com.mbt.deal.ui.BaseModel;
import com.mbt.deal.ui.BasePresenter;
import com.mbt.deal.ui.BaseView;

import java.util.List;
import java.util.Map;

import rx.Observable;

/**
 * @author Administrator
 * @createtime：2018/8/16 0:46
 */
public interface DealMainContract {
    interface View extends BaseView {
        void showPric(CoinPriceBean bean);

        void showCoinCount(String count);

        void showcurrency(List<CoinListBean> ls);

        void showOrderBean(List<OrderBean> list, int totalcount);
    }

    interface Model extends BaseModel {
        Observable<BaseListBean<CoinListBean>> loadCoinList(String token, Map map);

        Observable<BaseDataBean<CoinPriceBean>> loadprice(String token, Map map);

        Observable<BaseDataBean<String>> loaduserCoinCount(String token, Map map);

        Observable<BaseListBean<OrderBean>> loadOrderList(String token, Map map);
    }

    abstract class Presenter extends BasePresenter<View, Model> {
        public abstract void getCoinList();

        public abstract void getConinPrice(String id);

        public abstract void getCoinCount(String coinid);

        public abstract void getOrderList(int type, int pageNum, int pageSize);
    }
}
