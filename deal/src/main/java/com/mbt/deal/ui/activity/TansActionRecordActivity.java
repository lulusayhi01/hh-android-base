package com.mbt.deal.ui.activity;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.mbt.deal.R;
import com.mbt.deal.R2;
import com.mbt.deal.bean.TransActionRecordBean;
import com.mbt.deal.ui.adapter.TransActionRecordAdapter;
import com.mbt.deal.ui.contract.TransActionContract;
import com.mbt.deal.ui.model.TransActionModel;
import com.mbt.deal.ui.presenter.TransActionListPresenter;
import com.mbt.deal.ui.widgets.MoneyKindPop;
import com.mbt.deal.ui.widgets.TestPtrFrameLayout;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import in.srain.cube.views.ptr.PtrDefaultHandler2;
import in.srain.cube.views.ptr.PtrFrameLayout;

/**
 * 创建日期：2018/8/14 on 18:00
 * 描述:
 * 作者:王甜
 */
@Route(path = "/deal/TansActionRecordActivity")
public class TansActionRecordActivity extends BaseActivity<TransActionListPresenter, TransActionModel> implements TransActionContract.View {

    @BindView(R2.id.ll_back)
    LinearLayout llBack;
    @BindView(R2.id.txt_title)
    TextView txtTitle;
    @BindView(R2.id.ll_right_menu)
    LinearLayout llRightMenu;
    @BindView(R2.id.list_transaction_record)
    ListView listTransactionRecord;
    @BindView(R2.id.iv_right)
    View ivRight;
    TestPtrFrameLayout rotateHeaderListViewFrame;
    @BindView(R2.id.tv_no_data)
    TextView tvNodata;

    Unbinder mBind;//butterKnife

    TransActionRecordAdapter mAdapter;

    MoneyKindPop moneyKindPop;

    int pageNum = 1;//当前页数

    int pageSize = 20;//每页条数

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transaction_record);
        mBind = ButterKnife.bind(act);
        mPresenter.setVM(this, mModel);
        rotateHeaderListViewFrame = findViewById(R.id.rotate_header_list_view_frame);

        initData();

    }

    private void initData() {
        txtTitle.setText("交易记录");
        llRightMenu.setVisibility(View.GONE);
        mAdapter = new TransActionRecordAdapter(act);
        listTransactionRecord.setAdapter(mAdapter);

        mPresenter.getTraansActionOrderList(pageNum, pageSize);
        rotateHeaderListViewFrame.autoRefresh();
        rotateHeaderListViewFrame.setPtrHandler(new PtrDefaultHandler2() {
            @Override
            public void onLoadMoreBegin(PtrFrameLayout frame) {
                pageNum = pageNum + 1;
                mPresenter.getTraansActionOrderList(pageNum, pageSize);
            }

            @Override
            public void onRefreshBegin(PtrFrameLayout frame) {
                pageNum = 1;
                mPresenter.getTraansActionOrderList(pageNum, pageSize);
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mBind != null) {
            mBind.unbind();
            mBind = null;
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @OnClick({R2.id.ll_back, R2.id.ll_right_menu})
    public void onViewClicked(View view) {
        int i = view.getId();
        if (i == R.id.ll_back) {
            finish();

        } else if (i == R.id.ll_right_menu) {
            if (moneyKindPop == null) {
                moneyKindPop = new MoneyKindPop(act);
            }
            moneyKindPop.showAsDropDown(ivRight, 10, 15, Gravity.RIGHT);
        }
    }

    @Override
    public void showTransActionList(List<TransActionRecordBean> list, int totalcount) {
        rotateHeaderListViewFrame.refreshComplete();
        if (pageNum == 1){
            if (totalcount == 0){
                tvNodata.setVisibility(View.VISIBLE);
                rotateHeaderListViewFrame.setVisibility(View.GONE);
            } else {
                tvNodata.setVisibility(View.GONE);
                rotateHeaderListViewFrame.setVisibility(View.VISIBLE);
                mAdapter.notifyData(list);
            }
        } else {
            tvNodata.setVisibility(View.GONE);
            rotateHeaderListViewFrame.setVisibility(View.VISIBLE);
            if (mAdapter != null) {
                mAdapter.addAllList(list);
            }
        }
    }

    @Override
    public void onError(String errorMsg) {
        pageNum = pageNum - 1;
        rotateHeaderListViewFrame.refreshComplete();
    }
}
