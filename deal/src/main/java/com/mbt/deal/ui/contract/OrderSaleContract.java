package com.mbt.deal.ui.contract;

import com.mbt.deal.bean.BaseDataBean;
import com.mbt.deal.bean.BaseListBean;
import com.mbt.deal.bean.TransActionRecordBean;
import com.mbt.deal.ui.BaseModel;
import com.mbt.deal.ui.BasePresenter;
import com.mbt.deal.ui.BaseView;

import java.util.List;
import java.util.Map;

import rx.Observable;

/**
 * @author Administrator
 * @createtime：2018/8/16 0:46
 */
public interface OrderSaleContract {
    interface View extends BaseView {
        void sale(String jsonValues);

        void buy(String jsonValues);

        void onError(String errorMsg);
    }

    interface Model extends BaseModel {
        Observable<BaseDataBean<String>> sale(String token, Map map);

        Observable<BaseDataBean<String>> buy(String token, Map map);
    }

    abstract class Presenter extends BasePresenter<View, Model> {
        public abstract void sale(String coinid, String number, String price);

        public abstract void buy(String coinid, String number, String price);
    }
}
