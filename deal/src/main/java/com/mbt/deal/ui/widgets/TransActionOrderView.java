package com.mbt.deal.ui.widgets;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.mbt.deal.R;
import com.mbt.deal.bean.OrderListBean;
import com.mbt.deal.ui.BaseModel;
import com.mbt.deal.ui.BasePresenter;
import com.mbt.deal.ui.adapter.OrderListAdaoter;
import com.mbt.deal.ui.contract.OrderListContract;
import com.mbt.deal.ui.model.TransActionModel;
import com.mbt.deal.ui.model.TransActionOrderModel;
import com.mbt.deal.ui.presenter.OrderHistoryPresenter;
import com.mbt.deal.util.LogUtils;
import com.mbt.deal.util.TUtil;

import java.util.List;

import in.srain.cube.views.ptr.PtrDefaultHandler2;
import in.srain.cube.views.ptr.PtrFrameLayout;

/**
 * 创建日期：2018/8/16 on 21:29
 * 描述:
 * 作者:王甜
 */
public class TransActionOrderView extends LinearLayout implements OrderListContract.View {

    Context context;
    private boolean isInit;
    public OrderHistoryPresenter mPresenter;
    public TransActionOrderModel mModel;

    ListView order_list;

    TextView tv_no_data;

    TestPtrFrameLayout rotate_header_list_view_frame;

    OrderListAdaoter mAdapter;

    int pageNum = 1;//当前页数

    int pageSize = 20;//每页条数

    String type;

    public TransActionOrderView(Context context) {
        super(context);
        initData(context);
    }

    public TransActionOrderView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initData(context);
    }

    public TransActionOrderView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initData(context);
    }

    private void initData(Context context) {
        this.context = context;
        mPresenter = new OrderHistoryPresenter();
        mModel = new TransActionOrderModel();
        mPresenter.setVM(this, mModel);
        if (mPresenter != null) {
            mPresenter.mContext = context;
        }
        mAdapter = new OrderListAdaoter(context);

        this.removeAllViews();

        View root = LayoutInflater.from(context).inflate(R.layout.view_order_pager_adapter, null);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        root.setLayoutParams(params);

        order_list = root.findViewById(R.id.order_list);
        order_list.setAdapter(mAdapter);

        tv_no_data = root.findViewById(R.id.tv_no_data);

        rotate_header_list_view_frame = root.findViewById(R.id.rotate_header_list_view_frame);
        rotate_header_list_view_frame.autoRefresh();
        rotate_header_list_view_frame.setPtrHandler(new PtrDefaultHandler2() {
            @Override
            public void onLoadMoreBegin(PtrFrameLayout frame) {
                pageNum = pageNum + 1;
                mPresenter.showOrderList(pageNum, pageSize, orderStatus, type);
            }

            @Override
            public void onRefreshBegin(PtrFrameLayout frame) {
                tv_no_data.setVisibility(GONE);
                pageNum = 1;
                mPresenter.showOrderList(pageNum, pageSize, orderStatus, type);
            }
        });

        this.addView(root);
    }

    String orderStatus;//订单状态

    public void setOrderStatus(String orderStatus, String type) {
        this.orderStatus = orderStatus;
        this.type = type;
        LogUtils.e("订单状态：：：" + orderStatus);
        mPresenter.showOrderList(pageNum, pageSize, orderStatus, type);
    }

    @Override
    public void showOrderList(List<OrderListBean> list, int totalcount) {
        rotate_header_list_view_frame.refreshComplete();
        if (pageNum == 1) {
            if (totalcount == 0) {
                tv_no_data.setVisibility(View.VISIBLE);
                //   rotate_header_list_view_frame.setVisibility(View.GONE);
                mAdapter.clearData();
            } else {
                tv_no_data.setVisibility(View.GONE);
                // rotate_header_list_view_frame.setVisibility(View.VISIBLE);
                mAdapter.notifyData(list);
            }
        } else {
            tv_no_data.setVisibility(View.GONE);
            // rotate_header_list_view_frame.setVisibility(View.VISIBLE);
            mAdapter.addAllData(list);
        }
        mAdapter.setRefreshListen(new OrderListAdaoter.refershListen() {
            @Override
            public void refresh() {
                refreshData();
            }
        });
        isInit = true;
    }

    @Override
    public void onError(String errorMsg) {
        pageNum = pageNum - 1;
        rotate_header_list_view_frame.refreshComplete();
    }

    public void refreshData() {
        rotate_header_list_view_frame.autoRefresh();
        tv_no_data.setVisibility(View.GONE);
    }

    public boolean isInit() {
        return isInit;
    }
}
