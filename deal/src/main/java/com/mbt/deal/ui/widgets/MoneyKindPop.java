package com.mbt.deal.ui.widgets;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.PopupWindow;

import com.mbt.deal.R;
import com.mbt.deal.ui.adapter.PopMoneyAdapter;

/**
 * 创建日期：2018/8/14 on 19:42
 * 描述:
 * 作者:王甜
 */
public class MoneyKindPop extends PopupWindow {

    private Context mContext;

    private View popView;

    private ListView mListView;

    public MoneyKindPop(Context context) {
        super(context);
        mContext = context;
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        popView = inflater.inflate(R.layout.layout_right_menu_pop, null);
        mListView = popView.findViewById(R.id.list_pop);
        mListView.setAdapter(new PopMoneyAdapter(mContext));
        this.setContentView(popView);
        this.setWidth(214);
        //设置SelectPicPopupWindow弹出窗体的高
        this.setHeight(380);
        this.setOutsideTouchable(true);
        ColorDrawable dw = new ColorDrawable(0x00ffffff);
        //设置SelectPicPopupWindow弹出窗体的背景
        this.setBackgroundDrawable(dw);
    }
}
