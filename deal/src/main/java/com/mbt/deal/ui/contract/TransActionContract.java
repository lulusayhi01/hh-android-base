package com.mbt.deal.ui.contract;

import com.mbt.deal.bean.BaseDataBean;
import com.mbt.deal.bean.BaseListBean;
import com.mbt.deal.bean.CoinListBean;
import com.mbt.deal.bean.CoinPriceBean;
import com.mbt.deal.bean.OrderBean;
import com.mbt.deal.bean.TransActionRecordBean;
import com.mbt.deal.ui.BaseModel;
import com.mbt.deal.ui.BasePresenter;
import com.mbt.deal.ui.BaseView;

import java.util.List;
import java.util.Map;

import rx.Observable;

/**
 * @author Administrator
 * @createtime：2018/8/16 0:46
 */
public interface TransActionContract {
    interface View extends BaseView {
        void showTransActionList(List<TransActionRecordBean> list, int totalcount);

        void onError(String errorMsg);
    }

    interface Model extends BaseModel {
        Observable<BaseListBean<TransActionRecordBean>> showTransActionList(String token, Map map);
    }

    abstract class Presenter extends BasePresenter<View, Model> {
        public abstract void getTraansActionOrderList(int pageNum, int pageSize);
    }
}
