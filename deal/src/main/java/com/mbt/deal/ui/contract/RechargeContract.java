package com.mbt.deal.ui.contract;

import com.mbt.deal.bean.BaseDataBean;
import com.mbt.deal.bean.BaseListBean;
import com.mbt.deal.bean.TransActionRecordBean;
import com.mbt.deal.ui.BaseModel;
import com.mbt.deal.ui.BasePresenter;
import com.mbt.deal.ui.BaseView;

import java.util.List;
import java.util.Map;

import rx.Observable;

/**
 * @author Administrator
 * @createtime：2018/8/16 0:46
 */
public interface RechargeContract {
    interface View extends BaseView {
        void chargeSuccess(String jsonValues);

        void onError(String errorMsg);
    }

    interface Model extends BaseModel {
        Observable<BaseDataBean<String>> recharge(String token, Map map);
    }

    abstract class Presenter extends BasePresenter<View, Model> {
        public abstract void recharge(String price);
    }
}
