package com.mbt.deal.ui.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.mbt.deal.R;
import com.mbt.deal.R2;
import com.mbt.deal.api.Api;
import com.mbt.deal.api.ApiServer;
import com.mbt.deal.api.RxResultHelper;
import com.mbt.deal.api.RxSchedulers;
import com.mbt.deal.api.RxSubscriber;
import com.mbt.deal.bean.BaseDataBean;
import com.mbt.deal.ui.adapter.OrderViewPagerAdapter;
import com.mbt.deal.ui.widgets.PhotoAlbum;
import com.mbt.deal.ui.widgets.PicHandle;
import com.mbt.deal.ui.widgets.TransActionOrderView;
import com.mbt.deal.ui.widgets.XTextView;
import com.mbt.deal.util.LogUtils;
import com.mbt.deal.util.MyUtils;
import com.mbt.deal.util.ToastUtil;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

/**
 * 创建日期：2018/8/15 on 8:18
 * 描述:
 * 作者:王甜
 */
public class OrderListActivity extends BaseActivity implements ViewPager.OnPageChangeListener {

    @BindView(R2.id.ll_back)
    LinearLayout llBack;
    @BindView(R2.id.txt_title)
    TextView txtTitle;
    @BindView(R2.id.ll_right_menu)
    LinearLayout llRightMenu;
    @BindView(R2.id.viewpager)
    ViewPager viewPager;

    OrderViewPagerAdapter mOrderPagerAdapter;
    @BindView(R2.id.tv_jimai_ing)
    TextView tvJimaiIng;
    @BindView(R2.id.tv_jishou_ing)
    XTextView tvJishouIng;
    @BindView(R2.id.tv_sure_ing)
    XTextView tvSureIng;
    @BindView(R2.id.txt_order_finsh)
    TextView txtOrderFinsh;

    public static int orderId;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_list);
        ButterKnife.bind(this);

        initData();
    }

    private void initData() {
        mOrderPagerAdapter = new OrderViewPagerAdapter(act);
        viewPager.setAdapter(mOrderPagerAdapter);
        setSelected(0);
        txtTitle.setText("订单");
        llRightMenu.setVisibility(View.GONE);
        viewPager.setCurrentItem(0);
        viewPager.setOffscreenPageLimit(4);
        viewPager.addOnPageChangeListener(this);

    }

    @OnClick({R2.id.ll_back, R2.id.ll_right_menu, R2.id.tv_jimai_ing, R2.id.tv_jishou_ing, R2.id.tv_sure_ing, R2.id.txt_order_finsh})
    public void onViewClicked(View view) {
        int i = view.getId();
        if (i == R.id.ll_back) {
            finish();
        } else if (i == R.id.ll_right_menu) {

        } else if (i == R.id.tv_jimai_ing) {
            setSelected(0);
        } else if (i == R.id.tv_jishou_ing) {
            setSelected(1);
        } else if (i == R.id.tv_sure_ing) {
            setSelected(2);
        } else if (i == R.id.txt_order_finsh) {
            setSelected(3);
        }
    }

    /**
     * 设置选中
     *
     * @param type 0待处理1进行中2已完成
     */
    int type = 0;

    private void setSelected(int type) {
        this.type = type;
        switch (type) {
            case 0:
                tvJimaiIng.setSelected(true);
                tvJishouIng.setSelected(false);
                tvSureIng.setSelected(false);
                txtOrderFinsh.setSelected(false);
                break;
            case 1:
                tvJimaiIng.setSelected(false);
                tvJishouIng.setSelected(true);
                tvSureIng.setSelected(false);
                txtOrderFinsh.setSelected(false);
                break;
            case 2:
                tvJimaiIng.setSelected(false);
                tvJishouIng.setSelected(false);
                tvSureIng.setSelected(true);
                txtOrderFinsh.setSelected(false);
                break;
            case 3:
                tvJimaiIng.setSelected(false);
                tvJishouIng.setSelected(false);
                tvSureIng.setSelected(false);
                txtOrderFinsh.setSelected(true);
                break;
        }

        viewPager.setCurrentItem(type);
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        setSelected(position);
        TransActionOrderView viewByPoint = mOrderPagerAdapter.getViewByPoint(position);
        if (viewByPoint.isInit()) {
            viewByPoint.refreshData();
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PicHandle.OPEN_PHOTO && resultCode == Activity.RESULT_OK) {
            PhotoAlbum photoAlbum = new PhotoAlbum(this);
            String picImagePath = photoAlbum.getPicImagePath(data);
            Map map = new HashMap();
            map.put("id", orderId+"");
            OkHttpUtils.post().url("http://service.jiangaifen.com:38082/coinOrder/uploadOrderImg")
                    .addFile("file", "file", new File(picImagePath))
                    .addHeader("mbtToken", MyUtils.getToken(this))
                    .params(map)
                    .build().execute(new StringCallback() {
                @Override
                public void onError(Call call, Exception e) {
                    e.printStackTrace();
                }

                @Override
                public void onResponse(Call call, String s) {
                    LogUtils.i("返回数据了:"+s);
                    Gson gson = new Gson();
                    BaseDataBean baseDataBean = gson.fromJson(s, BaseDataBean.class);
                    if (baseDataBean.resultCode == 200){
                        ToastUtil.makeToast(act,"图片上传成功");
                    }else {
                        ToastUtil.makeToast(act,baseDataBean.msg);
                    }
                }
            });
       /*
            List<File> fileList = new ArrayList<>();
            fileList.add(new File(picImagePath));
            List<MultipartBody.Part> partList = filesToMultipartBodyParts(fileList);
            Map<String,RequestBody> params = new HashMap<>();
            params.put("id",convertToRequestBody(orderId+""));
            Api.getServer(ApiServer.class).uploadImage(params,partList).compose(RxSchedulers.<BaseDataBean>io_main())
                    .compose(RxResultHelper.<BaseDataBean>handleResult())
                    .subscribe(new RxSubscriber<BaseDataBean>(this) {
                        @Override
                        public void _onNext(BaseDataBean baseDataBean) {

                        }

                        @Override
                        public void _onError(String message) {

                        }

                        @Override
                        public void onCompleted() {

                        }
                    });*/
        }
    }

    private RequestBody convertToRequestBody(String param) {
        RequestBody requestBody = RequestBody.create(MediaType.parse("text/plain"), param);
        return requestBody;
    }

    private List<MultipartBody.Part> filesToMultipartBodyParts(List<File> files) {
        List<MultipartBody.Part> parts = new ArrayList<>(files.size());
        for (File file : files) {
            RequestBody requestBody = RequestBody.create(MediaType.parse("image/png"), file);
            MultipartBody.Part part = MultipartBody.Part.createFormData("multipartFiles", file.getName(), requestBody);
            parts.add(part);
        }
        return parts;
    }
}
