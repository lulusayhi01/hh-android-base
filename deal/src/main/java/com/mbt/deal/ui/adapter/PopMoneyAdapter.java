package com.mbt.deal.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.mbt.deal.R;

/**
 * 创建日期：2018/8/14 on 19:31
 * 描述:
 * 作者:王甜
 */
public class PopMoneyAdapter extends BaseAdapter {

    Context mContext;

    String[] moneyKinds = new String[]{"Vpay", "令", "比特币", "以太坊", "狗狗币"};

    public PopMoneyAdapter(Context context) {
        this.mContext = context;
    }

    @Override
    public int getCount() {
        return moneyKinds.length;
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {

        if (view == null || view.getTag() == null) {
            view = LayoutInflater.from(mContext).inflate(R.layout.item_right_pop_money, null);
        }

        TextView txt_money_kind = view.findViewById(R.id.txt_money_kind);

        View iv_line = view.findViewById(R.id.iv_line);

        txt_money_kind.setText(moneyKinds[position]);

        if (position == moneyKinds.length - 1) {
            iv_line.setVisibility(View.GONE);
        } else {
            iv_line.setVisibility(View.VISIBLE);
        }

        return view;
    }
}
