package com.mbt.deal.ui.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.mbt.deal.R;
import com.mbt.deal.api.Api;
import com.mbt.deal.api.ApiServer;
import com.mbt.deal.api.RxResultHelper;
import com.mbt.deal.api.RxSchedulers;
import com.mbt.deal.api.RxSubscriber;
import com.mbt.deal.bean.BaseDataBean;
import com.mbt.deal.bean.BaseListBean;
import com.mbt.deal.bean.MessageBean;
import com.mbt.deal.ui.widgets.TestPtrFrameLayout;
import com.mbt.deal.util.MyUtils;
import com.mbt.deal.util.ToastUtil;
import com.mbt.deal.widget.MyChart;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import in.srain.cube.views.ptr.PtrDefaultHandler2;
import in.srain.cube.views.ptr.PtrFrameLayout;

/**
 * @author Administrator
 * @createtime：2018/8/21 18:34
 */
public class MessageActivity extends BaseActivity {
    private TextView txt_title;
    private LinearLayout ll_back;
    private TestPtrFrameLayout ptrFrameLayout;
    private ListView listView;
    private TextView tv_no_data;
    private int pageNum = 1;
    private int pageSize = 5;
    private boolean noMoreData = false;
    private List<MessageBean> messageBeans = new ArrayList<>();
    private int totalConut;
    private MessageAdapter messageAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message_list);
        txt_title = findViewById(R.id.txt_title);
        txt_title.setText("公告列表");
        tv_no_data = findViewById(R.id.tv_no_data);
        ll_back = findViewById(R.id.ll_back);
        ll_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        ptrFrameLayout = findViewById(R.id.rotate_header_list_view_frame);
        listView = findViewById(R.id.list_transaction_record);
        messageAdapter = new MessageAdapter();
        listView.setAdapter(messageAdapter);
        ptrFrameLayout.setPtrHandler(new PtrDefaultHandler2() {
            @Override
            public void onLoadMoreBegin(PtrFrameLayout frame) {
                if (noMoreData) {
                    ToastUtil.makeToast(act, "已经展示全部数据");
                    ptrFrameLayout.refreshComplete();
                    return;
                }
                pageNum = pageNum + 1;
                getData();
            }

            @Override
            public void onRefreshBegin(PtrFrameLayout frame) {
                pageNum = 1;
                getData();
            }
        });
        ptrFrameLayout.autoRefresh();
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                int messageid = messageBeans.get(position).getId();
                Intent intent = new Intent(act, MessageDetailActivity.class);
                intent.putExtra("id", messageid);
                startActivity(intent);
            }
        });
    }

    private void getData() {
        Map map = new HashMap();

        map.put("pageNum", pageNum);
        map.put("pageSize", pageSize);
        Api.getServer(ApiServer.class).getmessages(MyUtils.getToken(act), map).compose(RxResultHelper.<BaseListBean<MessageBean>>handleResult())
                .compose(RxSchedulers.<BaseListBean<MessageBean>>io_main()).subscribe(new RxSubscriber<BaseListBean<MessageBean>>(act) {
            @Override
            public void _onNext(BaseListBean<MessageBean> stringBaseListBean) {
                if (pageNum == 1) {
                    messageBeans.clear();
                    totalConut = stringBaseListBean.count;
                    if (stringBaseListBean.data.size() == 0) {
                        tv_no_data.setVisibility(View.VISIBLE);
                    }

                }
                messageBeans.addAll(stringBaseListBean.data);
                messageAdapter.notifyDataSetChanged();
                if (messageBeans.size() == totalConut) {
                    noMoreData = true;
                }
            }

            @Override
            public void _onError(String message) {
                if (pageNum > 1) {
                    pageNum--;
                }
            }

            @Override
            public void onCompleted() {
                ptrFrameLayout.refreshComplete();
            }
        });
    }

    private class MessageAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return messageBeans.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = LayoutInflater.from(act).inflate(R.layout.message_list_iteam, parent, false);
            TextView content = view.findViewById(R.id.tv_content);
            content.setText(messageBeans.get(position).getTitle());
            return view;
        }
    }
}
