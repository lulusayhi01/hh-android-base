package com.mbt.deal.api;

import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.mbt.deal.util.LogUtils;

import org.json.JSONObject;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Converter;

/**
 * @author 赵盼龙
 * @createtime：2018/4/2 15:54
 */
public class AppGsonRequestBodyConverter<T> implements Converter<T, RequestBody> {
    private static final MediaType MEDIA_TYPE = MediaType.parse("application/json; charset=UTF-8");
    private static final Charset UTF_8 = Charset.forName("UTF-8");

    private final Gson gson;
    private final TypeAdapter<T> adapter;

    AppGsonRequestBodyConverter(Gson gson, TypeAdapter<T> adapter) {
        this.gson = gson;
        this.adapter = adapter;
    }

    @Override
    public RequestBody convert(T value) throws IOException {
        LogUtils.i("传入的数据:" + value.toString());
        String bodyString = null;
        if (value instanceof Map) {
            JSONObject jsonObject = new JSONObject((Map) value);
            LogUtils.i("传入的jsonobject:" + jsonObject.toString());
            bodyString = jsonObject.toString();
        } else if (value instanceof String) {
            bodyString = value.toString();
        } else {
            throw new ClassCastException();
        }

        return RequestBody.create(MEDIA_TYPE, bodyString);
    }
}
