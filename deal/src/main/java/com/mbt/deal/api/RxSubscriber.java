package com.mbt.deal.api;

import android.app.Activity;
import android.content.Context;
import android.widget.Toast;

import com.alibaba.android.arouter.launcher.ARouter;
import com.mbt.deal.bean.BaseBean;

import rx.Subscriber;

/**
 * @author 赵盼龙
 * @createtime：2018/4/3 9:40
 */
public abstract class RxSubscriber<T extends BaseBean> extends Subscriber<T> {
    private Context mContext;


    public RxSubscriber(Context context) {
        this.mContext = context;

    }

    @Override
    public void onStart() {
        super.onStart();
    }


    @Override
    public void onError(Throwable e) {
        // Toast.makeText(mContext, "网络异常", Toast.LENGTH_SHORT).show();
        _onError(e.getMessage());
        onCompleted();
    }

    @Override
    public void onNext(T t) {
        if (t.resultCode == 0) {
            _onNext(t);
        } else if (t.resultCode == 501) {
            Toast.makeText(mContext, t.msg, Toast.LENGTH_SHORT).show();
            RxBus.getInstance().cleanRxBus();
            ARouter.getInstance().build("/acitvity/login").navigation();
        } else if (t.resultCode == 50001){
            ARouter.getInstance().build("/personal/PersonAssountListActivity").navigation();
        }else {
            Toast.makeText(mContext, t.msg, Toast.LENGTH_SHORT).show();
            _onError(t.resultCode + "");
        }

    }

    public abstract void _onNext(T t);

    public abstract void _onError(String message);

    @Override
    public abstract void onCompleted();

}
