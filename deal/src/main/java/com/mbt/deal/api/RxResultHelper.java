package com.mbt.deal.api;

import com.mbt.deal.bean.BaseBean;

import rx.Observable;
import rx.functions.Func1;

/**
 * desc：Rx处理服务器返回,
 */

public class RxResultHelper {
    public static <T extends BaseBean> Observable.Transformer<T, T> handleResult() {
        return new Observable.Transformer<T, T>() {
            @Override
            public Observable<T> call(Observable<T> tObservable) {
                return tObservable.flatMap(new Func1<T, Observable<T>>() {
                    @Override
                    public Observable<T> call(T t) {
                        return Observable.just(t);
                    }
                });
            }
        };
    }

}
