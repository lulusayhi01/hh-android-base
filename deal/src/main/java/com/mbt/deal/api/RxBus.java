package com.mbt.deal.api;

import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import rx.Observable;
import rx.subjects.PublishSubject;
import rx.subjects.Subject;


/**
 * https://juejin.im/entry/58ff2e26a0bb9f0065d2c5f2
 *
 * @author 赵盼龙
 */


public class RxBus {

    private ConcurrentHashMap<Object, List<Subject>> subjectMapper = new ConcurrentHashMap<>();

    private RxBus() {

    }

    @NonNull
    public static RxBus getInstance() {
        return Holder.instance;
    }

    @NonNull
    public <T> Observable<T> register(@NonNull Class<T> clz) {
        return register(clz.getName());
    }

    @NonNull
    public <T> Observable<T> register(@NonNull Object tag) {
        List<Subject> subjectList = subjectMapper.get(tag);
        if (null == subjectList) {
            subjectList = new ArrayList<>();
            subjectMapper.put(tag, subjectList);
        }

        Subject<T, T> subject = PublishSubject.create();
        subjectList.add(subject);

        //System.out.println("注册到rxbus");
        return subject;
    }

    public <T> void unregister(@NonNull Class<T> clz, @NonNull Observable observable) {
        unregister(clz.getName(), observable);
    }

    public void unregister(@NonNull Object tag, @NonNull Observable observable) {
        List<Subject> subjects = subjectMapper.get(tag);
        if (null != subjects) {
            subjects.remove(observable);
            if (subjects.isEmpty()) {
                subjectMapper.remove(tag);
                //System.out.println("从rxbus取消注册");
            }
        }
    }

    public void post(@NonNull Object content) {
        post(content.getClass().getName(), content);
    }

    public void post(@NonNull Object tag, @NonNull Object content) {
        try {
            List<Subject> subjects = subjectMapper.get(tag);
            if (!subjects.isEmpty()) {
                for (Subject subject : subjects) {
                    subject.onNext(content);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static class Holder {
        private static RxBus instance = new RxBus();
    }

    public void cleanRxBus() {
        subjectMapper.clear();
    }
}
