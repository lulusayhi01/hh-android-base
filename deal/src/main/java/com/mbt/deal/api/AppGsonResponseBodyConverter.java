package com.mbt.deal.api;


import com.google.gson.Gson;
import com.mbt.deal.util.LogUtils;

import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Type;

import okhttp3.ResponseBody;
import retrofit2.Converter;

/**
 * @author 赵盼龙
 * @createtime：2018/4/2 15:07
 */
public class AppGsonResponseBodyConverter<T> implements Converter<ResponseBody, T> {
    private Gson gson;
    private Type type;

    public AppGsonResponseBodyConverter(Gson gson, Type type) {
        this.gson = gson;
        this.type = type;
    }

    @Override
    public T convert(ResponseBody value) throws IOException {
        try {
            String response = value.string();
            LogUtils.i("返回的数据结果::"+response);
            JSONObject jsonObject = new JSONObject(response);
            return gson.fromJson(jsonObject.toString(), type);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

}
