package com.mbt.deal.api;

import com.mbt.deal.bean.BaseBean;
import com.mbt.deal.bean.BaseDataBean;
import com.mbt.deal.bean.BaseListBean;
import com.mbt.deal.bean.CoinListBean;
import com.mbt.deal.bean.CoinPriceBean;
import com.mbt.deal.bean.KLinebean;
import com.mbt.deal.bean.MessageBean;
import com.mbt.deal.bean.MessageDetailBean;
import com.mbt.deal.bean.NowPriceBean;
import com.mbt.deal.bean.NullBean;
import com.mbt.deal.bean.OrderBean;
import com.mbt.deal.bean.OrderListBean;
import com.mbt.deal.bean.TransActionRecordBean;
import com.mbt.deal.bean.UserCoinCount;
import com.mbt.deal.bean.UserInfoBean;

import java.util.List;
import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Converter;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import rx.Observable;

/**
 * @author Administrator
 * @createtime：2018/8/15 23:47
 */
public interface ApiServer {
    public String URL = " http://service.jiangaifen.com:38082/";
    public Converter.Factory FACTORY = AppGsonConverterFactory.create();

    @POST("coin/list")
    @FormUrlEncoded
    public Observable<BaseListBean<CoinListBean>> getCurrencyList(@Header("mbtToken") String token, @FieldMap Map<String, Object> map);

    @POST("coinPrice/currentMaxMin")
    @FormUrlEncoded
    public Observable<BaseDataBean<CoinPriceBean>> getCoinPrice(@Header("mbtToken") String token, @FieldMap Map<String, Object> map);

    @POST("coin/getCoinCountByUser")
    @FormUrlEncoded
    public Observable<BaseDataBean<String>> getCoinCount(@Header("mbtToken") String token, @FieldMap Map<String, Object> map);

    @POST("coinOrder/listByPage")
    @FormUrlEncoded
    public Observable<BaseListBean<OrderBean>> getorderList(@Header("mbtToken") String token, @FieldMap Map<String, Object> map);

    @POST("userDeal/listByPage")
    @FormUrlEncoded
    public Observable<BaseListBean<TransActionRecordBean>> getTransactionList(@Header("mbtToken") String token, @FieldMap Map<String, Object> map);

    @POST("coinOrder/listHisByPage")
    @FormUrlEncoded
    public Observable<BaseListBean<OrderListBean>> getOrderList(@Header("mbtToken") String token, @FieldMap Map<String, Object> map);

    @POST("coinOrder/sendSell2")
    @FormUrlEncoded
    public Observable<BaseDataBean<String>> goSaleOrder(@Header("mbtToken") String token, @FieldMap Map<String, Object> map);

    @POST("coinOrder/sendBuy2")
    @FormUrlEncoded
    public Observable<BaseDataBean<String>> goBuyOrder(@Header("mbtToken") String token, @FieldMap Map<String, Object> map);

    @POST("userPwd/checkUserNumberPwd")
    @FormUrlEncoded
    public Observable<BaseDataBean<String>> checkPassword(@Header("mbtToken") String token, @FieldMap Map<String, Object> map);

    @POST("coinOrder/buy2")
    @FormUrlEncoded
    public Observable<BaseDataBean<String>> buyorder(@Header("mbtToken") String token, @FieldMap Map<String, Object> map);

    @POST("coinOrder/sell2")
    @FormUrlEncoded
    public Observable<BaseDataBean<String>> saleorder(@Header("mbtToken") String token, @FieldMap Map<String, Object> map);

    @POST("user/addMon")
    @FormUrlEncoded
    public Observable<BaseDataBean<String>> recharge(@Header("mbtToken") String token, @FieldMap Map<String, Object> map);

    @POST("user/getUser")
    @FormUrlEncoded
    public Observable<BaseDataBean<UserInfoBean>> getuserinfo(@Header("mbtToken") String token, @FieldMap Map<String, Object> map);

    @POST("message/listByPage")
    @FormUrlEncoded
    public Observable<BaseListBean<MessageBean>> getmessages(@Header("mbtToken") String token, @FieldMap Map<String, Object> map);

    @POST("message/detail")
    @FormUrlEncoded
    public Observable<BaseDataBean<MessageDetailBean>> getMessageDetail(@Header("mbtToken") String token, @FieldMap Map<String, Object> map);

    @POST("coinPrice/listByPage")
    @FormUrlEncoded
    public Observable<BaseListBean<KLinebean>> getPagePrice(@Header("mbtToken") String token, @FieldMap Map<String, Object> map);

    @POST("coinPrice/current")
    @FormUrlEncoded
    public Observable<BaseDataBean<NowPriceBean>> getNowPice(@Header("mbtToken") String token, @FieldMap Map<String, Object> map);

    @POST("coinOrder/affirmTransfer")
    @FormUrlEncoded
    public Observable<BaseDataBean> surezhuangzhang(@Header("mbtToken") String token, @FieldMap Map<String, Object> map);

    @POST("coinOrder/affirmOrder")
    @FormUrlEncoded
    public Observable<BaseDataBean> daozhang(@Header("mbtToken") String token, @FieldMap Map<String, Object> map);

    @Multipart
    @POST("coinOrder/uploadOrderImg")
    public Observable<BaseDataBean> uploadImage(@PartMap Map<String, RequestBody> map, @Part List<MultipartBody.Part> parts);
}
