package com.sj.hh.wxapi;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.tencent.mm.opensdk.modelbase.BaseReq;
import com.tencent.mm.opensdk.modelbase.BaseResp;
import com.tencent.mm.opensdk.openapi.IWXAPI;
import com.tencent.mm.opensdk.openapi.IWXAPIEventHandler;

@Route(path = "/app/WXEntryActivity")
public class WXEntryActivity extends AppCompatActivity implements IWXAPIEventHandler {

    private IWXAPI api;

    @Autowired()
    String code;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        ARouter.getInstance().inject(this);
//        api = WXAPIFactory.createWXAPI(this, "wxb4bb90d9d2814a7c", true);
//        api.handleIntent(getIntent(), this);
//
//        api.registerApp("wxb4bb90d9d2814a7c");
//
//        WXTextObject textObj = new WXTextObject();
//        textObj.text = "医疗\n我下在使用医疗APP，新型社区APP，你也来试试吧！邀请码："+code;
//
//        WXMediaMessage msg = new WXMediaMessage();
//        msg.mediaObject = textObj;
//        msg.description = "医疗\n我下在使用医疗APP，新型社区APP，你也来试试吧！邀请码："+code;
//
//        SendMessageToWX.Req req = new SendMessageToWX.Req();
//        req.transaction = "text" + System.currentTimeMillis();
//        req.message = msg;
//        req.scene = SendMessageToWX.Req.WXSceneSession;
//        api.sendReq(req);
        finish();
    }

    @Override
    public void onReq(BaseReq baseReq) {
    }

    @Override
    public void onResp(BaseResp baseResp) {
//        ARouter.getInstance().build("/activity/main").navigation();
//        finish();
        finish();
    }

    @Override
    public void onBackPressed() {
//       finish();
    }
}
