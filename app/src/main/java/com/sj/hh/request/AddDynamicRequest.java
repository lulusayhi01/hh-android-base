package com.sj.hh.request;

import android.content.Context;
import android.text.TextUtils;


import com.sj.hh.request.base.BaseRequestClient;
import com.sj.hh.request.exception.BmobException;

import java.util.ArrayList;
import java.util.List;

import cn.rongcloud.im.server.network.async.OnDataListener;
import cn.rongcloud.im.server.network.http.HttpException;
import cn.rongcloud.im.server.response.friendcircle.MomentContent;
import cn.rongcloud.im.server.response.friendcircle.MomentsInfo;
import cn.rongcloud.im.server.response.user.UserInfo;


/**
 * Created by huluhong on 2018/7/28.
 * <p>
 * 添加动态(暂时不处理文件上传)
 */

public class AddDynamicRequest extends BaseRequestClient<String> {

    private String authId;
    private String hostId;
    private String imgId;//imgId=上传图片返回的id（可传多个，分割）
    private MomentContent momentContent;
    private List<UserInfo> likesUserId;

    public AddDynamicRequest() {
        momentContent = new MomentContent();
        likesUserId = new ArrayList<>();
    }

    public AddDynamicRequest setAuthId(String authId) {
        this.authId = authId;
        return this;
    }

    public AddDynamicRequest setHostId(String hostId) {
        this.hostId = hostId;
        return this;
    }

    @Override
    protected void executeInternal(Context context, final int requestType, boolean showDialog) {
        if (mAsyncTaskManager != null) {
            mAsyncTaskManager.request(context, requestType, new OnDataListener() {
                @Override
                public Object doInBackground(int requestCode, String parameter) throws HttpException {
                    return mAction.addTrends(momentContent.getText(), imgId);
                }

                @Override
                public void onSuccess(int requestCode, Object result) {
                    if (result != null) {
                        MomentsInfo momentsInfo = (MomentsInfo) result;
                        onResponseSuccess("添加成功", requestType);
                    } else {
                        BmobException bmobException = new BmobException("朋友圈发布失败！");
                        onResponseError(bmobException, requestType);
                    }
                }

                @Override
                public void onFailure(int requestCode, int state, Object result) {

                }
            });
        }
        if (checkValided()) {
            MomentsInfo momentsInfo = new MomentsInfo();

//            UserInfo author = new UserInfo();
//            author.setObjectId(authId);
//            momentsInfo.setAuthor(author);
//
//            UserInfo host = new UserInfo();
//            host.setObjectId(hostId);
//            momentsInfo.setHostinfo(host);
//
//            momentsInfo.setContent(momentContent);
//
//            momentsInfo.save(new SaveListener<String>() {
//                @Override
//                public void done(String s, BmobException e) {
//                    if (e == null) {
//                        if (ToolUtil.isListEmpty(likesUserId)) {
//                            onResponseSuccess(s, requestType);
//                        } else {
//                            MomentsInfo resultMoment = new MomentsInfo();
//                            resultMoment.setObjectId(s);
//
//                            //关联点赞的
//                            BmobRelation relation = new BmobRelation();
//                            for (UserInfo user : likesUserId) {
//                                relation.add(user);
//                            }
//                            resultMoment.setLikesBmobRelation(relation);
//                            resultMoment.update(new UpdateListener() {
//                                @Override
//                                public void done(BmobException e) {
//                                    if (e == null) {
//                                        onResponseSuccess("添加成功", requestType);
//                                    } else {
//                                        onResponseError(e, requestType);
//                                    }
//                                }
//                            });
//                        }
//
//                    }
//                }
//            });

        }
    }


    private boolean checkValided() {
        return !(TextUtils.isEmpty(authId) || TextUtils.isEmpty(hostId)) && momentContent.isValided();
    }

    public AddDynamicRequest addText(String text) {
        momentContent.addText(text);
        return this;
    }

    public AddDynamicRequest addPicture(String pic) {
        momentContent.addPicture(pic);
        return this;
    }

    public AddDynamicRequest addWebUrl(String webUrl) {
        momentContent.addWebUrl(webUrl);
        return this;
    }

    public AddDynamicRequest addWebTitle(String webTitle) {
        momentContent.addWebTitle(webTitle);
        return this;
    }

    public AddDynamicRequest addWebImage(String webImage) {
        momentContent.addWebImage(webImage);
        return this;
    }

    public AddDynamicRequest addLikes(UserInfo user) {
        likesUserId.add(user);
        return this;
    }

    public AddDynamicRequest setImgId(String imgId) {
        this.imgId = imgId;
        return this;
    }
}
