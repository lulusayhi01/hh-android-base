package com.sj.hh.request.base;


import com.sj.hh.request.exception.BmobException;

/**
 * Created by huluhong on 2018/7/27.
 */

public interface OnResponseListener<T> {
    void onStart(int requestType);

    void onSuccess(T response, int requestType);

    void onError(BmobException e, int requestType);

    abstract class SimpleResponseListener<T> implements OnResponseListener<T> {

        @Override
        public void onStart(int requestType) {

        }


        @Override
        public void onError(BmobException e, int requestType) {

        }
    }
}
