package com.sj.hh.request;

import android.content.Context;
import android.text.TextUtils;

import com.sj.hh.request.base.BaseRequestClient;
import com.sj.hh.request.exception.BmobException;

import java.util.ArrayList;

import cn.rongcloud.im.server.network.async.OnDataListener;
import cn.rongcloud.im.server.network.http.HttpException;
import cn.rongcloud.im.server.response.BaseResponse;


/**
 * Created by huluhong on 2018/7/13.
 * <p>
 * 删除评论
 */

public class DeleteCommentRequest extends BaseRequestClient<String> {

    private String commentid;

    public void setCommentid(String commentid) {
        this.commentid = commentid;
    }


    @Override
    protected void executeInternal(Context context, final int requestType, boolean showDialog) {
        if (TextUtils.isEmpty(commentid)) {
            onResponseError(new BmobException("评论ID为空"), getRequestType());
            return;
        }
//        CommentInfo commentInfo = new CommentInfo();
//        commentInfo.setObjectId(commentid);
//        commentInfo.delete(new UpdateListener() {
//            @Override
//            public void done(BmobException e) {
//                if (e == null) {
//                    onResponseSuccess(commentid, requestType);
//                } else {
//                    onResponseError(e, requestType);
//                }
//            }
//        });
        if (mAsyncTaskManager != null) {
            mAsyncTaskManager.request(context, requestType, new OnDataListener() {
                @Override
                public Object doInBackground(int requestCode, String parameter) throws HttpException {
                    ArrayList<String> list = new ArrayList<String>() ;
                    list.add(commentid);
                    return mAction.deleteComment(list);
                }

                @Override
                public void onSuccess(int requestCode, Object result) {
                    if (result != null) {
                        BaseResponse baseResponse = (BaseResponse) result;
                        if (baseResponse.getResultCode() == 0) {
                            onResponseSuccess(commentid, requestType);
                        } else {
                            BmobException bmobException = new BmobException("删除评论失败！");
                            onResponseError(bmobException, requestType);
                        }
                    } else {
                        BmobException bmobException = new BmobException("删除评论失败！");
                        onResponseError(bmobException, requestType);
                    }
                }

                @Override
                public void onFailure(int requestCode, int state, Object result) {

                }
            });
        }
    }
}
