package com.sj.hh.request;

import android.content.Context;

import com.sj.hh.request.base.BaseRequestClient;
import com.sj.hh.request.exception.BmobException;

import cn.rongcloud.im.server.network.async.OnDataListener;
import cn.rongcloud.im.server.network.http.HttpException;
import cn.rongcloud.im.server.response.BaseResponse;


/**
 * Created by huluhong on 2018/7/6.
 */

public class AddLikeRequest extends BaseRequestClient<String> {

    private Context mContext;
    private String momentsId;
    private String userid;
    private String trendsid;

    public AddLikeRequest(Context context, String trendsid) {
        this.mContext = context;
        this.trendsid = trendsid;

    }

    public String getMomentsId() {
        return momentsId;
    }

    public AddLikeRequest setMomentsId(String momentsId) {
        this.momentsId = momentsId;
        return this;
    }

    public String getUserid() {
        return userid;
    }

    public AddLikeRequest setUserid(String userid) {
        this.userid = userid;
        return this;
    }

    public String getTrendsid() {
        return trendsid;
    }

    public AddLikeRequest setTrendsid(String trendsid) {
        this.trendsid = trendsid;
        return this;
    }

    @Override
    protected void executeInternal(Context context, final int requestType, boolean showDialog) {
//        LikesInfo info = new LikesInfo();
//        info.setMomentsid(momentsId);
//        info.setUserid(userid);
//        info.save(new SaveListener<String>() {
//            @Override
//            public void done(String s, BmobException e) {
//                KLog.i(s);
//                onResponseSuccess(s, requestType);
//            }
//        });

        if (mAsyncTaskManager != null) {
            mAsyncTaskManager.request(context, requestType, new OnDataListener() {
                @Override
                public Object doInBackground(int requestCode, String parameter) throws HttpException {
                    return mAction.trendsCommentLike(trendsid);
                }

                @Override
                public void onSuccess(int requestCode, Object result) {
                    if (result != null) {
                        BaseResponse baseResponse = (BaseResponse) result;
                        if (baseResponse.getResultCode() == 0) {
                            onResponseSuccess(baseResponse.getResultCode() + "", requestType);
                        } else {
                            BmobException bmobException = new BmobException("点赞失败！");
                            onResponseError(bmobException, requestType);
                        }
                    } else {
                        BmobException bmobException = new BmobException("点赞失败！");
                        onResponseError(bmobException, requestType);
                    }
                }

                @Override
                public void onFailure(int requestCode, int state, Object result) {

                }
            });
        }

    }
}
