package com.sj.hh.request;

import android.content.Context;

import com.sj.hh.request.base.BaseRequestClient;
import com.sj.hh.request.exception.BmobException;

import cn.rongcloud.im.server.network.async.OnDataListener;
import cn.rongcloud.im.server.network.http.HttpException;
import cn.rongcloud.im.server.response.friendcircle.MomentsInfo;


/**
 * Created by huluhong on 2018/7/28.
 * <p>
 * 朋友圈删除动态---图片
 */

public class DeleteDynamicImageRequest extends BaseRequestClient<Boolean> {

    private String imgId;//imgId=上传图片返回的id（可传多个，分割）

    public DeleteDynamicImageRequest() {
    }

    @Override
    protected void executeInternal(Context context, final int requestType, boolean showDialog) {
        if (mAsyncTaskManager != null) {
            mAsyncTaskManager.request(context, requestType, new OnDataListener() {
                @Override
                public Object doInBackground(int requestCode, String parameter) throws HttpException {
                    return mAction.deleteImg(imgId);
                }

                @Override
                public void onSuccess(int requestCode, Object result) {
                    if (result != null) {
                        MomentsInfo momentsInfo = (MomentsInfo) result;
                        onResponseSuccess(true, requestType);
                    } else {
                        BmobException bmobException = new BmobException("朋友圈发布失败！");
                        onResponseError(bmobException, requestType);
                    }
                }

                @Override
                public void onFailure(int requestCode, int state, Object result) {

                }
            });
        }
    }

    public DeleteDynamicImageRequest setImgId(String imgId) {
        this.imgId = imgId;
        return this;
    }
}
