package com.sj.hh.request.base;


import android.content.Context;

import com.sj.hh.request.exception.BmobException;

import cn.rongcloud.im.server.SealAction;
import cn.rongcloud.im.server.network.async.AsyncTaskManager;

/**
 * Created by huluhong on 2018/7/27.
 * <p>
 * 封装bmob的请求
 */

public abstract class BaseRequestClient<T> {
    private int requestType = 0x10;
    private boolean showDialog;
    private OnResponseListener<T> onResponseListener;

    protected AsyncTaskManager mAsyncTaskManager;
    protected Context mContext;
    protected SealAction mAction;

    public BaseRequestClient() {

    }

//    public void execute() {
//        execute(false);
//    }

    public void execute(Context context){
        this.mContext = context;
        mAsyncTaskManager = AsyncTaskManager.getInstance(mContext);
        mAction = new SealAction(mContext);
        execute(mContext , false);
    }

    public void execute(Context mContext, boolean showDialog) {
        this.showDialog = showDialog;
        onResponseStart(requestType);
        executeInternal(mContext ,requestType, showDialog);
    }

    protected abstract void executeInternal(Context context, final int requestType, final boolean showDialog);

    protected void onResponseStart(int requestType) {
        if (onResponseListener != null) {
            onResponseListener.onStart(requestType);
        }
    }

    protected void onResponseSuccess(T response, int requestType) {
        if (onResponseListener != null) {
            onResponseListener.onSuccess(response, requestType);
        }
    }

    protected void onResponseError(BmobException e, int requestType) {
        if (onResponseListener != null) {
            onResponseListener.onError(e, requestType);
        }
    }

    public int getRequestType() {
        return requestType;
    }

    public void setRequestType(int requestType) {
        this.requestType = requestType;
    }

    public boolean isShowDialog() {
        return showDialog;
    }

    public void setShowDialog(boolean showDialog) {
        this.showDialog = showDialog;
    }

    public OnResponseListener<T> getOnResponseListener() {
        return onResponseListener;
    }

    public BaseRequestClient setOnResponseListener(OnResponseListener<T> onResponseListener) {
        this.onResponseListener = onResponseListener;
        return this;
    }
}
