package com.sj.hh.request;

import android.content.Context;
import android.text.TextUtils;

import com.sj.hh.request.base.BaseRequestClient;
import com.sj.hh.request.exception.BmobException;

import cn.rongcloud.im.server.network.async.OnDataListener;
import cn.rongcloud.im.server.network.http.HttpException;
import cn.rongcloud.im.server.response.BaseResponse;


/**
 * Created by huluhong on 2018/7/8.
 */

public class UnLikeRequest extends BaseRequestClient<Boolean> {

    private String likesInfoid;
    private String trendsid;

    public UnLikeRequest(String trendsid) {
        this.trendsid = trendsid;
    }

    @Override
    protected void executeInternal(Context context, final int requestType, boolean showDialog) {
        if (TextUtils.isEmpty(trendsid)) {
            onResponseError(new BmobException("likesinfoid为空"), requestType);
            return;
        }
        if (mAsyncTaskManager != null) {
            mAsyncTaskManager.request(context, requestType, new OnDataListener() {
                @Override
                public Object doInBackground(int requestCode, String parameter) throws HttpException {
                    return mAction.trendsCommentLike(trendsid);
                }

                @Override
                public void onSuccess(int requestCode, Object result) {
                    if (result != null) {
                        BaseResponse baseResponse = (BaseResponse) result;
                        if (baseResponse.getResultCode() == 0) {
                            onResponseSuccess(true, requestType);
                        } else {
                            BmobException bmobException = new BmobException("取消点赞失败！");
                            onResponseError(bmobException, requestType);
                        }
                    } else {
                        BmobException bmobException = new BmobException("取消点赞失败！");
                        onResponseError(bmobException, requestType);
                    }
                }

                @Override
                public void onFailure(int requestCode, int state, Object result) {

                }
            });
        }
    }
}
