package com.sj.hh.request;

import android.content.Context;
import android.text.TextUtils;

import com.sj.friendcircle.lib.utils.StringUtil;
import com.sj.hh.request.base.BaseRequestClient;
import com.sj.hh.request.exception.BmobException;

import cn.rongcloud.im.server.network.async.OnDataListener;
import cn.rongcloud.im.server.network.http.HttpException;
import cn.rongcloud.im.server.response.friendcircle.CommentInfo;
import cn.rongcloud.im.server.response.friendcircle.MomentsInfo;
import cn.rongcloud.im.server.response.user.UserInfo;


/**
 * Created by huluhong on 2018/7/28.
 * <p>
 * 添加评论
 */

public class AddCommentRequest extends BaseRequestClient<CommentInfo> {

    private String content;
    private String authorId;
    private String replyUserId;
    private String momentsInfoId;//动态详情id
    private String commentid;
    private String trendsid;//动态详情id
    private UserInfo author;

    public void setContent(String content) {
        this.content = content;
    }

    public void setAuthorId(String authorId) {
        this.authorId = authorId;
    }

    public void setReplyUserId(String replyUserId) {
        this.replyUserId = replyUserId;
    }

    public void setMomentsInfoId(String momentsInfoId) {
        this.momentsInfoId = momentsInfoId;
    }

    public void setTrendsid(String trendsid) {
        this.trendsid = trendsid;
    }

    public void setAuthor(UserInfo author) {
        this.author = author;
    }

    @Override
    protected void executeInternal(Context context, final int requestType, boolean showDialog) {
        if (TextUtils.isEmpty(authorId)) {
            onResponseError(new BmobException("创建用户为空"), getRequestType());
            return;
        }
        if (TextUtils.isEmpty(momentsInfoId)) {
            onResponseError(new BmobException("动态为空"), getRequestType());
            return;
        }

        final CommentInfo commentInfo = new CommentInfo();
        commentInfo.setContent(content);

        UserInfo userInfo = this.author;
        UserInfo author = new UserInfo();
        author.setUserid(authorId);
        String nick;
        if (userInfo != null && !TextUtils.isEmpty(userInfo.getNick())){
            nick = userInfo.getNick();
        }else {
            nick = userInfo.getUsername();
        }
        author.setNick(nick);
        commentInfo.setAuthor(author);

        if (StringUtil.noEmpty(replyUserId)) {
            UserInfo replyUser = new UserInfo();
            replyUser.setUserid(replyUserId);
            commentInfo.setReply(replyUser);
        }

        MomentsInfo momentsInfo = new MomentsInfo();
//        momentsInfo.setObjectId(momentsInfoId);
        commentInfo.setMoment(momentsInfo);

        if (mAsyncTaskManager != null) {
            mAsyncTaskManager.request(context, requestType, new OnDataListener() {
                @Override
                public Object doInBackground(int requestCode, String parameter) throws HttpException {
                    return mAction.addComment(momentsInfoId,commentid,content);
                }

                @Override
                public void onSuccess(int requestCode, Object result) {
                    if (result != null) {
                        MomentsInfo momentsInfo = (MomentsInfo) result;
                        if (momentsInfo.getResultCode() == 0){
                            onResponseSuccess(commentInfo, requestType);
                        }else {
                            BmobException bmobException = new BmobException("评论失败！");
                            onResponseError(bmobException, requestType);
                        }
                    } else {
                        BmobException bmobException = new BmobException("评论失败！");
                        onResponseError(bmobException, requestType);
                    }
                }

                @Override
                public void onFailure(int requestCode, int state, Object result) {

                }
            });
        }



//        commentInfo.save(new SaveListener<String>() {
//            @Override
//            public void done(String s, BmobException e) {
//                if (e == null) {
//                    BmobQuery<CommentInfo> commentQuery = new BmobQuery<CommentInfo>();
//                    commentQuery.include(CommentFields.AUTHOR_USER + "," + CommentFields.REPLY_USER + "," + CommentFields.MOMENT);
//                    commentQuery.getObject(s, new QueryListener<CommentInfo>() {
//                        @Override
//                        public void done(CommentInfo commentInfo, BmobException e) {
//                            if (e == null) {
//                                onResponseSuccess(commentInfo, requestType);
//                            } else {
//                                onResponseError(e, requestType);
//                            }
//                        }
//                    });
//                } else {
//                    onResponseError(e, requestType);
//                }
//            }
//        });
    }
}
