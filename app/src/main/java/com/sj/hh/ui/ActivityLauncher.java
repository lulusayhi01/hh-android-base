package com.sj.hh.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;


import com.sj.friendcircle.common.router.RouterList;
import com.sj.friendcircle.lib.entity.ImageInfo;
import com.sj.friendcircle.photoselect.view.PhotoSelectActivity;
import com.sj.friendcircle.ui.util.SwitchActivityTransitionUtil;
import com.sj.hh.ui.gallery.PhotoBrowseActivity;
import com.sj.hh.ui.publish.PublishActivity;

import java.util.ArrayList;
import java.util.List;

import com.sj.friendcircle.lib.entity.PhotoBrowseInfo;
import cn.rongcloud.im.server.response.friendcircle.other.ServiceInfo;


/**
 * activity发射器~
 */

public class ActivityLauncher {

    /**
     * 发射到发布朋友圈页面
     *
     * @param act
     * @param mode
     * @param requestCode
     */
    public static void startToPublishActivityWithResult(Activity act, @RouterList.PublishActivity int mode, @Nullable List<ImageInfo> selectedPhotos, int requestCode) {
        Intent intent = new Intent(act, PublishActivity.class);
        intent.putExtra(RouterList.PublishActivity.key_mode, mode);
        if (selectedPhotos != null) {
            intent.putParcelableArrayListExtra(RouterList.PublishActivity.key_photoList, (ArrayList<? extends Parcelable>) selectedPhotos);
        }
        act.startActivityForResult(intent, requestCode);
        SwitchActivityTransitionUtil.transitionVerticalIn(act);
    }


    /**
     * 图片浏览
     * @param act
     * @param info
     */
    public static void startToPhotoBrosweActivity(Activity act, @NonNull PhotoBrowseInfo info) {
        if (info == null) return;
        PhotoBrowseActivity.startToPhotoBrowseActivity(act, info);
    }

    /**
     * 发射到选择图片页面
     *
     * @param act
     */
    public static void startToPhotoSelectActivity(Activity act, int requestCode) {
        Intent intent = new Intent(act, PhotoSelectActivity.class);
        act.startActivityForResult(intent, requestCode);
        SwitchActivityTransitionUtil.transitionVerticalIn(act);
    }

    /**
     * 发射到服务器消息页面
     *
     * @param act
     */
    public static void startToServiceInfoActivity(Activity act, ServiceInfo info) {
//        Intent intent = new Intent(act, ServiceInfoActivity.class);
//        intent.putExtra("info", info);
//        act.startActivity(intent);
//        SwitchActivityTransitionUtil.transitionVerticalIn(act);
    }

}
