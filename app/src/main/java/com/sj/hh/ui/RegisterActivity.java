package com.sj.hh.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.AppCompatEditText;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.sj.hh.R;
import com.sj.hh.util.CountDownTimerUtils;
import com.sj.hh.widget.ClearEditText;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.rongcloud.im.server.network.http.HttpException;
import cn.rongcloud.im.server.response.RegisterResponse;
import cn.rongcloud.im.server.response.SendCodeResponse;
import cn.rongcloud.im.server.utils.AMUtils;
import cn.rongcloud.im.server.utils.NToast;
import cn.rongcloud.im.server.widget.LoadDialog;
import cn.rongcloud.im.ui.activity.base.BaseActivity;
import cn.rongcloud.im.ui.widget.switchbutton.SwitchButton;


/**
 * 注册
 */
public class RegisterActivity extends BaseActivity {

    private static final int SEND_CODE = 2;
    private static final int REGISTER = 4;

    private static final int REGISTER_BACK = 1001;

    @BindView(R.id.ll_navigation_bar_left_back)
    LinearLayout llNavigationBarLeftBack;
    @BindView(R.id.tv_navigation_bar_center)
    TextView tvNavigationBarCenter;
    @BindView(R.id.tv_verification_code)
    TextView tvVerificationCode;
    @BindView(R.id.checkbox)
    CheckBox checkbox;
    @BindView(R.id.ll_register_protocol)
    LinearLayout llRegisterProtocol;
    @BindView(R.id.submit)
    Button submit;
    @BindView(R.id.edit_phone)
    AppCompatEditText editPhone;
    @BindView(R.id.edit_vcode)
    ClearEditText editVcode;
    @BindView(R.id.edit_password)
    ClearEditText editPassword;
    @BindView(R.id.edit_passsure)
    ClearEditText editPasssure;
    @BindView(R.id.switch_button)
    SwitchButton switchButton;

    private CountDownTimerUtils mCountDownTimerUtils;
    private String mPhone, mCode, mNickName, mPassword, mCodeToken, mPasssure;
    private String type = "0";//0普通  1医生


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);

        tvNavigationBarCenter.setText("注册账号");
        setHeadVisibility(View.GONE);
        initView();

        editPhone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //S：变化后的所有字符；start：字符起始的位置；before: 变化之前的总字节数；count:变化后的字节数
//                Toast.makeText(getApplicationContext(), "变化后:"+s+";"+start+";"+before+";"+count, Toast.LENGTH_SHORT).show();
                if (!TextUtils.isEmpty(s) && s.length() >= 11) {
                    if (AMUtils.isMobile(s.toString().trim())) {
                        mPhone = s.toString().trim();
                        tvVerificationCode.setTextColor(getResources().getColor(R.color.C_main));
                        tvVerificationCode.setClickable(true);
                        AMUtils.onInactive(mContext, editPhone);
                    } else {
                        Toast.makeText(mContext, com.mbt.im.R.string.Illegal_phone_number, Toast.LENGTH_SHORT).show();
                    }
                } else {
                    tvVerificationCode.setTextColor(getResources().getColor(R.color.c_6));
                    tvVerificationCode.setClickable(false);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void initView() {

    }

    @Override
    public Object doInBackground(int requestCode, String id) throws HttpException {
        switch (requestCode) {
            case SEND_CODE:
                return action.sendCode("86", mPhone);
            case REGISTER:
                return action.register(mNickName, mPassword, mPasssure, mCodeToken, type);
        }
        return super.doInBackground(requestCode, id);
    }

    @Override
    public void onSuccess(int requestCode, Object result) {
        if (result != null) {
            switch (requestCode) {
                case SEND_CODE:
                    SendCodeResponse scrres = (SendCodeResponse) result;
                    if (scrres.getResultCode() == 0) {
                        NToast.shortToast(mContext, com.mbt.im.R.string.messge_send);
                    } else {
                        NToast.shortToast(mContext, scrres.getMsg());
                    }
                    break;

                case REGISTER:
                    RegisterResponse rres = (RegisterResponse) result;
                    switch (rres.getResultCode()) {
                        case 0:
                            LoadDialog.dismiss(mContext);
                            NToast.shortToast(mContext, com.mbt.im.R.string.register_success);
                            Intent data = new Intent();
                            data.putExtra("phone", mPhone);
                            data.putExtra("password", mPassword);
                            data.putExtra("nickname", mNickName);
//                            data.putExtra("id", rres.getResult().getId());
                            setResult(REGISTER_BACK, data);
                            this.finish();
                            break;
                        default:
                            NToast.shortToast(mContext, rres.getMsg());
                            break;
                    }
                    break;
            }
        }
    }

    @Override
    public void onFailure(int requestCode, int state, Object result) {
        switch (requestCode) {
            case SEND_CODE:
//                NToast.shortToast(mContext, "获取验证码请求失败");
                NToast.shortToast(mContext, result + "");
                break;
            case REGISTER:
                LoadDialog.dismiss(mContext);
                NToast.shortToast(mContext, "注册请求失败");
                break;
        }
    }

    @OnClick({R.id.ll_navigation_bar_left_back, R.id.tv_verification_code, R.id.ll_register_protocol, R.id.submit})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ll_navigation_bar_left_back:
                finish();
                break;
            case R.id.tv_verification_code:
                if (TextUtils.isEmpty(editPhone.getText().toString().trim())) {
                    NToast.longToast(mContext, com.mbt.im.R.string.phone_number_is_null);
                } else if (editPhone.getText().toString().trim().length() >= 11) {
                    mCountDownTimerUtils = new CountDownTimerUtils(tvVerificationCode, 60000, 1000);
                    mCountDownTimerUtils.start();

                    request(SEND_CODE);
                } else {
                    Toast.makeText(mContext, com.mbt.im.R.string.Illegal_phone_number, Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.ll_register_protocol:/**协议*/
                if (checkbox.isChecked()) {
                    checkbox.setChecked(false);
                } else {
                    checkbox.setChecked(true);
                }
                break;
            case R.id.submit:
                if (!checkbox.isChecked()) {
                    Toast.makeText(mContext, "请认真阅读使用协议并同意！", Toast.LENGTH_SHORT).show();
                    return;
                }
                mNickName = editPhone.getText().toString().trim();
                mCodeToken = editVcode.getText().toString().trim();
                mPassword = editPassword.getText().toString().trim();
                mPasssure = editPasssure.getText().toString().trim();
                if (switchButton.isChecked()) {
                    type = "1";
                } else {
                    type = "0";
                }

                request(REGISTER);

                break;
        }
    }
}
