package com.sj.hh.ui.adapter;


import android.content.Context;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.sj.hh.R;
import com.sj.hh.ui.adapter.base.ListBaseAdapter;
import com.sj.hh.ui.adapter.base.SuperViewHolder;
import com.sj.hh.ui.blackbox.record.AddBehaviorRecordActivity;
import com.sj.hh.util.SelectValueUtil;

import cn.rongcloud.im.server.response.RecordBehaviorResponse;

/**
 *
 */
public class RecordBehaviorAdapter extends ListBaseAdapter<RecordBehaviorResponse.DataBean> {


    private OnOpenItemClickListener onOpenItemClickListener;

    public RecordBehaviorAdapter(Context context) {
        super(context, 0);
    }

    @Override
    public int getLayoutId() {
        return R.layout.item_behavior_record_list;
    }

    @Override
    public void onBindItemHolder(SuperViewHolder holder, final int position, @Nullable final RecordBehaviorResponse.DataBean bean, boolean isLastItem) {

        final TextView tvType = holder.getTextView(R.id.tv_type);
        final TextView tvContent = holder.getTextView(R.id.tv_content);
        final TextView tvTimeStart = holder.getTextView(R.id.tv_time_start);
        final TextView tvTimeEnd = holder.getTextView(R.id.tv_time_end);
        final TextView tvRemark = holder.getTextView(R.id.tv_remark);
        final TextView delete = holder.getTextView(R.id.delete);
        final TextView update = holder.getTextView(R.id.update);

        if (bean == null) return;
        tvType.setText(SelectValueUtil.getInstance().getMatching(SelectValueUtil.getInstance().actionType, bean.getType() + ""));
        tvContent.setText(bean.getContent());
        tvTimeStart.setText(bean.getTimestart());
        tvTimeEnd.setText(bean.getTimeend());
        String remark = bean.getRemark();
        if (TextUtils.isEmpty(remark)) {
            tvRemark.setVisibility(View.GONE);
        } else {
            tvRemark.setVisibility(View.VISIBLE);
            tvRemark.setText(remark);
        }

        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mContext.startActivity(new Intent(mContext, AddBehaviorRecordActivity.class).putExtra("RecordBehaviorResponse", bean));
            }
        });

        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onOpenItemClickListener != null)
                    onOpenItemClickListener.onOpenItemClick(v, position, bean);
            }
        });

    }

    public interface OnOpenItemClickListener {
        void onOpenItemClick(View v, int position, RecordBehaviorResponse.DataBean data);
    }

    public void setOnOpenItemClickListener(OnOpenItemClickListener onOpenItemClickListener) {
        this.onOpenItemClickListener = onOpenItemClickListener;
    }
}
