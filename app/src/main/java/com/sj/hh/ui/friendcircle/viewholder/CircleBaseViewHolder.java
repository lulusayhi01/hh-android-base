package com.sj.hh.ui.friendcircle.viewholder;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sj.friendcircle.lib.utils.StringUtil;
import com.sj.friendcircle.lib.utils.TimeUtil;
import com.sj.friendcircle.lib.utils.ToolUtil;
import com.sj.friendcircle.ui.base.adapter.BaseMultiRecyclerViewHolder;
import com.sj.friendcircle.ui.imageloader.ImageLoadMnanger;
import com.sj.friendcircle.ui.util.UIHelper;
import com.sj.friendcircle.ui.util.ViewUtil;
import com.sj.friendcircle.ui.widget.commentwidget.CommentContentsLayout;
import com.sj.friendcircle.ui.widget.commentwidget.CommentWidget;
import com.sj.friendcircle.ui.widget.commentwidget.IComment;
import com.sj.friendcircle.ui.widget.common.ClickShowMoreLayout;
import com.sj.hh.R;
import com.sj.hh.mvp.presenter.impl.MomentPresenter;
import com.sj.hh.widget.popup.CommentPopup;
import com.sj.hh.widget.popup.DeleteCommentPopup;
import com.sj.hh.widget.praise.PraiseWidget;
import com.socks.library.KLog;

import java.util.List;

import cn.rongcloud.im.server.response.friendcircle.CommentInfo;
import cn.rongcloud.im.server.response.friendcircle.LikesInfo;
import cn.rongcloud.im.server.response.friendcircle.MomentsInfo;
import cn.rongcloud.im.server.response.user.UserInfo;
import cn.rongcloud.im.user.UserManager;


/**
 * Created by huluhong on 2018/7/1.
 * <p>
 * 朋友圈基本item
 */
public abstract class CircleBaseViewHolder extends BaseMultiRecyclerViewHolder<MomentsInfo.DataBean> implements BaseMomentVH<MomentsInfo.DataBean> {


    //头部
    protected ImageView avatar;
    protected TextView nick;
    protected ClickShowMoreLayout userText;

    //底部
    protected TextView createTime;
    protected TextView deleteMoments;
    protected ImageView commentImage;
    protected FrameLayout menuButton;
    protected LinearLayout commentAndPraiseLayout;
    protected PraiseWidget praiseWidget;//点赞
    protected View line;
    protected CommentContentsLayout commentLayout;

    //内容区
    protected LinearLayout contentLayout;

    private CommentPopup commentPopup;
    private DeleteCommentPopup deleteCommentPopup;

    private MomentPresenter momentPresenter;
    private int itemPosition;
    private MomentsInfo.DataBean momentsInfo;


    public CircleBaseViewHolder(View itemView, int viewType) {
        super(itemView, viewType);
        onFindView(itemView);

        //header
        avatar = (ImageView) findView(avatar, R.id.avatar);
        nick = (TextView) findView(nick, R.id.nick);
        userText = (ClickShowMoreLayout) findView(userText, R.id.item_text_field);
        if (userText != null)
            userText.setOnStateKeyGenerateListener(new ClickShowMoreLayout.OnStateKeyGenerateListener() {
                @Override
                public int onStateKeyGenerated(int originKey) {
                    return originKey + itemPosition;
                }
            });

        //bottom
        createTime = (TextView) findView(createTime, R.id.create_time);
        deleteMoments = (TextView) findView(deleteMoments, R.id.tv_delete_moment);
        commentImage = (ImageView) findView(commentImage, R.id.menu_img);
        menuButton = (FrameLayout) findView(menuButton, R.id.menu_button);
        commentAndPraiseLayout = (LinearLayout) findView(commentAndPraiseLayout, R.id.comment_praise_layout);
        praiseWidget = (PraiseWidget) findView(praiseWidget, R.id.praise);
        line = findView(line, R.id.divider);
        commentLayout = (CommentContentsLayout) findView(commentLayout, R.id.comment_layout);
        commentLayout.setMode(CommentContentsLayout.Mode.EXPANDABLE);
        commentLayout.setOnCommentItemClickListener(onCommentItemClickListener);
        commentLayout.setOnCommentItemLongClickListener(onCommentItemLongClickListener);
        commentLayout.setOnCommentWidgetItemClickListener(onCommentWidgetItemClickListener);
        // FIXME: 2018/1/3 暂时未开发完
//        commentLayout.setMode(CommentContentsLayout.Mode.EXPANDABLE);
        //content
//        contentLayout = (LinearLayout) findView(contentLayout, R.id.content);

        if (commentPopup == null) {
            commentPopup = new CommentPopup((Activity) getContext());
            commentPopup.setOnCommentPopupClickListener(onCommentPopupClickListener);
        }

        if (deleteCommentPopup == null) {
            deleteCommentPopup = new DeleteCommentPopup((Activity) getContext());
            deleteCommentPopup.setOnDeleteCommentClickListener(onDeleteCommentClickListener);
        }
    }

    public void setPresenter(MomentPresenter momentPresenter) {
        this.momentPresenter = momentPresenter;
    }

    public MomentPresenter getPresenter() {
        return momentPresenter;
    }

    @Override
    public void onBindData(MomentsInfo.DataBean data, int position) {
        if (data == null) {
            KLog.e("数据是空的！！！！");
            findView(userText, R.id.item_text_field);
            userText.setText("这个动态的数据是空的。。。。OMG");
            return;
        }
        this.momentsInfo = data;
        this.itemPosition = position;
        //通用数据绑定
        onBindMutualDataToViews(data);
        //点击事件
        menuButton.setOnClickListener(onMenuButtonClickListener);
        menuButton.setTag(R.id.momentinfo_data_tag_id, data);
        deleteMoments.setOnClickListener(onDeleteMomentClickListener);
        //传递到子类
        onBindDataToView(data, position, getViewType());
    }

    private void onBindMutualDataToViews(MomentsInfo.DataBean data) {
        //header
        ImageLoadMnanger.INSTANCE.loadImage(avatar, data.getUserHeadImage());
        if (TextUtils.isEmpty(data.getUserName()))
            nick.setText(data.getUserPhone());
        else
            nick.setText(data.getUserName());
        if (userText != null) {
            userText.setText(data.getContent().getText());
            ViewUtil.setViewsVisible(StringUtil.noEmpty(data.getContent().getText()) ?
                    View.VISIBLE : View.GONE, userText);
        }

        //bottom
        createTime.setText(TimeUtil.getTimeStringFromBmob(data.getTrendsCreateTime()));
        String localUserid = UserManager.getInstance().getUserInfo(getContext()).getUserid();
        ViewUtil.setViewsVisible(TextUtils.equals(momentsInfo.getUserId() + "", localUserid) ?
                View.VISIBLE : View.GONE, deleteMoments);
        boolean needPraiseData = addLikes(data.getLikesList());
        boolean needCommentData = commentLayout.addComments(data.getCommentList());
        praiseWidget.setVisibility(needPraiseData ? View.VISIBLE : View.GONE);
        commentLayout.setVisibility(needCommentData ? View.VISIBLE : View.GONE);
        line.setVisibility(needPraiseData && needCommentData ? View.VISIBLE : View.GONE);
        commentAndPraiseLayout.setVisibility(needCommentData || needPraiseData ? View.VISIBLE : View.GONE);

    }


    /**
     * 添加点赞
     *
     * @param likesList
     * @return ture=显示点赞，false=不显示点赞
     */
    private boolean addLikes(List<LikesInfo> likesList) {
        if (ToolUtil.isListEmpty(likesList)) {
            return false;
        }
        praiseWidget.setDatas(likesList);
        return true;
    }

    /**
     * ==================  click listener block
     */

    private CommentContentsLayout.OnCommentWidgetItemClickListener onCommentWidgetItemClickListener = new CommentContentsLayout.OnCommentWidgetItemClickListener() {
        @Override
        public void onCommentItemClicked(@NonNull IComment comment, CharSequence text) {
            if (comment instanceof CommentInfo) {
                UIHelper.ToastMessage("点击的用户 ： 【 " + text + " 】");
            }
        }
    };

    private CommentContentsLayout.OnCommentItemClickListener onCommentItemClickListener = new CommentContentsLayout.OnCommentItemClickListener() {
        @Override
        public void onCommentWidgetClick(@NonNull CommentWidget widget) {
            IComment comment = widget.getData();
            CommentInfo commentInfo = null;
            if (comment instanceof CommentInfo) {
                commentInfo = (CommentInfo) comment;
            }
            if (commentInfo == null) return;
            UserInfo userInfo = UserManager.getInstance().getUserInfo(getContext());
            if (commentInfo.canDelete(userInfo.getUserid())) {
                deleteCommentPopup.showPopupWindow(commentInfo);
            } else {
                momentPresenter.showCommentBox(null, itemPosition, momentsInfo.getTrendsId() + "", widget);
            }
        }
    };

    private CommentContentsLayout.OnCommentItemLongClickListener onCommentItemLongClickListener = new CommentContentsLayout.OnCommentItemLongClickListener() {
        @Override
        public boolean onCommentWidgetLongClick(@NonNull CommentWidget widget) {
            return false;
        }
    };

    private DeleteCommentPopup.OnDeleteCommentClickListener onDeleteCommentClickListener = new DeleteCommentPopup.OnDeleteCommentClickListener() {
        @Override
        public void onDelClick(CommentInfo commentInfo) {
            momentPresenter.deleteComment(getContext(), itemPosition, commentInfo.getCommentid() + "", momentsInfo.getCommentList());
        }
    };

    private View.OnClickListener onDeleteMomentClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (momentPresenter != null)
                momentPresenter.deleteMoments(v.getContext(), momentsInfo);
        }
    };


    private View.OnClickListener onMenuButtonClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            MomentsInfo.DataBean info = (MomentsInfo.DataBean) v.getTag(R.id.momentinfo_data_tag_id);
            if (info != null) {
                commentPopup.updateMomentInfo(info);
                commentPopup.showPopupWindow(commentImage);
            }
        }
    };


    private CommentPopup.OnCommentPopupClickListener onCommentPopupClickListener = new CommentPopup.OnCommentPopupClickListener() {
        @Override
        public void onLikeClick(View v, @NonNull MomentsInfo.DataBean info, boolean hasLiked) {
            if (hasLiked) {
                momentPresenter.unLike(v.getContext(), itemPosition, info.getTrendsId() + "", info.getLikesList(), info);
            } else {
                momentPresenter.addLike(v.getContext(), itemPosition, info.getTrendsId() + "", info.getLikesList(), info);
            }

        }

        @Override
        public void onCommentClick(View v, @NonNull MomentsInfo.DataBean info) {
            momentPresenter.showCommentBox(itemView, itemPosition, info.getTrendsId() + "", null);
        }
    };

    /**
     * ============  tools method block
     */


    protected final View findView(View view, int resid) {
        if (resid > 0 && itemView != null && view == null) {
            return itemView.findViewById(resid);
        }
        return view;
    }


}
