package com.sj.hh.ui.machine;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import com.github.jdsjlzx.interfaces.OnRefreshListener;
import com.github.jdsjlzx.recyclerview.LRecyclerView;
import com.github.jdsjlzx.recyclerview.LRecyclerViewAdapter;
import com.github.jdsjlzx.recyclerview.ProgressStyle;
import com.sj.friendcircle.ui.base.adapter.OnRecyclerViewItemClickListener;
import com.sj.hh.R;
import com.sj.hh.ui.adapter.SellMachineListAdapter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.rongcloud.im.server.network.http.HttpException;
import cn.rongcloud.im.server.response.SellMachineListResponse;
import cn.rongcloud.im.server.utils.NToast;
import cn.rongcloud.im.ui.activity.base.BaseCircleActivity;

/**
 * 购买挖车
 */
public class AddMachineActivity extends BaseCircleActivity implements OnRecyclerViewItemClickListener<SellMachineListResponse.DataBean>, OnRefreshListener {

    private static final int SELL_MACHINE_LIST = 1;
    private static final int MACHINE_BUY = 2;

    @BindView(R.id.recycle_view)
    LRecyclerView recycleView;
    @BindView(R.id.ll_content)
    LinearLayout llContent;
    @BindView(R.id.btn_buy)
    Button btnBuy;
    @BindView(R.id.ib_close)
    ImageButton ibClose;


    private int pageNum = 1;
    private int pageSize = 20;
    private String machineid;
    private SellMachineListAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_machine);
        ButterKnife.bind(this);

        adapter = new SellMachineListAdapter(mContext);
        adapter.setOnRecyclerViewItemClickListener(this);
        recycleView.setOnRefreshListener(this);

        request(SELL_MACHINE_LIST);
    }

    @Override
    public Object doInBackground(int requestCode, String id) throws HttpException {
        switch (requestCode) {
            case SELL_MACHINE_LIST:
                return action.sellMachineList(pageNum + "", pageSize + "");
            case MACHINE_BUY:
                return action.machineBuy(machineid);
        }
        return super.doInBackground(requestCode, id);
    }

    @Override
    public void onSuccess(int requestCode, Object result) {
        if (result != null) {
            switch (requestCode) {
                case SELL_MACHINE_LIST:
                    SellMachineListResponse response = (SellMachineListResponse) result;
                    if (response.getResultCode() == 0) {
                        adapter.setDataList(response.getData());
                        LRecyclerViewAdapter lRecyclerViewAdapter = new LRecyclerViewAdapter(adapter);
                        recycleView.setAdapter(lRecyclerViewAdapter);
                        recycleView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
                        recycleView.setRefreshProgressStyle(ProgressStyle.BallSpinFadeLoader);
//                        recycleView.setArrowImageView(R.drawable.iconfont_downgrey);

                        recycleView.refreshComplete(response.getData().size());
                        lRecyclerViewAdapter.notifyDataSetChanged();
                    } else {
                        NToast.shortToast(mContext, response.getMsg());
                    }
                    break;
                case MACHINE_BUY:
                    SellMachineListResponse listResponse = (SellMachineListResponse) result;
                    if (listResponse.getResultCode() == 0) {
                        this.finish();
                    }
                    NToast.shortToast(mContext, listResponse.getMsg());
                    break;
            }
        }
    }

    @Override
    public void onFailure(int requestCode, int state, Object result) {
        switch (requestCode) {
            case SELL_MACHINE_LIST:
            case MACHINE_BUY:
                NToast.shortToast(mContext, "请求失败！");
                break;
        }
    }


    @OnClick({R.id.ib_close, R.id.btn_buy})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ib_close:
                this.finish();
                break;
            case R.id.btn_buy:
                if (TextUtils.isEmpty(machineid)) {
                    NToast.shortToast(mContext, "请选择购买的挖机类型！");
                    return;
                }
                request(MACHINE_BUY);
                break;
        }
    }

    @Override
    public void onItemClick(View v, int position, SellMachineListResponse.DataBean data) {
        if (data.isSelect())
            return;
        machineid = data.getId() + "";

        if (adapter != null) {
            List<SellMachineListResponse.DataBean> dataList = adapter.getDataList();
            for (int i = 0; i < dataList.size(); i++) {
                if (i == position - 1) {
                    dataList.get(i).setSelect(true);
                } else {
                    dataList.get(i).setSelect(false);
                }
            }
            List<SellMachineListResponse.DataBean> dataBeanList = new ArrayList<>();
            dataBeanList.addAll(dataList);
            adapter.setDataList(dataBeanList);

        }
    }

    @Override
    public void onRefresh() {
        request(SELL_MACHINE_LIST);
    }
}
