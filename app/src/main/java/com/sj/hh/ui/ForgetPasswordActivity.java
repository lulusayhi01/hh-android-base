package com.sj.hh.ui;

import android.os.Bundle;

import com.sj.hh.R;

import cn.rongcloud.im.ui.activity.base.BaseActivity;

/**
 * 忘记密码
 */
public class ForgetPasswordActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_password);
    }
}
