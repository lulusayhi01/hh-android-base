package com.sj.hh.ui.adapter;

import android.content.Context;
import android.support.annotation.NonNull;

import com.sj.friendcircle.ui.base.adapter.BaseMultiRecyclerViewAdapter;
import com.sj.friendcircle.ui.base.adapter.BaseRecyclerViewHolder;
import com.sj.hh.mvp.presenter.impl.MomentPresenter;
import com.sj.hh.ui.friendcircle.viewholder.CircleBaseViewHolder;

import java.util.List;

import cn.rongcloud.im.server.response.friendcircle.MomentsInfo;


/**
 * Created by huluhong on 2018/7/1.
 * <p>
 * 朋友圈adapter
 */

public class CircleMomentsAdapter extends BaseMultiRecyclerViewAdapter<CircleMomentsAdapter, MomentsInfo.DataBean> {

    private MomentPresenter momentPresenter;

    public CircleMomentsAdapter(@NonNull Context context, @NonNull List<MomentsInfo.DataBean> datas, MomentPresenter presenter) {
        super(context, datas);
        this.momentPresenter = presenter;
    }

    @Override
    protected void onInitViewHolder(BaseRecyclerViewHolder holder) {
        if (holder instanceof CircleBaseViewHolder) {
            ((CircleBaseViewHolder) holder).setPresenter(momentPresenter);
        }
    }
}
