package com.sj.hh.ui.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.sj.hh.ui.machine.fragment.MachineFragment;

import java.util.ArrayList;

/**
 * Created by Administrator on 2018/9/6 0006.
 */

public class MachinePagerAdapter extends FragmentPagerAdapter {

    private ArrayList<MachineFragment> mFragments = new ArrayList<>();
    private final String[] mTitles;

    public MachinePagerAdapter(FragmentManager fm, ArrayList<MachineFragment> mFragments, String[] mTitles) {
        super(fm);
        this.mFragments = mFragments;
        this.mTitles = mTitles;
    }

    @Override
    public int getCount() {
        return mFragments.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mTitles[position];
    }

    @Override
    public Fragment getItem(int position) {
        return mFragments.get(position);
    }

    public ArrayList<MachineFragment> getmFragments() {
        return mFragments;
    }

    public void setmFragments(ArrayList<MachineFragment> mFragments) {
        this.mFragments = mFragments;
    }

    public String[] getmTitles() {
        return mTitles;
    }
}
