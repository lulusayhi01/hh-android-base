package com.sj.hh.ui.adapter;


import android.content.Context;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.sj.hh.R;
import com.sj.hh.ui.adapter.base.ListBaseAdapter;
import com.sj.hh.ui.adapter.base.SuperViewHolder;
import com.sj.hh.ui.blackbox.record.AddCommunicationRecordActivity;
import com.sj.hh.util.SelectValueUtil;

import cn.rongcloud.im.server.response.RecordCommunicationResponse;

/**
 *
 */
public class RecordCommunicationAdapter extends ListBaseAdapter<RecordCommunicationResponse.DataBean> {

    private OnOpenItemClickListener onOpenItemClickListener;

    public RecordCommunicationAdapter(Context context) {
        super(context, 0);
    }

    @Override
    public int getLayoutId() {
        return R.layout.item_communication_record_list;
    }

    @Override
    public void onBindItemHolder(SuperViewHolder holder, final int position, @Nullable final RecordCommunicationResponse.DataBean bean, boolean isLastItem) {

        final TextView tvType = holder.getTextView(R.id.tv_type);
        final TextView tvContent = holder.getTextView(R.id.tv_content);
        final TextView tvNum = holder.getTextView(R.id.tv_num);
        final TextView delete = holder.getTextView(R.id.delete);
        final TextView update = holder.getTextView(R.id.update);

        if (bean == null) return;
        tvType.setText(SelectValueUtil.getInstance().getMatching(SelectValueUtil.getInstance().exchangeType, bean.getType() + ""));
        tvContent.setText(bean.getContent());
        tvNum.setText(bean.getTuserid() + "");
        String content = bean.getContent();
        if (TextUtils.isEmpty(content)) {
            tvContent.setVisibility(View.GONE);
        } else {
            tvContent.setVisibility(View.VISIBLE);
            tvContent.setText(content);
        }

        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mContext.startActivity(new Intent(mContext, AddCommunicationRecordActivity.class).putExtra("RecordCommunicationResponse", bean));
            }
        });

        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onOpenItemClickListener != null)
                    onOpenItemClickListener.onOpenItemClick(v, position, bean);
            }
        });

    }

    public interface OnOpenItemClickListener {
        void onOpenItemClick(View v, int position, RecordCommunicationResponse.DataBean data);
    }

    public void setOnOpenItemClickListener(OnOpenItemClickListener onOpenItemClickListener) {
        this.onOpenItemClickListener = onOpenItemClickListener;
    }
}
