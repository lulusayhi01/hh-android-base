package com.sj.hh.ui.blackbox.record;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.widget.Button;

import com.github.jdsjlzx.interfaces.OnLoadMoreListener;
import com.github.jdsjlzx.interfaces.OnRefreshListener;
import com.github.jdsjlzx.recyclerview.LRecyclerView;
import com.github.jdsjlzx.recyclerview.LRecyclerViewAdapter;
import com.github.jdsjlzx.recyclerview.ProgressStyle;
import com.sj.hh.R;
import com.sj.hh.ui.adapter.RecordBehaviorAdapter;
import com.sj.hh.ui.adapter.RecordCommunicationAdapter;
import com.sj.hh.ui.adapter.RecordFileAdapter;
import com.sj.hh.ui.adapter.RecordFinancialAdapter;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.rongcloud.im.server.network.http.HttpException;
import cn.rongcloud.im.server.response.BaseResponse;
import cn.rongcloud.im.server.response.RecordBehaviorResponse;
import cn.rongcloud.im.server.response.RecordCommunicationResponse;
import cn.rongcloud.im.server.response.RecordFileResponse;
import cn.rongcloud.im.server.response.RecordFinancialResponse;
import cn.rongcloud.im.server.utils.NToast;
import cn.rongcloud.im.server.widget.LoadDialog;
import cn.rongcloud.im.ui.activity.base.BaseActivity;

public class AllRecordActivity extends BaseActivity implements OnRefreshListener, OnLoadMoreListener, RecordFileAdapter.OnOpenItemClickListener, RecordBehaviorAdapter.OnOpenItemClickListener, RecordFinancialAdapter.OnOpenItemClickListener, RecordCommunicationAdapter.OnOpenItemClickListener {

    private static final int FILE_LIST_BY_PAGE = 1;
    private static final int RECORD_ACTION_LIST_BY_PAGE = 2;
    private static final int RECORD_EXCHANGE_LIST_BY_PAGE = 3;
    private static final int RECORD_FINANCE_LIST_BY_PAGE = 4;
    private static final int RECORD_FINANCE_DELETE = 5;
    private static final int RECORD_ACTION_DELETE = 6;
    private static final int FILE_DELETE = 7;
    private static final int RECORD_EXCHANGE_DELETE = 8;

    @BindView(R.id.recycler_view)
    LRecyclerView recyclerView;

    private int pageNum = 1;
    private ActivityType activityType;
    private LRecyclerViewAdapter lRecyclerViewAdapter;
    private String ids;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_record);
        ButterKnife.bind(this);

        initView();
        initData();

    }

    private void initView() {
        activityType = (ActivityType) getIntent().getSerializableExtra("ActivityType");
        Button rightButton = getHeadRightButton();
        rightButton.setBackground(getResources().getDrawable(R.drawable.view_img_add));
        rightButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toActivity();
            }
        });

        if (activityType == ActivityType.FileRecord) {
            RecordFileAdapter adapter = new RecordFileAdapter(mContext);
            adapter.setOnOpenItemClickListener(this);
            lRecyclerViewAdapter = new LRecyclerViewAdapter(adapter);
        } else if (activityType == ActivityType.BehaviorRecord) {
            RecordBehaviorAdapter adapter = new RecordBehaviorAdapter(mContext);
            adapter.setOnOpenItemClickListener(this);
            lRecyclerViewAdapter = new LRecyclerViewAdapter(adapter);
        } else if (activityType == ActivityType.FinancialRecord) {
            RecordFinancialAdapter adapter = new RecordFinancialAdapter(mContext);
            adapter.setOnOpenItemClickListener(this);
            lRecyclerViewAdapter = new LRecyclerViewAdapter(adapter);
        } else if (activityType == ActivityType.CommunicationRecord) {
            RecordCommunicationAdapter adapter = new RecordCommunicationAdapter(mContext);
            adapter.setOnOpenItemClickListener(this);
            lRecyclerViewAdapter = new LRecyclerViewAdapter(adapter);
        }


        recyclerView.setAdapter(lRecyclerViewAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));
        recyclerView.setRefreshProgressStyle(ProgressStyle.BallSpinFadeLoader);
        recyclerView.setOnRefreshListener(this);
        recyclerView.setOnLoadMoreListener(this);
    }

    private void toActivity() {
        if (activityType == ActivityType.FileRecord) {
            startActivityForResult(new Intent(AllRecordActivity.this, AddFileRecordActivity.class), 1);
        } else if (activityType == ActivityType.BehaviorRecord) {
            startActivityForResult(new Intent(AllRecordActivity.this, AddBehaviorRecordActivity.class), 2);
        } else if (activityType == ActivityType.FinancialRecord) {
            startActivityForResult(new Intent(AllRecordActivity.this, AddFinancialRecordActivity.class), 3);
        } else if (activityType == ActivityType.CommunicationRecord) {
            startActivityForResult(new Intent(AllRecordActivity.this, AddCommunicationRecordActivity.class), 4);
        }
    }

    private void initData() {
        if (activityType == ActivityType.FileRecord) {
            setTitle("文件记录");
            request(FILE_LIST_BY_PAGE);
        } else if (activityType == ActivityType.BehaviorRecord) {
            setTitle("行为记录");
            request(RECORD_ACTION_LIST_BY_PAGE);
        } else if (activityType == ActivityType.FinancialRecord) {
            setTitle("财务记录");
            request(RECORD_FINANCE_LIST_BY_PAGE);
        } else if (activityType == ActivityType.CommunicationRecord) {
            setTitle("交流记录");
            request(RECORD_EXCHANGE_LIST_BY_PAGE);
        }

    }

    @Override
    public Object doInBackground(int requestCode, String id) throws HttpException {
        switch (requestCode) {
            case FILE_LIST_BY_PAGE:
                return action.fileListByPage(pageNum, 20);
            case RECORD_ACTION_LIST_BY_PAGE:
                return action.recordActionListByPage(pageNum, 20);
            case RECORD_EXCHANGE_LIST_BY_PAGE:
                return action.recordExchangeListByPage(pageNum, 20);
            case RECORD_FINANCE_LIST_BY_PAGE:
                return action.recordFinanceListByPage(pageNum, 20);
            case FILE_DELETE:
                return action.fileDelete(ids);
            case RECORD_ACTION_DELETE:
                return action.recordActionDelete(ids);
            case RECORD_FINANCE_DELETE:
                return action.recordFinanceDelete(ids);
            case RECORD_EXCHANGE_DELETE:
                return action.recordExchangeDelete(ids);

        }
        return super.doInBackground(requestCode, id);
    }

    @Override
    public void onSuccess(int requestCode, Object result) {
        if (result != null) {
            switch (requestCode) {
                case FILE_LIST_BY_PAGE:
                    RecordFileResponse scrres = (RecordFileResponse) result;
                    if (scrres.getResultCode() == 0) {
                        List<RecordFileResponse.DataBean> data = scrres.getData();
                        if (data == null && data.size() == 0) {
                            recyclerView.setNoMore(true);
                            return;
                        }
                        RecordFileAdapter innerAdapter = (RecordFileAdapter) lRecyclerViewAdapter.getInnerAdapter();
                        if (pageNum == 1) {
                            recyclerView.setNoMore(false);
                            innerAdapter.setDataList(data);
                        } else {
                            innerAdapter.addAll(data);
                        }
                        recyclerView.refreshComplete(scrres.getData().size());
                        lRecyclerViewAdapter.notifyDataSetChanged();
//                        LocalJsonUtil.getInstance().setPhoneContact(mContext, data);
//                        for (MachineFragment fragment : adapter.getmFragments()) {
//                            fragment.setData(data);
//                        }
                    } else {
                        NToast.shortToast(mContext, scrres.getMsg());
                    }
                    LoadDialog.dismiss(mContext);
                    break;
                case RECORD_ACTION_LIST_BY_PAGE:
                    RecordBehaviorResponse response = (RecordBehaviorResponse) result;
                    if (response.getResultCode() == 0) {
                        List<RecordBehaviorResponse.DataBean> data = response.getData();
                        if (data == null && data.size() == 0) {
                            recyclerView.setNoMore(true);
                            return;
                        }
                        RecordBehaviorAdapter innerAdapter = (RecordBehaviorAdapter) lRecyclerViewAdapter.getInnerAdapter();
                        if (pageNum == 1) {
                            recyclerView.setNoMore(false);
                            innerAdapter.setDataList(data);
                        } else {
                            innerAdapter.addAll(data);
                        }
                        recyclerView.refreshComplete(response.getData().size());
                        lRecyclerViewAdapter.notifyDataSetChanged();
                    } else {
                        NToast.shortToast(mContext, response.getMsg());
                    }
                    LoadDialog.dismiss(mContext);
                    break;
                case RECORD_FINANCE_LIST_BY_PAGE:
                    RecordFinancialResponse response2 = (RecordFinancialResponse) result;
                    if (response2.getResultCode() == 0) {
                        List<RecordFinancialResponse.DataBean> data = response2.getData();
                        if (data == null && data.size() == 0) {
                            recyclerView.setNoMore(true);
                            return;
                        }
                        RecordFinancialAdapter innerAdapter = (RecordFinancialAdapter) lRecyclerViewAdapter.getInnerAdapter();
                        if (pageNum == 1) {
                            recyclerView.setNoMore(false);
                            innerAdapter.setDataList(data);
                        } else {
                            innerAdapter.addAll(data);
                        }
                        recyclerView.refreshComplete(response2.getData().size());
                        lRecyclerViewAdapter.notifyDataSetChanged();
                    } else {
                        NToast.shortToast(mContext, response2.getMsg());
                    }
                    LoadDialog.dismiss(mContext);
                    break;
                case RECORD_EXCHANGE_LIST_BY_PAGE:
                    RecordCommunicationResponse response3 = (RecordCommunicationResponse) result;
                    if (response3.getResultCode() == 0) {
                        List<RecordCommunicationResponse.DataBean> data = response3.getData();
                        if (data == null && data.size() == 0) {
                            recyclerView.setNoMore(true);
                            return;
                        }
                        RecordCommunicationAdapter innerAdapter = (RecordCommunicationAdapter) lRecyclerViewAdapter.getInnerAdapter();
                        if (pageNum == 1) {
                            recyclerView.setNoMore(false);
                            innerAdapter.setDataList(data);
                        } else {
                            innerAdapter.addAll(data);
                        }
                        recyclerView.refreshComplete(response3.getData().size());
                        lRecyclerViewAdapter.notifyDataSetChanged();
                    } else {
                        NToast.shortToast(mContext, response3.getMsg());
                    }
                    LoadDialog.dismiss(mContext);
                    break;
                case FILE_DELETE:
                case RECORD_ACTION_DELETE:
                case RECORD_FINANCE_DELETE:
                    BaseResponse baseResponse = (BaseResponse) result;
                    if (baseResponse.getResultCode() == 0) {
                        ids = "";
                        pageNum = 1;
                        initData();
                    } else {
                        NToast.shortToast(mContext, baseResponse.getMsg());
                    }
                    LoadDialog.dismiss(mContext);
                    break;
            }
        }
    }

    @Override
    public void onFailure(int requestCode, int state, Object result) {
        switch (requestCode) {
            default:
                LoadDialog.dismiss(mContext);
                NToast.shortToast(mContext, "请求失败！");
                break;
        }
    }

    @Override
    public void onRefresh() {
        pageNum = 1;
        initData();
    }

    @Override
    public void onLoadMore() {
        pageNum++;
        initData();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        pageNum = 1;
        initData();
    }

    @Override
    public void onOpenItemClick(View v, int position, RecordFileResponse.DataBean data) {
        if (data == null) return;
        ids = data.getId() + "";
        request(FILE_DELETE);
    }

    @Override
    public void onOpenItemClick(View v, int position, RecordBehaviorResponse.DataBean data) {
        if (data == null) return;
        ids = data.getId() + "";
        request(RECORD_ACTION_DELETE);
    }

    @Override
    public void onOpenItemClick(View v, int position, RecordFinancialResponse.DataBean data) {
        if (data == null) return;
        ids = data.getId() + "";
        request(RECORD_FINANCE_DELETE);
    }

    @Override
    public void onOpenItemClick(View v, int position, RecordCommunicationResponse.DataBean data) {
        if (data == null) return;
        ids = data.getId() + "";
        request(RECORD_EXCHANGE_DELETE);
    }

    public enum ActivityType {
        //文件记录
        FileRecord(1),
        //行为记录
        BehaviorRecord(2),
        //财务记录
        FinancialRecord(3),
        //交流记录
        CommunicationRecord(4),
        //购买记录
        PurchaseRecord(5);

        private int type;

        ActivityType(int type) {
            this.type = type;
        }

        static ActivityType get(int value) {
            switch (value) {
                case 1:
                    return FileRecord;
                case 2:
                    return BehaviorRecord;
                case 3:
                    return FinancialRecord;
                case 4:
                    return CommunicationRecord;
                case 5:
                    return PurchaseRecord;
            }

            return FileRecord;
        }
    }

}
