package com.sj.hh.ui.adapter;


import android.content.Context;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.sj.hh.R;
import com.sj.hh.ui.adapter.base.ListBaseAdapter;
import com.sj.hh.ui.adapter.base.SuperViewHolder;
import com.sj.hh.ui.blackbox.record.AddFileRecordActivity;
import com.sj.hh.util.SelectValueUtil;

import cn.rongcloud.im.server.response.RecordFileResponse;

/**
 *
 */
public class RecordFileAdapter extends ListBaseAdapter<RecordFileResponse.DataBean> {


    private OnOpenItemClickListener onOpenItemClickListener;

    public RecordFileAdapter(Context context) {
        super(context, 0);
    }

    @Override
    public int getLayoutId() {
        return R.layout.item_file_record_list;
    }

    @Override
    public void onBindItemHolder(SuperViewHolder holder, final int position, @Nullable final RecordFileResponse.DataBean bean, boolean isLastItem) {

        final TextView tvFileType = holder.getTextView(R.id.tv_file_type);
        final TextView tvFilePath = holder.getTextView(R.id.tv_file_path);
        final TextView tvRemark = holder.getTextView(R.id.tv_remark);
        final TextView delete = holder.getTextView(R.id.delete);
        final TextView update = holder.getTextView(R.id.update);

        if (bean == null) return;
        tvFileType.setText(SelectValueUtil.getInstance().getMatching(SelectValueUtil.getInstance().type, bean.getType() + ""));
        tvFilePath.setText(bean.getImgpath());
        String remark = bean.getRemark();
        if (TextUtils.isEmpty(remark)) {
            tvRemark.setVisibility(View.GONE);
        } else {
            tvRemark.setVisibility(View.VISIBLE);
            tvRemark.setText(remark);
        }

        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mContext.startActivity(new Intent(mContext, AddFileRecordActivity.class).putExtra("RecordFileResponse", bean));
            }
        });

        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onOpenItemClickListener != null)
                    onOpenItemClickListener.onOpenItemClick(v, position, bean);
            }
        });

    }

    public interface OnOpenItemClickListener {
        void onOpenItemClick(View v, int position, RecordFileResponse.DataBean data);
    }

    public void setOnOpenItemClickListener(OnOpenItemClickListener onOpenItemClickListener) {
        this.onOpenItemClickListener = onOpenItemClickListener;
    }
}
