package com.sj.hh.ui.adapter.base;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sj.friendcircle.ui.base.adapter.OnRecyclerViewItemClickListener;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * 封装adapter
 *
 * @param <T> 数据类型
 */
public abstract class ListBaseAdapter<T> extends RecyclerView.Adapter<SuperViewHolder> {
    protected Context mContext;
    private LayoutInflater mInflater;
    private int addCount;

    private OnRecyclerViewItemClickListener<T> onRecyclerViewItemClickListener;


    protected List<T> mDataList = new ArrayList<>();

    public ListBaseAdapter(Context context, int addCount) {
        this.mContext = context;
        this.addCount = addCount;
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public SuperViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = mInflater.inflate(getLayoutId(), parent, false);
        SuperViewHolder superViewHolder = new SuperViewHolder(itemView);
        setUpItemEvent(superViewHolder);
        return superViewHolder;
    }

    @Override
    public void onBindViewHolder(SuperViewHolder holder, int position) {
        try {
            if (addCount != 0 && position >= mDataList.size()) {
                T bean = null;
                onBindItemHolder(holder, position, bean, true);
            } else {
                T t = mDataList.get(position);
                onBindItemHolder(holder, position, t, false);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setUpItemEvent(final SuperViewHolder holder) {
        if (onRecyclerViewItemClickListener != null) {
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        //这个获取位置的方法，防止添加删除导致位置不变
                        int layoutPosition = holder.getAdapterPosition();
                        onRecyclerViewItemClickListener.onItemClick(holder.itemView, layoutPosition, mDataList.get(layoutPosition - 1));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }

    //局部刷新关键：带payload的这个onBindViewHolder方法必须实现
    @Override
    public void onBindViewHolder(SuperViewHolder holder, int position, List<Object> payloads) {
        if (payloads.isEmpty()) {
            onBindViewHolder(holder, position);
        } else {
            onBindItemHolder(holder, position, payloads);
        }

    }

    public abstract int getLayoutId();

    public abstract void onBindItemHolder(SuperViewHolder holder, int position, @Nullable T bean, boolean isAdd);

    public void onBindItemHolder(SuperViewHolder holder, int position, List<Object> payloads) {

    }

    @Override
    public int getItemCount() {
        return mDataList.size() + addCount;
    }

    public List<T> getDataList() {
        return mDataList;
    }

    public void setDataList(Collection<T> list) {
        this.mDataList.clear();
        this.mDataList.addAll(list);
        notifyDataSetChanged();
    }

    public void addAll(Collection<T> list) {
        int lastIndex = this.mDataList.size();
        if (this.mDataList.addAll(list)) {
            notifyItemRangeInserted(lastIndex, list.size());
        }
    }

    public void remove(int position) {
        this.mDataList.remove(position);
        notifyItemRemoved(position);
        if (position != (getDataList().size())) { // 如果移除的是最后一个，忽略
            notifyItemRangeChanged(position, this.mDataList.size() - position);
        }
    }

    public void clear() {
        mDataList.clear();
        notifyDataSetChanged();
    }

    public void setOnRecyclerViewItemClickListener(OnRecyclerViewItemClickListener<T> onRecyclerViewItemClickListener) {
        this.onRecyclerViewItemClickListener = onRecyclerViewItemClickListener;
    }
}
