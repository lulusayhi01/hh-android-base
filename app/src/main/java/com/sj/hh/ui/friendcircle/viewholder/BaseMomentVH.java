package com.sj.hh.ui.friendcircle.viewholder;

import android.support.annotation.NonNull;
import android.view.View;

/**
 * 抽象出的vh接口
 */

public interface BaseMomentVH<T> {

    void onFindView(@NonNull View rootView);

    void onBindDataToView(@NonNull final T data, int position, int viewType);
}
