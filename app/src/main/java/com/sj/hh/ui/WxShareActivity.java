package com.sj.hh.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.sj.hh.R;
import com.sj.hh.wxapi.WXEntryActivity;


@Route(path = "/app/WxShareActivity")
public class WxShareActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wx_share);

    }

    public void goWxEntryAct(View view) {
        startActivity(new Intent(WxShareActivity.this, WXEntryActivity.class));
    }
}
