package com.sj.hh.ui.group;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.github.jdsjlzx.interfaces.OnLoadMoreListener;
import com.github.jdsjlzx.interfaces.OnRefreshListener;
import com.github.jdsjlzx.recyclerview.LRecyclerView;
import com.github.jdsjlzx.recyclerview.LRecyclerViewAdapter;
import com.github.jdsjlzx.recyclerview.ProgressStyle;
import com.sj.friendcircle.common.router.RouterList;
import com.sj.hh.R;
import com.sj.hh.ui.adapter.GroupListAdapter;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.rongcloud.im.server.network.http.HttpException;
import cn.rongcloud.im.server.response.GroupListResponse;
import cn.rongcloud.im.server.utils.NToast;
import cn.rongcloud.im.server.widget.LoadDialog;
import cn.rongcloud.im.ui.activity.base.BaseActivity;

/**
 * 群聊列表
 */
@Route(path = RouterList.GroupListActivity.path)
public class GroupListActivity extends BaseActivity implements OnRefreshListener, OnLoadMoreListener {

    private static final int CHAT_GROUP_LIST_BY_PAGE = 1;

    @BindView(R.id.recycler_view)
    LRecyclerView recyclerView;

    private int pageNum = 1;
    private LRecyclerViewAdapter lRecyclerViewAdapter;
    private GroupListAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group_list);
        ButterKnife.bind(this);
        setTitle(getResources().getString(R.string.group));

        initView();
        request(CHAT_GROUP_LIST_BY_PAGE);
    }

    private void initView() {
        adapter = new GroupListAdapter(mContext);
        lRecyclerViewAdapter = new LRecyclerViewAdapter(adapter);
        recyclerView.setAdapter(lRecyclerViewAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));
        recyclerView.setRefreshProgressStyle(ProgressStyle.BallSpinFadeLoader);
        recyclerView.setOnRefreshListener(this);
        recyclerView.setOnLoadMoreListener(this);
    }

    @Override
    public void onRefresh() {
        pageNum = 1;
        request(CHAT_GROUP_LIST_BY_PAGE);
    }

    @Override
    public void onLoadMore() {
        pageNum++;
        request(CHAT_GROUP_LIST_BY_PAGE);
    }

    @Override
    public Object doInBackground(int requestCode, String id) throws HttpException {
        switch (requestCode) {
            case CHAT_GROUP_LIST_BY_PAGE:
                return action.chatGroupListByPage(pageNum, 20);
        }
        return super.doInBackground(requestCode, id);
    }

    @Override
    public void onSuccess(int requestCode, Object result) {
        if (result != null) {
            switch (requestCode) {
                case CHAT_GROUP_LIST_BY_PAGE:
                    GroupListResponse scrres = (GroupListResponse) result;
                    if (scrres.getResultCode() == 0) {
                        List<GroupListResponse.DataBean> data = scrres.getData();
                        if (data == null && data.size() == 0){
                            recyclerView.setNoMore(true);
                            return;
                        }
                        GroupListAdapter innerAdapter = (GroupListAdapter) lRecyclerViewAdapter.getInnerAdapter();
                        if (pageNum == 1){
                            recyclerView.setNoMore(false);
                            innerAdapter.setDataList(data);
                        }else {
                            innerAdapter.addAll(data);
                        }
                        recyclerView.refreshComplete(scrres.getData().size());
                        lRecyclerViewAdapter.notifyDataSetChanged();
                    } else {
                        NToast.shortToast(mContext, scrres.getMsg());
                    }
                    LoadDialog.dismiss(mContext);
                    break;
            }
        }
    }

    @Override
    public void onFailure(int requestCode, int state, Object result) {
        switch (requestCode) {
            default:
                LoadDialog.dismiss(mContext);
                NToast.shortToast(mContext, "请求失败！");
                break;
        }
    }
}
