package com.sj.hh.ui.machine;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.github.jdsjlzx.interfaces.OnRefreshListener;
import com.github.jdsjlzx.recyclerview.LRecyclerView;
import com.github.jdsjlzx.recyclerview.LRecyclerViewAdapter;
import com.github.jdsjlzx.recyclerview.ProgressStyle;
import com.sj.hh.R;
import com.sj.hh.ui.adapter.MachineListAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.rongcloud.im.App;
import cn.rongcloud.im.server.network.http.HttpException;
import cn.rongcloud.im.server.response.CoinCountByUserResponse;
import cn.rongcloud.im.server.response.MachineUserListResponse;
import cn.rongcloud.im.server.response.UserInfoResponse;
import cn.rongcloud.im.server.utils.NToast;
import cn.rongcloud.im.server.widget.SelectableRoundedImageView;
import cn.rongcloud.im.ui.activity.base.BaseActivity;
import io.rong.imageloader.core.ImageLoader;

/**
 * 矿机
 */
//@Route(path = RouterList.MachineListActivity.path)
@Deprecated
public class MachineListActivity extends BaseActivity implements MachineListAdapter.OnOpenItemClickListener, MachineListAdapter.OnCloseItemClickListener, OnRefreshListener {

    private static final int MACHINE_USER_BY_PAGE = 1;
    private static final int GET_USER = 2;
    private static final int COIN_COUNT_BY_USER = 3;
    private static final int COIN_LIST = 4;
    private static final int MACHINE_OPEN = 5;
    private static final int MACHINE_CLOSE = 6;


    @BindView(R.id.my_header)
    SelectableRoundedImageView myHeader;
    @BindView(R.id.tv_name)
    TextView tvName;
    @BindView(R.id.tv_coin)
    TextView tvCoin;
    @BindView(R.id.tv_machine_user_level)
    TextView tvMachineUserLevel;
    @BindView(R.id.ll_add_machine)
    LinearLayout llAddMachine;
    @BindView(R.id.recycle_view)
    LRecyclerView recycleView;


    private int pageNum = 1;
    private int pageSize = 20;
    private String coinid = "1";//脉宝币
    private MachineListAdapter adapter;
    private String machineId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_machine_list);
        ButterKnife.bind(this);
        setTitle("挖机");

        adapter = new MachineListAdapter(mContext);
        adapter.setOnOpenItemClickListener(this);
        adapter.setOnCloseItemClickListener(this);
        recycleView.setOnRefreshListener(this);

        request(MACHINE_USER_BY_PAGE);
        request(GET_USER);
        request(COIN_COUNT_BY_USER);
//        request(COIN_LIST);
    }


    @Override
    public Object doInBackground(int requestCode, String id) throws HttpException {
        switch (requestCode) {
            case MACHINE_USER_BY_PAGE:
                return action.listMachineUserByPage(pageNum + "", pageSize + "");
            case GET_USER:
                return action.getUser();
            case COIN_COUNT_BY_USER:
                return action.getCoinCountByUser(coinid);
            case COIN_LIST:
                return action.coinList();
            case MACHINE_OPEN:
                return action.machineOpen(machineId);
            case MACHINE_CLOSE:
                return action.machineClose(machineId);
        }
        return super.doInBackground(requestCode, id);
    }

    @Override
    public void onSuccess(int requestCode, Object result) {
        if (result != null) {
            switch (requestCode) {
                case MACHINE_USER_BY_PAGE:
                    MachineUserListResponse scrres = (MachineUserListResponse) result;
                    if (scrres.getResultCode() == 0) {
                        adapter.setDataList(scrres.getData());
                        LRecyclerViewAdapter lRecyclerViewAdapter = new LRecyclerViewAdapter(adapter);
                        recycleView.setAdapter(lRecyclerViewAdapter);
                        recycleView.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));
                        recycleView.setRefreshProgressStyle(ProgressStyle.BallSpinFadeLoader);

                        recycleView.refreshComplete(scrres.getData().size());
                        lRecyclerViewAdapter.notifyDataSetChanged();
                    } else {
                        NToast.shortToast(mContext, scrres.getMsg());
                    }
                    break;
                case COIN_COUNT_BY_USER:
                    CoinCountByUserResponse coinCountByUserResponse = (CoinCountByUserResponse) result;
                    if (coinCountByUserResponse.getResultCode() == 0) {
                        tvCoin.setText(coinCountByUserResponse.getData());
                    } else {
                        NToast.shortToast(mContext, coinCountByUserResponse.getMsg());
                    }
                    break;
                case GET_USER:
                    UserInfoResponse userInfoResponse = (UserInfoResponse) result;
                    if (userInfoResponse.getResultCode() == 0) {
                        String portraitUri = userInfoResponse.getData().getHeadimage();
                        ImageLoader.getInstance().displayImage(portraitUri, myHeader, App.getOptions());
                        tvName.setText(userInfoResponse.getData().getName());
                    } else {
                        NToast.shortToast(mContext, userInfoResponse.getMsg());
                    }
                    break;
            }
        }
    }

    @Override
    public void onFailure(int requestCode, int state, Object result) {
        switch (requestCode) {
            case MACHINE_USER_BY_PAGE:
                NToast.shortToast(mContext, "请求失败！");
                break;
        }
    }

    @OnClick(R.id.ll_add_machine)
    public void onViewClicked() {
        startActivityForResult(new Intent(this, AddMachineActivity.class), 10);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        request(MACHINE_USER_BY_PAGE);
    }

    @Override
    public void onOpenItemClick(ImageView v, int position, MachineUserListResponse.DataBean data) {
        machineId = data.getMuid()+"";
        request(MACHINE_OPEN);
    }

    @Override
    public void onCloseItemClick(ImageView v, int position, MachineUserListResponse.DataBean data) {
        machineId = data.getMuid()+"";
        request(MACHINE_CLOSE);
    }

    @Override
    public void onRefresh() {
        request(MACHINE_USER_BY_PAGE);
    }
}
