package com.sj.hh.ui.blackbox.record;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bigkoo.pickerview.listener.OnOptionsSelectListener;
import com.bigkoo.pickerview.listener.OnTimeSelectListener;
import com.sj.friendcircle.lib.utils.TimeUtil;
import com.sj.hh.R;
import com.sj.hh.util.SelectValueUtil;

import java.util.ArrayList;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.rongcloud.im.server.entity.PickerType;
import cn.rongcloud.im.server.network.http.HttpException;
import cn.rongcloud.im.server.response.BaseResponse;
import cn.rongcloud.im.server.response.RecordBehaviorResponse;
import cn.rongcloud.im.server.utils.NToast;
import cn.rongcloud.im.server.widget.LoadDialog;
import cn.rongcloud.im.ui.activity.base.BaseActivity;

/**
 * 添加行为记录
 */
public class AddBehaviorRecordActivity extends BaseActivity {

    private static final int RECORD_ACTION_EDIT = 1;

    @BindView(R.id.tv_behavior_type)
    TextView tvBehaviorType;
    @BindView(R.id.ll_behavior_type)
    LinearLayout llBehaviorType;
    @BindView(R.id.edit_behavior_content)
    EditText editBehaviorContent;
    @BindView(R.id.tv_time_start)
    TextView tvTimeStart;
    @BindView(R.id.ll_time_start)
    LinearLayout llTimeStart;
    @BindView(R.id.tv_time_end)
    TextView tvTimeEnd;
    @BindView(R.id.ll_time_end)
    LinearLayout llTimeEnd;
    @BindView(R.id.edit_remark)
    EditText editRemark;

    private String behaviorType;
    private String content;
    private String timestart;
    private String timeend;
    private String remark;
    private String responseId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_behavior_record);
        ButterKnife.bind(this);
        setTitle("添加行为记录");

        initView();
        initData();

    }

    private void initView() {
        Button rightButton = getHeadRightButton();
        rightButton.setVisibility(View.GONE);
        mHeadRightText.setVisibility(View.VISIBLE);
        mHeadRightText.setText(R.string.de_save);
        mHeadRightText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                content = editBehaviorContent.getText().toString().trim();
                remark = editRemark.getText().toString().trim();
                request(RECORD_ACTION_EDIT);
            }
        });
    }

    private void initData() {
        RecordBehaviorResponse.DataBean response = (RecordBehaviorResponse.DataBean) getIntent().getSerializableExtra("RecordBehaviorResponse");
        if (response != null) {
            responseId = response.getId() + "";
            tvTimeStart.setText(response.getTimestart());
            tvTimeEnd.setText(response.getTimeend());
            behaviorType = response.getType() + "";
            tvBehaviorType.setText(SelectValueUtil.getInstance().getMatching(SelectValueUtil.getInstance().actionType, behaviorType));
            editBehaviorContent.setText(response.getContent());
            editRemark.setText(response.getRemark());
        }

    }

    @Override
    public Object doInBackground(int requestCode, String id) throws HttpException {
        switch (requestCode) {
            case RECORD_ACTION_EDIT:
                return action.recordActionEdit(responseId, behaviorType, content, timestart, timeend, remark);
        }
        return super.doInBackground(requestCode, id);
    }

    @Override
    public void onSuccess(int requestCode, Object result) {
        if (result != null) {
            switch (requestCode) {
                case RECORD_ACTION_EDIT:
                    BaseResponse scrres = (BaseResponse) result;
                    if (scrres.getResultCode() == 0) {
                        finish();
                    } else {
                        NToast.shortToast(mContext, scrres.getMsg());
                    }
                    LoadDialog.dismiss(mContext);
                    break;
            }
        }
    }

    @OnClick({R.id.ll_behavior_type, R.id.ll_time_start, R.id.ll_time_end})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ll_behavior_type:
                final ArrayList<PickerType> actionType = SelectValueUtil.getInstance().getPickerData(SelectValueUtil.getInstance().actionType);
                showPickerView(actionType, new OnOptionsSelectListener() {
                    @Override
                    public void onOptionsSelect(int options1, int options2, int options3, View v) {
                        tvBehaviorType.setText(actionType.get(options1).getValue());
                        behaviorType = actionType.get(options1).getId();
                    }
                });
                break;
            case R.id.ll_time_start:
                showTimePickerView(new OnTimeSelectListener() {
                    @Override
                    public void onTimeSelect(Date date, View v) {
                        String time = TimeUtil.dateToString(date, TimeUtil.YYYYMMDD);
                        tvTimeStart.setText(time);
                        timestart = time;
                    }
                });
                break;
            case R.id.ll_time_end:
                showTimePickerView(new OnTimeSelectListener() {
                    @Override
                    public void onTimeSelect(Date date, View v) {
                        String time = TimeUtil.dateToString(date, TimeUtil.YYYYMMDD);
                        tvTimeEnd.setText(time);
                        timeend = time;
                    }
                });
                break;
        }
    }
}
