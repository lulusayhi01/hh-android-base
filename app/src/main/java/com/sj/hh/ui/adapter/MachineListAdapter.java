package com.sj.hh.ui.adapter;


import android.content.Context;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.sj.hh.R;
import com.sj.hh.ui.adapter.base.ListBaseAdapter;
import com.sj.hh.ui.adapter.base.SuperViewHolder;

import cn.rongcloud.im.server.response.MachineUserListResponse;

/**
 * Created by Administrator on 2018/8/17 0017.
 */
@Deprecated
public class MachineListAdapter extends ListBaseAdapter<MachineUserListResponse.DataBean> {


    private OnOpenItemClickListener onOpenItemClickListener;
    private OnCloseItemClickListener onCloseItemClickListener;


    public MachineListAdapter(Context context) {
        super(context, 0);
    }

    @Override
    public int getLayoutId() {
        return R.layout.item_machine_list;
    }

    @Override
    public void onBindItemHolder(SuperViewHolder holder, final int position, @Nullable final MachineUserListResponse.DataBean bean, boolean isAdd) {

        final ImageView ibMachineStart = holder.getImageView(R.id.ib_machine_start);
        final ImageView ibMachineStop = holder.getImageView(R.id.ib_machine_stop);
        final ImageView ivMachineShacy = holder.getImageView(R.id.iv_machine_shacy);//挖车
        final View rlItemMachineBg = holder.getView(R.id.rl_item_machine_bg);
        final TextView itemTvMachineTime = holder.getTextView(R.id.item_tv_machine_time);


        if (bean == null) return;
        String manhour = bean.getManhour() + "h";

        /**
         *  'machineistatus' 矿机用户状态 3202, "未开启"  3203, "开启中" 3204, "报废"  3205, "已完成任务(天)"
         */
        if (bean.getMachineistatus() == 3202) {
            ibMachineStart.setVisibility(View.VISIBLE);
            showImageUrl(bean, ivMachineShacy, rlItemMachineBg, true);
            manhour = bean.getManhour() + "h";
        } else if (bean.getMachineistatus() == 3203) {
            ibMachineStart.setVisibility(View.GONE);
            showGifImage(bean, ivMachineShacy);
            showImageUrl(bean, ivMachineShacy, rlItemMachineBg, false);
            manhour = (TextUtils.isEmpty(bean.getManhour()) ? 0 : (Integer.parseInt(bean.getManhour()) * 60)) - bean.getCoinMinute() + "m";
        }

        /**
         *coinMinute矿机工作时间（分钟）。 计算剩余多少分钟 可根据manhour（小时）* 60 -  coinMinute
         */
        itemTvMachineTime.setText(manhour);//剩余时间

        if (onOpenItemClickListener != null) {
            ibMachineStart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ibMachineStart.setVisibility(View.GONE);
                    showGifImage(bean, ivMachineShacy);
                    itemTvMachineTime.setText((TextUtils.isEmpty(bean.getManhour()) ? 0 : (Integer.parseInt(bean.getManhour()) * 60)) - bean.getCoinMinute() + "m");//剩余时间
                    onOpenItemClickListener.onOpenItemClick(ivMachineShacy, position, bean);
                }
            });
        }

        if (onCloseItemClickListener != null) {
            ibMachineStop.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ibMachineStart.setVisibility(View.VISIBLE);
                    itemTvMachineTime.setText(bean.getManhour() + "h");//剩余时间
                    showImageUrl(bean, ivMachineShacy, rlItemMachineBg, true);
                    onCloseItemClickListener.onCloseItemClick(ivMachineShacy, position, bean);
                }
            });
        }

    }

    /**
     * @param bean
     * @param ivMachineShacy
     */
    private void showGifImage(@Nullable MachineUserListResponse.DataBean bean, ImageView ivMachineShacy) {
        int imageId = R.mipmap.abc_machine_shacy_gif1;
        if (bean.getLevel() == 5201) {
            imageId = R.mipmap.abc_machine_shacy_gif1;
        } else if (bean.getLevel() == 5202) {
            imageId = R.mipmap.abc_machine_shacy_gif2;
        } else if (bean.getLevel() == 5203) {
            imageId = R.mipmap.abc_machine_shacy_gif3;
        } else if (bean.getLevel() == 5204) {
            imageId = R.mipmap.abc_machine_shacy_gif4;
        }
        Glide.with(mContext).asGif().load(imageId).into(ivMachineShacy);
    }

    /**
     * @param bean
     * @param ivMachineShacy
     * @param rlItemMachineBg
     * @param isShacy         是否加载挖机图片
     */
    private void showImageUrl(@Nullable MachineUserListResponse.DataBean bean, ImageView ivMachineShacy, View rlItemMachineBg, boolean isShacy) {
        /**
         * 矿机级别 5201 迷你 5202 初级 5203 中级 5204 高级
         */
        if (bean.getLevel() == 5201) {
            if (isShacy)
                Glide.with(mContext).load(R.mipmap.abc_machine_shacy1).into(ivMachineShacy);
            rlItemMachineBg.setBackground(mContext.getResources().getDrawable(R.mipmap.abc_machine_bg1));
        } else if (bean.getLevel() == 5202) {
            if (isShacy)
                Glide.with(mContext).load(R.mipmap.abc_machine_shacy2).into(ivMachineShacy);
            rlItemMachineBg.setBackground(mContext.getResources().getDrawable(R.mipmap.abc_machine_bg2));
        } else if (bean.getLevel() == 5203) {
            if (isShacy)
                Glide.with(mContext).load(R.mipmap.abc_machine_shacy3).into(ivMachineShacy);
            rlItemMachineBg.setBackground(mContext.getResources().getDrawable(R.mipmap.abc_machine_bg3));
        } else if (bean.getLevel() == 5204) {
            if (isShacy)
                Glide.with(mContext).load(R.mipmap.abc_machine_shacy4).into(ivMachineShacy);
            rlItemMachineBg.setBackground(mContext.getResources().getDrawable(R.mipmap.abc_machine_bg4));
        }
    }

    public interface OnOpenItemClickListener {
        void onOpenItemClick(ImageView v, int position, MachineUserListResponse.DataBean data);
    }

    public void setOnOpenItemClickListener(OnOpenItemClickListener onOpenItemClickListener) {
        this.onOpenItemClickListener = onOpenItemClickListener;
    }

    public interface OnCloseItemClickListener {
        void onCloseItemClick(ImageView v, int position, MachineUserListResponse.DataBean data);
    }

    public void setOnCloseItemClickListener(OnCloseItemClickListener onCloseItemClickListener) {
        this.onCloseItemClickListener = onCloseItemClickListener;
    }
}
