package com.sj.hh.ui.blackbox.record;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bigkoo.pickerview.listener.OnOptionsSelectListener;
import com.sj.hh.R;
import com.sj.hh.util.SelectValueUtil;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.rongcloud.im.server.entity.PickerType;
import cn.rongcloud.im.server.network.http.HttpException;
import cn.rongcloud.im.server.response.BaseResponse;
import cn.rongcloud.im.server.response.RecordFinancialResponse;
import cn.rongcloud.im.server.utils.NToast;
import cn.rongcloud.im.server.widget.LoadDialog;
import cn.rongcloud.im.ui.activity.base.BaseActivity;

/**
 * 添加财务记录
 */
public class AddFinancialRecordActivity extends BaseActivity {

    private static final int RECORD_FINANCE_EDIT = 1;

    @BindView(R.id.tv_type)
    TextView tvType;
    @BindView(R.id.ll_type)
    LinearLayout llType;
    @BindView(R.id.edit_financial_num)
    EditText editFinancialNum;
    @BindView(R.id.edit_content)
    EditText editContent;

    private String financeType;
    private String num;
    private String content;
    private String responseId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_financial_record);
        ButterKnife.bind(this);
        setTitle("添加财务记录");

        initView();
        initData();

    }

    private void initView() {
        Button rightButton = getHeadRightButton();
        rightButton.setVisibility(View.GONE);
        mHeadRightText.setVisibility(View.VISIBLE);
        mHeadRightText.setText(R.string.de_save);
        mHeadRightText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                num = editFinancialNum.getText().toString().trim();
                content = editContent.getText().toString().trim();
                request(RECORD_FINANCE_EDIT);
            }
        });


    }

    private void initData() {
        RecordFinancialResponse.DataBean response = (RecordFinancialResponse.DataBean) getIntent().getSerializableExtra("RecordFinancialResponse");
        if (response != null) {
            responseId = response.getId()+"";
            financeType = response.getType() + "";
            tvType.setText(SelectValueUtil.getInstance().getMatching(SelectValueUtil.getInstance().financeType, financeType));
            editContent.setText(response.getContent());
            editFinancialNum.setText(response.getNum() + "");
        }

    }

    @Override
    public Object doInBackground(int requestCode, String id) throws HttpException {
        switch (requestCode) {
            case RECORD_FINANCE_EDIT:
                return action.recordFinanceEdit(responseId, financeType, num, content);
        }
        return super.doInBackground(requestCode, id);
    }

    @Override
    public void onSuccess(int requestCode, Object result) {
        if (result != null) {
            switch (requestCode) {
                case RECORD_FINANCE_EDIT:
                    BaseResponse scrres = (BaseResponse) result;
                    if (scrres.getResultCode() == 0) {
                        finish();
                    } else {
                        NToast.shortToast(mContext, scrres.getMsg());
                    }
                    LoadDialog.dismiss(mContext);
                    break;
            }
        }
    }

    @OnClick(R.id.ll_type)
    public void onViewClicked() {
        final ArrayList<PickerType> financeTypes = SelectValueUtil.getInstance().getPickerData(SelectValueUtil.getInstance().financeType);
        showPickerView(financeTypes, new OnOptionsSelectListener() {
            @Override
            public void onOptionsSelect(int options1, int options2, int options3, View v) {
                tvType.setText(financeTypes.get(options1).getValue());
                financeType = financeTypes.get(options1).getId();
            }
        });
    }
}
