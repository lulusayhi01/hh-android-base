package com.sj.hh.ui;

import android.os.Bundle;

import com.sj.hh.R;

import cn.rongcloud.im.ui.activity.base.BaseActivity;


/**
 * 附近人
 */
public class NearbyPeopleActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nearby_people);

        setTitle("附近人");

    }
}
