package com.sj.hh.ui.friendcircle.viewholder;

import android.support.annotation.NonNull;
import android.view.View;

import com.sj.friendcircle.ui.base.adapter.LayoutId;
import com.sj.hh.R;

import cn.rongcloud.im.server.response.friendcircle.MomentsInfo;


/**
 *
 * 空内容的vh
 *
 * @see
 */

@LayoutId(id = R.layout.moments_empty_content)
public class EmptyMomentsVH extends CircleBaseViewHolder {


    public EmptyMomentsVH(View itemView, int viewType) {
        super(itemView, viewType);
    }

    @Override
    public void onFindView(@NonNull View rootView) {

    }

    @Override
    public void onBindDataToView(@NonNull MomentsInfo.DataBean data, int position, int viewType) {

    }
}
