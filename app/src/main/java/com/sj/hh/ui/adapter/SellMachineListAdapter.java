package com.sj.hh.ui.adapter;


import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.sj.hh.R;
import com.sj.hh.ui.adapter.base.ListBaseAdapter;
import com.sj.hh.ui.adapter.base.SuperViewHolder;

import cn.rongcloud.im.server.response.SellMachineListResponse;

/**
 * Created by Administrator on 2018/8/17 0017.
 */

public class SellMachineListAdapter extends ListBaseAdapter<SellMachineListResponse.DataBean> {


    public SellMachineListAdapter(Context context) {
        super(context, 0);
    }

    @Override
    public int getLayoutId() {
        return R.layout.item_add_machine_list;
    }

    @Override
    public void onBindItemHolder(SuperViewHolder holder, int position, SellMachineListResponse.DataBean bean, boolean isAdd) {
        TextView itemTvMachineName = holder.getTextView(R.id.item_tv_machine_name);
        TextView itemTvMachineLevel = holder.getTextView(R.id.item_tv_machine_level);
        TextView itemTvMachinePrice = holder.getTextView(R.id.item_tv_machine_price);
        ImageView ivMachineShacy = holder.getImageView(R.id.iv_machine_shacy);
        View rlItemMachineBg = holder.getView(R.id.rl_item_machine_bg);
        ImageView ivMachineSelect = holder.getImageView(R.id.iv_machine_select);


        if (bean == null) return;
        if (bean.isSelect())
            ivMachineSelect.setVisibility(View.VISIBLE);
        else
            ivMachineSelect.setVisibility(View.GONE);
        /**
         *  'buytype' 购买类型 '5101币购买 5102余额购买'
         */
        String price;
        if (bean.getBuytype() == 5101) {
            price = bean.getCoinnum() + "SDA币";
        } else if (bean.getBuytype() == 5102) {
            price = "￥" + bean.getPrice();
        } else {
            price = bean.getPrice();
        }
        itemTvMachineName.setText(bean.getName());
        itemTvMachineLevel.setText(bean.getLevel() + "");
        itemTvMachinePrice.setText(price);

        /**
         * 矿机级别 5201 迷你 5202 初级 5203 中级 5204 高级
         */
        if (bean.getLevel() == 5201) {
//            Glide.with(mContext).load(R.mipmap.abc_machine_shacy1).into(ivMachineShacy);
            Glide.with(mContext).load(R.mipmap.abc_machine_new_shacy1).into(ivMachineShacy);
        } else if (bean.getLevel() == 5202) {
//            Glide.with(mContext).load(R.mipmap.abc_machine_shacy2).into(ivMachineShacy);
            Glide.with(mContext).load(R.mipmap.abc_machine_new_shacy2).into(ivMachineShacy);
        } else if (bean.getLevel() == 5203) {
//            Glide.with(mContext).load(R.mipmap.abc_machine_shacy3).into(ivMachineShacy);
            Glide.with(mContext).load(R.mipmap.abc_machine_new_shacy3).into(ivMachineShacy);
        } else if (bean.getLevel() == 5204) {
//            Glide.with(mContext).load(R.mipmap.abc_machine_shacy4).into(ivMachineShacy);
            Glide.with(mContext).load(R.mipmap.abc_machine_new_shacy4).into(ivMachineShacy);
        }

    }
}
