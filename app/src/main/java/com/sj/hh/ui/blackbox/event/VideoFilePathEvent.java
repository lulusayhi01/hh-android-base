package com.sj.hh.ui.blackbox.event;

/**
 * @Author: huluhong
 * @Time: 2018/11/30 0030
 * @Description：This is VideoFilePathEvent
 */

public class VideoFilePathEvent {

    private String videoFilePath;

    public VideoFilePathEvent(String videoFilePath) {
        this.videoFilePath = videoFilePath;
    }

    public String getVideoFilePath() {
        return videoFilePath;
    }

    public void setVideoFilePath(String videoFilePath) {
        this.videoFilePath = videoFilePath;
    }
}
