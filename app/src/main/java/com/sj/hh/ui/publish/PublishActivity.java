package com.sj.hh.ui.publish;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.text.Editable;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.sj.friendcircle.common.router.RouterList;
import com.sj.friendcircle.lib.entity.ImageInfo;
import com.sj.friendcircle.lib.interfaces.adapter.TextWatcherAdapter;
import com.sj.friendcircle.lib.manager.compress.CompressManager;
import com.sj.friendcircle.lib.manager.compress.OnCompressListener;
import com.sj.friendcircle.lib.utils.StringUtil;
import com.sj.friendcircle.lib.utils.ToolUtil;
import com.sj.friendcircle.ui.base.BaseTitleBarActivity;
import com.sj.friendcircle.ui.helper.PhotoHelper;
import com.sj.friendcircle.ui.imageloader.ImageLoadMnanger;
import com.sj.friendcircle.ui.util.SwitchActivityTransitionUtil;
import com.sj.friendcircle.ui.util.UIHelper;
import com.sj.friendcircle.ui.util.ViewUtil;
import com.sj.friendcircle.ui.widget.common.TitleBar;
import com.sj.friendcircle.ui.widget.imageview.PreviewImageView;
import com.sj.friendcircle.ui.widget.popup.PopupProgress;
import com.sj.friendcircle.ui.widget.popup.SelectPhotoMenuPopup;
import com.sj.hh.R;
import com.sj.hh.request.AddDynamicRequest;
import com.sj.hh.request.base.OnResponseListener;
import com.sj.hh.request.exception.BmobException;
import com.sj.hh.util.OkHttp3Util;
import com.socks.library.KLog;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import com.sj.friendcircle.lib.entity.photo.PhotoBrowserInfo;

import cn.rongcloud.im.SealConst;
import cn.rongcloud.im.server.response.PublishImageResponse;
import cn.rongcloud.im.server.utils.json.JsonMananger;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;


/**
 * Created by huluhong on 2018/7/1.
 * <p>
 * 发布朋友圈页面
 */

@Route(path = RouterList.PublishActivity.path)
@RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
public class PublishActivity extends BaseTitleBarActivity {
    @Autowired(name = RouterList.PublishActivity.key_mode)
    int mode = -1;

    private boolean canTitleRightClick = false;
    private List<ImageInfo> selectedPhotos;

    private EditText mInputContent;
    private PreviewImageView<ImageInfo> mPreviewImageView;

    private SelectPhotoMenuPopup mSelectPhotoMenuPopup;
    private PopupProgress mPopupProgress;

    @Override
    public void onHandleIntent(Intent intent) {
        mode = intent.getIntExtra(RouterList.PublishActivity.key_mode, -1);
        selectedPhotos = intent.getParcelableArrayListExtra(RouterList.PublishActivity.key_photoList);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_publish);
        if (mode == -1) {
            finish();
            return;
        } else if (mode == RouterList.PublishActivity.MODE_MULTI && selectedPhotos == null) {
            finish();
            return;
        }
        initView();
    }

    private void initView() {
        initTitle();
        mInputContent = findView(R.id.publish_input);
        mPreviewImageView = findView(R.id.preview_image_content);
        ViewUtil.setViewsVisible(mode == RouterList.PublishActivity.MODE_TEXT ? View.GONE : View.VISIBLE, mPreviewImageView);
        mInputContent.setHint(mode == RouterList.PublishActivity.MODE_MULTI ? "这一刻的想法..." : null);
        mInputContent.addTextChangedListener(new TextWatcherAdapter() {
            @Override
            public void afterTextChanged(Editable s) {
                refreshTitleRightClickable();
            }
        });

        if (mode == RouterList.PublishActivity.MODE_TEXT) {
            UIHelper.showInputMethod(mInputContent, 300);
        }

        if (mode == RouterList.PublishActivity.MODE_MULTI) {
            initPreviewImageView();
            loadImage();
        }
        refreshTitleRightClickable();
    }

    private void initPreviewImageView() {
        mPreviewImageView.setOnPhotoClickListener(new PreviewImageView.OnPhotoClickListener<ImageInfo>() {
            @Override
            public void onPhotoClickListener(int pos, ImageInfo data, @NonNull ImageView imageView) {
                PhotoBrowserInfo info = PhotoBrowserInfo.create(pos, null, selectedPhotos);
                ARouter.getInstance()
                        .build(RouterList.PhotoMultiBrowserActivity.path)
                        .withParcelable(RouterList.PhotoMultiBrowserActivity.key_browserinfo, info)
                        .withInt(RouterList.PhotoMultiBrowserActivity.key_maxSelectCount, selectedPhotos.size())
                        .navigation(PublishActivity.this, RouterList.PhotoMultiBrowserActivity.requestCode);
            }
        });
        mPreviewImageView.setOnAddPhotoClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showSelectPhotoPopup();
            }
        });
    }

    private void loadImage() {
        mPreviewImageView.setDatas(selectedPhotos, new PreviewImageView.OnLoadPhotoListener<ImageInfo>() {
            @Override
            public void onPhotoLoading(int pos, ImageInfo data, @NonNull ImageView imageView) {
                KLog.i(data.getImagePath());
                ImageLoadMnanger.INSTANCE.loadImage(imageView, data.getImagePath());
            }
        });
    }

    private void showSelectPhotoPopup() {
        if (mSelectPhotoMenuPopup == null) {
            mSelectPhotoMenuPopup = new SelectPhotoMenuPopup(this);
            mSelectPhotoMenuPopup.setOnSelectPhotoMenuClickListener(new SelectPhotoMenuPopup.OnSelectPhotoMenuClickListener() {
                @Override
                public void onShootClick() {
                    PhotoHelper.fromCamera(PublishActivity.this, false);
                }

                @Override
                public void onAlbumClick() {
                    ARouter.getInstance()
                            .build(RouterList.PhotoSelectActivity.path)
                            .withInt(RouterList.PhotoSelectActivity.key_maxSelectCount, mPreviewImageView.getRestPhotoCount())
                            .navigation(PublishActivity.this, RouterList.PhotoSelectActivity.requestCode);
                }
            });
        }
        mSelectPhotoMenuPopup.showPopupWindow();
    }

    //title init
    private void initTitle() {
        setTitle(mode == RouterList.PublishActivity.MODE_TEXT ? "发表文字" : null);
        setTitleRightTextColor(mode != RouterList.PublishActivity.MODE_TEXT);
        setTitleBarBackground(getResources().getColor(R.color.color_c5181818));
        setTitleMode(TitleBar.MODE_BOTH);
        setTitleLeftText("取消");
        setTitleLeftIcon(0);
        setTitleRightText("发送");
        setTitleRightIcon(0);
    }


    private void setTitleRightTextColor(boolean canClick) {
        setRightTextColor(canClick ? UIHelper.getResourceColor(R.color.wechat_green_bg) : UIHelper.getResourceColor(R.color.wechat_green_transparent));
        canTitleRightClick = canClick;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        PhotoHelper.handleActivityResult(this, requestCode, resultCode, data, new PhotoHelper.PhotoCallback() {
            @Override
            public void onFinish(String filePath) {
                mPreviewImageView.addData(new ImageInfo(filePath, null, null, 0, 0));
                refreshTitleRightClickable();
            }

            @Override
            public void onError(String msg) {
                UIHelper.ToastMessage(msg);
            }
        });
        if (requestCode == RouterList.PhotoSelectActivity.requestCode && resultCode == RESULT_OK) {
            List<ImageInfo> result = data.getParcelableArrayListExtra(RouterList.PhotoSelectActivity.key_result);
            if (result != null) {
                mPreviewImageView.addData(result);
            }
            refreshTitleRightClickable();
        }
    }

    @Override
    public void onTitleRightClick() {
        if (!canTitleRightClick) return;
        publish();
    }

    private void refreshTitleRightClickable() {
        String inputContent = mInputContent.getText().toString();
        switch (mode) {
            case RouterList.PublishActivity.MODE_MULTI:
                setTitleRightTextColor(!ToolUtil.isListEmpty(mPreviewImageView.getDatas()) && StringUtil.noEmpty(inputContent));
                break;
            case RouterList.PublishActivity.MODE_TEXT:
                setTitleRightTextColor(StringUtil.noEmpty(inputContent));
                break;
        }

    }

    @Override
    public void finish() {
        super.finish();
        if (mPopupProgress != null) {
            mPopupProgress.dismiss();
        }
        SwitchActivityTransitionUtil.transitionVerticalOnFinish(this);
    }

    private void publish() {
        UIHelper.hideInputMethod(mInputContent);
        List<ImageInfo> datas = mPreviewImageView.getDatas();
        final boolean hasImage = !ToolUtil.isListEmpty(datas);
        final String inputContent = mInputContent.getText().toString();
        if (mPopupProgress == null) {
            mPopupProgress = new PopupProgress(this);
        }

        final String[] uploadTaskPaths;
        if (hasImage) {
            uploadTaskPaths = new String[datas.size()];
            for (int i = 0; i < datas.size(); i++) {
                uploadTaskPaths[i] = datas.get(i).getImagePath();
            }
            doCompress(uploadTaskPaths, new OnCompressListener.OnCompressListenerAdapter() {
                @Override
                public void onSuccess(List<String> imagePath) {
                    if (!ToolUtil.isListEmpty(imagePath)) {
                        for (int i = 0; i < imagePath.size(); i++) {
                            uploadTaskPaths[i] = imagePath.get(i);
                        }
                        doUpload(uploadTaskPaths, inputContent);
                    } else {
                        publishInternal(inputContent, uploadTaskPaths, null);
                    }
                }
            });
        } else {
            publishInternal(inputContent, null, null);
        }
    }

    private void doCompress(String[] uploadPaths, final OnCompressListener.OnCompressListenerAdapter listener) {
        CompressManager compressManager = CompressManager.create(this);
        for (String uploadPath : uploadPaths) {
            compressManager.addTask().setOriginalImagePath(uploadPath);
        }
        mPopupProgress.showPopupWindow();
        compressManager.start(new OnCompressListener.OnCompressListenerAdapter() {
            @Override
            public void onSuccess(List<String> imagePath) {
                if (listener != null) {
                    listener.onSuccess(imagePath);
                }
                mPopupProgress.dismiss();
            }

            @Override
            public void onCompress(long current, long target) {
                float progress = (float) current / target;
                mPopupProgress.setProgressTips("正在压缩第" + current + "/" + target + "张图片");
                mPopupProgress.setProgress((int) (progress * 100));
            }

            @Override
            public void onError(String tag) {
                mPopupProgress.dismiss();
                UIHelper.ToastMessage(tag);
            }
        });
    }

    private void doUpload(final String[] uploadTaskPaths, final String inputContent) {
        if (!mPopupProgress.isShowing()) {
            mPopupProgress.showPopupWindow();
        }
        mPopupProgress.setProgressTips("正在发布");

        HashMap<String, String> heard = new HashMap<>();
        heard.put("mbtToken", getToken(this));

        OkHttp3Util.uploadFile(this,OkHttp3Util.uploadImageUrl, uploadTaskPaths, heard, null, new Callback() {

            @Override
            public void onFailure(Call call, IOException e) {
                Log.e("+++", "onFailure: " + e.getMessage());
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                try {
                    String json = response.body().string();
                    Log.e("---", "onResponse: " + json);

                    PublishImageResponse baseResponse = JsonMananger.jsonToBean(json, PublishImageResponse.class);
                    if (response.isSuccessful()) {
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getActivity(), "图片上传成功!!", Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                    String imgId = baseResponse.getData();
                    if (!TextUtils.isEmpty(imgId)) {
                        String[] split = imgId.split(",");
                        List<String> stringList = Arrays.asList(split);
                        //通过onSuccess回调方法中的files或urls集合的大小与上传的总文件个数比较，如果一样，则表示全部文件上传成功。
                        if (!ToolUtil.isListEmpty(stringList) && stringList.size() == uploadTaskPaths.length) {
                            publishInternal(inputContent, uploadTaskPaths, imgId);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            UIHelper.ToastMessage("发布失败！");
                        }
                    });
                }
            }
        });


//        BmobFile.uploadBatch(uploadTaskPaths, new UploadBatchListener() {
//            @Override
//            public void onSuccess(List<BmobFile> list, List<String> list1) {
//                //1、有多少个文件上传，onSuccess方法就会执行多少次;
//                //2、通过onSuccess回调方法中的files或urls集合的大小与上传的总文件个数比较，如果一样，则表示全部文件上传成功。
//                if (!ToolUtil.isListEmpty(list1) && list1.size() == uploadTaskPaths.length) {
//                    publishInternal(inputContent, list1);
//                }
//            }
//
//            @Override
//            public void onProgress(int curIndex, int curPercent, int total, int totalPercent) {
//                //1、curIndex--表示当前第几个文件正在上传
//                //2、curPercent--表示当前上传文件的进度值（百分比）
//                //3、total--表示总的上传文件数
//                //4、totalPercent--表示总的上传进度（百分比）
//                mPopupProgress.setProgressTips("正在上传第" + curIndex + "/" + total + "张图片");
//                mPopupProgress.setProgress(totalPercent);
//                if (!mPopupProgress.isShowing()) {
//                    mPopupProgress.showPopupWindow();
//                }
//            }
//
//            @Override
//            public void onError(int i, String s) {
//                mPopupProgress.dismiss();
//                UIHelper.ToastMessage(s);
//            }
//        });
    }

    private void publishInternal(String input, String[] uploadPicPaths, String imgId) {
        SharedPreferences config = getSharedPreferences("config", Context.MODE_PRIVATE);
        String userId = config.getString(SealConst.SEALTALK_LOGIN_ID, "");

        AddDynamicRequest addDynamicRequest = new AddDynamicRequest();
        addDynamicRequest.setImgId(imgId)
                .setAuthId(userId)
                .addText(input);
        if (uploadPicPaths != null) {
            for (String uploadPicPath : uploadPicPaths) {
                addDynamicRequest.addPicture(uploadPicPath);
            }
        }
        addDynamicRequest.setRequestType(0x10);
        addDynamicRequest.setOnResponseListener(new OnResponseListener.SimpleResponseListener<String>() {
            @Override
            public void onSuccess(String response, int requestType) {
                if (mPopupProgress != null)
                    mPopupProgress.dismiss();
                UIHelper.ToastMessage("发布成功");
                setResult(RESULT_OK);
                finish();
            }

            @Override
            public void onError(BmobException e, int requestType) {
                UIHelper.ToastMessage(e.toString());
            }
        });
        addDynamicRequest.execute(this);
    }
}
