package com.sj.hh.ui.adapter;


import android.content.Context;
import android.graphics.Color;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.othershe.combinebitmap.CombineBitmap;
import com.othershe.combinebitmap.layout.WechatLayoutManager;
import com.othershe.combinebitmap.listener.OnSubItemClickListener;
import com.sj.hh.R;
import com.sj.hh.ui.adapter.base.ListBaseAdapter;
import com.sj.hh.ui.adapter.base.SuperViewHolder;

import java.util.ArrayList;
import java.util.List;

import cn.rongcloud.im.server.response.GroupListResponse;
import io.rong.imkit.RongIM;

/**
 *
 */
public class GroupListAdapter extends ListBaseAdapter<GroupListResponse.DataBean> {


    private OnItemClickListener onItemClickListener;

    public GroupListAdapter(Context context) {
        super(context, 0);
    }

    @Override
    public int getLayoutId() {
        return R.layout.item_group_list;
    }

    @Override
    public void onBindItemHolder(SuperViewHolder holder, final int position, @Nullable final GroupListResponse.DataBean bean, boolean isLastItem) {

        final ImageView ivHeadImage = holder.getImageView(R.id.iv_head_image);
        final TextView title = holder.getTextView(R.id.title);
        final TextView number = holder.getTextView(R.id.number);
        final TextView tvRemark = holder.getTextView(R.id.tv_remark);
        final View item = holder.getView(R.id.item_list);


        if (bean == null) return;
        title.setText(bean.getName());
        tvRemark.setVisibility(View.GONE);
        item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RongIM.getInstance().startDiscussionChat(mContext, bean.getCode(), bean.getName());
            }
        });

        if (bean.getUser() != null && bean.getUser().size() > 0) {
            List<GroupListResponse.DataBean.UserBean> user = bean.getUser();
            String[] urls = getUrls(user);
            CombineBitmap.init(mContext)
                    .setLayoutManager(new WechatLayoutManager())
                    .setSize(180)
                    .setGap(3)
                    .setGapColor(Color.parseColor("#E8E8E8"))
                    .setUrls(urls)
                    .setImageView(ivHeadImage)
                    .setOnSubItemClickListener(new OnSubItemClickListener() {
                        @Override
                        public void onSubItemClick(int index) {
//                        Log.e("SubItemIndex", "--->" + index);
                        }
                    })
                    .build();
        }
    }

    private String[] getUrls(List<GroupListResponse.DataBean.UserBean> userBeen) {
        int size = userBeen.size();
        if (userBeen.size() > 9) {
            size = 9;
        }
        String[] arr = new String[size];
        ArrayList<String> list = new ArrayList<>();
        for (int i = 0; i < userBeen.size(); i++) {
            if (i < 9)
                list.add(userBeen.get(i).getHeadimage());
        }
        return list.toArray(arr);
    }

    public interface OnItemClickListener {
        void onItemClick(View v, int position, GroupListResponse.DataBean data);
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }
}
