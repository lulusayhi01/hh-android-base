package com.sj.hh.ui.friendcircle.viewholder;

import android.support.annotation.NonNull;
import android.view.View;

import com.sj.friendcircle.ui.base.adapter.LayoutId;
import com.sj.hh.R;

import cn.rongcloud.im.server.response.friendcircle.MomentsInfo;


/**
 * Created by huluhong on 2018/7/3.
 *
 * 衹有文字的vh
 *
 * @see
 */

@LayoutId(id =  R.layout.moments_only_text)
public class TextOnlyMomentsVH extends CircleBaseViewHolder {

    public TextOnlyMomentsVH(View itemView, int viewType) {
        super(itemView, viewType);
    }

    @Override
    public void onFindView(@NonNull View rootView) {

    }

    @Override
    public void onBindDataToView(@NonNull MomentsInfo.DataBean data, int position, int viewType) {

    }
}
