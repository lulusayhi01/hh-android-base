package com.sj.hh.ui.adapter;


import android.content.Context;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.sj.hh.R;
import com.sj.hh.ui.adapter.base.ListBaseAdapter;
import com.sj.hh.ui.adapter.base.SuperViewHolder;

import cn.rongcloud.im.server.response.PhoneContactResponse;
import cn.rongcloud.im.server.widget.SelectableRoundedImageView;

/**
 * @Author: huluhong
 * @Time: 2018/10/9 0009
 * @Description：This is RegisteredAdapter
 */

public class RegisteredAdapter extends ListBaseAdapter<PhoneContactResponse.DataBean> {

    private OnItemClickListener onItemClickListener;

    public RegisteredAdapter(Context context) {
        super(context, 0);
    }

    @Override
    public int getLayoutId() {
        return R.layout.item_registered_list;
    }

    @Override
    public void onBindItemHolder(SuperViewHolder holder, final int position, @Nullable final PhoneContactResponse.DataBean bean, boolean isLastItem) {

        final SelectableRoundedImageView ivHeadImage = (SelectableRoundedImageView) holder.getImageView(R.id.iv_head_image);
        final TextView number = holder.getTextView(R.id.number);
        final Button btnSendInvite = holder.getButton(R.id.btn_send_invite);
        final TextView title = holder.getTextView(R.id.title);

        if (bean == null) return;
        title.setText(bean.getName());
        number.setText(bean.getPhone());
        Glide.with(mContext).load(bean.getHeadimage()).into(ivHeadImage);
        if (bean.getIsf() == 0){
            btnSendInvite.setBackground(mContext.getResources().getDrawable(R.color.c_65));
            btnSendInvite.setTextColor(mContext.getResources().getColor(R.color.white));
            btnSendInvite.setText("添加好友");
            if (onItemClickListener != null) {
                btnSendInvite.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onItemClickListener.onItemClick(v, position, bean);
                    }
                });
            }
        }else {
            btnSendInvite.setBackground(mContext.getResources().getDrawable(R.color.white));
            btnSendInvite.setTextColor(mContext.getResources().getColor(R.color.c_6d));
            btnSendInvite.setText("已是好友");
        }

    }

    public interface OnItemClickListener {
        void onItemClick(View v, int position, PhoneContactResponse.DataBean data);
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }
}
