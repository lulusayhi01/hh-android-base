package com.sj.hh.ui.adapter;


import android.content.Context;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.sj.hh.R;
import com.sj.hh.ui.adapter.base.ListBaseAdapter;
import com.sj.hh.ui.adapter.base.SuperViewHolder;

import cn.rongcloud.im.server.response.PeopleNearbyResponse;
import cn.rongcloud.im.server.widget.SelectableRoundedImageView;

/**
 *
 */
public class PoepleNearbyAdapter extends ListBaseAdapter<PeopleNearbyResponse.DataBean> {


    private OnOpenItemClickListener onOpenItemClickListener;

    public PoepleNearbyAdapter(Context context) {
        super(context, 0);
    }

    @Override
    public int getLayoutId() {
        return R.layout.item_people_nearby_list;
    }

    @Override
    public void onBindItemHolder(SuperViewHolder holder, final int position, @Nullable final PeopleNearbyResponse.DataBean bean, boolean isLastItem) {

        final SelectableRoundedImageView ivHeadImage = (SelectableRoundedImageView) holder.getImageView(R.id.iv_head_image);
        final TextView title = holder.getTextView(R.id.title);
        final TextView number = holder.getTextView(R.id.number);
        final TextView tvRemark = holder.getTextView(R.id.tv_remark);


        if (bean == null) return;
        Glide.with(mContext).load(bean.getHeadimage()).into(ivHeadImage);
        title.setText(bean.getName());
        number.setText(bean.getPhone());
        String friendRemark = bean.getFriendRemark();
        if (TextUtils.isEmpty(friendRemark)){
            tvRemark.setVisibility(View.GONE);
        }else {
            tvRemark.setVisibility(View.VISIBLE);
            tvRemark.setText(bean.getFriendRemark());
        }

    }

    public interface OnOpenItemClickListener {
        void onOpenItemClick(ImageView v, int position, PeopleNearbyResponse.DataBean data);
    }
}
