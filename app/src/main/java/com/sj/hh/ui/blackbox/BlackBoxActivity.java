package com.sj.hh.ui.blackbox;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.sj.friendcircle.common.router.RouterList;
import com.sj.hh.R;
import com.sj.hh.ui.blackbox.record.AllRecordActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.rongcloud.im.ui.activity.base.BaseActivity;

/**
 * 黑匣子
 */
@Route(path = RouterList.BlackBoxActivity.path)
public class BlackBoxActivity extends BaseActivity {

    @BindView(R.id.ll_financial_record)
    LinearLayout llFinancialRecord;
    @BindView(R.id.ll_behavior_record)
    LinearLayout llBehaviorRecord;
    @BindView(R.id.ll_document_record)
    LinearLayout llDocumentRecord;
    @BindView(R.id.ll_communication_record)
    LinearLayout llCommunicationRecord;
    @BindView(R.id.ll_purchase_record)
    LinearLayout llPurchaseRecord;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_black_box);
        ButterKnife.bind(this);
        setHeadVisibility(View.VISIBLE);
        setTitle("黑匣子");
    }

    @OnClick({R.id.ll_financial_record, R.id.ll_behavior_record, R.id.ll_document_record, R.id.ll_communication_record, R.id.ll_purchase_record})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ll_financial_record:/**财务记录*/
                startActivity(new Intent(this, AllRecordActivity.class).putExtra("ActivityType", AllRecordActivity.ActivityType.FinancialRecord));
                break;
            case R.id.ll_behavior_record:/**行为记录*/
                startActivity(new Intent(this, AllRecordActivity.class).putExtra("ActivityType", AllRecordActivity.ActivityType.BehaviorRecord));
                break;
            case R.id.ll_document_record:/**文件记录*/
                startActivity(new Intent(this, AllRecordActivity.class).putExtra("ActivityType", AllRecordActivity.ActivityType.FileRecord));
                break;
            case R.id.ll_communication_record:/**交流记录*/
                startActivity(new Intent(this, AllRecordActivity.class).putExtra("ActivityType", AllRecordActivity.ActivityType.CommunicationRecord));
                break;
            case R.id.ll_purchase_record:/**购买记录*/
                ARouter.getInstance().build("/deal/TansActionRecordActivity").navigation();
                break;
        }
    }
}
