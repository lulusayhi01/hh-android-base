package com.sj.hh.widget.contact;

import com.sj.hh.widget.contact.entity.SortModel;

import java.util.Comparator;

/**
 * @Author: huluhong
 * @Time: 2018/10/8 0008
 * @Description：This is PinyinComparator
 */

public class PinyinComparator implements Comparator<SortModel> {

    public int compare(SortModel o1, SortModel o2) {
        if (o1.sortLetters.equals("@") || o2.sortLetters.equals("#")) {
            return -1;
        } else if (o1.sortLetters.equals("#") || o2.sortLetters.equals("@")) {
            return 1;
        } else {
            return o1.sortLetters.compareTo(o2.sortLetters);
        }
    }

}
