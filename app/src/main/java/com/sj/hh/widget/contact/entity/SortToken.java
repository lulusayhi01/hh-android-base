package com.sj.hh.widget.contact.entity;

import java.io.Serializable;

/**
 * @Author: huluhong
 * @Time: 2018/10/8 0008
 * @Description：This is SortToken
 */

public class SortToken implements Serializable{
    /** 简拼 */
    public String simpleSpell = "";
    /** 全拼 */
    public String wholeSpell = "";
    /** 中文全名 */
    public String chName = "";
    @Override
    public String toString() {
        return "[simpleSpell=" + simpleSpell + ", wholeSpell=" + wholeSpell + ", chName=" + chName + "]";
    }
}
