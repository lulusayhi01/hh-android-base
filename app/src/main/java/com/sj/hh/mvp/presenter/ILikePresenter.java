package com.sj.hh.mvp.presenter;

import android.content.Context;

import com.sj.hh.mvp.callback.OnLikeChangeCallback;


/**
 * Created by huluhong on 2018/7/6.
 */

public interface ILikePresenter {


    /**
     * 添加点赞
     */
    void addLike(Context context,String trendsid, OnLikeChangeCallback onLikeChangeCallback);

    /**
     * 移除点赞
     */
    void unLike(Context context,String trendsid, OnLikeChangeCallback onLikeChangeCallback);
}
