package com.sj.hh.mvp.presenter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.sj.hh.mvp.callback.OnCommentChangeCallback;

import cn.rongcloud.im.server.response.user.UserInfo;


/**
 * Created by huluhong on 2018/7/6.
 */

public interface ICommentPresenter {


    /**
     * 添加评论
     */
    void addComment(@NonNull Context context,
                    @NonNull String momentsId,
                    @NonNull String authorId,
                    @NonNull UserInfo author,
                    @Nullable String replyUserId,
                    @NonNull UserInfo reply,
                    @NonNull String content,
                    @NonNull OnCommentChangeCallback onCommentChangeCallback);

    void deleteComment(@NonNull Context context,@NonNull String commentid, @NonNull final OnCommentChangeCallback onCommentChangeCallback);
}
