package com.sj.hh.mvp.callback;

/**
 * Created by huluhong on 2018/7/7.
 * <p>
 * 删除动态allback
 */

public interface OnDeleteCallback {

    void onDeleteSuccess(String likeinfoid);

    void onDeleteImageSuccess();

}
