package com.sj.hh.mvp.presenter;

import android.content.Context;

import com.sj.hh.mvp.callback.OnDeleteCallback;


/**
 * Created by huluhong on 2018/7/6.
 */

public interface IDeleteDynamicPresenter {


    /**
     * 删除动态
     */
    void deleteDynamic(Context context, String ids, OnDeleteCallback onDeleteCallback);

    /**
     * 删除动态图片
     */
    void deleteDynamicImage(Context context, String momentid, OnDeleteCallback onDeleteCallback);
}
