package com.sj.hh.mvp.presenter.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import com.sj.hh.mvp.callback.OnCommentChangeCallback;
import com.sj.hh.mvp.presenter.ICommentPresenter;
import com.sj.hh.request.AddCommentRequest;
import com.sj.hh.request.DeleteCommentRequest;
import com.sj.hh.request.base.OnResponseListener;

import cn.rongcloud.im.server.response.friendcircle.CommentInfo;
import cn.rongcloud.im.server.response.user.UserInfo;


/**
 * Created by huluhong on 2018/7/7.
 * <p>
 * 评论Model
 */

public class CommentImpl implements ICommentPresenter {
    @Override
    public void addComment(@NonNull Context context,
                           @NonNull String momentid,
                           @NonNull String authorid,
                           @NonNull UserInfo author,
                           @Nullable String replyUserId,
                           @NonNull UserInfo reply,
                           @NonNull String content,
                           @NonNull final OnCommentChangeCallback onCommentChangeCallback) {
        if (onCommentChangeCallback == null) return;
        AddCommentRequest addCommentRequest = new AddCommentRequest();
        addCommentRequest.setContent(content);
        addCommentRequest.setMomentsInfoId(momentid);
        addCommentRequest.setAuthor(author);
        addCommentRequest.setAuthorId(authorid);
        if (!TextUtils.isEmpty(replyUserId)) {
            addCommentRequest.setReplyUserId(replyUserId);
        }
        addCommentRequest.setOnResponseListener(new OnResponseListener.SimpleResponseListener<CommentInfo>() {
            @Override
            public void onSuccess(CommentInfo response, int requestType) {
                onCommentChangeCallback.onAddComment(response);
            }
        });
        addCommentRequest.execute(context);
    }

    @Override
    public void deleteComment(@NonNull Context context,@NonNull String commentid, @NonNull final OnCommentChangeCallback onCommentChangeCallback) {
        if (onCommentChangeCallback == null) return;
        DeleteCommentRequest deleteCommentRequest = new DeleteCommentRequest();
        deleteCommentRequest.setCommentid(commentid);
        deleteCommentRequest.setOnResponseListener(new OnResponseListener.SimpleResponseListener<String>() {
            @Override
            public void onSuccess(String response, int requestType) {
                onCommentChangeCallback.onDeleteComment(response);
            }
        });
        deleteCommentRequest.execute(context);

    }
}
