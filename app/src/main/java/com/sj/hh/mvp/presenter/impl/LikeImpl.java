package com.sj.hh.mvp.presenter.impl;


import android.content.Context;

import com.sj.hh.mvp.callback.OnLikeChangeCallback;
import com.sj.hh.mvp.presenter.ILikePresenter;
import com.sj.hh.request.AddLikeRequest;
import com.sj.hh.request.UnLikeRequest;
import com.sj.hh.request.base.OnResponseListener;
import com.sj.hh.request.exception.BmobException;

/**
 * Created by huluhong on 2018/7/7.
 * <p>
 * 点赞model
 */

public class LikeImpl implements ILikePresenter {

    @Override
    public void addLike(Context context,String trendsid, final OnLikeChangeCallback onLikeChangeCallback) {
        if (onLikeChangeCallback == null) return;
        AddLikeRequest request = new AddLikeRequest(context,trendsid);
        request.setOnResponseListener(new OnResponseListener<String>() {
            @Override
            public void onStart(int requestType) {

            }

            @Override
            public void onSuccess(String response, int requestType) {
                onLikeChangeCallback.onLike(response);
            }

            @Override
            public void onError(BmobException e, int requestType) {

            }
        });
        request.execute(context);
    }

    @Override
    public void unLike(Context context,String trendsid, final OnLikeChangeCallback onLikeChangeCallback) {
        if (onLikeChangeCallback == null) return;
        UnLikeRequest request = new UnLikeRequest(trendsid);
        request.setOnResponseListener(new OnResponseListener<Boolean>() {
            @Override
            public void onStart(int requestType) {

            }

            @Override
            public void onSuccess(Boolean response, int requestType) {
                if (response) {
                    onLikeChangeCallback.onUnLike();
                }
            }

            @Override
            public void onError(BmobException e, int requestType) {

            }
        });
        request.execute(context);
    }
}
