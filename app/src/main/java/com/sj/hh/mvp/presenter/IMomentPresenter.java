package com.sj.hh.mvp.presenter;

import android.content.Context;
import android.support.annotation.NonNull;


import com.sj.friendcircle.lib.mvp.IBasePresenter;
import com.sj.hh.mvp.view.IMomentView;

import java.util.List;

import cn.rongcloud.im.server.response.friendcircle.CommentInfo;
import cn.rongcloud.im.server.response.friendcircle.LikesInfo;
import cn.rongcloud.im.server.response.friendcircle.MomentsInfo;


/**
 * Created by huluhong on 2018/7/7.
 */

public interface IMomentPresenter extends IBasePresenter<IMomentView> {


    void addLike(Context context, int viewHolderPos, String momentid, List<LikesInfo> currentLikeList, MomentsInfo.DataBean info);

    void unLike(Context context, int viewHolderPos, String likesid, List<LikesInfo> currentLikeList, MomentsInfo.DataBean info);

    void addComment(Context context,int viewHolderPos, String momentid, String replyUserid, String commentContent, List<CommentInfo> currentCommentList);

    void deleteComment(Context context,int viewHolderPos, String commentid, List<CommentInfo> currentCommentList);

    void deleteMoments(Context context, @NonNull MomentsInfo.DataBean momentsInfo);

}
