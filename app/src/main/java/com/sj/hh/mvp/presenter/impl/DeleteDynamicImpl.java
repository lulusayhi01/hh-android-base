package com.sj.hh.mvp.presenter.impl;


import android.content.Context;

import com.sj.hh.mvp.callback.OnDeleteCallback;
import com.sj.hh.mvp.presenter.IDeleteDynamicPresenter;
import com.sj.hh.request.DeleteDynamicImageRequest;
import com.sj.hh.request.DeleteDynamicRequest;
import com.sj.hh.request.base.OnResponseListener;
import com.sj.hh.request.exception.BmobException;

/**
 * Created by huluhong on 2018/7/7.
 * <p>
 *  删除动态model
 */

public class DeleteDynamicImpl implements IDeleteDynamicPresenter {

    @Override
    public void deleteDynamic(Context context,String ids, final OnDeleteCallback onDeleteCallback) {
        if (onDeleteCallback == null) return;
        DeleteDynamicRequest request = new DeleteDynamicRequest();
        request.setIds(ids);
        request.setOnResponseListener(new OnResponseListener<String>() {
            @Override
            public void onStart(int requestType) {

            }

            @Override
            public void onSuccess(String response, int requestType) {
                onDeleteCallback.onDeleteSuccess(response);
            }

            @Override
            public void onError(BmobException e, int requestType) {

            }
        });
        request.execute(context);
    }

    @Override
    public void deleteDynamicImage(Context context,String imgs, final OnDeleteCallback onDeleteCallback) {
        if (onDeleteCallback == null) return;
        DeleteDynamicImageRequest request = new DeleteDynamicImageRequest();
        request.setImgId(imgs);
        request.setOnResponseListener(new OnResponseListener<Boolean>() {
            @Override
            public void onStart(int requestType) {

            }

            @Override
            public void onSuccess(Boolean response, int requestType) {
                if (response) {
                    onDeleteCallback.onDeleteImageSuccess();
                }
            }

            @Override
            public void onError(BmobException e, int requestType) {

            }
        });
        request.execute(context);
    }
}
