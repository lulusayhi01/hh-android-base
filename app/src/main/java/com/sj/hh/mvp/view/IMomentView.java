package com.sj.hh.mvp.view;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;


import com.sj.friendcircle.lib.mvp.IBaseView;
import com.sj.friendcircle.ui.widget.commentwidget.CommentWidget;

import java.util.List;

import cn.rongcloud.im.server.response.friendcircle.CommentInfo;
import cn.rongcloud.im.server.response.friendcircle.LikesInfo;
import cn.rongcloud.im.server.response.friendcircle.MomentsInfo;


/**
 * Created by huluhong on 2018/7/7.
 */

public interface IMomentView extends IBaseView {

    void onLikeChange(int itemPos, List<LikesInfo> likeUserList);

    void onCommentChange(int itemPos, List<CommentInfo> commentInfoList);

    /**
     * 因为recyclerview通过位置找到itemview有可能会找不到对应的View，所以这次直接传值
     *
     * @param viewHolderRootView
     * @param itemPos
     * @param momentid
     * @param commentWidget
     */
    void showCommentBox(@Nullable View viewHolderRootView, int itemPos, String momentid, CommentWidget commentWidget);

    void onDeleteMomentsInfo(@NonNull MomentsInfo.DataBean momentsInfo);

}
