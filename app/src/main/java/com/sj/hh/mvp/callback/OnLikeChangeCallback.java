package com.sj.hh.mvp.callback;

/**
 * Created by huluhong on 2018/7/7.
 * <p>
 * 点赞callback
 */

public interface OnLikeChangeCallback {

    void onLike(String likeinfoid);

    void onUnLike();

}
