package com.sj.hh.mvp.callback;


import cn.rongcloud.im.server.response.friendcircle.CommentInfo;

/**
 * Created by huluhong on 2018/12/9.
 * <p>
 * 评论Callback
 */

public interface OnCommentChangeCallback {

    void onAddComment(CommentInfo response);

    void onDeleteComment(String commentid);

}
