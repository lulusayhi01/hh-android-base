package com.sj.hh.util;

import android.content.Context;

import com.alibaba.fastjson.JSON;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sj.hh.widget.contact.entity.SortModel;

import java.util.ArrayList;
import java.util.List;

import cn.rongcloud.im.server.response.PhoneContactResponse;
import cn.rongcloud.im.utils.ShareUtil;

/**
 * @Author: huluhong
 * @Time: 2018/10/8 0008
 * @Description：This is LocalJsonUtil
 */

public class LocalJsonUtil {

    private static LocalJsonUtil instance = null;

    public static LocalJsonUtil getInstance() {
        synchronized (LocalJsonUtil.class) {
            if (instance == null) {
                instance = new LocalJsonUtil();
            }
        }
        return instance;
    }

    public void setSortModel(Context context, ArrayList<SortModel> models) {
        ShareUtil.putData(context, "SortModel", JSON.toJSONString(models));
    }

    public List<SortModel> getSortModel(Context context) {
        List<SortModel> sortModel = new Gson().fromJson(ShareUtil.getString(context, "SortModel"), new TypeToken<List<SortModel>>() {
        }.getType());
        return sortModel;
    }

    public void setPhoneContact(Context context, List<PhoneContactResponse.DataBean> data) {
        ShareUtil.putData(context, "PhoneContactResponse", JSON.toJSONString(data));
    }

    public List<PhoneContactResponse.DataBean> getPhoneContact(Context context) {
        List<PhoneContactResponse.DataBean> sortModel = new Gson().fromJson(ShareUtil.getString(context, "PhoneContactResponse"), new TypeToken<List<PhoneContactResponse.DataBean>>() {
        }.getType());
        return sortModel;
    }
}
