package com.sj.hh.util;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.telephony.PhoneNumberUtils;

/**
 * @Author: huluhong
 * @Time: 2018/10/9 0009
 * @Description：This is SendSMSUtil
 */

public class SendSMSUtil {

    public static final String SMS_MODE = "我正在使用医疗APP，邀请码：@p，赶快下载体验吧~注册地址：http://app.afwlkj.cn/Home/Index/share/user_self_recommend_code/     注册页面网址";

    private static SendSMSUtil instance = null;

    public static SendSMSUtil getInstance() {
        synchronized (SendSMSUtil.class) {
            if (instance == null) {
                instance = new SendSMSUtil();
            }
        }
        return instance;
    }

    /**
     * 调起系统发短信功能
     *
     * @param phoneNumber
     * @param message
     */
    public void doSendSMSTo(Context context, String phoneNumber, String message) {
        SharedPreferences config = context.getSharedPreferences("config", Context.MODE_PRIVATE);
        String belongcode = config.getString("belongcode","");//邀请码
        message = message.replace("@p",belongcode);

        if (PhoneNumberUtils.isGlobalPhoneNumber(phoneNumber)) {
            Intent intent = new Intent(Intent.ACTION_SENDTO, Uri.parse("smsto:" + phoneNumber));
            intent.putExtra("sms_body", message);
            context.startActivity(intent);
        }
    }
}
