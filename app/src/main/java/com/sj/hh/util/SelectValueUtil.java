package com.sj.hh.util;

import android.text.TextUtils;

import java.util.ArrayList;

import cn.rongcloud.im.server.entity.PickerType;

/**
 * @Author: huluhong
 * @Time: 2018/11/27 0027
 * @Description：This is SelectValueUtil
 */

public class SelectValueUtil {

    private static SelectValueUtil instance = null;

    public static SelectValueUtil getInstance() {
        synchronized (SelectValueUtil.class) {
            if (instance == null) {
                instance = new SelectValueUtil();
            }
        }
        return instance;
    }

    /**
     *
     * @param value
     * @return
     */
    public ArrayList<PickerType> getPickerData(String value) {
        ArrayList<PickerType> pickerTypes = new ArrayList<>();
        if (TextUtils.isEmpty(value)) return pickerTypes;
        String[] split = value.split(";");
        for (int i = 0; i < split.length; i++) {
            if (!TextUtils.isEmpty(split[i])) {
                String item = split[i];
                if (TextUtils.isEmpty(item)) continue;
                String[] split1 = item.split(":");
                if (split1.length > 1) {
                    PickerType pickerType = new PickerType();
                    pickerType.setId(split1[0]);
                    pickerType.setValue(split1[1]);
                    pickerTypes.add(pickerType);
                }
            }
        }
        return pickerTypes;
    }

    /**
     *
     * @param dataAll
     * @param value
     * @return
     */
    public String getMatching(String dataAll, String value) {
        if (TextUtils.isEmpty(value)) return "";
        ArrayList<PickerType> pickerData = getPickerData(dataAll);
        for (PickerType pic :
                pickerData) {
            if (value.equals(pic.getId())) return pic.getValue();
        }
        return value;
    }

    //重要文件记录    type: 类型 10001 首页轮播图 10002 商品详情 10003 商品sku 10004, "遗嘱"  10005, "借条"  10006, "秘密"  10007, "合同"  10008, "发票"  10009, "证书"  10010, "房产证"  10011, "证明" 10012 普通图片，10013 普通视频 10014 普通文件
    public final String type = "10004:遗嘱;10005:借条;10006:秘密;10007:合同;10008:发票;10009:证书;10010:房产证;10011:证明;10012:普通图片;10013:普通视频;10014:普通文件";

    //ft:文件类型（1：普通文件  3：重要文件）
    public final String fileType = "1:普通文件;3:重要文件";

    //df:是否默认展示  0是 1否
    public final String isTure = "0:是;1:否";

    //dealtype 操作类型 4101 支出  4102 收入;
    public final String dealtype = "4101:支出;4102:收入";

    //typedetailid	(操作类型详情 4301交易 4302余额佣金 4303 提现 4304 充值 4305 购买矿机4306 聊天红包 4307 币交易  4308 系统回收订单 4309 币佣金4310 退回币 4311 退回余额  4314 兑换币 4315 兑换余额);
    public final String typedetailid = "4301:交易;4302:余额佣金;4303:提现;4304:充值;4305:购买矿机;4306:聊天红包;4307:币交易;4308:系统回收订单;4309:币佣金;4310:退回币;4311:退回余额;4314:兑换币;4315:兑换余额";

    //dealstatus	int	(交易状态 4201交易中 4202交易成功 4203 交易失败)
    public final String dealstatus = "4201:交易中;4202:交易成功;4203:交易失败";

    //行为记录      type（类型）: 30101, "走路"  30102, "xx"  30103, "跑步"  30104, "上班"  30105, "会议"  30106, "吃饭"  30107, "打游戏"  30108, "思考"  30109, "看书"  30110, "睡觉
    public final String actionType = "30101:走路;30102:xx;30103:跑步;30104:上班;30105:会议;30106:吃饭;30107:打游戏;30108:思考;30109:看书;30110:睡觉";

    //交流列表      type（类型）:30201, "文字"  30202, "图片"  30203, "声音"  30204, "视频"
    public final String exchangeType = "30201:文字;30202:图片;30203:声音;30204:视频";

    //财务记录      type（类型）:30001, "工资收入"  30002, "转账收入"  30003, "餐饮支出"  30004, "服装支出"  30005, "转账支出"  30006, "理财收入"  30007, "xx支出"
    public final String financeType = "30001:工资收入;30002:转账收入;30003:餐饮支出;30004:服装支出;30005:转账支出;30006:理财收入;30007:xx支出";


}
